This chapter provides a summary of the major findings and conclusions arising from the results presented in the previous chapters. In addition, possible lines of future work are provided as a continuation of the present ones.
%=====================SECTION - Conclusions_conclusions=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Conclusions}\label{sec:Conclusions_conclusions}

The current thesis presents an in-depth analysis of the aerodynamic characteristics of flows around wings with a wavy leading edge obtained via high-fidelity numerical simulations. The simulations were performed using an in-house FORTRAN90 code (CANARD) developed by Dr Jae-Wook Kim that provided numerical solutions using an ILES approach on a body-fitted grid the details of which were provided in \S\ref{cha:Methodology}.

In the first two chapters (\S\ref{cha:Introduction} and \S\ref{cha:LiteratureReview}) the target of the investigation was presented as well as a review of the published work related to it. Based on the literature review provided in \S\ref{cha:LiteratureReview}, the following objectives were set:
\begin{itemize}
	\item To provide a thorough analysis of the flow of wings with a WLE in deep stall and to be able to identify the key aerodynamic features that allow this modified geometry to provide enhanced capabilities in this regime.
	\item To provide a detailed characterisation of the flow features present in wings with a WLE at angles of attack prior to stall. Especially, the structure of the typical LSBs that form in the trough regions and how it varies with the angle of attack was to be investigated. 
	\item To provide a characterisation of the unsteady flow features of wings with a WLE and in particular to find an explanation for the reduced force fluctuations presented by such type of wings in deep stall conditions.
	\item To investigate the stall development process in wings with a WLE and in particular the process that leads to the breakdown of the peak-to-peak periodicity of the flow.
\end{itemize}

In order to meet the goals, the work was divided into three major investigations that concerned the flow of a wing with a WLE in post-stall, pre-stall and near-stall regimes. In all cases, the flow conditions were always the same with a Reynolds number of $Re_\infty=120\times10^3$ and a Mach number of $M_\infty=0.3$.

The first objective was accomplished in \S\ref{cha:StalledWLE}, where the flow over a WLE wing was studied at an angle of attack of $\alpha=20\dg$, well in the stall regime. The study included a spanwise domain length analysis that proved to be of critical importance. It was shown that the number of WLE wavelengths $\kappa_{LE}$ used in a spanwise-periodic domain greatly affected the results and that at the very least, a minimum of $\kappa_{LE}=8$ was needed to obtain results that matched the previously published experimental data. While prior to stall, LSBs are formed in each trough and the flow is periodic trough-to-trough, in the post-stall regime the bursting of one of the LSBs leads to the formation of LSB-groups. The structure of these groups depended on $\kappa_{LE}$. 

The presence of a counter-rotating pair of streamwise vortices (SVs) was found to be responsible for preventing some of the LSBs from bursting. The SVs, which become stronger after the bursting of one of the LSBs, serve as an interface/buffer between the separated flow region created after the bursting and the remaining LSBs. On the other hand, the flow was observed to remain attached on behind the remaining LSB group.

The ability to retain some of the LSBs without bursting proved to be in direct relationship with the increased aerodynamic capabilities of the undulated wings in comparison with regular straight ones because of the following reasons:
\begin{enumerate}
	\item Since LSBs manage to trap low-pressure flow at their inside, retaining them creates high suction in the leading edge area that directly translates into lift production.
	\item The attached flow downstream of the LSB group greatly reduces the volume of the wake and therefore drag.
\end{enumerate}

In addition, the simultaneous presence of attached flow behind the LSB group and detached flow behind the burst LSB greatly reduces the spanwise homogeneity of the flow and deteriorates the \vonK (periodic) vortex shedding. This breakdown of large spanwise coherent structures minimises the force fluctuations suffered by the WLE models in comparison with their SLE counterparts.

The second objective was fulfilled in \S\ref{cha:lowAoA}, where the flow over a WLE wing was studied at three different angles of attack prior to stall: $\alpha=0\dg$, $5\dg$ and $10\dg$. A characterisation of the LSBs was accomplished by measuring the separation locations on the surface and the height of the LSBs. From these measurements, top and side-view profiles were created, which, for both cases were found to match the shape of a parabola. 

The size of the LSBs reduced and the position of the LSBs moved upstream as the angle of attack was increased as a consequence of an augmented adverse pressure gradient. However, since the separation point is limited to go no further upstream than the leading edge, the most upstream separation points were found to asymptote to a certain location close to the leading edge. At the same time, pressure was also reduced inside of the LSB as the angle of attack was increased.

More interestingly, it was found that for the combination of flow and geometry parameters used in the current study, WLE wings provided an increased aerodynamic efficiency over the regular SLE wings despite producing less lift. The reduced lift generation is explained by the reduced size of the LSBs in the wavy case. While for SLE wings, only a single LSB is formed that spans the entire upper surface, for WLE wings, LSBs only form in the trough regions. Therefore, despite the pressure inside them is lower than that of the SLE's LSB, due to a flow channelling effect, the full integration of the pressure forces over the entire wing surface results in less lift production for the WLE cases. Hence the increased aerodynamic efficiency was obtained by a drag reduction mechanism that was explained by the reduced exposure to stagnated flow. WLE wings only experience stagnation pressure on the regions of the leading edge where the surface normal is aligned with the direction of the incoming flow, i.e. only on the peaks and troughs. In contrast, SLE wings experience stagnation pressure along the entire span and therefore produce more drag than the undulated models.

In terms of the unsteady characteristics of WLE wings in the pre-stall regime, it was found that in the absence of \vonK vortex shedding, the most prominent unsteady flow feature is the shedding of vortical structures at the frequency of $f_{L_c}=28$ by the LSBs. This frequency was found to remain unchanged for the three pre-stall angles of attack tested and to be equal to the shedding frequency of the SLE's LSB. 

Detailed investigation of the unsteady flow field inside the LSBs suggested that, although the time-averaged flow-field provides a good understanding of the main flow structure inside the LSB, it cannot completely capture the entire LSB flow physics. In fact, the typical RFV seen in the steady picture is only the consequence of the mechanics of the shedding event. Vortices are formed in the LSB shear layer and grow in size and strength as they convect downstream. The vortices reduce their convection velocity as they reach the rear of the LSB and, as a result, the higher amount of time spent by the vortex in the RFV region leads to the distorted time-averaged view.

Chapters \S\ref{cha:StalledWLE} and \S\ref{cha:lowAoA} are representative examples of the two flow modes observed for flows over undulated wings:
\begin{itemize}
	\item \textbf{Flow mode-A}: Trough-to-trough periodic mode.

	This mode is found at low values of the angle of attack $\alpha$ before stall. A three-dimensional LSB can be found in each trough region and the structure of each LSB is identical for each of them. The size and shape of the bubbles vary with the incidence angle $\alpha$.

	\item \textbf{Flow mode-B}: Collocated mode.

	This mode is found at high angles of attack $\alpha$, where three-dimensional LSBs can be found only in some trough regions that all together form an LSB group. The size of the LSB group is found to be dependent on the domain spanwise size $L_z$. The structure of each LSB is different for each trough and two major types are identified:\begin{itemize}
		\item Central LSB: A symmetrical LSB that is always located in the middle of the LSB group and whose size is largest in comparison with the rest of the LSBs forming the group.
		\item Skewed-Satellite LSB: A LSB that is found on either side of the central LSB and of smaller size.
	\end{itemize}
	
\end{itemize}

In chapter \ref{cha:nearStall} the transition from flow mode-A to flow mode-B was investigated via two different experiments. In the first experiment both the WLE and SLE models were forced to follow a two-stage heaving profile that varied the incidence angle from a pre-stall angle of attack ($\alpha=10\dg$) to a post-stall angle of attack ($\alpha=20\dg$) and then moved back to the original incidence angle. This experiment showed that WLE models not only develop stall in a smoother way but also recovered much faster from it in comparison with SLE models. As a result, the overall aerodynamic performance of the undulated wings proved to be much higher than the regular leading-edge-shaped ones. In comparison with the SLE wings, where stall spread quite rapidly across the entire span, the WLE wings only suffered separation on a single trough section due to the burst of only one of the LSBs. As previously described, increasing the angle of attack moved the separation point of each LSB upstream until it reached a point near the leading edge. However, the reattachment point at the rear of the LSB kept moving upstream. As a result, the LSBs were squashed and forced to grow wider. The wider LSBs got the higher the interaction between the vortical structures shed by the LSBs and the peak regions was. These interactions produce strong spanwise velocity fluctuations at the peaks that otherwise would experience a quasi-two-dimensional flow. 

The connection between the spanwise flow fluctuations at the peaks and the shedding of vortical structures by the LSBs was discovered thanks to flow decomposition techniques (POD and DMD) and the Fourier analysis of probes sampled at high frequencies at both the peak and trough planes. These allowed to identify key flow features being shed at discrete frequencies. In particular, it was found that low-frequency hairpin-shaped vortical structures were correlated with the spanwise flow perturbations at the peaks.

In the second experiment, a flow mode-A solution obtained from a single wavelength domain simulation at $\alpha=17\dg$ was forced into an eight wavelength domain. In the absence of any additional perturbations, the flow mode-A solution was found to be absolutely unstable and rapidly evolved into a flow mode-B solution where one of the LSBs burst and the others remain to form the LSB group typically found for undulated wings at high angles of attack.

In the current state of the work, the initial objectives have all been met and thus the outcome of the investigations is concluded to be successful.

%=====================SECTION - futureResearch=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Future Lines of Research}\label{sec:futureResearch}
The investigations presented in this thesis are by no means complete. They rather unveiled the necessity of performing further investigations in order to obtain a complete understanding of the flow around wings with a WLE. The following points summarise the proposed future lines of research:
\begin{itemize}
	\item In this thesis, it was observed that the shedding of vortical structures by the LSBs represents one of the biggest sources of unsteadiness in the flow. Therefore, it is envisaged that, from an aeroacoustic perspective, LSBs could be also a significant source of noise production which needs further analysis.
	\item For both pre-stall and post-stall regimes, a characterisation of the LSBs' internal flow structure was performed using critical point theory. Given the great impact that the LSBs have on the flow, it is believed that by performing a similar analysis over a more varied range of both flow and WLE parameters, a simpler model for the entire flow field could be inferred.
	\item In \S\ref{cha:lowAoA}, it was shown that WLE wings produce less drag than regular wings thanks to a reduced exposure to stagnation pressure in the leading edge region. It is conjectured that the same effect could be obtained with simpler leading edge modifications. This needs further investigation.
	\item Flow decomposition techniques revealed that small vortical structures that form in the leading edge region undergo a merging process that leads to the creation of bigger ones that resemble the large vortex tubes seen further downstream at high angles of attack when \vonK vortex shedding occurs. This could be an indication of a direct relationship between the two shedding mechanisms that deserves further investigation.
	\item Although some light was shed on the process leading to transition from flow mode-A to flow mode-B, the process is still not fully understood. In this regard, the use of a forced flow mode-A solution at a high angle of attack showed promising results that need to be further investigated.
\end{itemize}
