%=====================SUBSECTION - LSBsStructure=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Surface-flow structure and critical points}\label{sub:StalledWLE_surfaceFlowSkeletons}

Vector fields can be represented by a series of points and the lines connecting them. This is a common practice in the study of dynamical systems and usually known as "\emph{Critical Point Theory}" (CPT). Critical points are points in the vector field where the slope of the streamlines is not defined and the limiting streamlines are defined as the lines connecting critical points. For example, for a set of first-order ordinary differential equations:
\begin{equation}
\label{eq:ODE1}
 \bm{\dot{x}}=\bm{A}\bm{x},
\end{equation}
critical points $\bm{\bar{x}}$ are defined by the null space of the $\bm{A}$ matrix.
\begin{equation}
\label{eq:criticalPoints}
	\bm{\bar{x}}=\text{Null}(\bm{A}) \qquad \implies \qquad \bm{A}\bm{\bar{x}}=\bm{0}
\end{equation}

Once the critical points are localised, one can Taylor expand about each critical point so that ignoring higher order terms and using (\ref{eq:criticalPoints}) the following expression is obtained:
\begin{equation}
 \begin{aligned}
 \bm{\dot{x}}&=\bm{A}\bm{\bar{x}}+\bm{J}\bm{\Delta x}+\orderof{\bm{\Delta x^2}} \\
 \bm{\dot{x}}&=\bm{J}\bm{\Delta x},
 \end{aligned}
\end{equation}
where $\bm{J}$ is the Jacobian of $\bm{\dot{x}}$ evaluated at the critical point $\bm{\bar{x}}$, and $\bm{\Delta x}=\bm{x}-\bm{\bar{x}}$ is the distance from the critical point. Critical points are then classified by the invariants of the Jacobian matrix $\bm{J}$.

\citet{Perry1975, Perry1984, Perry1986, Perry1987} and \citet{Chong1990} provide an extensive insight into the application of CPT to fluid dynamics and in particular to LSB internal structures. On the one hand, borrowing the nomenclature from CPT, a critical point is stable if it behaves as a sink, i.e. a nearby particle will be attracted to it. On the other hand, an unstable point is such if it behaves as a source, i.e. a nearby particle will be repelled or pushed away from it. Alternatively, a point can have both a stable direction (particles moving in this direction will be attracted) and an unstable direction (particles moving in this direction will be repelled). A point like this is known as a saddle point. For both stable and unstable critical points, if the nearby particles move in straight lines the critical point is classified as a node-type critical point whereas if it moves in spirals is a focus-type critical point.

In fluid dynamics, the Jacobian matrix $\bm{J}$ is the rate-of-deformation tensor $\bm{J}=\partial{u_i}/\partial{x_j}$ for $i=1,2,3$ and $j=1,2,3$. The characterisation of the critical points is given by the eigenvalues of $\bm{J}$, i.e. the roots of the characteristic polynomial of $\bm{J}$
\begin{equation}
\label{eq:characteristicEq}
	\lambda^3+P\lambda^2+Q\lambda+R=0
\end{equation}
where $P$, $Q$ and $R$ are the three invariants of $\bm{J}$ defined as
\begin{equation}
\label{eq:3dinvariants}
	\begin{aligned}
		\qquad P&=-\bm{S}_{ii}=-\tr{(\bm{J})},\\
		Q&=\frac{1}{2}(P^2-\bm{S}_{ij}\bm{S}_{ji}-\bm{R}_{ij}\bm{R}_{ji})=\frac{1}{2}(P^2-\tr{(\bm{J}^2)}),\\
		R&=\frac{1}{3}(-P^3+3PQ-\bm{S}_{ij}\bm{S}_{jk}\bm{S}_{ki}-3\bm{R}_{ij}\bm{R}_{jk}\bm{S}_{ki})\\
		&=\frac{1}{3}(-P^3+3PQ-\tr(\bm{J}^3))=-\det{(\bm{J})},\\
		\text{where}\qquad\bm{S}&=\frac{1}{2}(\partial{u_i}/\partial{x_j}+\partial{u_j}/\partial{x_i}),\\
		\text{and}\qquad\bm{R}&=\frac{1}{2}(\partial{u_i}/\partial{x_j}-\partial{u_j}/\partial{x_i}).
	\end{aligned}
\end{equation}
As a consequence, the local behaviour of the flow about the critical point can be simply characterised by the values of the three invariants. In the general cases where none of the invariants vanishes, the $P$--$Q$--$R$ space is divided into several different regions delimited by three surfaces. The equations of these surfaces are defined by algebraic relations between $P$--$Q$--$R$ and can be found in \citet{Chong1990} along with a classification of all possible critical points~\citep[see fig. 8]{Chong1990}. 

Special cases arise when some of the invariants vanish. For example, it is interesting to note that when $P=0$ the flow can be thought incompressible and, if $R=0$ and $\rank(\bm{J})=2$ the flow can be thought as two-dimensional and the critical points can be classified in the $P$--$Q$ plane as in \citet{Perry1975}. In the latter case, the characteristic equation becomes
\begin{equation}
\label{eq:characteristicEq2D}
	\lambda(\lambda^2+P\lambda+Q)=0
\end{equation}
with only two non-trivial solutions. Furthermore, if only the non-zero elements are retained, a new matrix $\bm{J_{2D}}$ can be defined such that the invariants $P$ and $Q$ in terms of $\bm{J_{2D}}$ are
%
\begin{equation}
\label{eq:invariants}
	P=-\tr{(\bm{J_{2D}})} \qquad \text{and} \qquad Q=\det(\bm{J_{2D}})
\end{equation}
so that the characteristic polynomial of $\bm{J_{2D}}$ has roots $\lambda_{1,2}$ given by
%
\begin{equation}
\label{eq:roots}
	\lambda_{1,2}=\frac{-P\pm\sqrt{P^2-4Q}}{2}.
\end{equation}

Depending on the values of $P$ and $Q$, the roots may be: 1) \emph{unstable nodes} when $\lambda$ is positive real-valued and $Q>0$; 2) \emph{stable nodes} when $\lambda$ is negative real-valued and $Q>0$; 3) \emph{unstable foci} when $\lambda$ is imaginary with positive real part, i.e. $Q>P^2/4$ and $P<0$; 4) \emph{stable foci} when $\lambda$ is imaginary with positive real part, i.e. $Q>P^2/4$ and $P>0$; and 5) \emph{saddle points} when $\lambda$ is real-valued and the two roots are $\lambda_1>0$ and $\lambda_2<-P$, i.e. $Q<0$. Cases where $P=0$, $Q=0$, or $Q=P^2/4$ are known as \emph{degenerates}. The position of each of this critical points on the $P$--$Q$ diagram can be seen in figure~\ref{fig:StalledWLE_criticalPoints2D}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_criticalPoints2D}
   \input{\folder/StalledWLE/criticalPoints2D}
   \caption{Classification of critical points in the $P$--$Q$ plane. Points in region 1 and 2 are unstable and stable nodes respectively; points in region 3 and 4 are unstable and stable foci respectively; and points in region 5 are saddles.}
   \label{fig:StalledWLE_criticalPoints2D}
\end{figure}

At the wall, the flow vector field is given by the wall-shear-stress which can be thought as two-dimensional if the wall-normal component is ignored and only the tangent surface flow is considered. With these considerations, critical points can be classified based on figure~\ref{fig:StalledWLE_criticalPoints2D} and the Jacobian is given by
\begin{equation}
\label{eq:jacobianWSS}
\begin{aligned}
	\bm{J}&=\frac{\partial{\tau_{w_i}}}{\partial{x_j}}, \\
	\text{where}\qquad \bm{\tau_w}&=(\tau_{wx},\tau_{wy},\tau_{wz})	\qquad \text{and} \qquad \bm{x}=(x,y,z),
\end{aligned}
\end{equation}
and the critical points are therefore located where the wall-shear-stress vanishes. 

Due to the high unsteady character of the flow over the WLE models studied in this work the location of the critical points is cumbersome even for the time-averaged fields. The identification of critical points is hence carried by a qualitative analysis of the surface flow. Figures \ref{fig:StalledWLE_criticalPoints2WLE}, \ref{fig:StalledWLE_criticalPoints3WLE},\ref{fig:StalledWLE_criticalPoints4WLE} and \ref{fig:StalledWLE_criticalPoints8WLE} provide a more detailed look at the near-wall flow structures obtained by applying the line-integral-convolution (LIC) technique \citep{Cabral1993} to the wall shear-stress vector field. In essence, the result of implementing LIC is similar to an oil-based surface flow visualisation commonly used in wind-tunnel experiments and can be used to gain insight of the surface streamlines generated from the wall-shear-stress vector field. Also in figures \ref{fig:StalledWLE_criticalPoints2WLE} to \ref{fig:StalledWLE_criticalPoints8WLE}, surface-flow skeletons are sketched so that a simplified view of the surface flow in terms of basic critical points and limiting streamlines is given.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_criticalPoints2WLE}
   \input{\folder/StalledWLE/criticalPoints2WLE}
   \caption{Near-wall flow structure for the WLE-2 case represented by an LIC of the wall-shear-stress coloured by the streamwise wall-shear-stress where the direction of the flow is indicated by black arrows. Additional sketch of the critical points and limiting streamlines are also provided.}
   \label{fig:StalledWLE_criticalPoints2WLE}
\end{figure}

For the WLE-2 case, shown in figure~\ref{fig:StalledWLE_criticalPoints2WLE}, it is seen that flow inside the LSB is driven by two stable foci (f$_{0a,b}$) that attract flow from the neighbouring peaks and a couple of saddle points (s$_{0,3}$). The SSL, on the other hand, is mainly constructed by a pair of unstable nodes (n$_{1a,b}$) and a triad of saddle points (s$_{7,8,9}$). It is seen that most of the flow that moves in the streamwise direction at the rear of the LSB comes from the SSL regions driven by two stable foci (f$_{3a,b}$). The structure formed by this pair of stable foci, in combination with the saddle point (s$_{10}$) is reminiscent of the stall cell described by \citet{Rodriguez2011}. However, in the stall cell described by \citeauthor*{Rodriguez2011} the two foci driving the stall cell are unstable whereas in this case, they are stable. Nonetheless, figure~\ref{fig:StalledWLE_vtxNorm} shows that the stall cell driving foci can either be stable or unstable depending on the wall normal distance. The blue line in figure~\ref{fig:StalledWLE_vtxNorm} shows that the flow attracted by the stall cell foci comes from the separated region at the rear of the aerofoil. The flow initially spirals in towards f$_{3a}$ as if the focus was stable. However, after that starts spiralling out until it finally lifts off the surface escaping to the free stream flow. Also in figure~\ref{fig:StalledWLE_vtxNorm} the forward and backward streamlines released from n$_{1a}$ are presented. This set of streamlines show how the flow released by the unstable node n$_{1a}$ also comes from the reversed flow region. After passing through n$_{1a}$ the flow is attracted inside the LSB and then also escapes towards the free stream. It is viewed from large-scale perspectives that the stall cell acts as if it interconnects the streamlines leaving the aerofoil into the wake region and the others coming back to the aerofoil. This process results in a closed loop of streamlines consisting of the wake recirculation zone. It is envisaged that the size, location and strength of the stall cell have a close relationship with the rear wake, which deserves further investigations particularly in the study of undulated aerofoils since the rear wake is strongly related to the aerodynamic performance.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_vtxNorm}
\input{\folder/StalledWLE/vtxNorm}
\caption{Streamlines obtained from time-averaged flow data (WLE-2) visualising the three-dimensional flow. The streamlines are colour coded to distinguish those moving from (yellow and red) and towards (cyan and blue) the release points (n$_{1a}$ and f$_{3a}$) where the arrow-head indicate the direction of the streamlines. Additional streamlines (grey) are provided on a fixed cross-section to show the flow recirculation in the wake region.}
\label{fig:StalledWLE_vtxNorm}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_criticalPoints3WLE}
   \input{\folder/StalledWLE/criticalPoints3WLE}
   \caption{Near-wall flow structure for the WLE-3 case represented by an LIC of the wall-shear-stress coloured by the streamwise wall-shear-stress where the direction of the flow is indicated by black arrows. Additional sketch of the critical points and limiting streamlines are also provided.}
   \label{fig:StalledWLE_criticalPoints3WLE}
\end{figure}

For the WLE-3 case, figure~\ref{fig:StalledWLE_criticalPoints3WLE} reveals the internal structure of the pair of skewed LSBs. It is observed that while the LSB of the WLE-2 case was mostly driven by two stable foci, for the WLE-3 case the structure of the bubbles consists of a couple of unstable foci (f$_{1a,b}$), a couple of saddles (s$_{4,5}$) and a stable focus (f$_{2a,b}$). The latter seems to attract most of the flow from the SSL regions while the absence of a symmetrical/central LSB results in the flow from the peak between skewed LSBs coming straight towards the saddle point (s$_{6a,b}$). When the flow reaches this point, it is diverted by the saddle (s$_{6a,b}$) towards the pair of stable foci (f$_{3a,b}$) driving the stall cell similarly to the WLE-2 case. The structure of the SSL skeleton seems to be identical to the WLE-2 case.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_criticalPoints4WLE}
   \input{\folder/StalledWLE/criticalPoints4WLE}
   \caption{Near-wall flow structure for the WLE-4 case represented by an LIC of the wall-shear-stress coloured by the streamwise wall-shear-stress where the direction of the flow is indicated by black arrows. Additional sketch of the critical points and limiting streamlines are also provided.}
   \label{fig:StalledWLE_criticalPoints4WLE}
\end{figure}

Figure~\ref{fig:StalledWLE_criticalPoints4WLE}, representing the flow over the WLE-4 case, shows that the flow structure gets more complicated with increasing $\kappa_{LE}=L_z/\lambda_{LE}$. In this case, the skewed LSBs and the SSL conserve the same structure as in the WLE-3 case. On the other hand, the central LSB seems to be quite different from its WLE-2 predecessor. In fact, it seems to bare more similarities with the skewed LSBs since its skeleton is formed by a pair of unstable foci (f$_{0a,b}$), and a pair of saddle points (s$_{0,1}$). Additionally, the structure of the central LSB also contains a couple of stable nodes (n$_{0a,b}$) connected by another saddle (s$_2$). It appears that the stable focus present in the skewed LSBs (f$_{2a,b}$) is due to a fusion of two initial stable nodes (n$_{0a,b}$) into a single point. Although the structure of the SSL remains still the same, it worth noting that the pair of foci driving the stall cell (f$_{3a,b}$) are unstable (same as in \citet{Rodriguez2011}) in contrast with the previous cases (WLE-2 and WLE-3). The flow coming from the two central-LSB neighbouring peaks circumvents the frontal part of the LSB and gets attracted by the two stable nodes (n$_{0a,b}$). Some portion of this flow stream is also attracted by the stable directions of the two neighbouring saddles (s$_{6a,b}$) but then is diverted towards the central section where the saddle point (s$_3$) reverses its direction towards the LSB.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_criticalPoints8WLE}
   \input{\folder/StalledWLE/criticalPoints8WLE}
   \caption{Near-wall flow structure for the WLE-8 case represented by an LIC of the wall-shear-stress coloured by the streamwise wall-shear-stress where the direction of the flow is indicated by black arrows. Additional sketch of the critical points and limiting streamlines are also provided where some of the critical points and limiting streamlines in the leading edge area have been omitted for clarity purposes. For a more detailed view of the leading edge region see figure~\ref{fig:StalledWLE_criticalPoints8WLEcentral} and figure~\ref{fig:StalledWLE_criticalPoints8WLEssl}.}
   \label{fig:StalledWLE_criticalPoints8WLE}
\end{figure}

Again, when the number of wavelengths $\kappa_{LE}$ increases from 4 to 8 it is seen that the complexity of the surface flow structure is increased. As a consequence, the description of the flow structure of the WLE-8 case has been split into three figures: figure~\ref{fig:StalledWLE_criticalPoints8WLE} shows the full structure including the structure of the stall cell; figure~\ref{fig:StalledWLE_criticalPoints8WLEcentral} shows in detail the structure of the central LSB and its two neighbouring skewed LSBs; and figure~\ref{fig:StalledWLE_criticalPoints8WLEssl} focuses on the description of SSL surface flow and its neighbouring skewed LSBs. In figure~\ref{fig:StalledWLE_criticalPoints8WLE}, all the skewed LSBs internal structures have been removed from the sketch in order to improve the readability of the diagram. Nonetheless, the structure of these skewed LSBs will be described in figures~\ref{fig:StalledWLE_criticalPoints8WLEcentral} and~\ref{fig:StalledWLE_criticalPoints8WLEssl}. 

In the WLE-8 case, the two foci driving the stall cell (f$_{3a,b}$) are stable as depicted in figure~\ref{fig:StalledWLE_criticalPoints8WLE}. However, the symmetry of the stall cell structure is not preserved as well as in previous cases due to the high unsteadiness of the flow and therefore focus f$_{3a}$ is located much more upstream than its counterpart f$_{3b}$. It is seen how the flow comes upstream from the trailing edge region through the stall cell until it gets diverted by the saddle s$_9$ towards opposite spanwise directions. On the right side of the image, the flow circumvents the unstable focus (until it gets diverted by the saddle s$_9$ towards opposite spanwise directions. On the right side of the image, the flow circumvents the unstable focus (f$_{5a}$) and then is sucked into n$_3b$. In contrast, flow on the other side circumvents the stable focus (f$_{5b}$) and then bounces from saddle to saddle (s$_{13a}$, s$_{12a}$ and s$_{3}$) until finally arrives at the foremost saddle (s$_{10}$) and is redirected towards the stall cell driving foci (f$_{3a,b}$).
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_criticalPoints8WLEcentral}
   \input{\folder/StalledWLE/criticalPoints8WLEcentral}
   \caption{Detail of the near-wall flow structure for the WLE-8 case covering the central LSB region. The near-flow structure is represented by an LIC of the wall-shear-stress coloured by the streamwise wall-shear-stress where the direction of the flow is indicated by black arrows. Additional sketch of the critical points and limiting streamlines are also provided. For a general view of the entire surface see figure~\ref{fig:StalledWLE_criticalPoints8WLE}.}
   \label{fig:StalledWLE_criticalPoints8WLEcentral}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_criticalPoints8WLEssl}
   \input{\folder/StalledWLE/criticalPoints8WLEssl}
   \caption{Detail of the near-wall flow structure for the WLE-8 case covering the SSL region. The near-flow structure is represented by a LIC of the wall-shear-stress coloured by the streamwise wall-shear-stress where the direction of the flow is indicated by black arrows. Additional sketch of the critical points and limiting streamlines are also provided. For a general view of the entire surface see figure~\ref{fig:StalledWLE_criticalPoints8WLE}.}
   \label{fig:StalledWLE_criticalPoints8WLEssl}
\end{figure}

In the WLE-8 case, figure~\ref{fig:StalledWLE_criticalPoints8WLEcentral} shows that the structure of both the central and skewed LSBs is different from all previous cases. In this case, it appears that foci f$_{0a,b}$ and saddle s$_{2}$ (see figure~\ref{fig:StalledWLE_criticalPoints4WLE}) have merged into the unstable node n$_2$. At the same time, the rear of the LSB is constructed by a saddle (s$_2$) and a foci pair (f$_{6a,b}$). The skewed LSBs for the WLE-8 case are also different from previous cases. The only difference, in this case, is again the merging of the unstable foci f$_{1a,b}$ into an unstable node n$_{4a,b}$. The rest of the skewed LSB structure remains the same as in previous cases. Another difference with previous cases is that the flow from peaks neighbouring the central LSB is not attracted towards the interior of the LSB. In this case, the flow on either side of the central LSB is attracted by the stable foci of the skewed LSBs (f$_{2a,b}$). The flow entering the rear of the central LSB originates from further exterior peak sections and is deflected by s$_{12a,b}$, s$_{6a,b}$ and finally s$_3$.

Figure~\ref{fig:StalledWLE_criticalPoints8WLEssl} shows in detail the structure of the SSL for the WLE-8 case. While in the central LSB section described above critical points tended to be squashed in comparison with lower $\kappa_{LE}$ cases, in the SSL region it can be seen that saddle s$_8$ on figure~\ref{fig:StalledWLE_criticalPoints8WLEcentral} has now dislocated into saddles s$_{8a,b}$  connected via the unstable node n$_5$. Also worth noting is the structure of the skewed LSBs neighbouring the SSL region which are different to the ones shown in figure~\ref{fig:StalledWLE_criticalPoints8WLEcentral}. This pair of skewed LSBs are formed by a pair of saddles (s$_{15b}$ and s$_{16b}$) and a triad of stable foci f$_{6a,b}$ and f$_{7b}$.

%\begin{figure}[H]\centering
%   \tikzsetnextfilename{LSBs}
%   \input{\folder/StalledWLE/LSBs}
%   \caption{LSBs}
%   \label{fig:LSBs}
%\end{figure}
%
%\begin{figure}[H]\centering
%   \tikzsetnextfilename{StalledWLE_2WLE_LSBProfiles}
%   \input{\folder/StalledWLE/2WLE_LSBProfiles}
%   \caption{2WLELSBProfiles}
%   \label{fig:stallWLE__2WLE_LSBProfiles}
%\end{figure}
%
%\begin{figure}[H]\centering
%   \tikzsetnextfilename{StalledWLE_4WLE_LSBProfiles}
%   \input{\folder/StalledWLE/4WLE_LSBProfiles}
%   \caption{4WLELSBProfiles}
%   \label{fig:stallWLE__4WLE_LSBProfiles}
%\end{figure}
%
%\begin{figure}[H]\centering
%   \tikzsetnextfilename{StalledWLE_8WLE_LSBProfiles}
%   \input{\folder/StalledWLE/8WLE_LSBProfiles}
%   \caption{8WLELSBProfiles}
%   \label{fig:stallWLE__8WLE_LSBProfiles}
%\end{figure}
