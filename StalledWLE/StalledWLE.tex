In \S\ref{cha:LiteratureReview} the work of several researchers demonstrated the enhanced aerodynamic characteristics of WLE aerofoils in the post-stall regime has been outlined. Nonetheless, a formal explanation for the enhanced lift generation and delayed stall is still missing. Moreover, and although the aeroacoustic studies have shed some light on the topic, the unsteady characteristics of WLE aerofoils in deep stall are still to be revealed. In this chapter, a thorough analysis of the steady flow features of WLE aerofoils is conducted which reveals the source of the extra lift and decreased drag produced by WLE aerofoils in comparison with their SLE counterparts in \S\ref{sec:wleSteady}. 

The unsteady flow field induced by the WLE geometry is also examined in the present chapter. As shown in \S\ref{cha:LiteratureReview}, when a streamlined aerodynamic surface is submerged in a free-stream flow  at a large incidence angle vortex shedding is likely to occur in a similar fashion to bluff body vortex shedding. In this chapter, it is shown how the leading edge modifications greatly affect the vortex shedding event as well as the unsteady characteristics of the typical three-dimensional LSBs present in the leading edge region of an undulated wing. 

%=====================SECTION - Time-averaged characteristics=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Time-averaged Characteristics}\label{sec:wleSteady}
%
Table~\ref{tab:StalledWLE_ClCd} shows the time-averaged force coefficients for each simulation presented here compared with available experimental and computational data in the literature. The table covers various cases with different spanwise domain lengths $\kappa_{LE}=L_z/\lambda_{LE}$ used for the same flow condition, $Re_\infty=1.2\times 10^5$ and $\alpha=20^\circ$. The present results are compared with the previous measurement by \citet{Hansen2011} who used 66 WLE wavelengths in span ($\kappa_{LE}=66$ and $AR=7.26$). The dataset reveals that there is a clear trend of convergence in the simulation data towards the experimental ones as the aspect ratio increases. The convergence trend and the level of agreement are consistent for $\avg{C_L}$, $\avg{C_D}$ and $\avg{C_L}/\avg{C_D}$ alike. Although some uncertainty in the experimental data is implied in figure \ref{fig:StalledWLE_clReAR}\emph{a} (compared with data from the 1930s), the authors have to support the new data by \citet{Hansen2011} because the simulation results display a clear convergence towards them in both SLE and WLE cases (shown in figure \ref{fig:StalledWLE_clReAR}\emph{b}).
%
\begin{figure}[t]\centering
\tikzsetnextfilename{StalledWLE_clReAR}
\input{\folder/StalledWLE/clReAR}
\caption{Comparisons of time-averaged lift coefficient at $\alpha=20^\circ$: (\emph{a}) $\avg{C_L}$ varying with Reynolds numbers and (\emph{b}) the relative difference (\%) of $\avg{C_L}$ against \citet{Hansen2011} at $Re_\infty=1.2\times 10^5$ varying with the spanwise domain sizes (aspect ratios) used. The initials represent data from \citet{Hansen2011}, \citet{Stack1931}, \citet{Jacobs1932} and \citet{Skillen2014}.}
\label{fig:StalledWLE_clReAR}
\end{figure}
%
\begin{table}[t!]\centering
\caption{Time-averaged lift and drag coefficients; aerodynamic efficiency ($\avg{C_L}/\avg{C_D}$); standard deviations of the lift and drag coefficients ($\sigma_{C_L}$ and $\sigma_{C_D}$); and, relative differences of the lift and drag coefficients with respect to the experimental data by \citet{Hansen2011} ($\epsilon_{C_L}$ and $\epsilon_{C_D}$). All values listed above are obtained at $Re_\infty=1.2\times 10^5$ and $\alpha=20^\circ$.}
\resizebox{1\textwidth}{!}{
%
\begin{tabular}{l r r r r r r r r r}
&$\kappa_{LE}$&$AR$&$\avg{C_L}$&$\avg{C_D}$&$\frac{\avg{C_L}}{\avg{C_D}}$&$\sigma_{C_L}$&$\sigma_{C_D}$&$\epsilon_{C_L}$&$\epsilon_{C_D}$\\\hline
SLE-2 (Current)		     &2 &0.22&0.720   &0.375	&1.920   &0.101&0.037&33\%&21\%\\
SLE-4 (Current)		     &4 &0.44&0.622   &0.334	&1.862   &0.077&0.022&15\%& 8\%\\
SLE-8 (Current)		     &8 &0.88&0.572   &0.313	&1.827   &0.048&0.014&6\%& 2\%\\
SLE-66 \citep{Hansen2011}&66 &7.26&\tld0.54&\tld0.31&\tld1.74&--   &--   &--  &--\\
SLE-4 \citep{Skillen2014}&4 &0.44&0.64    &0.32	&2.00    &--   &--   &19\%& 3\%\\
\\
WLE-2 (Current)		     &2 &0.22&0.665   &0.305	&2.180   &0.066&0.024& 8\%& 5\%\\
WLE-3 (Current)		     &3 &0.33&0.586   &0.260	&2.254   &0.051&0.022&19\%&10\%\\
WLE-4 (Current)		     &4 &0.44&0.600   &0.270	&2.222   &0.034&0.015&16\%& 7\%\\
WLE-8 (Current)		     &8 &0.88&0.636   &0.267	&2.684   &0.033&0.013&12\%& 8\%\\
WLE-66 \citep{Hansen2011}&66 &7.26&\tld0.72&\tld0.29&\tld2.48&--   &--   &--  &--\\
WLE-4 \citep{Skillen2014}&4 &0.44&1.03	   &0.13	&7.92    &--   &--	 &41\%&54\%\\
\end{tabular}}
\label{tab:StalledWLE_ClCd}
\end{table}

Comparing the SLE-8 and WLE-8 cases, the current simulation confirms that the WLE geometry produces a higher $\avg{C_L}$ and a lower $\avg{C_D}$ than the SLE counterpart as reported in the literature. An additional aspect revealed in this work is that unsteady fluctuations in both $C_L$ and $C_D$ are reduced in the WLE cases (rather consistently regardless of the aspect ratio), which supports the previous observations by \citet{Favier2012} and \citet{Skillen2014}. The rest of the section is then devoted to making detailed investigations into the flow development and behaviour on and around the aerofoil in order to understand and explain as to how the WLE geometry creates the aerodynamic benefits compared to the SLE counterpart.

\input{StalledWLE/steadyOverview}

\input{StalledWLE/SVorigin}

\input{StalledWLE/SVevolution}

\input{StalledWLE/surfaceFlowSkeletons}

\input{StalledWLE/influences}

%=====================SECTION - Unsteady=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Unsteady Characteristics}\label{sec:Unsteady}
%
In this section, the focus is moved on to the unsteady flow features of the undulated aerofoils. The effect of WLEs on the aerodynamic force fluctuations is investigated first. Secondly, the enhanced flow variety in the spanwise direction influencing the large-scale flow behaviour is discussed. Lastly, additional discussions are provided with regard to the shear layers generated at the leading-edge area.

\input{StalledWLE/forceFluctuations}

\input{StalledWLE/spanwiseCoherence}

\input{StalledWLE/vortexDynamics}

\input{StalledWLE/summary}
