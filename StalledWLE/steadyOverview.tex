%=====================SUBSECTION - steadyOverview=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Overview of the time-averaged flow field}\label{sub:steadyOverview}
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{StalledWLE_cpAoA20}
\input{\folder/StalledWLE/cpAoA20}
\caption{Contour plots of time-averaged wall pressure coefficient on the suction side obtained from the WLE-2, WLE-3, WLE-4 and WLE-8 cases compared to the SLE-8 case at $Re_\infty=1.2\times 10^5$ and $\alpha=20^\circ$. P1 and T1 denote the first (far-bottom) peak and trough, respectively. The solution for WLE-8 has been moved to match the separation region at the same trough as the other WLE simulations.}
\label{fig:StalledWLE_cpAoA20}
\end{figure}
%
Figure \ref{fig:StalledWLE_cpAoA20} shows that the flow field is highly three-dimensional in the WLE cases, particularly near the leading edge, and confirms that the solution is significantly dependent on the spanwise domain size used. One of the most distinctive features made by the WLE compared to the SLE case is the low-pressure spots behind the troughs which are identified as laminar separation bubbles (LSBs) -- to be shown in figure \ref{fig:StalledWLE_U0}. It is manifested that the LSBs tend to group together creating a large zone of low pressure. As indicated by the figure caption, the solution for the WLE-8 case has been rearranged in order to match the separated flow region with the other WLE simulations. In the original simulations, the bursting occurred in the fifth trough, i.e. T5.

This LSB-grouping behaviour is a known feature of the flow around WLE-wings at high angle of attack as seen in \citet{Rostamzadeh2014}. At high angles of attack, where the SLE model stalls at all spanwise locations, the WLE model only stalls at certain troughs. When this happens, the flow is no longer periodic from trough-to-trough as it happens at low angles of attack. As shown in figure~\ref{fig:StalledWLE_cpAoA20}, using different spanwise-domain sizes result in different sizes of the LSB-group. For the WLE-2 case, one of the troughs retains its LSB whilst the other bursts and flow separates. For the WLE-3 case, two troughs retain their LSBs whilst the remaining one bursts. This trend is followed by the WLE-4 and WLE-8 cases. However, the LSB group is not expected to grow indefinitely with increasing spanwise domain size since the agreement with the WLE-66 from \citet{Hansen2011} shows an asymptotic trend with increasing aspect ratio, which suggests that the flow structure seen for WLE-8 is a representative case similar to that that \citeauthor*{Hansen2011} would have seen in their experiments. Further support of this hypothesis is obtained by the simulations of \citet{Rostamzadeh2016} where, for example, their A2W7.5 simulation with 16 wavelengths in the span shows 2 troughs with separated flow and two groups of 6 LSBs \citep[fig. 16]{Rostamzadeh2016}. Therefore, it seems clear that for WLE wings there exist two main flow modes:
\begin{enumerate}
	\item \textbf{Flow mode-A}: Trough-to-trough periodic mode found at low values of the angle of attack $\alpha$ where a three-dimensional LSB can be found in each trough region. The structure of each LSB is identical for each of them. The size and shape of the bubbles vary with the incidence angle $\alpha$.

	\item \textbf{Flow mode-B}: Collocated mode found at high angles of attack $\alpha$, where three-dimensional LSBs can be found only in some trough regions that all together form an LSB group. The size of the LSB group is found to be dependent on the domain spanwise size $L_z$. The structure of each LSB is different for each trough and two major types are identified:\begin{itemize}
		\item Central LSB (see figure~\ref{fig:StalledWLE_U0}): A symmetrical LSB that is always located at the middle of the LSB group and whose size is largest in comparison with the rest of the LSBs forming the group.
		\item Skewed-Satellite LSB (see figure~\ref{fig:StalledWLE_U0}): A LSB that is found on either side of the central LSB and of smaller size.
	\end{itemize}
\end{enumerate}

This chapter is focused on flow mode-B whereas \S\ref{cha:lowAoA} and \S\ref{cha:nearStall} are devoted to investigate flow mode-A and the transition process between the two modes respectively.

The LSB grouping intensifies the low-pressure spots (figure \ref{fig:StalledWLE_cpSecAoA20}\emph{d}) and generates a high lift force from there. On the other hand, in each WLE case, there is only one trough area left without an LSB as far as the current simulations are concerned. The isolated trough areas are submerged in a fully separated flow and make a loss in lift force there. In addition, figure \ref{fig:StalledWLE_cpSecAoA20} indicates that the gap between the lowest and the largest pressure on the suction side becomes larger as the aspect ratio increases, which results in a strong pressure gradient in the span direction.
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{StalledWLE_cpSecAoA20}
\input{\folder/StalledWLE/cpSecAoA20}
\caption{Profiles of time-averaged wall pressure coefficient along the chord on the suction side obtained from various $xy$-plane cross-sections in the WLE cases compared with the SLE-8 case (averaged in span). All cases are at $Re_\infty=1.2\times 10^5$ and $\alpha=20^\circ$. The locations of the cross-sections (T1, T2, etc.) are indicated in figure~\ref{fig:StalledWLE_cpAoA20}.}
\label{fig:StalledWLE_cpSecAoA20}
\end{figure}
%
\begin{table}[!t]\centering
\caption{Breakdown of the total drag into the pressure ($C_{Dp}$) and skin-friction ($C_{Df}$) components: time-averaged and fluctuating (standard deviation) parts.}
\begin{tabular}{l c c c c c c}
&$\avg{C_{Df}}$&$\avg{C_{Dp}}$&$\frac{\avg{C_{Df}}}{\avg{C_{Dp}}}$&$\sigma_{C_{Df}}$&$\sigma_{C_{Dp}}$&$\frac{\sigma_{C_{Df}}}{\sigma_{C_{Dp}}}$\\\hline
SLE-8&0.00465&0.308&0.015&0.00251&0.0137&0.183\\
WLE-8&0.00591&0.261&0.023&0.00013&0.0132&0.010
\end{tabular}
\label{tab:StalledWLE_pvCd}
\end{table}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_TwxContours}
\input{\folder/StalledWLE/TwxContours}
\caption{Contour plots of streamwise wall-shear-stress on the suction side obtained from the WLE-2, WLE-3, WLE-4 and WLE-8 cases compared to the SLE-8 case at $Re_\infty=1.2\times 10^5$ and $\alpha=20^\circ$. P1 and T1 denote the first (far-bottom) peak and trough, respectively. The locations of $\avg{\tau_{w_x}}=0$ are highlighted by thick black lines. The solution for WLE-8 has been moved to match the separation region at the same trough as the other WLE simulations.}
\label{fig:StalledWLE_TwxContours}
\end{figure}

The distributions of wall-shear-stress as defined by
%
\begin{equation}
\label{eq:wss}
	\bm{\tau_w}=(\tau_{wx},\tau_{wy},\tau_{wz})=\begin{pmatrix}
		\tau_{xx}	&\tau_{xy}	&\tau_{xz} \\
		\tau_{xy}	&\tau_{yy}	&\tau_{yz} \\
		\tau_{xz}	&\tau_{yz}	&\tau_{zz} \\
	\end{pmatrix}
	\begin{pmatrix}
		\eta_x \\
		\eta_y \\
		\eta_z \\
	\end{pmatrix}_w,
\end{equation}

for all WLE cases are shown in figure \ref{fig:StalledWLE_TwxContours} compared with the SLE-8 case. Typically, attached flows behind the peaks and the locally separated flows (LSBs) behind the troughs are displayed. More importantly, it is revealed that the grouping of LSBs is followed by a large zone of attached flow. The size of the attached flow zone becomes so substantial in the WLE-8 case that it stretches all the way down to the point close to the trailing edge. Also, it seems that the central LSB becomes dominant and advances downstream as the size of the LSB group grows. Figure \ref{fig:StalledWLE_cfSec} shows the profiles of the skin friction coefficient along the chord at various spanwise locations. It is clearly manifested in the WLE-8 case that the skin friction is reduced inside the LSBs (where the flow direction is reversed) and increased in the reattached flow region in comparison with the SLE case. Meanwhile, the profile at the isolated trough T8 (with a fully separated flow) is almost the same as that of the SLE case. Based on the surface data, the skin friction drag is calculated and compared between the SLE and WLE cases in table~\ref{tab:StalledWLE_pvCd}. The skin friction drag is also compared with the pressure drag. The table shows that the skin friction drag is increased in the WLE case which is due to the contribution of the attached flow behind the LSBs. However, it is obvious that the skin friction drag is only a tiny fraction of the pressure drag.

%
\begin{figure}[!t]\centering
   \tikzsetnextfilename{StalledWLE_cfSec}
\input{\folder/StalledWLE/cfSec}
\caption{Profiles of time-averaged skin friction coefficient along the chord on the suction side obtained from various $xy$-plane cross-sections in the WLE cases compared to the SLE-8 case (averaged in span). All cases are at $Re_\infty=1.2\times 10^5$ and $\alpha=20^\circ$. The locations of the cross-sections (T1, T2, etc.) are indicated in figure~\ref{fig:StalledWLE_TwxContours}.}
\label{fig:StalledWLE_cfSec}
\end{figure}

A clear picture of the separation/reattachment points is found in figure~\ref{fig:StalledWLE_U0} by plotting of iso-surfaces of $\avg{u}=0$ (zero time-averaged streamwise velocity). The iso-surfaces are the location where the direction of the flow reverses and hence show interfaces between the attached and separated flow regions. In figure~\ref{fig:StalledWLE_U0} it is visualised that the WLE cases produce a significantly large zone of attached flow downstream of the LSB group. In the SLE case, the leading-edge flow separation without reattachment is most likely to be due to the burst of the LSB in the early stage of flow development.

\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_U0}
\input{\folder/StalledWLE/U0}
\caption{Iso-surfaces of $\avg{u}=0$ on the suction side obtained from the WLE-2, WLE-3, WLE-4 and WLE-8 cases compared to the SLE-8 case at $Re_\infty=1.2\times 10^5$ and $\alpha=20^\circ$. The light grey areas indicate attached flow regions and the dark grey surfaces represent separated shear layers. The solution for WLE-8 has been moved to match the separation region at the same trough as the other WLE simulations.}
\label{fig:StalledWLE_U0}
\end{figure}
