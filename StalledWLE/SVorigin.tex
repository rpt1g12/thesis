%=====================SUBSECTION - SVorigin=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Origin of streamwise vorticity at the leading edge}\label{sub:StalledWLE_SVorigin}

Earlier it has been shown that the flow passing through a trough area either undergoes a full separation immediately or forms an LSB followed by a reattachment downstream. Regardless, a shear layer appears after each trough where the flow separation occurs. The shear layer then rapidly rolls up and produces vortical structures due to the Kelvin-Helmholtz instability \citep{Pauley1990, Watmuff1999, Yarusevych2009} as it has been shown in \S\ref{sec:LSBReview}. Figure~\ref{fig:StalledWLE_qcr} displays some of the vortical structures captured instantaneously comparing the SLE and WLE cases. It can be seen that spanwise vortices are predominant in the SLE case because of the spanwise uniformity in the flow separation at the leading edge. On the other hand, the WLE case exhibits more diverse vortical structures around the LSB due to a high three-dimensionality in the flow there. In particular, a pair of counter-rotating streamwise vortices surrounding the rear part of the LSB is observed. This observation is in line with those reported in the literature.

\begin{figure}[!t]\centering
   \tikzsetnextfilename{StalledWLE_qcr}
\input{\folder/StalledWLE/qcr}
\caption{Instantaneous iso-surfaces of Q-criterion (the second invariant of the velocity gradient tensor) coloured by streamwise vorticity for SLE-2 and WLE-2 cases: (\emph{a}) $Q=1500$ and (\emph{b}) $Q=150$.}
\label{fig:StalledWLE_qcr}
\end{figure}
%
\begin{figure}[!t]\centering
   \tikzsetnextfilename{StalledWLE_turnskt}
\input{\folder/StalledWLE/turnskt}
\caption{A diagram describing the streamwise turning of the spanwise vorticity driven by the streamwise velocity gradient: (\emph{a}) at the leading edge and (\emph{b}) around an LSB, in connection with \eqref{eq:wtrans} and figure \ref{fig:StalledWLE_isoOmega}.}
\label{fig:StalledWLE_turnskt}
\end{figure}
%
\begin{figure}[!t]\centering
   \tikzsetnextfilename{StalledWLE_isoOmega}
\input{\folder/StalledWLE/isoOmega}
\caption{Iso-surfaces of time-averaged vorticity magnitude $\avg{|\boldsymbol{\omega}|}=45$ coloured by (\emph{a}, \emph{e} \& \emph{i}) streamwise vorticity; (\emph{b}, \emph{f} \& \emph{j}) streamwise turning of spanwise vorticity; (\emph{c}, \emph{g} \& \emph{k}) streamwise turning of vertical vorticity; and, (\emph{d}, \emph{h} \& \emph{l}) the sum of the turning components. For three different WLEs: (\emph{a} to \emph{d}) WLE-2;  (\emph{e} to \emph{h}) WLE-3; and, (\emph{i} to \emph{l}) WLE-4 cases. SSL represents the fully separated shear layer from the isolated trough without an LSB.}
\label{fig:StalledWLE_isoOmega}
\end{figure}

As similarly attempted by \citet{Hansen2016}, the origin and development of these streamwise vortical structures may be explained by using the stretching-turning term of the vorticity transport equation:
%
\begin{equation}
\label{eq:wtrans}
\frac{d\omega_i}{dt}=
\underbrace{\omega_j\frac{\partial{u_i}}{\partial{x_j}}}_{\substack{\text{Stretching}\\
\text{and turning}}}
-\underbrace{\omega_i\frac{\partial{u_j}}{\partial{x_j}}}_{\substack{\text{Compressibility}\\
\text{stretching}}}
+\underbrace{e_{ijk}\frac{1}{\rho^2}\frac{\partial{\rho}}{\partial{x_j}}\frac{\partial{p}}{\partial{x_k}}}_{\text{{Baroclinic}}}
+\underbrace{e_{ijk}\frac{\partial{}}{\partial{x_j}}\left(\frac{1}{\rho}\frac{\partial{\tau_{km}}}{\partial{x_m}}\right)}_{\text{{Viscous dissipation}}}.
\end{equation}

According to the equation, the time evolution of streamwise vorticity ($\omega_x$) is related with the turning of the spanwise ($\omega_z$) and vertical ($\omega_y$) vortices driven by the gradients of streamwise velocity, i.e. $\omega_z(\partial{u}/\partial{z})$ and $\omega_y(\partial{u}/\partial{y})$. Figure \ref{fig:StalledWLE_turnskt} briefly describes the turning mechanism. Figure \ref{fig:StalledWLE_isoOmega} shows that $\avg{\omega_z}\avg{\partial{u}/\partial{z}}+\avg{\omega_y}\avg{\partial{u}/\partial{y}}$ produces an almost identical picture to that of $\avg{\omega_x}$. This explains the creation of the streamwise vorticity at the leading edge. \citet{Hansen2016} have provided a similar explanation at a much lower Reynolds number ($Re_\infty=2,230$). The current study unfolds a very different evolution of the leading-edge vortices afterwards (to follow in \S\ref{sub:StalledWLE_SVevolution}).

Figure \ref{fig:StalledWLE_isoOmega} also shows that $\avg{\omega_z}\avg{\partial{u}/\partial{z}}$ (streamwise turning of spanwise vorticity) and $\avg{\omega_y}\avg{\partial{u}/\partial{y}}$ (streamwise turning of vertical vorticity) appear significant around the LSBs when viewed individually but they seem to disappear when put together. It may be due to the fact that the sign of $\avg{\omega_z}$ is constantly negative (rolled up) where $\avg{\partial{u}/\partial{z}}$ is negative at the left-hand side of an LSB and positive at the right-hand side viewed from the front (see figure \ref{fig:StalledWLE_turnskt}). In the meantime, $\avg{\partial{u}/\partial{y}}$ is constantly positive on the LSB where $\avg{\omega_y}$ is negative on the left-hand side of the LSB and positive at the right-hand side. Therefore the turning terms yield opposite signs that cancel each other around the LSBs. However, the contribution of $\avg{\omega_z}\avg{\partial{u}/\partial{z}}$ created right at the leading edge remains strong and convects downstream surrounding the LSBs (figure \ref{fig:StalledWLE_isoOmega}\emph{b}, \emph{f} and \emph{j}).
