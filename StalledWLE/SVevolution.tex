%=====================SUBSECTION - SVevolution=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Evolution of streamwise vortices behind the leading edge}\label{sub:StalledWLE_SVevolution}
%
In order to investigate the evolution of the streamwise vorticity from the leading edge to the rear of the LSBs, contour plots of $\avg{\omega_x}$ have been investigated at various streamwise locations. Since the flow features are very similar for all cases (WLE-2, WLE-3, WLE-4 and WLE-8) only the most representative case, i.e. the WLE-4 case depicted in figure~\ref{fig:StalledWLE_4wx}, will be explained in detail. Nevertheless, cases WLE-2, WLE-3 and WLE-8 are also shown in figures~\ref{fig:StalledWLE_3wx}, \ref{fig:StalledWLE_2wx}, and \ref{fig:StalledWLE_8wx}. 
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{StalledWLE_4wx}
\input{\folder/StalledWLE/4wx}
\caption{Contour plots of time-averaged streamwise vorticity ($\avg{\omega_x}$) for the WLE-4 case at various streamwise locations. The frames cover from the leading edge to the rear of the LSB group.}
\label{fig:StalledWLE_4wx}
\end{figure}

Up to the position $x=-0.47$ the thin streamwise vortex (SV) sheets are observed to remain almost undisturbed. Non-uniform features start to appear at $x=-0.46$. The SV sheets are detached straight away from the wall and diffused fast at T4 where the trough undergoes a fully separated shear layer (SSL) as mentioned earlier. At the other troughs (T1 to T3), the SV sheets remain close to the wall but they are slightly lifted up from the wall due to the presence of the LSBs ($x=-0.45$). Underneath of the uplifted SV sheets, a thin sub-layer of streamwise vorticity (rotating in the opposite direction) starts to emerge ($x=-0.44$), which takes place at the skewed satellite LSBs (T1 and T3) but not at the central LSB (T2). The footprints of the sub-layers mainly driven by $\avg{{\partial{w}}/{\partial{y}}}$ are displayed in figure \ref{fig:stallWLE_StalledWLE_surfaceVorticity}\emph{e} and \emph{f} where the opposite sign of the sub-layers to that of the initial SV sheets is revealed. 
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_surfaceVorticity}
   \input{\folder/StalledWLE/surfaceVorticity}
   \caption{Contour plots of velocity gradients contributing to the streamwise vorticity at the wall (on the suction side) for the (\emph{a} and \emph{b}) WLE-2, (\emph{c} and \emph{d}) WLE-3, (\emph{e} and \emph{f}) WLE-4 and (\emph{g} and \emph{h}) WLE-8 cases.}
   \label{fig:stallWLE_StalledWLE_surfaceVorticity}
\end{figure}

Interestingly, the sub-layers appear to interact with the uplifted SV sheets to create a strong vortex roll-up at T1 and T3 ($x=-0.44$). The core of the rolled-up vortices is connected with the uplifted SV sheets with the same direction of rotation. At $x=-0.43$, the uplifted SV sheets are now reaching deep into the adjacent SSLs. This implies that the rolled-up vortex core is connected with the adjacent SSL from where a feeding of streamwise vorticity (as well as turbulent kinetic energy -- to follow) is available. The rolled-up vortices remain as the major flow structure but they eventually fade away downstream since the feeding on the SSLs is no longer available.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_3wx}
   \input{\folder/StalledWLE/3wx}
   \caption{Contour plots of time-averaged streamwise vorticity ($\avg{\omega_x}$) for the WLE-3 case at various streamwise locations. The frames cover from the leading edge to the rear of the LSB group.}
   \label{fig:StalledWLE_3wx}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_2wx}
   \input{\folder/StalledWLE/2wx}
   \caption{Contour plots of time-averaged streamwise vorticity ($\avg{\omega_x}$) for the WLE-2 case at various streamwise locations. The frames cover from the leading edge to the rear of the LSB group.}
   \label{fig:StalledWLE_2wx}
\end{figure}

It is seen in figures~\ref{fig:StalledWLE_2wx}, \ref{fig:StalledWLE_3wx} and \ref{fig:StalledWLE_8wx} that the development of the SV pair is basically the same independently of the number of wavelengths $\kappa_{LE}$ used in the simulations. Only the WLE-3 case exhibits some major differences. Due to the odd value of $\kappa_{LE}$, the WLE-3 case shown in figure~\ref{fig:StalledWLE_3wx} does not have a central LSB. In all cases but the WLE-2 (see figure~\ref{fig:StalledWLE_2wx}) each of the vortices forming the SV pair is contained inside a skewed LSB. For the WLE-2 case, there are no skewed LSBs and the legs of the SSL seat both inside the symmetric LSB as seen in figure~\ref{fig:StalledWLE_2wx} for $x\ge-0.41$. It is also worth noting that the central LSB in the WLE-8 case has dimensions comparable to the SSL (see figure~\ref{fig:StalledWLE_8wx}). 

The vorticity distributions shown in figures~\ref{fig:StalledWLE_4wx} to \ref{fig:StalledWLE_8wx} are reminiscent of other types of flows. As mentioned in \S\ref{sec:WLEReview}, WLEs are a passive flow control device that acts in a similar way to VGs and therefore the resulting flow fields are quite alike. In both cases, pairs of counter-rotating vortices are created that extract energy from outer boundary layer flow and energise a boundary layer that would otherwise detach. In the case of VGs, the vortex pair is created by pressure differences on the sides of the vanes. In the WLE case, on the other hand, the vortices shown in the above figures are a result of a vorticity turning mechanism where spanwise and wall-normal vorticity is turned into the streamwise direction due to the spanwise velocity gradients. 

But streamwise vortices are not uniquely found on aerofoils. \citet{Coles1978} identified high and low momentum streaks in within the sub-layer of a turbulent boundary layer which in turn promote the appearance of small streamwise vortices \citep{Lee2001, Dean2010}. Riblets take advantage of these vortices and are designed in a way that these streamwise vortices are pushed away from the near-wall region reducing then the high-shear at the wall. The scale of these vortices seen in turbulent boundary layers is many orders of magnitude smaller than the ones seen in the current study. However, more recently, larger streamwise vortices have been detected over flat plates with spanwise organised roughness. \citet{Vanderwel2015} showed that for a flat plate with streamwise strips of roughness, streamwise vortex pairs of a size comparable to the boundary layer size are seen to appear in between the roughness strips. Additional smaller vortex pairs of opposite sign were also observed immediately next to the roughness strip upper edges. \citet{Vanderwel2015} found that the spanwise separation between roughness strips was of critical importance. For roughness spacings smaller than one half of the boundary layer height, the vortices are confined to the roughness sub-layer, but for larger spacings, these streamwise vortices get stronger up to the limit when the spacing becomes comparable to the boundary layer thickness. A similar behaviour would be expected for WLE for different $\lambda_{LE}$. Reducing $\lambda_{LE}$ would reduce the space available for the vortices to grow in a similar way reducing the spacing between roughness strips constrains the growth of the streamwise vortices in \citet{Vanderwel2015}.

Therefore, it appears that the flow generated by the leading edge modifications used for the current study fall into a much larger category of flows where streamwise vortices are created due to spanwise velocity gradients. The mechanism by which these spanwise velocity gradients are originated varies, but the ultimate result is quite similar. In this particular case, the spanwise velocity gradients are generated initially by the leading edge geometry that promotes a channelling effect that results in higher velocities in the troughs. Further downstream, the presence of the three-dimensional LSBs also introduce high spanwise velocity-gradients as explained in the previous section.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_8wx}
   \input{\folder/StalledWLE/8wx}
   \caption{Contour plots of time-averaged streamwise vorticity ($\avg{\omega_x}$) for the WLE-8 case at various streamwise locations. The frames cover from the leading edge to the rear of the LSB group.}
   \label{fig:StalledWLE_8wx}
\end{figure}

It is observed in the current simulations that only one pair of rolled-up (counter-rotating) streamwise vortices (SVs) survives at the furthest troughs from the central LSB. It is presumably because they need feeding on the adjacent SSLs to maintain the streamwise vorticity. It is shown in figure \ref{fig:StalledWLE_wxStream} that the pair of rolled-up SVs stretches downstream wrapping around the LSB group and effectively forms an interface/buffer preventing the SSLs from penetrating into the LSB group. The two counter-rotating SVs get closer to each other moving downstream before they end up lifting each other up due to the induced upwash velocity between them. The upwash velocity seems to cease the life of the attached flow as well. 
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_wxStream}
\input{\folder/StalledWLE/wxStream}
\caption{Time-averaged streamlines on the suction side of the WLE-4 case, showing (\emph{a}) a pair of counter-rotating streamwise vortices surrounding the LSB group (coloured by $\avg{\omega_x}$) and the internal structure of the central LSB (coloured by $\avg{u}$) in a perspective view.}
\label{fig:StalledWLE_wxStream}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_4tke}
\input{\folder/StalledWLE/4tke}
\caption{Contour plots of time-averaged energy in the fluctuating velocity field for the WLE-4 case at various streamwise locations.}
\label{fig:StalledWLE_4tke}
\end{figure}

An additional investigation is made here with regard to the distribution of energy, measured as half the sum of the variance of each velocity component, in order to see where the vortical structures extract the energy from that allows them to sustain. Once again, the description of the flow is focused on the WLE-4 case (figure~\ref{fig:StalledWLE_4tke}) although cases other WLE cases are also provided (figures~\ref{fig:StalledWLE_3tke} to \ref{fig:StalledWLE_8tke}). Figure~\ref{fig:StalledWLE_4tke} shows the distribution of $k=(\avg{u^{\prime 2}}+\avg{v^{\prime 2}}+\avg{w^{\prime 2}})/2$ where $\boldsymbol{u}^\prime=\boldsymbol{u}-\avg{\boldsymbol{u}}$ for the WLE-4 case at various streamwise locations. The reader may realise that $k$ is in fact defined in the same way that turbulent kinetic energy (TKE) is defined traditionally. Nonetheless, the fluctuations present in the regions shown in figures~\ref{fig:StalledWLE_4tke} to \ref{fig:StalledWLE_8tke} are not random but are the effect of the shedding of spanwise vortical structures at a particular frequency. The characteristic frequency of this shedding will be revealed in \S\ref{sub:StalledWLE_vortexDynamics}. Therefore, $k$ represents a measure of the energy present in the flow fluctuations, particularly in the leading edge region where the flow is actually laminar. On the other hand, for sections already in the turbulent region, $k$ can be understood in the traditional way as a measure of TKE.

At $x=-0.43$ the plot shows a clear picture of the major flow characteristics, i.e. the central LSB (T2), the skewed satellite LSBs (T1 and T3), the SSL (T4) and the pair of rolled-up SVs at the side edges of the LSB group. The canopies of the LSBs where a thin turbulent shear layer takes place are well captured. A high level of turbulence is manifested in the thick SSL. Also, there is a connected channel existing between the rolled-up SVs and the adjacent SSLs. The presence of the connected channel suggests that TKE can be transferred from the SSLs to the rolled-up SVs, which supports the feeding mechanism hypothesised earlier. Moving downstream, the TKE keeps increasing particularly through the canopies of the LSBs and the turbulent flow behind the LSBs remains attached to the wall ($x=-0.32$) until it finally separates ($x=0$). 

% An additional investigation is made here with regard to the distribution of turbulent kinetic energy in order to see how the vortical structures evolve with the flow turbulence. Once again, the description of the flow is focused on the WLE-4 case (figure~\ref{fig:StalledWLE_4tke}) although cases other WLE cases are also provided (figures~\ref{fig:StalledWLE_3tke} to \ref{fig:StalledWLE_8tke}). Figure~\ref{fig:StalledWLE_4tke} shows the distribution of turbulent kinetic energy (TKE) for the WLE-4 case at various streamwise locations, where TKE is defined as $k=(\avg{u^{\prime 2}}+\avg{v^{\prime 2}}+\avg{w^{\prime 2}})/2$ and $\boldsymbol{u}^\prime=\boldsymbol{u}-\avg{\boldsymbol{u}}$. At $x=-0.43$ the plot shows a clear picture of the major flow characteristics, i.e. the central LSB (T2), the skewed satellite LSBs (T1 and T3), the SSL (T4) and the pair of rolled-up SVs at the side edges of the LSB group. The canopies of the LSBs where a thin turbulent shear layer takes place are well captured. A high level of turbulence is manifested in the thick SSL. Also, there is a connected channel existing between the rolled-up SVs and the adjacent SSLs. The presence of the connected channel suggests that TKE can be transferred from the SSLs to the rolled-up SVs, which supports the feeding mechanism hypothesised earlier. Moving downstream, the TKE keeps increasing particularly through the canopies of the LSBs and the turbulent flow behind the LSBs remains attached to the wall ($x=-0.32$) until it finally separates ($x=0$). 
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_3tke}
   \input{\folder/StalledWLE/3tke}
   \caption{Contour plots of time-averaged energy in the fluctuating velocity field for the WLE-3 case at various streamwise locations.}
   \label{fig:StalledWLE_3tke}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_2tke}
   \input{\folder/StalledWLE/2tke}
   \caption{Contour plots of time-averaged energy in the fluctuating velocity field for the WLE-2 case at various streamwise locations.}
   \label{fig:StalledWLE_2tke}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_8tke}
   \input{\folder/StalledWLE/8tke}
   \caption{Contour plots of time-averaged energy in the fluctuating velocity field for the WLE-8 case at various streamwise locations.}
   \label{fig:StalledWLE_8tke}
\end{figure}
