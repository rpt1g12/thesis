%=====================SUBSECTION - forceFluctuations=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Force fluctuations and \vK vortex shedding}\label{sub:StalledWLE_forceFluctuations}
%
\begin{figure}[!b]\centering
   \tikzsetnextfilename{StalledWLE_clPSD}
\input{\folder/StalledWLE/clPSD}
\caption{Power spectral density (PSD) of $C_L$ and $C_D$ fluctuations for the WLE cases compared with the SLE-8 case.}
\label{fig:StalledWLE_clPSD}
\end{figure}

It is shown in table~\ref{tab:StalledWLE_ClCd} that the level of lift and drag fluctuations in time ($\sigma_{C_L}$ and $\sigma_{C_D}$) is reduced in the WLE cases compared to the SLE counterpart. For a stalled aerofoil, the force fluctuations are usually associated with \vK vortex shedding in the rear wake. The current results also exhibit such a periodic vortex shedding at a particular frequency. A non-dimensional frequency is defined as follows in order to identify the Strouhal number of the vortex shedding:
%
\begin{equation}
\label{eq:st}
f^*=\frac{fL_c\sin\alpha}{u_\infty}.
\end{equation}

For bluff bodies at the current Reynolds number, the Strouhal number is typically in the range of $St\approx0.19$ to $0.20$ \citep{Lienhard1966}. In order to assess the nature of the force fluctuations of all simulations, the power spectral density (PSD) of a time signal $q(t)$ is defined as $S_{qq}(f)=|\hat{q}(f)|^2$ where $\hat{q}(f)$ is the Fourier transform of the signal. Figure \ref{fig:StalledWLE_clPSD} shows the PSD of lift and drag fluctuations, where the SLE case exhibits a clear peak at $f^*=0.2$ indicating \vK vortex shedding as mentioned above. A secondary peak is also identified at the first harmonic of the fundamental frequency. It is envisaged that the second peak is related with the repetitive low-pressure contribution from each shed vortex, whereas the fundamental peak is related to the change of circulation by the induced velocity between two successive counter-rotating vortices.

\begin{figure}[!t]\centering
   \tikzsetnextfilename{StalledWLE_vStreet}
\input{\folder/StalledWLE/vStreet}
\caption{Instantaneous contour plots of (\emph{a} \& \emph{b}) spanwise vorticity and (\emph{c} \& \emph{d}) perturbed pressure ($p^\prime=p-\avg{p}$) in the rear wake region comparing the SLE-8 and WLE-8 cases. The plots are taken from T4 cross-section (denoted in figure~\ref{fig:StalledWLE_TwxContours}).}
\label{fig:StalledWLE_vStreet}
\end{figure}
%
\begin{table}[!t]\centering
\caption{Relative changes in the PSD of lift and drag fluctuations for the WLE cases compared to the SLE-8 case, obtained from figure \ref{fig:StalledWLE_clPSD} at the vortex shedding frequency ($f^*=0.2$).}
\begin{tabular}{l c c c c}
&WLE-2&WLE-3&WLE-4&WLE-8\\\hline
$\tilde{S}_{C_L}(f^*=0.2)$&0.77&0.04&0.06&0.05\\
$\tilde{S}_{C_D}(f^*=0.2)$&2.39&0.19&0.50&0.30\\
\end{tabular}
\label{tab:StalledWLE_reductions}
\end{table}

In the meantime, the WLE cases (except WLE-2) show no obvious signs of the vortex shedding at $f^*=0.2$. The WLE-to-SLE ratios of the PSD magnitude at this frequency in figure \ref{fig:StalledWLE_clPSD} are shown in table \ref{tab:StalledWLE_reductions} where $\tilde{S}_{C_L}(f^*)$ and $\tilde{S}_{C_D}(f^*)$ are defined as
%
\begin{equation}
\tilde{S}_{C_L}(f^*)=\frac{S_{C_LC_L}(f^*)|_{WLE}}{S_{C_LC_L}(f^*)|_{SLE-8}}\quad\mathrm{and}\quad \tilde{S}_{C_D}(f^*)=\frac{S_{C_DC_D}(f^*)|_{WLE}}{S_{C_DC_D}(f^*)|_{SLE-8}}.
\end{equation}

The weakening of fluctuations at the shedding frequency is evident in lift and drag. Also, the WLE cases generate weaker fluctuations in a wide range of frequencies rather than locally at the shedding frequency. It seems that the WLE cases yield vortex shedding at slightly higher frequencies close to $f^*\sim 0.3$. For example, in figure \ref{fig:StalledWLE_vStreet}, the WLE-8 case displays a larger number of trailing vortices (counting those with positive $\omega_z$) than the SLE-8 case within a given distance from the trailing edge. However, the vortex shedding has an almost insignificant impact on the aerodynamic force fluctuations in the WLE cases as shown in figure \ref{fig:StalledWLE_clPSD}.
%
\begin{figure}[!t]\centering
   \tikzsetnextfilename{StalledWLE_cdPSD}
\input{\folder/StalledWLE/cdPSD}
\caption{The power spectral density (PSD) of $C_{Dp}$ and $C_{Dv}$ fluctuations: a comparison between the SLE-8 and WLE-8 cases. $C_{Dp}$ and $C_{Dv}$ denote pressure and skin-friction drag coefficients, respectively. }
\label{fig:StalledWLE_cdPSD}
\end{figure}

Figure~\ref{fig:StalledWLE_cdPSD} shows the PSD of the pressure drag ($C_{Dp}$) and skin-friction drag ($C_{Df}$) fluctuations for the SLE-8 and WLE-8 cases. Due to the fact that $\avg{C_{Dp}}$ is dominant over $\avg{C_{Df}}$, their fluctuation levels also show a large difference. It is found that the WLE case yields significantly lower fluctuations of $C_{Dp}$ at mid-to-high frequencies (figure \ref{fig:StalledWLE_cdPSD}\emph{a}) and of $C_{Df}$ at low-to-mid frequencies (figure \ref{fig:StalledWLE_cdPSD}\emph{b}). The reduced fluctuation level around $f^*\sim 0.2$ is consistent for both components. However, the fluctuations in $C_{Df}$ seem to increase at high frequencies on the contrary. The small-scale near-wall turbulence in the large zone of attached flow may give rise to the high-frequency components of $C_{Df}$ fluctuations. Nevertheless, the overall level of fluctuations is reduced in both $C_{Dp}$ and $C_{Df}$ for the WLE case where the reduction is more pronounced in $C_{Df}$ as shown in table \ref{tab:StalledWLE_pvCd}. It should be noted, however, that the time-averaged skin-friction drag ($\avg{C_{Df}}$) is increased in the WLE case as also shown in table \ref{tab:StalledWLE_pvCd}.
