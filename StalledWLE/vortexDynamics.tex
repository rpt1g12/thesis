%=====================SUBSECTION - vortexDynamics=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Leading-edge vortex dynamics}\label{sub:StalledWLE_vortexDynamics}
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{StalledWLE_0pPSD}
\input{\folder/StalledWLE/0pPSD}
\caption{Power spectral density (PSD) of fluctuating pressure in the SLE-2 case obtained at some probe points: (\emph{a}) the locations of the probe points aligned on the shear layer (most upstream and downstream probes are $p_0$ and $p_6$); (\emph{b}) PSDs obtained at the probe points on $z=0.11$ cross-section; and, (\emph{c}) those on $z=-0.11$ cross-section.}
\label{fig:StalledWLE_0pPSD}
\end{figure}

Returning our focus back to the leading edge area, the unsteady characteristics of the long spanwise coherent structures shown earlier in figure~\ref{fig:StalledWLE_qcr} are investigated here. The long spanwise coherent structures are formed in the leading-edge shear layers due to a Kelvin-Helmholtz instability \citep{Pauley1990, Watmuff1999, Yarusevych2009}. It has been discussed in \S\ref{sub:StalledWLE_SVorigin} that they tend to turn towards the streamwise direction in the WLE cases and interact with the LSBs nearby.

\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_0pPSD8SLE}
   \input{\folder/StalledWLE/0pPSD8SLE}
 \caption{Power spectral density (PSD) of fluctuating pressure in the SLE-8 case obtained at some probe points: (\emph{a}) the locations of the probe points aligned on the shear layer represented by a dashed line (most upstream and downstream probes are $p_0$ and $p_6$); (\emph{b}) PSDs obtained at the probe points on $z=-0.22$ cross-section; and, (\emph{c}) those on $z=0.22$ cross-section.}
   \label{fig:StalledWLE_0pPSD8SLE}
\end{figure}

%\begin{figure}\centering
%   \tikzsetnextfilename{figure25_3WLE}
%   \input{\folder/StalledWLE/figure25_3WLE}
%   \caption{Power spectral density (PSD) of fluctuating pressure in the WLE-3 case obtained at some probe points: (\emph{a}) the locations of the probe points aligned on the shear layer;  PSDs obtained at the probe points on (\emph{b}) T1, (\emph{c}) T2 and (\emph{d}) T3 cross-sections. The positions of T1, T2 and T3 are denoted in figure~\ref{fig:stallWLE_20lic}.}
%   \label{fig:stallWLE_figure25_3WLE}
%\end{figure}
%
%\begin{figure}\centering
%   \tikzsetnextfilename{figure25_2WLE}
%   \input{\folder/StalledWLE/figure25_2WLE}
% \caption{Power spectral density (PSD) of fluctuating pressure in the WLE-4 case obtained at some probe points: (\emph{a}) the locations of the probe points aligned on the shear layer; (\emph{b}) PSDs obtained at the probe points on T1 cross-section; and, (\emph{c}) those on T2 cross-section. The positions of T1 and T2 are denoted in figure~\ref{fig:stallWLE_20lic}.}
%   \label{fig:stallWLE_figure25_2WLE}
%\end{figure}

A series of pressure probes are distributed along a curve where the root-mean-square of the pressure fluctuations is highest (tracing the shear layer), on two different $xy$-planes for comparison. The PSDs of the pressure fluctuations obtained are shown in figures \ref{fig:StalledWLE_0pPSD} to \ref{fig:StalledWLE_4pPSD8WLE} for the SLE-2, SLE-8, WLE-4 and WLE-8 cases, respectively. %In this case, the frequencies are made dimensionless based on the momentum thickness $\theta_{sep}$ and the boundary layer edge velocity $U_{sep}$ at separation to produce~\citep{Pauley1990}:

\begin{equation}
\label{eq:ftheta}
	f_\theta=\frac{f\theta_{sep}}{U_{sep}}
\end{equation}

In the SLE-2 case (figure \ref{fig:StalledWLE_0pPSD}), there is no major difference between the cross-sections T1 and T2 and they display strong local peaks at $f^*\approx6.5$ across the first two probe points indicative of periodic vortex shedding. This value, although higher, is still in the same order of magnitude as that of \citet{Pauley1990}, who reported $f_\theta\approx0.0068$, where $f_\theta={f\theta_{sep}}/{U_{sep}}$. As the probe moves downstream, the signature of the periodic mode fades away and the overall energy level rises until it reaches a saturation. A similar behaviour is seen in the SLE-8 case in figure~\ref{fig:StalledWLE_0pPSD8SLE}. For the five most-upstream probes the frequency peak moves from $f^*\approx6.5$ to $f^*\approx5.0$. In this case, the frequency peaks seem to better agree with the value suggested by \citeauthor*{Pauley1990}, with $f_\theta\approx0.007$. Additionally, figure~\ref{fig:StalledWLE_0pPSD8SLE} also shows the spectral content of a signal measured much further downstream. For this last probe, the frequency peak has disappeared and the energy content seems to be better distributed among all frequencies. Again, as it was shown in figure~\ref{fig:StalledWLE_0pPSD} for the SLE-2 case, the spectral content of the two spanwise-sections is almost identical.
	
%
\begin{figure}[!t]\centering
   \tikzsetnextfilename{StalledWLE_4pPSD}
\input{\folder/StalledWLE/4pPSD}
 \caption{Power spectral density (PSD) of fluctuating pressure in the WLE-4 case obtained at some probe points: (\emph{a}) the locations of the probe points aligned on the shear layer (most upstream and downstream probes are $p_0$ and $p_6$); (\emph{b}) PSDs obtained at the probe points on T2 cross-section; and, (\emph{c}) those on T4 cross-section. The positions of T2 and T4 are denoted in figure~\ref{fig:StalledWLE_TwxContours}.}
\label{fig:StalledWLE_4pPSD}
\end{figure}

In the WLE-4 case, however, there are significant differences between the cross-sections T2 and T4 due to the enhanced flow three-dimensionality. In figure \ref{fig:StalledWLE_4pPSD}\emph{b} obtained from the cross-section T2 cutting through the central LSB, a local peak is observed at $f^*\approx7.5$ on the two most upstream probe points ($p_0$ and $p_1$). The peak broadens out as the probe moves downstream and the energy seems to be transferred to lower frequencies. The overall energy level also decreases with the probe moving downstream. In contrast, figure \ref{fig:StalledWLE_4pPSD}\emph{c} obtained from the cross-section T4 cutting through the SSL shows a major peak at a much lower frequency $f^*\approx3$ in the upstream region. In this case, the overall energy level does not seem to decay as much as that from the cross-section T2 although the energy cascade towards lower frequencies is still apparent.

Even though it appears that the shedding dynamics of the central LSB are completely different from those of the SSL section, figure~\ref{fig:StalledWLE_4pPSD8WLE} shows that they can be actually very similar in some cases. For the WLE-8 case, the size of the central LSB is bigger than in any of the previous wavy cases. As a result, the flow separation angle is much higher than that of, for example, the WLE-4 case. In fact, figure~\ref{fig:StalledWLE_4pPSD8WLE}\emph{a} shows that the flow separation angle is quite similar to that of the SSL section. Consequently, probes for both sections (T4 and T8) lay on top of each other in the leading edge region. Further downstream, due to flow reattachment behind the LSB group, the shear layers on each section diverge (see dashed lines in figure~\ref{fig:StalledWLE_4pPSD8WLE}\emph{a}). 

The similar separation angle for the two sections results in more similar vortex dynamics. For the T4 section, the spectra show a clear peak at $f_\theta\approx10$ and for the T8 section $f_\theta\approx8.5$ for probes $p_1$, $p_2$ and $p_3$. The evolution of the spectral content still, seems to be very much alike. The energy contained at these frequencies grows as the probe moves downstream and, as seen in the previous cases, the energy cascades towards lower frequencies. It is worth noting that while the level of energy captured by the most upstream probe ($p_0$) is similar for both cases, for the most downstream probe ($p_6$) the SSL region (T8) seems to have a higher energy content. In both cases, no spectral peaks can be observed, however, it appears that while at the T4 section the total energy content decays, at the T8 section the decay is not as severe. Moreover, at T8 the energy contained at the lowest frequency captured is maximum for the $p_6$ probe.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_4pPSD8WLE}
   \input{\folder/StalledWLE/4pPSD8WLE}
 \caption{Power spectral density (PSD) of fluctuating pressure in the WLE-8 case obtained at some probe points: (\emph{a}) the locations of the probe points aligned on the shear layer represented by a dashed line (most upstream and downstream probes are $p_0$ and $p_6$); (\emph{b}) PSDs obtained at the probe points on T4 cross-section; and, (\emph{c}) those on T8 cross-section. The positions of T4 and T8 are denoted in figure~\ref{fig:StalledWLE_TwxContours}.}
   \label{fig:StalledWLE_4pPSD8WLE}
\end{figure}
%

The vortex dynamics taking place on the cross-section T4 (in relation with figure \ref{fig:StalledWLE_4pPSD}\emph{c}) are displayed in figure \ref{fig:StalledWLE_4WLE20LSBPicsT4}. In this figure, it can be appreciated how the trailing edge vortex shedding affects the leading edge by means of two time-sequences. During one phase of large \vonK vortex formation in the leading edge, at $t=121.00$, three small vortices A, B and E are observed along with a medium size vortex C and a large vortex D. Later on, at $t=121.23$, it is observed that vortices A and B have merged into AB, C and D keep drifting and E starts approaching C. At this time also, two new vortices have been formed: F and G. At $t=121.70$, small and medium vortices have all merged up (FG, AB and CE) and D has grown in size.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_4WLE20LSBPicsT4}
   \input{\folder/StalledWLE/4WLE20LSBPicsT4}
\caption{Two series of instantaneous contour plots of pressure coefficient in the WLE-4 case obtained on the cross-section T4 intersecting the SSL region. The first series (\emph{a}) is representative of the large \vonK vortical structures formation process while the second (\emph{b}) represents another phase of the \vonK shedding cycle when the large vortical structure has already been shed. The position of T4 is denoted in figure \ref{fig:StalledWLE_TwxContours}.}
   \label{fig:StalledWLE_4WLE20LSBPicsT4}
\end{figure}

On figure~\ref{fig:StalledWLE_4WLE20LSBPicsT4}\emph{b} another time sequence is presented which represents another part of the \vonK shedding cycle, just after the large \vonK vortex has been shed. In this case, two triads of vortices (A-B-C and D-E-F at $t=124.80$) promptly merge together into ABC and DEF by $t=124.97$. At the same time vortex G, that seems to be a previous occurrence of a triad-merging-process, convects downstream carried by the mean flow. However, in this sequence, significant dissipation of the vortices is exhibited further downstream compared to the previous case. This process of vortex merging and dissipation taking place intermittently seems to result in the wide broadband spectra shown in figure \ref{fig:StalledWLE_4pPSD}\emph{c}.
%
%\begin{figure}[!t]\centering
%   \tikzsetnextfilename{paper-figure26}
%\input{\folder/StalledWLE/paper-figure26}
%\caption{Instantaneous contour plots of pressure coefficient in the WLE-4 case obtained on the cross-section T4 intersecting the SSL region. The position of T4 is denoted in figure \ref{fig:StalledWLE_TwxContours}.}
%\label{fig:StalledWLE_4WLE20LSBPicsT4}
%\end{figure}
%
%\begin{figure}[t!]\centering
%   \tikzsetnextfilename{figure26_8SLE}
%   \input{\folder/StalledWLE/figure26_8SLE}
%   \caption{8SLE}
%   \label{fig:stallWLE_figure26_8SLE}
%\end{figure}
%%
%\begin{figure}[t!]\centering
%   \tikzsetnextfilename{figure26_8WLE}
%   \input{\folder/StalledWLE/figure26_8WLE}
%   \caption{8WLE}
%   \label{fig:stallWLE_figure26_8WLE}
%\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{StalledWLE_4WLE20LSBPicsT2}
   \input{\folder/StalledWLE/4WLE20LSBPicsT2}
\caption{A series of instantaneous contour plots of pressure coefficient in the WLE-4 case obtained on the cross-section T2 intersecting the central LSB. The position of T2 is denoted in figure \ref{fig:StalledWLE_TwxContours}.}
   \label{fig:StalledWLE_4WLE20LSBPicsT2}
\end{figure}
%
%\begin{figure}[!t]\centering
%   \tikzsetnextfilename{paper-figure27}
%\input{\folder/StalledWLE/paper-figure27}
%\caption{Instantaneous contour plots of pressure coefficient in the WLE-4 case obtained on the cross-section T2 intersecting the central LSB. The position of T2 is denoted in figure \ref{fig:StalledWLE_TwxContours}.}
%\label{fig:StalledWLE_4WLE20LSBPicsT2}
%\end{figure}

In contrast, the vortical structures viewed on the cross-section T2 that cuts through the central LSB seem to undergo a much more rapid merging process as shown in figure \ref{fig:StalledWLE_4WLE20LSBPicsT2}. Since the vortical structures are restricted to a much smaller space to roll up/down around each other due to the presence of the wall, the merging process starts almost immediately after shedding. In figure \ref{fig:StalledWLE_4WLE20LSBPicsT2}, the small vortices A, B and C can be individually recognised along with a larger one (D). However, a short time later at $t=121.55$, B and C have already merged into BC. The continued merging of A-BC into ABC is complete at $t=121.69$ while at the same time D is being pushed towards the wall due to the flow reattachment at the rear of the LSB. At this point, two newly-shed vortices are observed as well (E and F). Later on, at $t=121.78$, the triad vortex ABC has merged with D forming a much larger structure (ABCD) that starts drifting into the turbulent region downstream of the LSB. Alongside the merging of ABCD, at $t=121.94$, another triad (G-F-E) is observed that eventually merges into the EFG structure at $t=122.08$

At $t=122.08$, the large vortical structure ABCD starts to dissipate in the turbulent boundary layer downstream of the LSB. The high shear near the wall also promotes a much rapid dissipation of the vortical structures due to straining and stretching in the streamwise direction. The combination of the fast merging and dissipation process could explain the widening and disappearance of the spectral peak observed in figure \ref{fig:StalledWLE_4pPSD}\emph{b}. Finally, it appears that the reduced pressure inside the LSB is created by the recirculation of vortical structures that are pushed towards the wall by the reattaching flow at the rear of the LSB. When these vortices start merging, some of the low pressure carried in their cores escapes and remains inside the LSB decreasing its mean pressure.
