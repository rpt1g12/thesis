%**SECTION **% 
\FloatBarrier %Force all figures from previous sections to be placed 
\section{Grid Generation}\label{sec:grid}

In $\S$~\ref{sec:GoverningEquations} it has been shown that working in the generalised coordinates allows the use of finite difference techniques designed for constant spacing rectangular grids. However, despite the grid in generalised coordinates might be rectangular, the actual problem in physical coordinates is still a body-fitted curvilinear mesh. There are several methods available for the generation of structured curvilinear grids, e.g. conformal mapping, elliptic, or algebraic grid generation~\citep{Fletcher1988}. 

The grid used in this study is generated using algebraic grid generation combined with a multi-block strategy. The algebraic mesh generation is done by defining the points on the boundaries, the interior grid points are then obtained by interpolation between the boundaries. The multi-block technique consists in splitting the domain into smaller sub-domains called blocks. For each of the blocks the points are collocated at the boundaries and then interior ones are interpolated. It is important to try to resemble the computational grid as much as possible so the transformation between coordinate systems remains simpler. Also, it is important to maintain continuity in the spacing between adjacent blocks up to the first spacing derivative (although continuity in the second derivative is also desirable). The main reason for this to happens is because of the grid metrics, i.e. $\xi_x,\xi_y,\xi_z,\eta_x,\cdots,\zeta_z$, directly appear in the governing equations when transformed to the generalised coordinates. Any discontinuities (in the discrete sense) in the spacing will increase the value of the grid metrics in a great manner. So poor quality cells, like skewed or high aspect ratio cells, will also trigger errors that due to the low dissipative characteristic of the high-order methods described in the last section will be unmanageable. For that reason, the grid has to be of very good quality.

The spacing between the nodes is controlled by the first derivative of the grid functions $x(\xi,\eta,\zeta),y(\xi,\eta,\zeta)$ and $z(\xi,\eta,\zeta)$. So for example, if the spacing in the $\xi$ direction for the x-coordinate it will be required that $\partial^2{x}/\partial{\xi}^2$ is a continuous function. The grid for this study is generated using an in-house Fortran code that ensures the C--2 continuity of the grid function. The method used is easily understood if it is illustrated with a simple one-dimensional example. 

\begin{figure}\centering
   \tikzsetnextfilename{gridFunction}
   \input{\folder/Methodology/gridFunction}
   \caption{1--D Grid Function Matching Example}
   \label{fig:gridFunction} 
 \end{figure}

In figure~\ref{fig:gridFunction} the grid function derivative has been plotted. It is a piecewise function formed by the function $f(\xi)$ for $\xi_0<\xi<\xi_m$ and the function $g(\xi)$ for $\xi_m<\xi<\xi_n$. Here $\xi_0$ represents the start point of a boundary line and $\xi_n$ represents the end point, whereas $\xi_m$ is the point where the two functions $f_a$ and $f_b$ blend with C--1 continuity. Now, imagine two consecutive lines where grid points are to be collocated so the grid metrics vary smoothly. To ensure smooth variation function $g_{a}$ of the first line has to blend with function $f_{b}$ of the second line (subscript indicates line). For that to happen the following conditions have to be met:
%
\begin{subequations} \label{eq:gridCond} 
\begin{align}
   x_a(\xi_n)&=x_b(\xi_0)		&	g_b(\xi_n)&=f_a(\xi_0)	&	g_b'(\xi_n)&=f_a'(\xi_0), 
\end{align} 
\end{subequations}
%
where the prime indicates derivation with respect to $\xi$. Note that constant value of the slope in figure~\ref{fig:gridFunction} means constant spacing between points with higher slopes indicating more distance between points. If the slope of $x_\xi(\xi)$ is then kept low at the boundaries $\xi_0$ and $\xi_n$, the points will be clustered at the extremes of the grid line. Using this method the points are defined in the boundaries of the blocks shown in figure~\ref{fig:gridBlocks}\emph{a} where the numbers inside the block represent the block's id. The interior points are then simply interpolated from the values on the block boundaries. The result can be observed in figure~\ref{fig:gridBlocks}\emph{b}.

\begin{figure}\centering
   \tikzsetnextfilename{gridBlocks}
   \input{\folder/Methodology/gridBlocks}
   \caption{Overview of the (\emph{a}) grid block structure with block numbering and (\emph{b}) detail of grid near the aerofoil. Every third point plotted in each direction.}
   \label{fig:gridBlocks}
\end{figure}

In figure~\ref{fig:gridBlocks}\emph{b} it can be seen that the leading edge of the aerofoils is a complicated zone where is impossible to achieve continuity of all the grid functions. These regions are very prone to develop numerical errors that will feed into the final solution. For that reason, it is indispensable to have some treatment between block internal boundaries which will be discussed in the next sections. However, it is wise to start calling these internal boundaries "block interfaces". The physical boundaries of the computational domain are simply called boundaries herein.
