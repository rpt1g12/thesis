%**SECTION **%
\FloatBarrier %Force all figures from previous sections to be placed 
\section{Boundary Conditions}\label{sec:BoundaryConditions}

Solving a system of Partial Differential Equations it is usually known as solving the Boundary Value Problem. This is, the solution sought is a function that can uniquely represent the interior values for the given boundary conditions. The boundary conditions should represent the solution offside the solution domain and its influence on the solution. In one-dimensional problems for example, at its left boundary, the information is shared between the interior and the exterior at the boundary, where the effect of the external solution information is fed into the computational domain by means of waves of positive phase velocity, i.e. are propagating towards the interior of the domain. On the other hand, waves with negative phase velocity will transport information from the interior domain towards the exterior. At the right boundary, the same would happen with opposite velocity signs.

The outgoing waves are fully determined by the interior solution, however, the incoming waves need to be specified by the user in what is known as boundary conditions. Several approaches can be used for the definition of the boundary conditions, although here the generalised characteristic boundary conditions~\citep{Thompson1987,Thompson1990,Kim2000,Kim2004} are used. For that, Equation~\ref{eq:govEq} recovered here as a starting point. 
%
\begin{equation}
\label{eq:govEqBC}
	\frac{\partial\bm{Q}}{\partial t}+\frac{\partial\bm{E}}{\partial x}+\frac{\partial\bm{F}}{\partial y}+\frac{\partial\bm{G}}{\partial z}= \bm{S}_v,
\end{equation}

A quasi-linear expression can be achieved using the following Jacobian matrices:
%
\begin{equation}\label{eq:jacobianMatrices}
\begin{aligned}
   &\bm{A}=\frac{\partial{\bm{E}}}{\partial{\bm Q}},\qquad	&\bm{B}=\frac{\partial{\bm{F}}}{\partial{\bm Q}},\qquad	&\bm{C}=\frac{\partial{\bm{G}}}{\partial{\bm Q}},
\end{aligned}
\end{equation}
%
using them on Equation~\ref{eq:govEqBC} the following quasi-linear expression is obtained:
%
\begin{equation}
\label{eq:quasiGovEq_1}
   \frac{\partial\bm{Q}}{\partial t}+\bm{A}\frac{\partial\bm{Q}}{\partial x}+\bm{B}\frac{\partial\bm{Q}}{\partial y}+\bm{C}\frac{\partial\bm{Q}}{\partial z}= \bm{S}_v.
\end{equation}

Since the Jacobian matrices and their eigenvectors have simpler forms when the primitive variables $\bm{Q'}=[\rho,u,v,w,p]^T$ are used, Equation~\ref{eq:quasiGovEq_1} can be transformed into
%
\begin{equation}
\label{eq:priQuasiGovEq}
	\begin{aligned}
	&\bm{M}\frac{\partial\bm{Q'}}{\partial t}+\bm{A}\bm{M}\frac{\partial\bm{Q'}}{\partial x}+\bm{B}\bm{M}\frac{\partial\bm{Q'}}{\partial y}+\bm{C}\bm{M}\frac{\partial\bm{Q'}}{\partial z}=\bm{S}_s=\\
	&\frac{\partial\bm{Q'}}{\partial t}+\bm{A'}\frac{\partial\bm{Q'}}{\partial
	x}+\bm{B'}\frac{\partial\bm{Q'}}{\partial
	y}+\bm{C'}\frac{\partial\bm{Q'}}{\partial z}=\bm{M}^{-1}\bm{S}_v=\bm{S'}_v,
	\end{aligned}
\end{equation}
%
where
%
\begin{equation}
\label{eq:pri2cons}
\begin{aligned}
&\bm{M}=\frac{\partial{\bm Q}}{\partial{\bm Q'}}=\begin{pmatrix}
	1	&0	&0	&0	&0	\\
	u	&\rho	&0	&0	&0	\\
	v	&0	&\rho	&0	&0	\\
	w	&0	&0	&\rho	&0	\\
	\frac{u^2+v^2+w^2}{2}	&\rho{u}	&\rho{v}	&\rho{w}	&\frac{1}{\gamma-1}	\\
\end{pmatrix}
\end{aligned}
\end{equation}
%
and
%
\begin{equation}
\label{eq:pri2cons2}
\begin{aligned}
\bm{A'}=\bm{M^{-1}}\bm{A}\bm{M},\qquad	&\bm{B'}=\bm{M^{-1}}\bm{B}\bm{M},\qquad	&\bm{C'}=\bm{M^{-1}}\bm{C}\bm{M},\qquad	&\bm{S'}_v=\bm{M^{-1}}\bm{S}_v
\end{aligned}
\end{equation}

Transformation of Equations~\ref{eq:quasiGovEq_1}~and~\ref{eq:priQuasiGovEq} to the generalised coordinates $\xi$, $\eta$, and $\zeta$, is obtained by the chain rule.
%
\begin{subequations}
\label{eq:genGovEq2}
\begin{align}
&\frac{\partial\bm{Q}}{\partial t}+\bm{K}_\xi\frac{\partial\bm{Q}}{\partial\xi}+\bm{K}_\eta\frac{\partial\bm{Q}}{\partial\eta}+\bm{K}_\zeta\frac{\partial\bm{Q}}{\partial\zeta}= \bm{S}_v, \\
&\frac{\partial\bm{Q'}}{\partial t}+\bm{K'}_\xi\frac{\partial\bm{Q'}}{\partial\xi}+\bm{K'}_\eta\frac{\partial\bm{Q'}}{\partial\eta}+\bm{K'}_\zeta\frac{\partial\bm{Q'}}{\partial\zeta}= \bm{S'}_v.
\end{align}
\end{subequations}
%
where for example the flux Jacobian matrices $\bm{K_\xi}$ and $\bm{K'_\xi}$ are given by
%
\begin{subequations}
\label{eq:jacobianMatrices2}
\begin{align}
	&\bm{K}_\xi=\xi_x\bm{A}+\xi_y\bm{B}+\xi_z\bm{C}, \\
	&\bm{K'}_\xi=\xi_x\bm{A'}+\xi_y\bm{B'}+\xi_z\bm{C'},
\end{align}
\end{subequations}
%
which are projections of $\bm{A}$, $\bm{A}'$, $\bm{B}$, $\bm{B}'$, $\bm{C}$, and $\bm{C}'$ on the direction $\bm{\xi}=[\xi_x,\xi_y,\xi_z]$, which is normal to a given plane of constant $\xi$.

Again the relation between both matrices is given by $\bm{M}$ as
%
\begin{equation}
\label{eq:pri2cons3}
	\bm{K}_\xi=\bm{M}\bm{K'}_\xi\bm{M^{-1}}
\end{equation}

The elements of the flux Jacobian matrix $\bm{K'}_\xi$ is given by \citet[Equations~16.5.1~and~16.5.2]{Hirsch1990}, and recovered here
%
\begin{equation}
\label{eq:kxi}
	\bm{K'}_\xi=\bm{A'}\cdot\bm{\xi}=\begin{pmatrix}
		U_\xi	&\rho\xi_x	&\rho\xi_y	&\rho\xi_z	&0	\\
		0	&U_\xi		&0		&0		&{\xi_x}/{\rho}	\\
		0	&0		&U_\xi		&0		&{\xi_y}/{\rho}	\\
		0	&0		&0		&U_\xi		&{\xi_z}/{\rho}	\\
		0	&\rho{c^2}\xi_x	&\rho{c^2}\xi_y	&\rho{c^2}\xi_z	&U_\xi	\\
	\end{pmatrix}
\end{equation}
%
with
%
\begin{equation}
\label{eq:uxi}
	\begin{aligned}
	&\bm{U}=[u,v,w],\\
	&\bm{\xi}=[\xi_x,\xi_y,\xi_z],\\
	&U_\xi=\bm{U}\cdot\bm{\xi}=u\xi_x+v\xi_y+w\xi_z.
	\end{aligned}
\end{equation}

The diagonalisation of $\bm{K'}_xi$ is given by
%
\begin{equation}
\label{eq:kdiag}
	\bm{K'}_\xi=\bm{P'}\bm\Lambda\bm{P'}^{-1},
\end{equation}
%
where $\bm{\Lambda}=\bm{I}\bm\lambda$ is the matrix containing the eigenvalues of $\bm{K'}_\xi$, which are:
%
\begin{equation}
\label{eq:lambda}
\bm\lambda=[U_\xi,U_\xi,U_\xi,U_\xi+c|\bm{\xi}|,U_\xi-c|\bm{\xi}|].
\end{equation}

And $\bm{P'}$ and $\bm{P'}^{-1}$ are the right and left eigenvector matrices respectively.
Using Equation~\ref{eq:pri2cons3} one can also define for the conservative variables the
following matrices:
%
\begin{subequations}
\label{eq:kdiag2}
\begin{align}
	&\bm{P}=\bm{M}\bm{P'},	\\
	&\bm{P}^{-1}=\bm{M}\bm{P'}^{-1},	\\
	&\bm{K}_\xi=\bm{P}\bm\Lambda\bm{P}^{-1},
\end{align}
\end{subequations}
%
which then, by moving the terms involving spatial derivatives with respect to $\eta$ and $\zeta$ to the RHS of Equation~\ref{eq:genGovEq2} and merging all terms in a new source term $\bm{S'}^*_v$ the following expression is obtained: 
%
\begin{subequations}
\label{eq:genGovEq3}
\begin{align}
&\frac{\partial\bm{Q}}{\partial t}+\bm{K}_\xi\frac{\partial\bm{Q}}{\partial\xi}=\bm{S}^*_v=\bm{S}_v-\left(\bm{K}_\eta\frac{\partial\bm{Q}}{\partial\eta}+\bm{K}_\zeta\frac{\partial\bm{Q}}{\partial\zeta}\right), \\
&\frac{\partial\bm{Q'}}{\partial t}+\bm{K'}_\xi\frac{\partial\bm{Q'}}{\partial\xi}=\bm{S'}^*_v=\bm{S'}_v-\left(\bm{K'}_\eta\frac{\partial\bm{Q'}}{\partial\eta}+\bm{K'}_\zeta\frac{\partial\bm{Q'}}{\partial\zeta}\right).
\end{align}
\end{subequations}

The system of Equation~\ref{eq:genGovEq3} can be written in characteristic by premultilication by $\bm{P'}^{-1}$, and $\bm{P}^{-1}$ respectively to obtain:
%
\begin{subequations}
\label{eq:chaGovEq}
\begin{align}
	&\bm{P}^{-1}\frac{\partial{\bm{Q}}}{\partial{t}}+\bm{P}^{-1}\bm{K}_\xi\frac{\partial{\bm{Q}}}{\partial{\xi}}=\bm{P}^{-1}\bm{S}^*_v, \\
	&\bm{P'}^{-1}\frac{\partial{\bm{Q'}}}{\partial{t}}+\bm{P'}^{-1}\bm{K'}_\xi\frac{\partial{\bm{Q'}}}{\partial{\xi}}=\bm{P'}^{-1}\bm{S'}^*_v,
\end{align}
\end{subequations}
%
then using the following relations
%
\begin{equation}
\label{eq:q2r}
	\begin{aligned}
	\tilde{\bm{\xi}}&=\frac{\bm{\xi}}{|\bm{\xi}|}, &\delta\tilde{U_\xi}&=\tilde{\xi}_x\delta{u}+\tilde{\xi}_y\delta{v}+\tilde{\xi}_z\delta{w} \\
	\delta{\tilde{\vartheta_1}}&=\tilde{\xi}_z\delta{v}-\tilde{\xi}_y\delta{w}, &\delta\tilde{\vartheta_2}&=-\tilde{\xi}_z\delta{u}+\tilde{\xi}_x\delta{w}, \\
 	\delta{\tilde{\vartheta_3}}&=\tilde{\xi}_y\delta{u}-\tilde{\xi}_x\delta{v}, \\
	\end{aligned}
\end{equation}
%
and
%
\begin{equation}
\label{eq:R}
	\begin{aligned}
	\delta\bm{R}&=\bm{P}^{-1}\delta\bm{Q}=\bm{P'}^{-1}\delta\bm{Q'}
	&=\begin{pmatrix}
	\tilde{\xi}_x(\delta\rho-{\delta p}/{c^2})+\delta\tilde{\vartheta_1} \\
	\tilde{\xi}_y(\delta\rho-{\delta p}/{c^2})+\delta\tilde{\vartheta_2} \\
	\tilde{\xi}_z(\delta\rho-{\delta p}/{c^2})+\delta\tilde{\vartheta_3} \\
	{\delta p}/{\rho c}+\delta\tilde{U}_\xi \\
	{\delta p}/{\rho c}-\delta\tilde{U}_\xi
	\end{pmatrix}
	\end{aligned}
\end{equation}
%
finally, the following expression is obtained:
%
\begin{equation}
\label{eq:quasiGovEq_2}
	\frac{\partial{\bm{R}}}{\partial{t}}+\bm{\Lambda}\frac{\partial{\bm{R}}}{\partial{\xi}}=\bm{S}_c=\bm{P}^{-1}\bm{S}^*_v=\bm{P'}^{-1}\bm{S'}^*_v
\end{equation}

$U_\xi$ and $\delta{U}_\xi$ represent the velocity normal to the boundary and its derivative, and $\bm{\tilde\xi}$ the unitary boundary normal vector. The terms $\delta\vartheta_1$, $\delta\vartheta_2$ and $\delta\vartheta_3$ are the projections of the velocity vector on the intersection lines formed by the normal plane defined by $\bm{\tilde\xi}$ and the $yz$, $xz$, and $xy$ planes respectively. They are all three perpendicular to $\bm{\tilde\xi}$ consequently when written as the vectors $\delta\bm{\tilde\vartheta}_1=[0,\tilde\xi_z\delta{v},-\tilde\xi_y\delta{w}]$, $\delta\bm{\tilde\vartheta}_2=[-\tilde\xi_z\delta{u},0,\tilde\xi_x\delta{w}]$, and $\delta\bm{\tilde\vartheta}_3=[\tilde\xi_y\delta{u},-\tilde\xi_x\delta{v},0]$. The terms in the diagonal of $\bm\Lambda$ represent the entropy (first term), vorticity (second and third) and acoustic (fourth and fifth) waves. It is clear that the entropy and vorticity waves always convect with the flow at the boundary whereas velocity convection of the acoustic waves depend on the value of $c$, i.e. the sound speed. For subsonic flows, the first fourth waves are entering the domain whereas the fifth is exiting for an inlet boundary considered at $\xi=0$. At the outlet, $\xi=N$, on the contrary, just the fifth mode represents an incoming wave whereas the rest are outgoing waves. Equation~\ref{eq:quasiGovEq_2} is a quasi-one-dimensional expression, basis of the boundary treatment developed by \citet{Kim2000,Kim2004}. Detailed definitions of the matrix terms can be found in~\citet{Kim2000,Kim2004}. The second term in Equation~\ref{eq:quasiGovEq_2} is the convection term in characteristic form. Its relation to the original convection term is given by
%
\begin{equation}
\label{eq:convTerm}
	\xi_x \frac{\partial\bm{E}}{\partial\xi}+\xi_y \frac{\partial\bm{F}}{\partial\xi}+\xi_z \frac{\partial\bm{G}}{\partial\xi}=\bm{P}\bm\Lambda \frac{\partial\bm{R}}{\partial\xi}=\bm{P}\bm{L}.
\end{equation}

The matrices $\bm P'^{-1}$ and $\bm P'$ are:
%
\begin{subequations}
\label{eq:pMatrices}
\begin{align}
	\bm{P'}^{-1}&=\frac{\rho}{c}\begin{pmatrix}
	\tilde\xi_x{c}/{\rho}	&0		&\tilde\xi_z	&-\tilde\xi_y	&-{\tilde\xi_x}/(\rho c) \\
	\tilde\xi_y{c}/{\rho}	&-\tilde\xi_z	&0		&\tilde\xi_x	&-{\tilde\xi_y}/(\rho c) \\
	\tilde\xi_z{c}/{\rho}	&\tilde\xi_y	&-\tilde\xi_x	&0		&-{\tilde\xi_z}/(\rho c) \\
	0				&\tilde\xi_x	&\tilde\xi_y	&\tilde\xi_z	&{1}/(\rho c) \\
	0				&-\tilde\xi_x	&-\tilde\xi_y	&-\tilde\xi_z	&{1}/(\rho c) \\
	\end{pmatrix} \\
	\bm{P'}&=\frac{c}{\rho}\begin{pmatrix}
	\tilde\xi_x{\rho}/{c}	&\tilde\xi_y{\rho}/{c}	&\tilde\xi_z{\rho}/{c}	&{\rho}/(2c)	&{\rho}/(2c) \\
	0				&\tilde\xi_z	&-\tilde\xi_y  	&-{\tilde\xi_x}/{2}		&-{\tilde\xi_x}/{2} \\
	-\tilde\xi_z			&0		&\tilde\xi_x   	&-{\tilde\xi_y}/{2}		&-{\tilde\xi_y}/{2} \\
	\tilde\xi_y			&-\tilde\xi_x	&0		&-{\tilde\xi_z}/{2}		&-{\tilde\xi_z}/{2} \\
	0				&0		&0		&(\rho c)/{2}		&(\rho c)/{2} 
	\end{pmatrix} 
\end{align}
\end{subequations}

The primitive equations are recovered if Equation~\ref{eq:convTerm} is premultiplied by $\bm{P'}$ as:
%
\begin{equation}
\label{eq:quasiPrimitive}
	\frac{\partial{\bm{Q'}}}{\partial{t}}+\bm{P'}\bm{L}=\bm{P'}\bm{S}_c,
\end{equation}
%
and explicitly,
%
\begin{subequations}
\label{eq:systemL}
\begin{align}
	\frac{\partial{\rho}}{\partial{t}}+\tilde\xi_x{L_1}+\tilde\xi_y{L_2}+\tilde\xi_z{L_3}+\frac{1}{2}(L_4+L_5)&=\tilde\xi_x{S_{c1}}+\tilde\xi_y{S_{c2}}+\tilde\xi_z{S_{c3}}+\frac{1}{2}(S_{c4}+S_{c5})\label{eq:systemL1} \\
	\frac{\partial{u}}{\partial{t}}-\tilde\xi_z{L_2}+\tilde\xi_y{L_3}+\frac{\tilde\xi_x}{2}(L_4-L_5)&=-\tilde\xi_z{S_{c2}}+\tilde\xi_y{S_{c3}}+\frac{\tilde\xi_x}{2}(S_{c4}-S_{c5})\label{eq:systemL2} \\
	\frac{\partial{v}}{\partial{t}}+\tilde\xi_z{L_1}-\tilde\xi_x{L_3}+\frac{\tilde\xi_y}{2}(L_4-L_5)&=\tilde\xi_z{S_{c1}}-\tilde\xi_x{S_{c3}}+\frac{\tilde\xi_y}{2}(S_{c4}-S_{c5})\label{eq:systemL3} \\
	\frac{\partial{w}}{\partial{t}}-\tilde\xi_y{L_1}+\tilde\xi_x{L_2}+\frac{\tilde\xi_z}{2}(L_4-L_5)&=-\tilde\xi_y{S_{c1}}+\tilde\xi_x{S_{c2}}+\frac{\tilde\xi_z}{2}(S_{c4}-S_{c5})\label{eq:systemL4} \\
	\frac{\partial{p}}{\partial{t}}+\frac{c^2}{2}(L_4+L_5)&=\frac{c^2}{2}(S_{c4}+S_{c5})\label{eq:systemL5}
	\end{align}
\end{subequations}

Additionally if Equations~\ref{eq:systemL2},~\ref{eq:systemL3}~and~\ref{eq:systemL4} are multiplied by $\tilde\xi_x$,~$\tilde\xi_x$~and~$\tilde\xi_z$ respectively and then combined the following expression for the normal velocity is obtained:
%
\begin{equation}
\label{eq:norVel}
	\frac{\partial{\tilde{U}_\xi}}{\partial{t}}+\frac{1}{2}(L_4-L_5)=\frac{1}{2}(S_{c4}-S_{c5}).
\end{equation}

The convection term $\bm{L}$ is calculated at every time step during by an iterative process the steps of which are:	
%
\begin{enumerate}
   \item Fluxes in Equation~\ref{eq:genGovEq2} are evaluated using the finite difference techniques described in the previous sections and the normal-flux derivative $\partial\bm{\hat{E}}/\partial\xi$ is used as an initial guess to be corrected at the end of the iterative procedure.
   \item The convection term $\bm{L}$ in Equation~\ref{eq:quasiGovEq_2} is computed by the following expression:
   \begin{equation}\label{eq:conTermEq}
   \bm{L}=J\bm{P}^{-1}\left\{\frac{\partial{\bm{\hat{E}}}}{\partial{\xi}}-\left[\bm{E}\frac{\partial}{\partial{\xi}}\left(\frac{\xi_x}{J}\right)+\bm{F}\frac{\partial}{\partial{\xi}}\left(\frac{\xi_y}{J}\right)+\bm{G}\frac{\partial}{\partial{\xi}}\left(\frac{\xi_z}{J}\right)\right]\right\}.
   \end{equation}
   \item Physical conditions, e.g. soft inflow, non reflecting outflow, wall, are then imposed at the boundary and the corrected characteristic convected term $\bm{L}^*$ is obtained.
   \item The normal-flux derivative is then recomputed using the new corrected $\bm{L}^*$ in Equation~\ref{eq:conTermEq} rearranged as:
   \begin{equation}\label{eq:normFlux}
	\left(\frac{\partial{\bm{\hat{E}}}}{\partial{\xi}}\right)^*=\frac{1}{J}\bm{P}\bm{L}^*+\left[\bm{E}\frac{\partial}{\partial{\xi}}\left(\frac{\xi_x}{J}\right)+
	\bm{F}\frac{\partial}{\partial{\xi}}\left(\frac{\xi_y}{J}\right)+\bm{G}\frac{\partial}{\partial{\xi}}\left(\frac{\xi_z}{J}\right)\right],
   \end{equation}
   which is plugged again into Equation~\ref{eq:genGovEq2}. The solution then is obtained by time integration of Equation~\ref{eq:genGovEq2}.
\end{enumerate}

To implement step 3 physical boundary conditions have to be met by the terms of $\bm{L}$ in Equation~\ref{eq:systemL}. Depending on the conditions required for the system of Equation~\ref{eq:systemL} different physical boundary conditions can be achieved.

%**SUBSECTION **%
\FloatBarrier %Force all figures from previous sections to be placed 
\subsection{Soft inflow condition}\label{sub:softIn}
For a subsonic inlet, only the $L_5$ characteristic wave can be directly obtained from the domain's interior information because it is the only leaving wave. For the rest of the waves, they need to be prescribed using physical conditions. \citet{Kim2000} decided to impose the far upstream velocity and pressure, and considered quasi-isentropic conditions at the boundary. This conditions they expressed as
%
\begin{subequations}
\label{eq:softIn}
\begin{align}
	L_1^*&=0, \\
	L_2^*&=K_{in}\left\{\left[\tilde{\xi}_x(w-w_\infty)-\tilde{\xi}_z(u-u_\infty)\right]/2\right\}, \\
	L_3^*&=K_{in}\left\{\left[-\tilde{\xi}_x(v-v_\infty)+\tilde{\xi}_y(u-u_\infty)\right]/2\right\}, \\
	L_4^*=K_{in}\left[(\tilde{U}-\tilde{U}_\infty)+(p-p_\infty)/\rho{c}\right]&=K_{in}\left[\tilde{\xi}_x(u-u_\infty)+\tilde{\xi}_y(v-v_\infty)\right] \nonumber \\
\end{align}
\end{subequations}

where $K_{in}$ is a constant used to ensure a well-posed problem when pressure and velocity conditions are not close enough to those imposed in the far upstream. When this happens, non-physical reflections are created at the inlet boundary so the inlet velocity and pressure are forced to be close to the far upstream condition. The expression of $K_{in}$ is
%
\begin{equation}
\label{eq:kin}
	K_{in}=\sigma_{in}(1-M_{max}^2)(c/l),
\end{equation}
%
where $\sigma_{in}$ is a coefficient that defines the inlet reflections, $M_{max}$ is the maximum Mach number in the entire flow, $c$ is the speed of sound, and $l$ is the characteristic length of the domain. The value recommended in~\citet{Kim2000} was $\sigma_{in}=0.25$. Additionally, for viscous computations, the viscous stresses and heat flux gradients are set to zero in the boundary normal direction.

%**SUBSECTION **%
\FloatBarrier %Force all figures from previous sections to be placed 
\subsection{Nonreflecting outflow condition}\label{sub:nonOut}
For a subsonic outlet, in contrast with the inlet, only one wave cannot be obtained from the internal information, that is $L_5$. $L_5$ is the only characteristic wave entering the domain whereas all $L_1,L_2,L_3$ and $L_4$ are leaving it. Therefore an expression for the $L_5$ amplitude is needed at the boundary. If for example the pressure at the outlet is set to be equal to the far dowstream pressure, will lead to a well-posed problem that will, however, produce non-physical acoustic reflections.

\citet{Kim2000} imposed the value of the far downstream static pressure, and similarly to what done in the inlet, to avoid reflections a constant $K_{out}$ is used to control the boundary reflectivity. The condition for the incoming wave is
%
\begin{equation}
\label{eq:nonRefOut}
	L_5^*=K_{out}\left[(p-p_\infty)/\rho c\right],
\end{equation}
%
with
%
\begin{equation}
\label{eq:kout}
	K_{out}=\sigma_{out}(1-M_{max}^2)(c/l),
\end{equation}
%
where $\sigma_{out}=0.25$ as in the inlet as suggested by \citet{Poinsot1992}. Again, if the flow is considered viscous, both tangential stresses and heat flux gradients in the boundary normal direction are set to zero.

%**SUBSECTION **%
\FloatBarrier %Force all figures from previous sections to be placed 
\subsection{Inviscid wall condition}\label{sub:invscWall}
An inviscid wall is that where no penetration is allowed, i.e. the fluid velocity is zero in the direction normal to the wall, but there is freedom of movement in the tangential plane, i.e. slip condition. Considering a flow travelling at a subsonic velocity from left to right, for a wall located at the left boundary the phase velocity signs indicate that $L_5$ is the wave transporting the information from the interior domain whereas $L_4$ transfers information from outside the solution domain. If the wall is located on the right boundary the opposite occurs with $L_5$ incoming and $L_4$ leaving the domain. Depending on the wall relative position to the flow velocity the amplitudes are given by
%
\begin{subequations}
\label{eq:invsWall}
\begin{align}
	&(\text{left wall})		&	L_4^*&=L_5+S_{C_4}-S_{C_5}, \\
	&(\text{right wall})		&	L_5^*&=L_4-S_{C_4}+S_{C_5}. 
\end{align}
\end{subequations}

%**SUBSECTION **%
\FloatBarrier %Force all figures from previous sections to be placed 
\subsection{Viscous wall condition}\label{sub:viscWall}
In a viscous wall, as well as no penetration velocity, the tangential velocities are supposed to be zero too, i.e. non-slip condition. The amplitude of the characteristic waves then is given by
%
\begin{subequations}
\label{eq:viscWall}
\begin{align}
	&(\text{left wall})		&	L_4^*&=L_5+S_{C_4}-S_{C_5}, \\
	&(\text{right wall})		&	L_5^*&=L_4-S_{C_4}+S_{C_5}, \\
	&(\text{both sides})		&	&\begin{cases}
		L_2^*=L_3^*=0, \\
		S_{C_2}^*=S_{C_3}^*=0.
	\end{cases}
\label{eq:viscWall2}
\end{align}
\end{subequations}

It can be seen that the first two expressions are the same as if it was an inviscid wall. However, the absence of tangential velocity results in Equation~\ref{eq:viscWall2}. In this case, the viscous source term $\bm{\hat{S}}_v^*$ needs to be updated using the characteristic source term as
%
\begin{equation}
\label{eq:viscSource}
		\bm{\hat{S}}_v^*=\frac{1}{J}\bm{P}\bm{S}_C^*+\left[\bm{E}\frac{\partial}{\partial{\xi}}\left(\frac{\xi_x}{J}\right)+
		\bm{F}\frac{\partial}{\partial{\xi}}\left(\frac{\xi_y}{J}\right)+\bm{G}\frac{\partial}{\partial{\xi}}\left(\frac{\xi_z}{J}\right)
		+\frac{\partial{\hat{\bm{F}}}}{\partial{\eta}}+\frac{\partial{\hat{\bm{G}}}}{\partial{\zeta}}\right].
\end{equation}

Equation~\ref{eq:viscSource} is solved for the viscous source term where the characteristic source term has been replaced by its updated version so the boundary conditions are satisfied.

The boundary conditions presented here are sufficient to define the Equation~\ref{eq:genGovEq2} at the boundaries for the tandem aerofoil case studied here for both viscous and inviscid computations. Nevertheless because of the Multi-block grid strategy used additional interface conditions need to be supplied so the information is communicated between adjacent blocks. This is covered in the next subsection.

%**SUBSECTION **%
\FloatBarrier %Force all figures from previous sections to be placed 
\subsection{Interface boundary conditions}\label{sub:intCond}
As aforementioned, the inclusion of bodies in the computational domain usually lead to grid singularities that cause spurious oscillations that will end up contaminating the final solution. The grid singularities appear at points where the grid metric derivatives are not unique. This happens for example at both the leading and the trailing edge of the aerofoils of Figure~\ref{fig:gridBlocks}. A close view of the leading edge zone is shown in Figure~\ref{fig:leZoom} where the blue lines represent the block boundaries. It is seen that four blocks meet at the leading edge of the aerofoil and that block 5 shares a block boundary with block 6 on its right and another one with block 0 at its bottom. The shared boundaries are referred to as \emph{block interface boundaries} herein. The block interface between block 5 and 6 is a constant $\xi$ interface whereas the interface between block 5 and 0 is a constant $\eta$ interface. 
%
\begin{figure}\centering
   \tikzsetnextfilename{leZoom}
   \input{\folder/Methodology/leZoom}
   \caption{Leading edge zoomed view with block numbering.}
   \label{fig:leZoom}
\end{figure}

Consider the interface of $\xi=\text{const}$ between block 5 and 6. To avoid the grid metrics discontinuity at this boundary the boundary scheme is used at the block boundaries. However, this isolates the blocks not allowing the information to flow from one block to its neighbours. To let information flow between blocks an interface boundary condition has to be implemented. Now, the discontinuity can be expressed in terms of the metric gradient as $\nabla\xi^L\neq\nabla\xi^R$, i.e. the gradient on the left block does not match the gradient of the right block. This mismatch is in terms of the norm of the gradient but not in terms of its direction. Because both blocks share the same direction, the gradient in both blocks is always aligned with the normal direction of the $\xi=const$ boundary. That is
%
\begin{equation}
\label{eq:interNorm}
\begin{aligned}
	&\frac{\nabla\xi^L}{|\nabla\xi^L|}=\nabla\tilde{\xi}^L, & &\frac{\nabla\xi^R}{|\nabla\xi^R|}=\nabla\tilde{\xi}^R, & &\nabla\tilde{\xi}^L=\nabla\tilde{\xi}^R.
\end{aligned}
\end{equation}

In past subsections, the characteristic wave equation in the boundary normal direction has been used to define several boundary conditions. \citet{Kim2003} cast the characteristic equation in the block interfaces where all variables and variables time derivatives must match. Consequently also the characteristic variables time derivative must also match at the interface.
%
\begin{equation}
\label{eq:matchR}
	\frac{\partial{\bm{R}^L}}{\partial{t}}=\frac{\partial{\bm{R}^R}}{\partial{t}},
\end{equation}
%
which leads to the following relation between left and right sided waves:
%
\begin{equation}
\label{eq:waveRel}
	\bm{L}^L-\bm{S}_C^L=\bm{L}^R-\bm{S}_C^R
\end{equation}

Again here the sign of the convection speeds will determine whether a wave is entering or exiting a block unit. However, an incoming wave of one block will also be an exiting wave of its immediate adjacent block. This, in contrast to the other boundary conditions, means that both incoming and outgoing waves can be obtained from the interior values of the blocks. The method then consists in that every block calculates its outgoing waves form its own information whilst the incoming waves are corrected by the outgoing waves of its immediate block neighbour.
%
\begin{equation}
\label{eq:interfCond}
	\text{for}\,m=1,\cdots,5 \begin{cases}
		L_m^{L^*}=L_m^R-S_{C_m}^R+S_{C_m}^L, &\text{ if }\lambda_m^L/|\lambda_m^L|=\lambda_m^R/|\lambda_m^R|\leq 0\\
		\\
		L_m^{R^*}=L_m^L-S_{C_m}^L+S_{C_m}^R, &\text{ if }\lambda_m^L/|\lambda_m^L|=\lambda_m^R/|\lambda_m^R|\geq 0
	\end{cases}
\end{equation}
