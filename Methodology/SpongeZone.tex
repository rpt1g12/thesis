%**SECTION **%
\FloatBarrier %Force all figures from previous sections to be placed 
\section{Sponge Zone} 
\label{sec:Sponge}
In Subsections~\ref{sub:softIn}~and~\ref{sub:nonOut} the inflow and outflow boundary conditions have been described. It has been highlighted the importance of avoiding flow reflections at the domain's boundaries. The \emph{Soft Inflow} and the \emph{Non-reflecting Outflow} conditions reduce the reflection level at the boundaries, however even with such boundary conditions reflections may appear. For this reason, the code used for this study uses what is known as \emph{Sponge Zone} boundary conditions as well. The sponge zone can be seen as an artificial linear forcing/damping near the boundaries. For simplicity the Euler Equations Equation~\ref{eq:euler} will be used here for describing the implementation of the sponge zone. With Equation~\ref{eq:euler} in mind, the sponge zone treatment is represented by an additional term added to the equations at the right-hand side:
%
\begin{equation}\label{eq:spongeBasic}
\begin{aligned}
S_S=(\bm{Q}-\bm{Q_{ref}})=\sigma(x,y)\begin{pmatrix}
	\rho-\rho_{\infty} \\
	\rho{u}-\rho_{\infty}u_{gust} \\
	\rho{v}-\rho_{\infty}v_{gust} \\
	\rho{w}-\rho_{\infty}w_{gust} \\
	\rho{e_t}-\rho_{\infty}e_{t_{gust}} \\
\end{pmatrix},\\ 
\\
\text{and: }e_{t_{gust}}=\frac{p_{\infty}}{(\gamma-1)\rho_\infty}+\frac{u^2_{gust}+v^2_{gust}+w^2_{gust}}{2},\\
\end{aligned}
\end{equation}
%
with: 
%
\begin{equation}
\label{eq:sponge2}
\begin{aligned}
\sigma(x,y)=\sigma_0\{1+cos[\pi A(x)B(y)]\}/2, \\
x\in[x_{min},x_{max}]\;\text{ and }\; y\in[y_{min},y_{max}], \\
\end{aligned}
\end{equation}
%
and:
%
\begin{equation}
\label{eq:sponge3}
	\begin{cases}
	A(x)=1-\max[1-(x-x_{min}/L_S,0]-\max[1-(x_{max}-x)/Ls,0], \\
	B(y)=1-\max[1-(y-y_{min}/L_S,0]-\max[1-(y_{max}-y)/Ls,0],
	\end{cases}
\end{equation}
%
where $\bm{Q}$ is the vector containing the flow variables, $\bm{Q_{ref}}$ ($\rho_{\infty}$, $\rho_{\infty}u_{gust}$, $\rho_{\infty}v_{gust}$, $\rho_{\infty}w_{gust}$, and $\rho_{\infty}e_{t_{gust}}$) is the vector containing the desired reference solution aimed at the sponge zone, and $\sigma$ is a free parameter known as sponge zone parameter that controls the strength of the forcing/damping in the sponge zone. The coefficient $\sigma(x,y)$ depends on the $x$ and $y$ position only, and consequently, it is the same for each "z-plane". Equation~\ref{eq:sponge2} enables a smooth transition from the interior where the sponge zone is not present to the boundary where its effect is the biggest ($\sigma_0$). The initial value used for the work done here was $\sigma_0=8$. It is worth noting that all velocities, energy and pressure terms are followed by the "$gust$" subscript. This indicates that as well as being used to control the reflection on the boundaries, the sponge zone can be used to introduce a certain flow profile at the sponge zone.
%
\begin{figure}\centering
   \tikzsetnextfilename{sponge}
   \input{\folder/Methodology/sponge}
   \caption{Plots of the (\emph{a}) sponge zone set-up and (\emph{b}) profile of the sponge zone velocity forcing.}
   \label{fig:sponge}
\end{figure}

The sponge zone treatment used in this project is the one proposed by \citet{Kim2010a}. In Figure~\ref{fig:sponge}\emph{a} one can see the location of the sponge zone close to the domain's boundaries and its geometric parameters $L_\Omega$ (distance from the domain's centre to the boundary limits) and $L_S$ (sponge zone's thickness). Because the problem studied in this project is a tandem aerofoil case, the arrangement is not exactly the same, nonetheless Figure~\ref{fig:sponge} will be enough to understand how the sponge is implemented in essence. The definition of the sponge source term proposed in~\citet{Kim2010a} is 
%
\begin{equation}
\label{eq:sponge}
\bm{S}_S=\sigma(x,y)\begin{pmatrix}
\rho-\rho_{\infty} \\
\lambda(x)(\rho{u}-\rho_{\infty}u_{gust}) \\
\lambda(x)(\rho{v}-\rho_{\infty}v_{gust}) \\
\lambda(x)(\rho{w}-\rho_{\infty}w_{gust}) \\
p-p_{\infty} \\
\end{pmatrix},
\end{equation}
%
where the total energy has been substituted by the pressure since forcing applied on total energy overrides density and velocity. Additionally, a weighting factor $\lambda(x)$ is introduced to modify the intensity of the sponge forcing depending on the streamwise direction. The weighting function is given by
%
\begin{equation}
\label{eq:spongeWeight}
	\lambda(x)=(1-\varepsilon)[1-\tanh(x/L)]/2+\varepsilon,
\end{equation}
%
where $\varepsilon$ is a constant representing the minimum desired value for $\lambda$, which in the case seen in Figure~\ref{fig:sponge}\emph{b} and also in this project has been set to $\varepsilon=0$ as suggested by \citet{Kim2010a} in order to avoid excessive constraint in the outflow condition.
