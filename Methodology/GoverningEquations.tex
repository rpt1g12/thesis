%**SECTION **% 
\FloatBarrier %Force all figures from previous sections to be placed 
\section{Governing Equations}\label{sec:GoverningEquations}

The governing equations of every fluid flow are the Navier-Stokes equations. These set of Partial Differential Equations (PDEs) describe how velocity, pressure, and density are related. The equations cast in conservation form are expressed in \ref{eq:govEq} as 
%
\begin{equation} \label{eq:govEq}
   \frac{\partial\bm{Q}}{\partial t}+\frac{\partial\bm{E}}{\partial x}+\frac{\partial\bm{F}}{\partial y}+\frac{\partial\bm{G}}{\partial z}= \bm{S}_v =
   \frac{M_\infty}{Re_\infty}\left(\frac{\partial\bm{E}_v}{\partial x}+\frac{\partial\bm{F}_v}{\partial y}+\frac{\partial\bm{G}_v}{\partial z}\right), 
\end{equation}
%
where the conservative variables vector is $\bm{Q}=[\rho, \rho u, \rho v, \rho w, e_t]^T$ and the with $\rho$, $u$, $v$, $w$, and $e_t$ being: density, x-velocity component, y-velocity component, z-velocity component, and energy respectively. The vectors $\bm{E}$, $\bm{F}$, and $\bm{G}$ represent the inviscid flux terms and $\bm{E}_v$, $\bm{F}_v$, and $\bm{G}_v$ are the viscous flux terms. The expressions for the flux vectors are
%
\begin{subequations} \label{eq:flux}
\begin{align}
   \bm{E}&=[\rho u,\rho u^2+p,\rho uv,\rho uw,(\rho e_t +p)u]^T, \nonumber \\ 
   \bm{F}&=[\rho v,\rho uv,\rho v^2+p,\rho vw,(\rho e_t +p)v]^T, \nonumber \\ 
   \bm{G}&=[\rho w,\rho uw,\rho vw,\rho w^2+p,(\rho e_t +p)w]^T, \nonumber \\ 
   \text{where}\quad p&=\rho(\gamma-1)\left[e_t-\frac{u^2+v^2+w^2}{2}\right] \nonumber 
\end{align} 
\end{subequations}
%
and for the viscous terms 
%
\begin{subequations} \label{eq:viscous}
\begin{align} 
   \bm{E_v}&=[0,\tau_{xx},\tau_{xy},\tau_{xz},(u\tau_{xx}+v\tau_{xy}+w\tau_{xz}+q_x)]^T,\nonumber \\ 
   \bm{F_v}&=[0,\tau_{xy},\tau_{yy},\tau_{yz},(u\tau_{xy}+v\tau_{yy}+w\tau_{yz}+q_y)]^T,\nonumber \\ 
   \bm{G_v}&=[0,\tau_{xz},\tau_{yz},\tau_{zz},(u\tau_{xz}+v\tau_{yz}+w\tau_{zz}+q_z)]^T.\nonumber 
\end{align} 
\end{subequations}
%
where $\tau_{ij}$ is the stress tensor the components which expressed in Einstein notation, i.e. $x=x_1,y=x_2,z=x_3$ and $u=u_1,v=u_2$, and $w=u_3$, are
%
\begin{equation}\label{eq:stress}
   \tau_{ij}=\mu \left(\frac{\partial{u_i}}{\partial{x_j}}+\frac{\partial{u_j}}{\partial{x_i}}\right)+\delta_{ij}\lambda \frac{\partial{u_i}}{\partial{x_i}},
\end{equation}
%
where $\mu$ represents the dynamic viscosity coefficient and $\lambda=-2/3\mu$ represents the bulk coefficient viscosity. Also, the heat fluxes $q_i$ expressed in Einstein notation are 
%
\begin{equation}\label{eq:heatflux}
   q_i=\frac{\mu}{(\gamma-1)Pr}\frac{\partial T}{\partial x_i}, 
\end{equation}
%
and $Re_\infty,M_\infty$ and $Pr$ are the free-stream Reynolds number and Mach number and the Prandtl number respectively.

If the Reynolds number is high enough, viscous effects can be neglected in the Navier-Stokes equations. The Right Hand Side (RHS) of \ref{eq:govEq} vanishes and the  equations are reduced to
%
\begin{equation}\label{eq:euler}
   \frac{\partial\bm{Q}}{\partial t}+\frac{\partial\bm{E}}{\partial x}+\frac{\partial\bm{F}}{\partial y}+\frac{\partial\bm{G}}{\partial z}= 0. 
\end{equation}
%
which are known also as the Euler equations.

%=====================SUBSECTION - ILES=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Implicit LES}\label{sub:ILES}
The study of the flow features are investigated here by means of implicit large eddy simulations (ILES), which consists in finding numerical solutions to the flow governing equations, i.e. Navier-Stokes equations, to which a high-order low-pass compact filter is applied (to be described in \S\ref{sub:filters}). The filter is applied directly to the solution at every time step and acts as an \emph{implicit} sub-grid scale (SGS) model that enforces dissipation of scales smaller than the cut-off filter wavelength.~\citet{Garmann2013} performed an extensive analysis of the ILES technique in comparison to more conventional techniques using an explicit SGS model and concluded that subject to an appropriate grid resolution, ILES simulations are capable to correctly capture the flow physics of a flow such as the one proposed in the current study.
%%**SUBSECTION **% 
%\FloatBarrier %Force all figures from previous sections to be placed 
%\subsection{Large Eddy Simulations Equations}\label{sub:LESEquations}
%
%For this project the technique chosen to tackle the turbulence modelling problem was the Implicit LES method (ILES). With this method \ref{eq:govEq} is solved numerically and then filtered so oscillations of wavenumber beyond the filter's cut-off wavenumber are removed from the final solution. By this means the energy containing eddies are fully resolved whilst the smallest eddies remain unresolved, and whose effect on the final solution is supposed to be equivalent to the numerical dissipation inherited by the numerical schemes used. This technique reduces the effort required to get the final solution compared to regular LES methods which add an additional diffusion term to \ref{eq:govEq} that needs to be computed at each time step.
%
%When using the traditional LES approach, \ref{eq:govEq} is transformed to its Favre Averaged version using a density weighted average, i.e. the so called Favre Averaging, technique shown in \ref{eq:favreAvg}
%%
%\begin{equation} \label{eq:favreAvg}
%   \tilde{\phi}=\frac{\widehat{\rho\phi}}{\hat{\rho}}, 
%\end{equation}
%%
%where the hat denotes the filter operation and the tilde the Favre operation. Here $\tilde{\phi}$ represents the resolved part of $\phi$, and the correspondent unresolved part is denoted by
%%
%\begin{equation}\label{eq:favreUnresolved}
%   \phi''=\phi-\tilde{\phi}. 
%\end{equation}
%
%Applying the above operation to the Navier-Stokes equations, and using Einstein notation results into
%%
%\begin{subequations}\label{eq:favreGovEq} 
%\begin{align}
%   &\frac{\partial{\hat{\rho}}}{\partial{t}}+\frac{\partial}{\partial{x_i}}[\hat{\rho}\widetilde{x_i}]=0 \\
%   &\frac{\partial}{\partial{t}}(\hat{\rho}\widetilde{u_i})+\frac{\partial}{\partial{x_j}}\left[\hat{\rho}\widetilde{u_i}\widetilde{u_j}+\hat{p}\delta_{ij}+\underbrace{\widehat{\rho u_i'' u_j''}}_\text{(1)}-\widetilde{\tau_{ji}}-\underbrace{\widehat{\tau_{ji}''}}_\text{(2)}\right]=0 \\
%   &\frac{\partial}{\partial{t}}(\hat{\rho}\tilde{e})+\frac{\partial}{\partial{x_j}}\left[\hat{\rho}\widetilde{u_j}\tilde{e}+\widetilde{u_j}\hat{p}+\underbrace{C_p\widehat{\rho u_j''T}}_\text{(3)}+\underbrace{\widetilde{u_i}\widehat{\rho u_i''u_j''}}_\text{(4)}\right.\nonumber \\
%   +&\left. \underbrace{\frac{1}{2}\widehat{\rho u_j'' u_i'' u_i''}}_\text{(5)} +q_j-\underbrace{C_p\frac{\mu}{Pr}\frac{\partial{\widehat{T''}}}{\partial{x_j}}}_\text{(6)}-\widetilde{u_i}\widetilde{\tau_{ij}}-\underbrace{\widehat{u_i''\tau_{ij}}}_\text{(7)}-\underbrace{\widetilde{u_i}\widehat{\tau_{ij}''}}_\text{(8)}\right]=0 
%\end{align} 
%\end{subequations}
%%
%where the terms (1) to (8) are all unknown. Luckily terms (2) and (8) are neglected since the stresses from the resolved field tend in general to be much bigger than those of the unresolved field. For flows not reaching the hyper-sonic regime it is also common to neglect terms (5) and (7). Additionally term (6) can also be neglected in cases where 
%%
%\begin{equation}
%   \left|\frac{\partial^2{\tilde{T}}}{\partial{x^2_j}}\right|>>\left|\frac{\partial^2{\widehat{T''}}}{\partial{x^2_j}}\right|, \nonumber 
%\end{equation}
%%
%which is true in all common flows. Finally, terms (1) and (4) can be modeled using an eddy-viscosity model such as
%%
%\begin{subequations}\label{eq:tauTurb} 
%\begin{align}
%   \tau^\text{SGS}_{ij}&\equiv -\widehat{\rho u_i''u_j''}\approx\mu^\text{SGS}\left( \frac{\partial{\widetilde{u_i}}}{\partial{x_j}}+\frac{\partial{\widetilde{u_j}}}{\partial{x_i}}-\frac{2}{3} \frac{\partial{\widetilde{u_k}}}{\partial{x_k}}\delta_{ij}\right)-\frac{2}{3}\hat{\rho}k^\text{SGS}\delta_{ij}, \\
%   \mu^\text{SGS}&=\hat{\rho}C_{s_1}\Delta^2\sqrt{2\widetilde{S_{ij}}\widetilde{S_{ij}}},\\
%   k^\text{SGS}&=C_{s_2}\Delta^2\widetilde{S_{ij}}\widetilde{S_{ij}}, 
%\end{align} 
%\end{subequations}
%%
%where $\Delta=\sqrt[3]{\Delta x\Delta y\Delta z}$ is the cubic root of the  cell volume, $C_{s_1}$ and $C_{s_2}$ are model constants usually set to $0.012$ and $0.0066$ respectively, and $\widetilde{S_{ij}}$ is the resolved strain rate tensor. To close the set of equations, term (3) is modelled using a gradient approximation as
%%
%\begin{equation}
%   q^\text{SGS}_j\equiv C_p\widehat{\rho u_j''T}\approx -C_p\frac{\mu^\text{SGS}}{Pr_t} \frac{\partial{\tilde{T}}}{\partial{x_j}}, 
%\end{equation}
%%
%which leads to the simplified form of 
%%
%\begin{subequations}\label{eq:LESGovEq} 
%\begin{align}
%   &\frac{\partial{\hat{\rho}}}{\partial{t}}+\frac{\partial}{\partial{x_i}}[\hat{\rho}\widetilde{x_i}]=0, \\
%   &\frac{\partial}{\partial{t}}(\hat{\rho}\widetilde{u_i})+\frac{\partial}{\partial{x_j}}\left[\hat{\rho}\widetilde{u_i}\widetilde{u_j}+\hat{p}\delta_{ij}-\tau^\text{TOT}_{ji}\right]=0, \\
%   &\frac{\partial}{\partial{t}}(\hat{\rho}\tilde{e})+\frac{\partial}{\partial{x_j}}\left[\hat{\rho}\widetilde{u_j}\tilde{e}+\widetilde{u_j}\hat{p}+q^\text{TOT}_j-\widetilde{u_i}\tau^\text{TOT}_{ij}\right]=0, \\
%   &\tau^\text{TOT}_{ij}=\widetilde{\tau_{ji}}+\tau^\text{SGS}_{ji}, \\
%   &q^\text{TOT}_{j}=q_j+q^\text{SGS}_j, 
%\end{align} 
%\end{subequations}
%%
%with all terms depending on the resolved quantities.
%
%It is clear now how the ILES methodology is less computationally expensive than traditional LES since all Sub-Grid Scales (SGS) are implicitly modelled by the numeric schemes.
%
%=====================SUBSECTION - Generalised Navier-Stokes equations=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Generalised Navier-Stokes equations}\label{sub:genNS}

The technique used to solve this set of equations is based on Finite Difference (FD) methods, which will be described in detail in the following sections. However, the use of FD is more convenient for domains that have been discretised in Cartesian grids with homogeneous grid spacing which allows the system of PDEs to be solved as a system of algebraic equations. Unfortunately, this is not possible in most of the problems which involve complex geometries, and that is the case here. In this case, it is usual to work in what are commonly known as Generalized coordinates $\xi,\eta,\zeta$. In this coordinate system, where the spacing is constant \ref{eq:govEq} is expressed as 
%
\begin{equation}\label{eq:genGovEq}
   \frac{\partial\bm{\hat{Q}}}{\partial t}+\frac{\partial\bm{\hat{E}}}{\partial\xi}+\frac{\partial\bm{\hat{F}}}{\partial\eta}+\frac{\partial\bm{\hat{G}}}{\partial\zeta}= \bm{\hat S}_v=
   \frac{M_\infty}{Re_\infty}\left(\frac{\partial\bm{\hat{E}}_v}{\partial\xi}+\frac{\partial\bm{\hat{F}}_v}{\partial\eta}+\frac{\partial\bm{\hat{G}}_v}{\partial\zeta}\right), 
\end{equation}
%
where the variables in the original coordinate system are related to the new ones by the Jacobian of the transformation $J$ as 
%
\begin{equation}\label{eq:genSource}
	\bm{\hat{Q}}=\bm{Q}/J, 	\nonumber 
\end{equation}
%
\begin{equation}\label{eq:jacobian}
	J=1/[x_\xi(y_\eta z_\zeta-y_\zeta z_\eta)+x_\eta(y_\zeta z_\xi-y_\xi z_\zeta)+x_\zeta(y_\xi z_\eta-y_\eta z_\xi)] \nonumber \end{equation} \begin{equation} \label{eq:genVectors}
	\begin{matrix}
	\bm{\hat{E}}=(1/J)(\xi_x\bm{E}+\xi_y\bm{F}+\xi_z\bm{G}),	&	&\bm{\hat{E}}_v=(1/J)(\xi_x\bm{E}_v+\xi_y\bm{F}_v+\xi_z\bm{G}_v),	\\ 
	\bm{\hat{F}}=(1/J)(\eta_x\bm{E}+\eta_y\bm{F}+\eta_z\bm{G}),	&	&\bm{\hat{F}}_v=(1/J)(\eta_x\bm{E}_v+\eta_y\bm{F}_v+\eta_z\bm{G}_v),	\\
	\bm{\hat{G}}=(1/J)(\zeta_x\bm{E}+\zeta_y\bm{F}+\zeta_z\bm{G}),	&	&\bm{\hat{G}}_v=(1/J)(\zeta_x\bm{E}_v+\zeta_y\bm{F}_v+\zeta_z\bm{G}_v).	\\
	\end{matrix}\nonumber
\end{equation} 
%
\begin{equation}\label{eq:metrics}
	\begin{pmatrix}
		\xi_x	&\xi_y	&\xi_z \\
		\eta_x 	&\eta_y	&\eta_z \\
		\zeta_x	&\zeta_y&\zeta_z 
	\end{pmatrix}
	=J
	\begin{pmatrix}
		y_\eta z_\zeta-y_\zeta z_\eta	&	z_\eta x_\zeta-z_\zeta x_\eta	&	x_\eta y_\zeta-x_\zeta y_\eta \\
		y_\zeta z_\xi-y_\xi z_\zeta	&	z_\zeta x_\xi-z_\xi x_\zeta	&	x_\zeta y_\xi-x_\xi y_\zeta \\
		y_\xi z_\xi-y_\zeta z_\xi	&	z_\zeta x_\xi-z_\zeta x_\xi	&	x_\xi y_\eta-x_\eta y_\xi 
	\end{pmatrix}\nonumber 
\end{equation}
%
where $\xi_x$ represents the derivative of $\xi$ with respect to $x$. These terms are known as grid metrics and their inverse, i.e. $x_\xi$, the inverse grid metrics.

Now that the domain is discretised in equally spaced grid points in all three directions the flux and source terms can be differentiated using FD schemes. In this report, the spatial differentiation technique used is the one proposed by \citet{Kim2007} described in the next section. The following sections will describe the schemes used for both the schemes used for differentiation of the interior grid points, i.e. nodes in the range $3\geq i \geq N-3$ where N represents the maximum number of points in one direction, and the ones used for the near boundary nodes, i.e. nodes 0,1,2 and N,N-1, and N-2.
