%**SECTION **%
\section{Code Parallelisation} 
\label{sec:parallel}
Nowadays High-Performance Computing clusters have become very popular among both industry and academia. Parallel computing is the key tool here which enables to have big jumps in computational efficiency by either sharing tasks or data between several computers. The in-house Fortran code has been parallelised using Message Passage Interface (MPI) libraries. MPI is a language-independent protocol by which the system will copy the program on to each node's processor where it will be executed independently. Unlike OMP, MPI encourages memory locality. This means that each processor will have its own private copy of each variable. There is no shared memory space, which means that if a variable allocated on some particular process is desired to be known by any other processor a communication between processors is needed. 

As everything in life, this has an upside and a downside. On one hand, memory locality removes any chance of two processors trying to write to the same memory location at the same time, i.e. race conditions. On the other hand, as mentioned above, communications between processors become capital in MPI. When two or more processors need to exchange information they need to `speak' through the interconnect. Communications are a synonym of overhead, and thus it is desirable to avoid them as much as possible. 

Additionally, some would think that a code that runs in parallel among multiple threads will perform much faster than the serial code. This is only true if some considerations are taken into account.  First of all, the code has to be designed in such a way that all the threads are doing useful work during execution time. No matter how many processors a program uses if they just sit there waiting for other processors to finish their individual tasks. A good analogy is viewing the team of processors as a cycling team on a time trial race. In this scenario, the time of the team is determined by the slowest member of the team. So a good way to reduce the time is to distribute the effort as equal as possible between all the teammates so they all finish the race in good shape. Following this analogy, if we overload a particular thread/cyclist it will take more time to finish and then the overall performance of the team will decrease. This is commonly known as load balancing, and a good parallel programmer should distribute the effort evenly between all threads.

A common strategy when dealing with grid-based problems, as it is the case here, is to split the domain as evenly as possible among the available processors. In a perfect world this should solve the problem supposing that once the data has been distributed among the processors, the problem can be then solved independently at every processor. However, in the real world if the grid is just split and distributed among the processors the information is confined inside every subdomain without any communication between subdomains. In $\S$~\ref{sec:BoundaryConditions} it has been shown that waves carrying information should be able to cross any internal (artificial) boundaries so information is convected from inlet to outlet and vice versa.

A possible solution that enables information to be exchanged between adjacent subdomains is the use of \emph{halo points}. These points are points at which all communication between subdomains happens. \citet{Kim2012} parallelised a system of compact differencing and filtering of~\citet{Kim2007,Kim2010} using three halo points for the differencing scheme and a predictor-corrector method for the filtering scheme. A similar one-sided compact finite difference schemes of~\citet{Kim2007} were used, however, the extrapolating function parameters were determined from the interior as well as the halo points. As in~\citet{Kim2007} the control variables were used to optimise the scheme in the wavenumber domain. Although the work of \citeauthor*{Kim2012} meant a great advantage with respect to previous work done by other researchers using overlapped subdomains~\citep{Sengupta2007}, still, parallel artefacts arose from the subdomain boundaries for vortex-driven flows.

In a more recent piece of work, \citet{Kim2013} presents a new set of both differencing and filtering \emph{subdomain boundary} (SB) schemes. The starting point was again the Optimised High-Order Compact finite difference schemes and filters of~\citet{Kim2007,Kim2010} (see (\ref{eq:pade}, \ref{eq:boundary}, \ref{eq:intFilter}, and \ref{eq:boundFilter})). The system of equations formed by the difference and the filtering schemes expressed in matrix-vector form is:
%
\begin{subequations}
\label{eq:matrix-vector}
\begin{align}
\label{eq:Dmatrix-vector}
	\text{Differencing System:}&\qquad\bm{P}\bar{\bm{f}}'=\frac{1}{\Delta x}\bm{Q}\bm{f}, \\
	\text{Filtering System:}&\qquad\bm{P}_F\hat{\Delta}\bm{f}=\bm{Q}_F\bm{f},		
\end{align}
\end{subequations}
%
where $\bm{f}$, $\bar{\bm{f}}'$, and $\hat\Delta\bm{f}$ are the objective function, numerical derivative, and filter's contribution nodal values respectively. The matrices $\bm{P}$, $\bm{P}_F$, $\bm{Q}$, and $\bm{Q}_F$ are the coefficient matrices for differencing and filtering respectively with the subscript F standing for filter matrices. Because the process used in~\citet{Kim2013}  is quite similar for the parallelisation of both the filtering and the differencing system, just the latter is shown here as an illustrative example.

\begin{equation}
\label{eq:pMatrix}
	\bm{P}=\begin{pmatrix}
		1 	    & \gamma_{01} & \gamma_{02} & 0 	      & \cdots      & \cdots      & \cdots      & \cdots      & 0 \\ 
		\gamma_{10} & 1           & \gamma_{12} & \gamma_{13} & 0           & \cdots      & \cdots      & \cdots      & 0 \\ 
		\gamma_{20} & \gamma_{21} & 1           & \gamma_{23} & \gamma_{24} & 0           & \cdots      & \cdots      & 0 \\ 
		0           & \beta       & \alpha      & 1           & \alpha      & \beta       & 0           & \cdots      & 0 \\ 
		\vdots      & \ddots      & \ddots      & \ddots      & \ddots      & \ddots      & \ddots      & \ddots      & \vdots \\ 
		0           & \cdots      & 0           & \beta       & \alpha      & 1           & \alpha      & \beta       & 0 \\ 
		0           & \cdots      & \cdots      & 0           & \gamma_{24} & \gamma_{23} & 1           & \gamma_{21} & \gamma_{20} \\ 
		0           & \cdots      & \cdots      & \cdots      & 0           & \gamma_{13} & \gamma_{12} & 1           & \gamma_{10} \\ 
 	        0           & \cdots      & \cdots      & \cdots      & \cdots      & 0           & \gamma_{02} & \gamma_{01} & 1 
	\end{pmatrix} 
\end{equation}
%
\begin{equation}
\label{eq:qMatrix}
\resizebox{0.8\textwidth}{!}{$
	\bm{Q}=\begin{pmatrix}
		b_{00}	&b_{01}	&b_{02}	&b_{03}	&b_{04}	&b_{05}	&b_{06}	&0	&\cdots	&\cdots	&0	\\
		b_{10}	&b_{11}	&b_{12}	&b_{13}	&b_{14}	&b_{15}	&b_{16}	&0	&\cdots	&\cdots	&0	\\
		b_{20}	&b_{21}	&b_{22}	&b_{23}	&b_{24}	&b_{25}	&b_{26}	&0	&\cdots	&\cdots	&0	\\
		-a_3	&-a_2	&-a_1	&0	&a_1	&a_2	&a_3	&0	&\cdots	&\cdots	&0	\\
		0	&-a_3	&-a_2	&-a_1	&0	&a_1	&a_2	&a_3	&0	&\cdots	&0	\\
		\vdots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\vdots	\\
		0	&\cdots	&0	&-a_3	&-a_2	&-a_1	&0	&a_1	&a_2	&a_3	&0	\\
		0	&\cdots	&\cdots	&0	&-a_3	&-a_2	&-a_1	&0	&a_1	&a_2	&a_3	\\
		0	&\cdots	&\cdots	&0	&-b_{26}&-b_{25}&-b_{24}&-b_{23}&-b_{22}&-b_{21}&-b_{20}\\
		0	&\cdots	&\cdots	&0	&-b_{16}&-b_{15}&-b_{14}&-b_{13}&-b_{12}&-b_{11}&-b_{10}\\
		0	&\cdots	&\cdots	&0	&-b_{06}&-b_{05}&-b_{04}&-b_{03}&-b_{02}&-b_{01}&-b_{00}
	\end{pmatrix}
$}
\end{equation}
%
\begin{equation}
\label{eq:rMatrix}
\resizebox{0.9\textwidth}{!}{$
\begin{aligned}
\bm{R}&=\left(\begin{array}{c:c}
\bm{R}_A	&\bm{0}	\\
\hdashline		
\bm{0}		&\bm{R}_B
\end{array}\right)= \\
&=\left(\begin{array}{c*{7}{c}:*{7}{c} c}
1		&\gamma_{01}	&\gamma_{02}	&0		&\cdots		&\cdots	&\cdots	&\cdots	&\cdots	&\cdots	&\cdots	&\cdots		&\cdots		&\cdots	&\cdots		&0	\\
\gamma_{10}	&1		&\gamma_{12}	&\gamma_{13}	&0		&\cdots	&\cdots	&\cdots	&\cdots	&\cdots	&\cdots	&\cdots		&\cdots		&\cdots	&\cdots		&0	\\
\gamma_{20}	&\gamma{21}	&1		&\gamma_{23}	&\gamma_{24}	&0	&\cdots	&\cdots	&\cdots	&\cdots	&\cdots	&\cdots		&\cdots		&\cdots	&\cdots		&0	\\
0		&\beta		&\alpha		&1		&\alpha		&\beta	&0	&\cdots	&\cdots	&\cdots	&\cdots	&\cdots		&\cdots		&\cdots	&\cdots		&0	\\
\vdots		&\ddots		&\ddots		&\ddots		&\ddots		&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots		&\ddots		&\ddots	&\ddots		&\vdots	\\
0		&\cdots		&0		&\beta		&\alpha		&1	&\alpha	&\beta	&0	&\cdots	&\cdots	&\cdots		&\cdots		&\cdots	&\cdots		&0	\\
0		&\cdots		&\cdots		&0		&\beta		&\alpha	&1	&\alpha	&0	&0	&\cdots	&\cdots		&\cdots		&\cdots	&\cdots		&0	\\
0		&\cdots		&\cdots		&\cdots		&0		&\beta	&\alpha	&1	&0	&0	&0	&\cdots		&\cdots		&\cdots	&\cdots		&0	\\
\hdashline
0		&\cdots		&\cdots		&\cdots		&\cdots		&0	&0	&0	&1	&\alpha	&\beta	&0		&\cdots		&\cdots	&\cdots		&0	\\
0		&\cdots		&\cdots		&\cdots		&\cdots		&\cdots	&0	&0	&\alpha	&1	&\alpha	&\beta		&0		&\cdots	&\cdots		&0	\\
0		&\cdots		&\cdots		&\cdots		&\cdots		&\cdots	&\cdots	&0	&\beta	&\alpha	&1	&\alpha		&\beta		&0	&\cdots		&0	\\
\vdots		&\ddots		&\ddots		&\ddots		&\ddots		&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots		&\ddots		&\ddots	&\ddots		&\vdots	\\
0		&\cdots		&\cdots		&\cdots		&\cdots		&\cdots	&\cdots	&\cdots	&\cdots	&0	&\beta	&\alpha		&1		&\alpha	&\beta		&0	\\
0		&\cdots		&\cdots		&\cdots		&\cdots		&\cdots	&\cdots	&\cdots	&\cdots	&\cdots	&0	&\gamma_{24}	&\gamma_{23}	&1	&\gamma_{21}	&\gamma_{20}	\\
0		&\cdots		&\cdots		&\cdots		&\cdots		&\cdots	&\cdots	&\cdots	&\cdots	&\cdots	&\cdots	&0		&\gamma_{13}	&\gamma_{12}&1		&\gamma_{10}	\\
0		&\cdots		&\cdots		&\cdots		&\cdots		&\cdots	&\cdots	&\cdots	&\cdots	&\cdots	&\cdots	&\cdots		&0	&\gamma_{02}&\gamma_{01}	&1	\\
\end{array}\right)
\end{aligned}
$}
\end{equation}

For explanatory purposes, let us consider a computational domain which is split into two smaller subdomains at its centre so each subdomain contains $M=(N-1)/2$ elements where $N+1$ is the total number of elements. The left subdomain is referred as Subdomain A and the right as Subdomain B. To be able to perform independent computations in each subdomain the pentadiagonal matrix $\bm{P}$ needs to substitute some of the off-diagonal elements by zero so it can be independently inverted for each subdomain. The resulting matrix is $\bm{R}$ (see (\ref{eq:rMatrix})). If (\ref{eq:Dmatrix-vector}) is pre-multiplied by $\bm{RP}^{-1}$ the following relations are obtained:
%
\begin{equation}
\label{eq:transMatrix-vector}
	\bm{R}\bar{\bm{f}}'=\frac{1}{\Delta x}\bm{RP}^{-1}\bm{Qf}=\frac{1}{\Delta x}\bm{Sf},
\end{equation}
 %
 where $\bm{S}=\bm{RP}^{-1}\bm{Q}$ is a matrix that is at the same time formed by four submatrices $\bm{S}_A$, $\bm{T}_A$, $\bm{S}_B$, and $\bm{T}_B$ (subscript indicates different subdomains) as:
%
\begin{equation}
\label{eq:sMatrix}
	\bm{S}=\left(\begin{array}{c:c}
	\bm{S}_A &\bm{T}_B \\
	\hdashline
	\bm{T}_A &\bm{S}_B
	\end{array}\right),
\end{equation}
%
which leads to the following quasi-disjoint system of equations:
%
\begin{subequations}
\label{eq:quasiD}
\begin{align}
	\bm{R}_A\bar{\bm{f}}'_A&=\frac{1}{\Delta x}(\bm{S}_A\bm{f}_A+\bm{T}_B\bm{f}_B), \\
	\bm{R}_B\bar{\bm{f}}'_B&=\frac{1}{\Delta x}(\bm{S}_B\bm{f}_B+\bm{T}_A\bm{f}_A).
\end{align}
\end{subequations}

The vectors $\bm{f}_A$, $\bm{f}_B$, $\bar{\bm{f}}'_A$, and $\bar{\bm{f}}'_B$ represent the subdomain-wise associated elements of the original vectors $\bm{f}$ and $\bar{\bm{f}}'$ and the matrices $\bm{S}_A$, $\bm{T}_A$, $\bm{S}_B$, and $\bm{T}_B$ are as shown in (\ref{eq:sandT})
%
\begin{equation}
\label{eq:sandT}
\resizebox{0.9\textwidth}{!}{$
\bm{S}_A=\begin{pmatrix}
b_{00}	&b_{01}	&b_{02}	&b_{03}	&b_{04}	&b_{05}	&b_{06}	&0	&\cdots	&\cdots	&0	\\
b_{10}	&b_{11}	&b_{12}	&b_{13}	&b_{14}	&b_{15}	&b_{16}	&0	&\cdots	&\cdots	&0	\\
b_{20}	&b_{21}	&b_{22}	&b_{23}	&b_{24}	&b_{25}	&b_{26}	&0	&\cdots	&\cdots	&0	\\
-a_3	&-a_2	&-a_1	&0	&a_1	&a_2	&a_3	&0	&\cdots	&\cdots	&0	\\
0	&-a_3	&-a_2	&-a_1	&0	&a_1	&a_2	&a_3	&0	&\cdots	&0	\\
\vdots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\ddots	&\vdots	\\
0	&\cdots	&0	&-a_3	&-a_2	&-a_1	&0	&a_1	&a_2	&a_3	&0	\\
0	&\cdots	&\cdots	&0	&-a_3	&-a_2	&-a_1	&0	&a_1	&a_2	&a_3	\\
0	&\cdots	&\cdots	&\cdots	&0	&-a_3	&-a_2	&-a_1	&0	&a_1	&a_1	\\
-c_{1M}	&\cdots	&\cdots	&\cdots	&-c_{16}&-c_{15}&-c_{14}&-c_{13}&-c_{12}&-c_{11}&-c_{10}\\
-c_{0M}	&\cdots	&\cdots	&\cdots	&-c_{16}&-c_{05}&-c_{04}&-c_{03}&-c_{02}&-c_{01}&-c_{00}
\end{pmatrix}
$}
\end{equation}
%
\begin{equation}
\label{eq:sandT2}
\bm{T}_B=\begin{pmatrix}
0	&\cdots	&\cdots	&\cdots	&\cdots	&0	\\	
\vdots	&\ddots	&\ddots	&\ddots	&\ddots	&\vdots	\\	
a_3	&0	&\cdots	&\cdots	&\cdots	&0	\\	
-c^*_{10}	&-c^*_{11}	&-c^*_{12}	&-c^*_{13}	&\cdots	&-c^*_{1M}	\\	
-c^*_{00}	&-c^*_{01}	&-c^*_{02}	&-c^*_{03}	&\cdots	&-c^*_{0M}	\\	
\end{pmatrix}
\end{equation}
%
\begin{equation}
\label{eq:sandT3}
\resizebox{0.8\textwidth}{!}{$
\left.\begin{array}{c}
	(\bm{S}_B)_{l,m}=-(\bm{S}_A)_{M-l,M-m} \\
	(\bm{T}_A)_{l,m}=-(\bm{T}_B)_{M-l,M-m}
\end{array}\right\}
\quad \text{for}\left\{\begin{array}{c}
	l=0,\cdots,M \\
	m=0,\cdots,M	
\end{array}\right\}
\quad \text{with } M=\frac{N-1}{2}
$}
\end{equation}

In (\ref{eq:sandT}) one can observe that only two rows at the bottom representing the nearest two points to the subdomain interface have been modified with respect to the original formulation, i.e. the rows containing $c_{0m}$, $c_{1m}$, $c^*_{0m}$ and $c^*_{1m}$, where the star represents values obtained from the adjacent subdomain. Based on the above matrices, the compact differencing scheme at the subdomain boundaries becomes:
%
\begin{equation}
\label{eq:sbScheme}
\resizebox{0.95\textwidth}{!}{$
	\begin{aligned}
	&\bar{f}'_i+\alpha\bar{f}'_{i\pm{1}}+\beta\bar{f}'_{i\pm{2}}=\pm\frac{1}{\Delta x}\left(\sum_{m=0}^{M} c_{0m}f_{i\pm{m}}+h_0\right),\\
	&\alpha\bar{f}'_i+\bar{f}'_{i\pm{1}}+\alpha\bar{f}'_{i\pm{2}}+\beta\bar{f}'_{i\pm{3}}=\pm\frac{1}{\Delta x}\left(\sum_{m=0}^{M} c_{1m}f_{i\pm{m}}+h_1\right),\\
	&\beta\bar{f}'_i+\alpha\bar{f}'_{i\pm{1}}+\bar{f}'_{i\pm{2}}+\alpha\bar{f}'_{i\pm{3}}+\beta\bar{f}'_{i\pm{4}}=\pm\frac{1}{\Delta x}\left[\sum_{m=1}^{2} a_m(f_{i\pm 2\pm m}-f_{i\pm 2\pm m})+a_3(f_{i\pm 5}-h_2)\right],\\
	\end{aligned}
$}
\end{equation}
%
where $i=0$ or $i=N_S$ for each subdomain defined by $i\in[0,N_S\ge M]$. The terms $h_0$, $h_1$ and $h_2$ are halo terms which are calculated with information owned by a single subdomain but which are used in the adjacent domains. The values of them are given by:
%
\begin{equation}
\label{eq:haloTerms}
\left.\begin{aligned}
&(h_0,h_1)=\sum_{m=0}^{M}(c_{0m}^*,c_{1m}^*)f^*_{l\pm m} \\
&h_2=f^*_l
\end{aligned}\right\}\quad \text{with } l=\left\{\begin{aligned}
	N_S^*\\
	0	
\end{aligned}\right. \quad \text{for } i=\left\{\begin{aligned}
	0\\
	N_S
\end{aligned}\right.
\end{equation}

The differencing scheme has been used here for explaining Kim's~\citep{Kim2013} method, nevertheless, all of the beforehand mention also applies to the derivation of the SB compact filtering scheme with minor differences (see~\citet{Kim2013} for further information). 

Up to here (\ref{eq:quasiD}) is just the result of a lineal-algebraic transformation and hence the results obtained are the same that those of a serial implementation. However, the stencil size could become extremely large if the entire rows of "c" coefficients are used, i.e. $M=N_S$. At the same time if $M$ is reduced too much the SB scheme largely deviates from its serial version and spurious fluctuations appear at the subdomain interfaces. As the ideal case is that when the parallel scheme leads to the same results as the serial scheme, \citet{Kim2013} transforms the new SB scheme to the wavenumber domain form where it compares its resolution with the interior scheme by computing a $L_2$-norm-based deviation from the original scheme. A plot of the deviation versus the stencil size $M$~\citep[Fig.~6]{Kim2013} shows that if the deviation is wanted to be always below 3\% the size of the stencil has to be at least $M=11$ for the differencing scheme and $M_F=8$ for the filtering scheme.

%**SUBSECTION **%
\subsection{Parallel scalability}\label{sub:parallelSca}

In this section, the scalability of the code is presented. The study was performed using University of Southampton's cluster \emph{Iridis4}. The problem size was kept constant in all cases, and computational time was measured for a different number of processors used. The time measured does not include I/O actions. Because of the nature of the code, at least 12 processors need to be used, since that is the minimum number of blocks used when simulating a single element aerofoil. The problem size was fixed to 30.72 millions of points and the maximum number of cores used was 384. In order to measure the scalability and the efficiency of the code, both Speed-Up $S_u$ and Parallel efficiency $P_{eff}$ were measured. The definition of these is 
%
\begin{subequations}
\label{eq:parallelParameters}
\begin{align}
	S_u&=\frac{\left[T_w/T_s\right]_{12}}{\left[T_w/T_s\right]_{n_p}}\\
	P_{eff}&=\frac{S_u}{n_p/12}
\end{align}
\end{subequations}
%
where $T_s$ is the simulation time, $T_w$ is the wall time, and $n_p$ is the number of processors used. The results obtained can be seen in Table~\ref{tab:scalability}.

Figure~\ref{fig:scalability} shows that the code scales well, being even better than the linear scalability, i.e. doubling the number of cores halves the time needed. This is due to the size of the problem being too big for the case of 12 processors, i.e. the memory needed is bigger than the cache memory, which is the fastest memory, and hence there is more likelihood of \emph{cache missing}. It is worth noticing that when using 384 processors nearly just an hour is needed of wall-time is needed per non-dimensional time unit of simulation time.
%
\begin{figure}\centering
   \tikzsetnextfilename{scalability}
   \input{\folder/Methodology/scalability}
   \caption{Plots of (\emph{a}) speed-up and (\emph{b}) parallel efficiency of the code using the local cluster \emph{Iridis4} at the University of Southampton.}
   \label{fig:scalability}
\end{figure}
%
\begin{table}[H]
\centering
\caption{Scalability Test Data}
\resizebox{1\textwidth}{!}{
\begin{tabular}{c c c c c c c}
$n_p$	&$T_w$[s]&$T_s$[Time Unit]&$T_w/T_s$[h/Time Unit]&$S_u$	&Linear $S_u$	&$P_{eff}$\\	
\hline
12	&$1.73\times 10^{4}$	&0.1	&48.089	&1.000	&1	&1.000\\
24	&$4.79\times 10^{3}$	&0.1	&13.312	&3.613	&2	&1.806\\
48	&$2.77\times 10^{3}$	&0.1	&7.700	&6.245	&4	&1.561\\
96	&$1.45\times 10^{3}$	&0.1	&4.016	&11.973	&8	&1.497\\
192	&$7.40\times 10^{2}$	&0.1	&2.055	&23.405	&16	&1.463\\
384	&$3.91\times 10^{2}$	&0.1	&1.085	&44.301	&32	&1.384
\end{tabular}}
\label{tab:scalability}
\end{table}
