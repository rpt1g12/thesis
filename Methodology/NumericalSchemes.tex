%**SECTION **%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Numerical Schemes}\label{sec:NumericalSchemes}

Because every problem in Fluid Mechanics is governed by a set of non-linear partial differential equations, i.e. the Navier-Stokes Equations, every problem tends to develop a very large range of length and time scales. So if maximum accuracy is requested, one has to fully resolve the entire range of scales, from the biggest to the smallest. However, there are many different ways of approaching a problem which may give different results in terms of accuracy where the most accurate calculations are Direct Numerical Simulations (DNS) followed by the Large Eddy Simulations (LES) in second place. For the former, the whole range of scales is computed whereas for the latter almost all the entire range is computed (resolved) and the effect of the rest, i.e. the effect of the smallest scales is modelled. However, there are other methods that, although less accurate, do not require such big computational power as the ones just mentioned. An example of this could be the (U)RANS or (Unsteady) Reynolds Averaged Navier-Stokes models. 

Of the approaches mentioned beforehand the last is the preferred by the industry because can offer "quick" results up to some degree of accuracy whereas in research and academy more accurate methods are required. Other fields such as aeroacoustics also demand a high level of accuracy, especially at the highest end of the wavelength spectrum. For such reasons, Spectral Methods have been traditionally used since they can faithfully represent the fluid dynamic variables and its derivatives among the whole range of scales. Unfortunately, the use of these methods strongly restricts the complexity of the geometries to study and the boundary conditions used. Other methods can overcome these limitations whilst still retaining the characteristic high-accuracy of the spectral methods up to some point. We focus here hence in one of these alternative methods: Optimised High-Order Compact (OHOC) Finite Difference schemes.

%**SUBSECTION **% 
\FloatBarrier %Force all figures from previous sections to be placed 
\subsection{Interior scheme}\label{sub:intScheme}

The OHOC finite difference schemes developed by \citet{Kim2007} are \nth{4} order accurate numerical schemes optimised to achieve the maximum resolution in the wavenumber domain based on the Pad\'e approximation of a seven-point stencil. The general expression for this is
%
\begin{equation}
\label{eq:pade}
   \beta\bar{f}'_{i-2}+\alpha\bar{f}'_{i-1} +\bar{f}'_i+\alpha\bar{f}'_{i+1} +\beta\bar{f}'_{i+2}=\frac{1}{\Delta{x}}\sum_{m=1}^{3} a_m(f_{i+m}-f_{i-m}),
\end{equation}
%
where $f_i$ and $\bar{f}'_{i}$ represent the function to differentiate and its numerical representation of its spacial derivative with respect to $x$ at the point $x_i$ in the interval $\Delta{x}=x_{i+1}-x_i$. Matching coefficients with its Taylor expansion up to \nth{10} order leads to the following conditions: 
%
\begin{subequations} \label{eq:taylor} 
\begin{align}
   \text{(\nth{2} order)} \qquad &1+2(\alpha+\beta)=2 \sum_{m=1}^{3} ma_m    \\
   \text{(\nth{4} order)} \qquad &3(\alpha+4\beta)=2\sum_{m=1}^{3} m^3a_m    \\
   \text{(\nth{6} order)} \qquad &10(\alpha+16\beta)=2\sum_{m=1}^{3} m^5a_m  \\
   \text{(\nth{8} order)} \qquad &14(\alpha+64\beta)=2\sum_{m=1}^{3} m^7a_m  \\
   \text{(\nth{10} order)}\qquad &18(\alpha+256\beta)=2\sum_{m=1}^{3} m^9a_m 
\end{align} 
\end{subequations}

If the dependent variable $f(x)$ is decomposed into its Fourier coefficients over a periodic domain $[0, L]$ with $\Delta x=L/N$ as
%
\begin{equation}
\label{eq:fourier}
   f(x)=\sum_{k=-N/2}^{k=N/2} \hat{f}_k \exp \left(\frac{2\pi ikx}{L}\right),
\end{equation}
%
then if the scaled wavenumber $\kappa=w\Delta x=2\pi k\Delta x/L=2\pi k/N$ is used \ref{eq:fourier} simplifies to just
\begin{equation}
\label{eq:fourier2}
   f(x)=\sum_{\kappa=-\pi}^{\kappa=\pi} \hat{f}_{\kappa} \exp \left(i\kappa x\right).
\end{equation}

The exact differentiation of \ref{eq:fourier2} has coefficients $\hat{f}'_{\kappa}=i\kappa\hat{f}_{\kappa}$ whereas the use of finite difference approximations gives $(\hat{f}'_{\kappa})_{fd}=i\bar{\kappa}\hat{f}$ where $\bar{\kappa}=\bar{w}\Delta x$ is the (scaled) modified wavenumber. The modified wavenumber is a function of the original wavenumber, i.e. $\bar{\kappa}=\bar{\kappa}(\kappa)$, and can be different for different derivative approximations. For schemes of the form of \ref{eq:pade} the modified wavenumber is given by:
%
\begin{equation}
\label{eq:modwaveno}
   \bar{\kappa}(\kappa)=\frac{2 \sum_{m=1}^{3} a_m \sin(m\kappa)}{1+2\alpha \cos{(\kappa)}+2\beta \cos{(2\kappa)}}
\end{equation}

Different schemes have different coefficients and hence the shape of its modified wavenumber is also different. Depending on how close a scheme's modified wavenumber remains to the exact solution determines the resolution characteristics of the scheme. \citet{Lele1992} presented the \emph{resolving efficiency} as the fraction of the entire wavenumber spectrum that a certain scheme is capable of approximate within a given error range. Thus depending on the formal accuracy imposed to \ref{eq:taylor} different families of schemes with different sets of parameters are obtained, which will offer different characteristics in terms of its resolution when studied in the wavenumber domain. Higher order schemes will have more restrictions and the number of free parameters will reduce, e.g. for a \nth{10} order scheme a complete (closed) set of equations is obtained and therefore there are no free parameters making this scheme unique at that order of accuracy.

An interesting fact arising is that the lower the formal accuracy the most likely is a scheme to have high-resolution characteristics. Because lower order models have more free parameters, more constraints can be added. In \citet{Lele1992} the modified wavenumber is forced to match the exact solution at three points. This gives three extra equations to add to the system formed by \ref{eq:taylor}~and~\ref{eq:pade}. The result arising from this is the so-called \emph{spectral-like resolution schemes}. The resulting scheme although being only \nth{4} order accurate remains closer to the exact solution in the wavenumber domain for a wider range of wave numbers than for example the \nth{10} order pentadiagonal scheme.

It can be seen in figure~\ref{fig:iSchemeComparison}\emph{b} that for different schemes different curves are obtained, where the black straight dash-dotted line corresponds to the exact differentiation. If $\kappa_f$ is defined as the wavenumber related to the shortest well-resolved wave then an error tolerance $\varepsilon$ can be defined as
%
\begin{equation}
\label{eq:errortol}
   \varepsilon = \frac{\bar{\kappa}(\kappa_f)-\kappa_f}{\kappa_f},
\end{equation}
%
and for a fixed value of $\varepsilon$ all the wavenumbers that satisfy \ref{eq:condition} represent well-resolved waves, being $\kappa=\kappa_f(\varepsilon)$ consequently the biggest among them.
\begin{equation}
\label{eq:condition}
   \frac{\bar{\kappa}(\kappa)-\kappa}{\kappa} \leq \varepsilon
\end{equation}

Taking all this into account, the fraction of the spectrum representing poorly-resolved waves is $r(\varepsilon) \equiv 1 - \kappa_f/\pi$ and $e(\varepsilon) \equiv \kappa_f/\pi=1-r(\varepsilon)$ is the \emph{resolving efficiency} of the scheme. The total error made by the approximations can be calculated by the integrated error $E$ defined below.
%
\begin{equation}
\label{eq:werror}
   E= \int_{0}^{r\pi} (\kappa - \bar{\kappa})^2 W(\kappa)\,d\kappa 
\end{equation}

Where $r$ and $W(\kappa)$ are introduced representing the factor determining the range $(0<r<\pi)$ where errors are accounted and the weighting function respectively. The introduction of the weighting function is necessary for two main reasons. In the first place, it makes possible to optimise the schemes to reduce the errors in the high-wavenumber range where most of the errors are made. Secondly, it simply makes \ref{eq:werror} analytically integrable. \citeauthor*{Kim2007} proposes to replace $\bar{\kappa}$ by $(1+\delta)\kappa$ and the use of $W(\kappa)=[\kappa/(r\pi)]^n$ as weighting function which results in the following expression of the integrated error:
%
 \begin{equation}
 \label{eq:newWerror}
 	E = \int_{0}^{r\pi} \left\{(1+\delta)\kappa[1+2\alpha \cos{(\kappa)}+2\beta \cos{(2\kappa)}]-2 \sum_{m=1}^{3} a_m \sin{(mk)} \right\}^2 \left(\frac{\kappa}{r\pi}\right)^n \, d\kappa 
 \end{equation}

Minimising this error equals to maximise the spectral resolution of the schemes. The conditions for minimum values of $E$ where given by \citet{Tam1993} in the form of
%
\begin{equation}
\label{eq:min}
\begin{aligned}
   \frac{\partial E}{\partial a_1}&=0,  &   \frac{\partial E}{\partial a_1}&=0,  &   \frac{\partial E}{\partial a_3}&=0,  &   \frac{\partial E}{\partial \alpha}&=0,  &  \frac{\partial E}{\partial \beta}&=0. 
\end{aligned}
\end{equation}

If the minimum conditions of \ref{eq:min} are combined with the restrictions that make \ref{eq:pade} \nth{4} Order accurate, i.e. \ref{eq:taylor}, a closed system of equations is obtained from where the coefficients $\alpha,\beta,a_1,a_2$ and $a_3$ can be obtained once the optimisation parameters $r,\delta$ and $n$ have been fixed. The values used here are the ones that \citet{Kim2007} suggests and can be checked out from Table~\ref{tab:optCoef} which lead to the scheme coefficients shown in Table~\ref{tab:intCoef}.

\begin{table}[t]\centering 
\caption{Interior Scheme Coefficients}\label{tab:intCoef} 
\resizebox{1\textwidth}{!}{ 
\begin{tabular}{l l l l l} 
\hline 
$\alpha$&$\beta$&$a_1$&$a_2$&$a_3$ \\ 
\hline 
0.5862704032801503&0.09549533555017055&0.6431406736919156&0.2586011023495066&0.007140953479797375 \\ 
\hline 
\end{tabular}} 
\end{table}

\begin{table}[t]\centering 
\caption{Optimisation parameters}\label{tab:optCoef} 
\begin{tabular}{c c c} 
\hline 
$r$&$\delta$&$n$ \\ 
\hline 
0.8505&-0.000233&10\\ 
\hline 
\end{tabular} 
\end{table}
%
\begin{figure}\centering
   \tikzsetnextfilename{schemeComparison}
   \input{\folder/Methodology/iSchemeComparison}
   \caption{Comparison of different numerical schemes resolution capabilities.}
   \label{fig:iSchemeComparison} 
\end{figure}

The resulting resolution characteristics of the scheme are shown in comparison with other compact finite differences in figure~\ref{fig:iSchemeComparison}. One may say that the older scheme presented in~\citet{Kim1996} (green dotted line) is able to approximate the exact solution for a larger range of wavenumbers than that presented in~\citet{Kim2007} and used here (purple dash-dotted line). However, the zoomed view (figure~\ref{fig:iSchemeComparison}\emph{b}) shows that this is done at the expense of larger error tolerance in the optimised region. For the sake of comparison, the classical central scheme is also represented. It is seen that the classical scheme is only able to faithfully represent the exact solution for the first 20\% of the spectral range. The coefficients of the scheme used for this study can be found in Table~\ref{tab:intCoef}. 

%**SECTION **% 
\FloatBarrier %Force all figures from previous sections to be placed 
\subsection{Boundary scheme}\label{sub:boundScheme}

Because the interior scheme needs information on both sides of the point of interest to compute its derivative it is not suitable for problems with non-periodic boundary conditions. In the case of the problem tackled here there exist a need for a scheme that allows the implementation of non-periodic boundaries such as walls in the aerofoil surface. In such cases, the scheme is of necessity non-central (or one-sided). In this study, the boundary scheme developed by \citet{Kim2007} which unlike its previous version~\citep{Kim1997} includes extrapolation techniques to obtain the information outside the computational domain boundaries is used. For this purpose, the following spline function is used:
%
\begin{subequations}\label{eq:spline} 
\begin{align} 
   g_i(x^*)&=f_i+\sum_{m=1}^{N_A} p_m(x^*)^m+\sum_{m=1}^{N_B} [q_m\cos(\phi_m{x^*})+r_m\sin(\phi_m{x^*})], \\ 
   g'_i(x^*)&=\frac{dg_i(x^*)}{dx} \nonumber \\
   & =\frac{1}{\Delta{x}}\left\{\sum_{m=1}^{N_A} mp_m(x^*)^{m-1}-\sum_{m=1}^{N_B} \phi_m[q_m\sin(\phi_m{x^*})-r_m\cos(\phi_m{x^*})]\right\}, 
\end{align} 
\end{subequations}
%
where $x^*=(x-x_i)/\Delta{x}$ is the non-dimensional coordinate from the point of interest, $N_A$, and $N_B$ determine the order of the polynomial and trigonometric series, the coefficients $p_m,q_m$ and $r_m$ are used to match the interior values for both the objective function and its derivative, and $\phi_m$ are control variables used to optimise the resolution characteristics of the boundary scheme when the exterior points of \ref{eq:pade} are substituted by extrapolated values.

Matching the interior profile of the objective function one may obtain the values of the coefficients $p_n,q_m$, and $r_m$ where $n=[1,\cdots,N_A]$ and $m=[1,\cdots,N_B]$, expressed in terms of the interior values of the objective function. For a \nth{4} order approximation the requirements are that $(N_A,N_B)$ equal $(4,3),(4,4)$ and $(4,4)$ for the nearest three boundary points respectively. Rearranging terms the interior scheme can be recast in a one-sided formulation including just interior values of the function and the unknown coefficients $\gamma_{i,j}$ and $b_{0m},b_{1m}$ and $b_{2m}$ where $i=0,1,2$, $j=0,1,2$, and $m=[1,\cdots,6]$ with $i\neq{j}$ and $m\neq{i}$ These unknown coefficients are all non-linear functions of the control variables, i.e. $\phi_1,\cdots,\phi_{N_B}$. 

\begin{subequations}\label{eq:boundary} 
\begin{align} 
   i&=0:  &  &\bar{f}'_0+\gamma_{01}\bar{f}'_1+\gamma_{02}\bar{f}'_2 = \frac{1}{\Delta x} \sum_{m=0,\neq 0}^{6} b_{0m}(f_m-f_0) \\ 
   i&=1:  &  &\gamma_{10}\bar{f}'_0+\bar{f}'_1+\gamma_{12}\bar{f}'_2+\gamma_{13}\bar{f}'_3 = \frac{1}{\Delta x} \sum_{m=0,\neq 1}^{6} b_{1m}(f_m-f_1) \\ 
   i&=2:  &  &\gamma_{20}\bar{f}'_0+\gamma_{21}\bar{f}'_1+\bar{f}'_2+\gamma_{23}\bar{f}'_3+\gamma_{24}\bar{f}'_4 =  \frac{1}{\Delta x} \sum_{m=0,\neq 2}^{6} b_{2m}(f_m-f_2) 
\end{align} 
\end{subequations}

\citet{Kim2007} relied on the Fourier analysis for the definition of these parameters in order to obtain maximum resolution. Applying Fourier transforms to \ref{eq:boundary} and grouping terms leads to the following expression of the modified wave number 
%
\begin{equation} \label{eq:boundaryModWaveNo}
   \bar{\kappa}(\kappa)=\frac{A(\kappa)D(\kappa)-B(\kappa)C(\kappa)}{[A(\kappa)]^2+[B(\kappa)]^2}-i\frac{A(\kappa)C(\kappa)+B(\kappa)D(\kappa)}{[A(\kappa)]^2+[B(\kappa)]^2}, 
\end{equation}
%
where the expressions of the functions $A,B,C$, and $D$ can be found in~\citet[p.~1003]{Kim2007}. The real part of \ref{eq:boundaryModWaveNo} is associated with the dispersion (phase) errors whereas its imaginary part is associated with dissipation (amplitude) errors. Therefore, the interior central scheme is non-dissipative, whereas the boundary non-central schemes are both dispersive and dissipative. The real part of the exact solution is given by $\operatorname{Re}(\bar{\kappa})=\kappa$ and its imaginary part is given by $\operatorname{Im}(\bar{\kappa})=0$ since the exact solution has no dissipation. Both errors can be measured as 
%
\begin{subequations}\label{eq:errors} 
\begin{align}
   \varepsilon_R(\kappa)&=\left|\frac{\operatorname{Re}[\bar{\kappa}(\kappa)]-\kappa}{\kappa}\right|, \\
   \varepsilon_I(\kappa)&=\left|\frac{\operatorname{Im}[\bar{\kappa}(\kappa)]}{\kappa}\right|. 
\end{align} 
\end{subequations}

It can be then defined a wavenumber $\kappa_c^\sigma$ up to which the numerical scheme approximates the true solution within a maximum error tolerance $\sigma$. This is 
%
\begin{subequations} \label{eq:maxWaveNo} 
\begin{align}
   \kappa_c^\sigma&=\frac{1}{2}(\kappa_{Rc}^\sigma+\kappa_{Ic}^\sigma)	\\
   \kappa_{Rc}^\sigma&=\min(\kappa|\varepsilon_R(\kappa)=\sigma,0<\kappa<\pi),	\\
   \kappa_{Ic}^\sigma&=\min(\kappa|\varepsilon_I(\kappa)=\sigma,0<\kappa<\pi), 
\end{align} 
\end{subequations}
%
where $\kappa_{Rc}^\sigma$ and $\kappa_{Ic}^\sigma$ represent the wavenumber up to which the numerical differentiation can approximate the true solution within an error tolerance $\sigma$ in its real and imaginary parts respectively. Then the values of $\phi_m$ that define the rest of the constants in \ref{eq:boundary} are computed by an iterative Newton-Raphson method that maximises the value of $\kappa_c^\sigma$. \citet{Kim2007} used an error tolerance of $\sigma=0.001$, $0.002$, and $0.003$ for $i=2$, $1$ and $0$ respectively, which lead to the values of Table~\ref{tab:boundaryCoef}. 
%
\begin{table}[H] \centering 
\caption{Boundary Scheme Coefficients} \label{tab:boundaryCoef} 
\resizebox{1\textwidth}{!}{ 
\begin{tabular}{l l l l} 
\hline 
Coefficients&$i=0$&$i=1$&$i=2$\\ 
\hline 
$\gamma_{i0}$&--&0.08360703307833438&0.03250008295108466\\ 
$\gamma_{i1}$&5.912678614078549&--&0.3998040493524358\\ 
$\gamma_{i2}$&3.775623951744012&2.058102869495757&--\\ 
$\gamma_{i3}$&--&0.9704052014790193&0.7719261277615860\\ 
$\gamma_{i4}$&--&--&0.1626635931256900\\ 
$b_{i0}$&--&-0.3177447290722621&-0.1219006056449124\\ 
$b_{i1}$&-3.456878182643609&--&-0.6301651351188667\\ 
$b_{i2}$&5.839043358834730&-0.02807631929593225&--\\
$b_{i3}$&1.015886726041007&1.593461635747659&0.6521195063966084\\ 
$b_{i4}$&-0.226526470654333&0.2533027046976367&0.3938843551210350\\ 
$b_{i5}$&0.3938843551210350&-0.03619652460174756&0.01904944407973912\\ 
$b_{i6}$&-0.01836710059356763&0.004080281417108407&-0.001057260523947668\\ 
\hline 
\end{tabular}} 
\end{table}
%
\begin{figure}\centering
   \tikzsetnextfilename{bSchemeComparison}
   \input{\folder/Methodology/bSchemeComparison}
   \caption{Dispersive (\emph{a}) and dissipative (\emph{b}) characteristics of the boundary scheme.}
   \label{fig:bSchemeComparison} 
\end{figure}

Figure~\ref{fig:bSchemeComparison} shows the modified-wavenumber profiles for all three boundary points. It is seen that the most accurate in terms of dispersive errors is the scheme cast at node $i=2$ and as the nodes get closer to the boundary, and hence need more points coming from extrapolation, the resolution of the schemes slightly decays. As regards the Dissipation discrepancies, the scheme cast at $i=1$ seems to be the closest to the true solution despite being the less accurate dispersion-wise.

%**SECTION **% 
\FloatBarrier %Force all figures from previous sections to be placed 
\subsection{High-order compact filters}\label{sub:filters}

The numerical recipes for spatial differentiation have been described in the last section. It has been shown that the methods used in this study are able to faithfully represent the true analytical differentiation up to very high wavenumbers (when transformed to the wavenumber domain). Nevertheless, it is impossible to generate a finite difference approximation that covers the entire spectra without any error. As a consequence, if the results are taken just straight away after the numerical differentiation without any kind of treatment, the errors made in the very high end of the wavenumber spectrum will be included in the final solution. Additionally, it has been shown that the grid topology is directly included in the governing equations when transformed to the generalized coordinate system (see \ref{eq:genGovEq}). If the grid is not smooth enough (as it happens in body-fitted curvilinear grids), spurious waves can easily develop in those regions. On top of that, the boundary schemes are dissipative at high wavenumbers but the interior OHOC schemes are of non-dissipative nature because of its central formulation. Consequently, with the lack of numerical dissipation, the errors made can grow unboundedly and highly deteriorate the quality of the results. 

There exists then the necessity of a numerical treatment that removes these spurious solutions and makes the general algorithm stable. For this reasons, dissipative numerical schemes are chosen to control the error growth. Other methods of doing this are to add artificially extra dissipation to the equations. The method chosen in this investigation is based on High-Order Compact Filters, and to be more exact, the method used is the one developed by \citet{Kim2010} which removes the spurious solution up to a user-defined \emph{"cut-off wavenumber"} ($\kappa_C$).

The filters have a similar formulation to the differentiating schemes and hence there is an interior version of the filter and also a boundary version. The filter used in the interior nodes has the following differential form 
%
\begin{equation}\label{eq:intFilter}
   \beta\hat{\Delta}f_{i-2}+\alpha\hat{\Delta}f_{i-1}+\hat{\Delta}f_i+\alpha\hat{\Delta}f_{i+1}+\beta\hat{\Delta}f_{i+2}=\sum_{m=1}^{3} a_m(f_{i-m}-2f_i+f_{i+m}), 
\end{equation}
%
where $\hat{\Delta}f_i=\hat{f}_i-f_i$ is the difference between the filtered value and the original with the hat representing the filtered value. In equation \ref{eq:intFilter} there are five unknown coefficients. Taylor expanding \ref{eq:intFilter} up to sixth-order and matching coefficients provides two extra equations leaving just three free parameters. The rest of the parameters are defined after shifting to the wavenumber domain where they can be expressed in terms of the cut-off wavenumber. The transfer function between the filtered and the unfiltered fields $T(\kappa)$ can be obtained in the wavenumber domain where $T(\kappa)=1$ means no-filtering and $T(\kappa)=0$ means complete filtering. The free parameters are fixed so the following conditions are met: 
%
\begin{subequations}\label{eq:filtCond} 
\begin{align}
   T(\pi)=0 \\
   \frac{d^2T(\pi)}{d\kappa^2}=0 \\
   T(\kappa_C)=\frac{1}{2} 
\end{align} 
\end{subequations}

The last condition defines the cut-off wavenumber as the wavenumber where the transfer function gives "half-filtering". Adding these constraints to the system of equations fully defines the interior filter's coefficients in terms of the desired cut-off wavenumber.

For the boundary nodes, e.g. $i=0,i=1$ and $i=2$ for a left boundary, the interior filters are no longer valid since exterior domain values are demanded in \ref{eq:intFilter}. \citet{Kim2010} uses a \nth{4} order polynomial extrapolating function to obtain these external values as a function of the interior values. The extrapolating function is of the form of 
%
\begin{subequations}\label{eq:filtExtrap} 
\begin{align}
   g(x^*)&=f_0+\sum_{m=1}^{4} c_m(x^*)^m, \\
   \hat{\Delta}g(x^*)&=\hat{\Delta}f_0+\sum_{m=1}^{4} d_m(x^*)^m, 
\end{align} 
\end{subequations}
%
where again $x^*=(x-x_0)$ is a dimensionless distance from the boundary point as it happened with the extrapolating functions used for the derivation of the boundary finite difference scheme. However, the extrapolating function is only based on polynomials for which eight coefficients need to be defined given the \nth{4} order of formal accuracy requested. Six equations can be derived by matching the interior values while still preserving the pentadiagonal shape at the boundary. Extra equations are obtained by blending the boundary filter with the interior filter. The resulting boundary filter is 
%
\begin{subequations}\label{eq:boundFilter} 
\begin{align} 
   i=0 &: \hat{\Delta}f_0+\gamma_{01}\hat{\Delta}f_1+\gamma_{02}\hat{\Delta}f_2=0 \\ 
   i=1 &: \gamma_{10}\hat{\Delta}f_0+\hat{\Delta}f_1+\gamma_{12}\hat{\Delta}f_2+\gamma_{13}\hat{\Delta}f_3=0 \\ 
   i=2 &: \gamma_{20}\hat{\Delta}f_0+\gamma_{21}\hat{\Delta}f_1+\hat{\Delta}f_2+\gamma_{23}\hat{\Delta}f_3+\gamma_{24}\hat{\Delta}f_4=\sum_{m=0,\neq{2}}^{5} b_{2m}(f_m-f_2), 
\end{align} 
\end{subequations}
%
where all the coefficients depend on the values of the interior filter's parameters (for more detailed expressions of the boundary filter parameters see~\citet[Eq.~3.6]{Kim2010}). Because the interior parameters are given by functions of the cut-off wavenumber, so are the boundary ones. It is possible to apply different cut-off wave numbers depending on the grid position. It is usual to have lower resolution schemes in the boundaries and hence it is desirable to increase the filtering in those zones. Increasing the filtering of the original function is achieved by a reduction in the cut-off wave number. For such reason, the cut-off wavenumber could be defined by 
%
\begin{equation}\label{eq:weightFilter}
\kappa_{C_i}=
\begin{cases}
   \kappa_C \quad &\text{for } 3\leq i\leq N-3 \\
   (1-\varepsilon)\kappa_C \quad &\text{for } i=2\,\wedge\,N-2 \\
   (1-2\varepsilon)\kappa_C \quad &\text{for } i=1\,\wedge\,N-1 \\
   (1-3\varepsilon)\kappa_C \quad &\text{for } i=0\,\wedge\,N
\end{cases}, 
\end{equation}
%
where $\varepsilon$ is a weighting factor that makes $\kappa_C$ smaller close to a boundary. The optimum value of this weighting factor was found to be $\varepsilon=0.085$ by \citet{Kim2010} after performing a linear stability analysis of the combination of both filters and both difference schemes. This is the maximum value of $\varepsilon$ that maintains the differencing-filtering system stable for $0.5\pi\leq\kappa_C\leq0.88\pi$ , i.e. the range where the system is stable for a fixed cut-off wavenumber all over the domain.
