# ============================================================================
# Name        : Makefile
# Author      : 
# Version     :
# Copyright   : Your copyright notice
# Description : Makefile for Fortran
# ============================================================================
.PHONY: all

# Change this line if you are using a different Fortran compiler
COMPILER = pdflatex
PDFCOMPILER = pdflatex
NAME = thesis
fNAME = testFig
 
all:
	make thesis

cleanfigs:
	rm tikzfigs/*
cleantex:
	rm *.log
	rm *.toc
	rm *.lof
	rm *.lot
	rm *.out
	rm *.aux
	rm *.blg
	rm *.bbl
	rm *.pdf
	rm *.auxlock
thesis: 
	$(COMPILER) -enable-write18 -draftmode $(NAME).tex
	bibtex $(NAME)
	$(COMPILER) -enable-write18 -draftmode $(NAME).tex >/dev/null
	$(PDFCOMPILER) -enable-write18 --interaction=nonstopmode $(NAME).tex | grep -e Warning -e Overfull
