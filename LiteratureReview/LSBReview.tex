%=====================SECTION - Laminar Separation Bubbles Review=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Laminar Separation Bubbles Review}\label{sec:LSBReview}
In the previous chapter, the concept of laminar separation bubbles (LSBs) has been introduced as a common feature in WLE aerofoils operating at low and medium $Re$ ranges. For WLE aerofoils the LSBs form in the trough region of the aerofoils whereas for standard SLE aerofoils the LSB expands along the entire wingspan. They appear due to the presence of an adverse pressure gradient that causes the flow to separate. Thus, LSBs can be formed also in channel flows experiencing strong adverse pressure gradients due to channel contractions, application of suction or blowing on the channel walls. 

Due to its simplicity, channel flow experiments/simulations are the most commonly used for LSB related investigations. In particular, the early work of \citet{Horton1968} and \citet{Gaster1967} provided key insights into the steady structure of the bubbles by imposing adverse pressure gradients on flows over a flat plate. The imposed adverse pressure gradient causes flow separation that reattaches further downstream forming a typical laminar separation bubble the (steady) structure of which is shown in figure~\ref{fig:LiteratureReview_Horton1968LSB} taken from \citet{Horton1968}. It is seen that after the laminar flow separates some portion of the flow is trapped in between the dividing streamline or separatrix and the no-slip boundary forming what is known as laminar separation bubble (LSB). In the first portion of the LSB, the flow is almost stagnant and thus, pressure remains almost constant, but immediately upstream of the reattachment point, one can notice the characteristic reversed flow vortex where the highest (negative) velocities can be found accompanied by a strong pressure gradient. On the other side of the dividing streamline, the flow follows a laminar profile that ultimately undergoes transition to turbulent flow near the reversed flow vortex centre. The turbulent character of this shear layer benefits from the enhanced momentum exchange with the free-stream flow and manages to reattach as a turbulent boundary layer.
%
\begin{figure}\centering
   \tikzsetnextfilename{LiteratureReview_Horton1968LSB}
   \input{\folder/LiteratureReview/Horton1968LSB}
   \caption{Steady structure of an LSB from \citet{Horton1968}.}
   \label{fig:LiteratureReview_Horton1968LSB}
\end{figure}

%=====================SUBSECTION - UnsteadyLSB=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Unsteady LSB fluctuations}\label{sub:UnsteadyLSB}

In his early works, \citet{Gaster1967} realised that the length of the LSBs varied depending on the flow properties. He found that increasing the strength of the adverse pressure gradient resulted in a reduced LSB length and therefore classified the LSBs in \emph{``short''} or \emph{``long''} depending on a dimensionless pressure gradient parameter based on the momentum-thickness Reynolds number and a dimensionless velocity gradient
\begin{equation}
\label{eq:gasterPavg}
\begin{aligned}
P_{avg}&=\frac{\theta^2_{sep}}{\nu}\left(\frac{\dd{u_{inv}}}{\dd{x}}\right)_{avg},\\
\text{with}\qquad \theta&=\int_{0}^{\infty} \frac{u(\hat{y})}{u_\infty} \left(1-\frac{u(\hat{y})}{u_\infty}\right)\,d\hat{y},
\end{aligned}
\end{equation}
and where $\hat{y}$ is the axis normal to the wall surface and the velocity gradient term is the averaged inviscid (with no separation) velocity gradient over the separated section.

\citet{Pauley1990} confirmed the findings of \citet{Gaster1967} and found that increasing the suction, i.e. prescribing a stronger adverse pressure gradient, the length of the LSB increased and at the same time, small oscillations in the skin friction started appearing downstream of the reattachment point. After further suction increase, the LSB became unsteady and vortex shedding began. \citeauthor*{Pauley1990} defined this unsteady mode as \emph{``unsteady separation''}. It was also found that the strength of the shed vortices was directly proportional to the level of suction applied. Despite the unsteady character of the simulations, the flow upstream of the separation point remained steady at all times. Flow entrainment from the boundary layer into the separated region was also observed to happen in a periodic fashion and as a consequence, the height of the separated region kept fluctuating about a mean value.

\citet{Pauley1990} also proposed a parameter $P_{max}$, similar to \citeauthor*{Gaster1967}'s non-dimensional pressure gradient parameter $P_{avg}$, that determines the onset of unsteady separation when
%
\begin{equation}
\label{eq:pauleyPmax}
P_{max}=\frac{\theta^2_{sep}}{\nu}\left(\frac{\dd{u_{inv}}}{\dd{x}}\right)_{max}\approx-0.24,
\end{equation}
%
where the velocity gradient term is given by the maximum (negative) inviscid velocity gradient in the separated region. Using this parameter and \citeauthor*{Gaster1967}'s data they concluded that what \citeauthor*{Gaster1967} defined as \emph{``bursting''} was, in fact, the demise of unsteady separation and \emph{``long''} LSBs were just steady LSBs.

The propagation velocity of the vortices was approximately $0.65u_\infty$ and the shedding frequency was found to be independent of the $Re_\infty$ if made dimensionless by the momentum thickness and boundary-layer-edge velocity
\begin{equation}
\label{eq:ftheta}
	f_\theta=\frac{f\theta_{sep}}{u_{es}}=0.00686\pm0.6\%.
\end{equation}
%
This unsteady shedding of vortical structures has been observed in many other studies \citep{Watmuff1999, Burgmann2008, Yarusevych2009, Jones2008a, Jones2010} for LSBs in both aerofoils and flat plates and the measured shedding frequency lays usually in the range $f_\theta=[0.005-0.011]$.

Although the Strouhal number defined by \citeauthor*{Pauley1990} is the most commonly used, other authors \citep{Yarusevych2009} have proposed different a frequency scaling based on the streamwise distance between consecutive vortices $\lambda_0$ and the boundary layer edge velocity at separation $u_{es}$ leading to
\begin{equation}
\label{eq:flambda0}
	f_{\lambda}=\frac{f\lambda_0}{u_{es}}\approx 0.475\pm 0.025.
\end{equation}

Additionally, another non-dimensional frequency can be computed based on inviscid linear stability theory
%
\begin{equation}
\label{eq:omegastar}
	\omega^*=\frac{1}{4}\delta_\omega \frac{(2\pi f)}{\bar{u}},
\end{equation}
%
where $\delta_\omega=(u_e-u_0)/(\partial{u}/\partial{y})_{max}$ is the vorticity thickness, $\bar{u}=(u_e+u_0)/2$ is the average velocity across the shear layer and $u_0$ is the velocity inside the LSB. It is worth noting that if $u_0=0$, then $\omega^*$ becomes
%
\begin{equation}
\label{eq:omegastar2}
	\omega^*=\frac{\pi f}{(\partial{u}/\partial{y})_{max}},
\end{equation}
%
which, if measured at the centre of the reverse flow vortex, coincides with the value of the most amplified frequency for a $\tanh$ profile as predicted by inviscid linear stability theory, i.e. $\omega^*\approx0.21$~\citep{Pauley1990, Watmuff1999}.


%=====================SUBSECTION - LSBstability=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Stability and receptivity of LSBs}\label{sub:LSBstability}
%
The stability and receptivity to external disturbances of the LSBs have been the focus of study for many researchers: \citet{Pauley1994, Watmuff1999, Alam2000, Marxen2004} and \citet{Jones2010} among others. The outcome of each of the studies was highly dependent on the type of perturbations imposed on the flow. \citet{Pauley1994}, for example, investigated the effect of three-dimensional disturbances on a flat-plate LSB. \citeauthor*{Pauley1994} used both random and harmonically distributed spanwise perturbations which, despite not changing the shedding frequency in any case, affected in a very distinct way the LSB. The random perturbations appeared to have a negligible effect on the structure of the LSB whereas the spanwise harmonic disturbances increased the length of the LSB and reduced the strength of the shed vortices. In addition, the harmonic perturbations promoted the formation of streamwise vortices which were identified as G\"ortler vortices. However, in the numerical investigations of \citet{Marxen2004}, who performed a similar study of that of \citet{Pauley1994}, claimed that, for spanwise harmonic perturbations of less than $0.03u_\infty$ amplitude, the transition process is still governed by the growth of two-dimensional waves. And that in general, even large three-dimensional disturbances had little effect on the development of the fundamental two-dimensional instability wave. Moreover, they rejected the idea of the G\"ortler-like steady vortices, which are likely to appear when spanwise harmonic disturbances are introduced upstream of the bubble, of being a main factor in the transition process.

Inspired by the works of \citet{Gaster1975}, \citet{Watmuff1999} introduced three-dimensional disturbances originated at a single point by means of small-amplitude perturbations introduced through a hole in the test surface at the pressure minimum of a convergent-divergent channel. According to \citet{Gaster1975}, this impulsive perturbation would excite all possible instability modes and the wave packet would then form through selective interference of these waves. In order to obtain a clearer picture of the flow, \citeauthor*{Watmuff1999} introduced the impulsive perturbations in a periodic manner and therefore, was able to obtain a phase-averaged field.

The convected wave packet triggered the formation and shedding of three-dimensional vortex loops the shape of which was reminiscent of a \emph{``hairpin''} or \emph{``arch''} vortex. \citeauthor*{Watmuff1999} argued that the contours of the vortex loop in its central symmetry plane resembled the \emph{``cat's eye''} pattern characteristic of a Kelvin-Helmholtz instability. Similarly to \citet{Pauley1990}, \citeauthor*{Watmuff1999} found that the non-dimensional frequency of the shed vortex loops, as defined in equation~\ref{eq:omegastar}, was $\omega^*=0.21$ if measured at the streamwise position where the shedding originated. This confirmed that the growth of the disturbances is governed by inviscid instability mechanisms. In contrast to \citeauthor*{Pauley1990}' results, the vortex shedding only occurred when the perturbations were introduced and therefore \citeauthor*{Watmuff1999} concluded that
%
\begin{quote}
	``... the vortex shedding is not directly associated with the bubble, but rather that the separation bubble is responsible for the establishment of a free-shear layer and that the shedding process is initiated by an inviscid instability associated with the inflection point which lies outside the bubble.''
\end{quote}

He suggested that the vortex shedding observed by \citet{Pauley1990} was likely to be the result of an absolute instability, although it could also be due to the impulsive application of the adverse pressure gradient which then triggered the instability in the LSB's shear layer. In any case, he suggested that while two-dimensional vortex shedding is a consequence of an absolute instability in the flow, three-dimensional vortex shedding could be classified as a convective instability.

A criterion for defining the stability characteristics of the LSBs were given by \citet{Alam2000}. A linear stability analysis of velocity profiles showed that LSBs can be both convective and absolute unstable depending on the magnitude of the reversed as shown in figure~\ref{fig:LiteratureReview_Alam2000Stability}. The solid line divides the region of absolute and convective instability for a given $Re_{\delta^*}$. It is seen that the curve asymptotes $u_{min}=-0.15u_\infty$, where $u_{min}$ is the maximum (negative) velocity of a given velocity profile, and therefore for reasonably high Reynolds number the flow becomes absolutely unstable if the magnitude of the reversed flow exceeds 15\% of the free-stream value. 
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{LiteratureReview_Alam2000Stability}
   \input{\folder/LiteratureReview/Alam2000Stability}
   \caption{Regions of absolute and convective instability as a function of the maximum reversed flow and $Re_{\delta^*}$ from \citet{Alam2000}.}
   \label{fig:LiteratureReview_Alam2000Stability}
\end{figure} 

In aerofoils, however, self-sustaining turbulence can also be found even when the reversed flow does not exceed the criterion of \citet{Alam2000}. This was the case in the DNS simulations of \citet{Jones2008a}. \citeauthor*{Jones2008a} performed DNS computations of an initially two-dimensional flow with spanwise velocity perturbations in the form of Gaussian white noise. The disturbances affected the LSB and grew in amplitude over time with an approximate growth rate of $G\approx e^{4t}$. The nature of this instability was found to be related to the production of streamwise vortices located in the braid region between successive spanwise vortices similar to mode-B instability of bluff-body wakes. The results of \citeauthor*{Jones2008} showed that classical linear stability of time-averaged flow fields cannot describe the transition mechanism since it is due to a secondary instability related to the unsteady vortex shedding occurring at the rear part of the LSB.

Further analysis of the stability characteristics of the flow over aerofoils with an LSB by means of both classical linear stability methods and a two and a three-dimensional forced Navier-Stokes solver was performed by \citet{Jones2010}. The second method integrates in time the evolution of local disturbances superimposed on an initial base-flow that is forced to remain steady. 

Contrary to \citet{Pauley1990}, who concluded that the LSB shedding was due to an inviscid instability mechanism, the classical linear stability analysis performed in \citet{Jones2010} revealed that the most amplified instability wave has a frequency much higher than the frequency of vortex shedding observed at the rear of the LSB. This confirmed the findings of \citet{Jones2008}, where instability waves of zero group velocity with a positive growth rate could not be found, and therefore, the reason for the self-sustained turbulence could not be explained by means of (parallel and steady) linear stability theory.

On the other hand, the use of forced N-S simulations revealed the existence of an acoustic-feedback loop that results in a global instability mode that grows as $e^{0.25t}$ in the two-dimensional case and $e^{0.21t}$ in the three-dimensional case. In addition, the analysis of the frequency content of the wavepackets showed that, although the energy of the initial wavepacket is contained in a frequency closer to the frequency of the most convectively amplified instability waves as predicted by linear stability theory, after several cycles of the feedback loop the fundamental frequency of the wavepacket is much closer to the LSB-vortex-shedding frequency. Therefore, \citeauthor*{Jones2010} suggests that the acoustic-feedback loop also acts as a frequency selector for the LSB shedding. These findings were later on confirmed by \citet{Probsting2015}.
