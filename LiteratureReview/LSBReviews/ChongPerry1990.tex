%=====================SECTION - ChongPerry1990=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{A general classification of three-dimensional flow field}\label{sub:ChongPerry1990}

In this work, \citet{Chong1990} extend their previous work on critical-point theory to general three-dimensional flow fields. The authors classify critical points, where all the velocity components vanish, based on the three invariants of the rate-of-deformation tensor $\bm{A}=\partial{u_i}/\partial{x_j}$ for $i=1,2,3$ and $j=1,2,3$. The values of the invariants $P$, $Q$ and $R$, defined in equation~\ref{eq:3dinvariants}, determine the roots of the characteristic polynomial of $\bm{A}$ and therefore determine the local behaviour of the flow about the critical point.

\begin{equation}
\label{eq:3dinvariants}
	\begin{aligned}
		P&=-\bm{S}_{ii}=-\tr{(\bm{A})},\\
		Q&=\frac{1}{2}(P^2-\bm{S}_{ij}\bm{S}_{ji}-\bm{R}_{ij}\bm{R}_{ji})=\frac{1}{2}(P^2-\tr{(\bm{A}^2)}),\\
		R&=\frac{1}{3}(-P^3+3PQ-\bm{S}_{ij}\bm{S}_{jk}\bm{S}_{ki}-3\bm{R}_{ij}\bm{R}_{jk}\bm{S}_{ki})\\
		&=\frac{1}{3}(-P^3+3PQ-\tr(\bm{A}^3))=-\det{(\bm{A})},\\
		\text{where}\qquad \bm{S}&=\frac{1}{2}(\partial{u_i}/\partial{x_j}+\partial{u_j}/\partial{x_i}),\\
		\text{and}\qquad\bm{R}&=\frac{1}{2}(\partial{u_i}/\partial{x_j}-\partial{u_j}/\partial{x_i}).
	\end{aligned}
\end{equation}

It is interesting to note that when $P=0$ the flow can be thought incompressible and, if $R=0$ and $\rank(\bm{A})=2$ the flow can be thought as two-dimensional and the critical points can be classified in the $P$--$Q$ plane as in \citet{Perry1975}. In the general cases where none of the invariants vanish, the $P$--$Q$--$R$ space is divided into several different regions delimited by three surfaces. The equations of these surfaces are defined by algebraic relations between $P$--$Q$--$R$ and can be found in \citet{Chong1990} as well as a classification of all possible critical points~\citep[see fig. 8]{Chong1990}. For convenience, only planes of constant $P$ are reproduced in figure~\ref{fig:LiteratureReview_Chong1990QR}. The lines $S_{1a}$, $S_{1b}$ are the intersections of surface $S_1$ in \citet{Chong1990} with the plane of constant $P$ and separate complex solutions ($2_{a,b}$ and $4_{a,b}$) from the real ones. Line $m$ separates \emph{stable foci} ($2_a$ and $4_a$) from \emph{unstable foci} ($2_b$ and $4_b$) whereas line $b$ separates \emph{stretching foci} ($2_{a,b}$) from \emph{compressing foci} ($4_{a,b}$) as well as \emph{stable nodes} ($3_a$) from \emph{unstable nodes} ($3_b$). Points enclosed by lines $S_{1a}$, $S_{1b}$ and $b$ are \emph{stable nodes} if $R>0$ and \emph{unstable nodes} if $R<0$, regardless of the plane used to plot the solution trajectories~\citep[see fig.9]{Chong1990}. All critical points that lie exactly on top of the lines of figure~\ref{fig:LiteratureReview_Chong1990QR} are \emph{degenerates}, with the points lying on $m$ being \emph{centers}. The equations of the separating lines in figure~\ref{fig:LiteratureReview_Chong1990QR} are given by:
\begin{equation}
\label{eq:QRlines}
	\begin{aligned}
	S_{1a}&:\quad \frac{1}{3}P(Q-\frac{2}{9}P^2)-\frac{2}{27}(-3Q+P^2)^{3/2}-R=0,\\
	S_{1b}&:\quad \frac{1}{3}P(Q-\frac{2}{9}P^2)+\frac{2}{27}(-3Q+P^2)^{3/2}-R=0,\\
	m&:\quad QP-R=0,\\
	b&:\quad R=0.
	\end{aligned}
\end{equation}

\begin{figure}\centering
   \tikzsetnextfilename{LiteratureReview_Chong1990QR}
   \input{\folder/LiteratureReview/Chong1990QR}
   \caption{Chong1990QR}
   \label{fig:LiteratureReview_Chong1990QR}
\end{figure}
