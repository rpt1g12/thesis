%=====================SUBSECTION - Pauley1990=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{The structure of two-dimensional separation}\label{sub:Pauley1990}

Following the experimental work of \citet{Gaster1967} and \citet{Horton1968}, among others, \citet{Pauley1990} investigated by means of computational fluid dynamics the steady and unsteady characteristics of two-dimensional separation bubbles. The laminar separation was induced by imposing an adverse pressure gradient on a two-dimensional channel with suction on the upper wall. The imposed adverse pressure gradient causes flow separation that reattaches further downstream forming a typical laminar separation bubble the (steady) structure of which is shown in figure~\ref{fig:LiteratureReview_Horton1968LSB} (see \citet{Horton1968} for a more exhaustive analysis). It is seen that after the laminar flow separation some portion of the flow is trapped in between the dividing streamline or separatrix and the no-slip boundary forming what is known as laminar separation bubble (LSB). In the first portion of the LSB the flow is almost stagnant and thus, pressure remains almost constant, but immediately upstream of the reattachment point one can notice the characteristic reversed flow vortex where the highest (negative) velocities and can be found accompanied by a strong pressure gradient. On the other side of the dividing streamline the flow follows a laminar profile that ultimately undergoes transition to turbulent flow near the reversed flow vortex centre. The turbulent character of this shear layer benefits from the enhanced momentum exchange with the free-stream flow and manages to reattach as a turbulent boundary layer.
%
\begin{figure}\centering
   \tikzsetnextfilename{LiteratureReview_Horton1968LSB}
   \input{\folder/LiteratureReview/Horton1968LSB}
   \caption{Horton1968LSB}
   \label{fig:LiteratureReview_Horton1968LSB}
\end{figure}

In his early works, \citeauthor*{Gaster1967} realised that the length of the LSBs varied depending on the flow properties. He found that increasing the strength of the adverse pressure gradient resulted in a reduced LSB length and therefore classified the LSBs in \emph{``short''} or \emph{``long''} depending on a dimensionless pressure gradient parameter based on the momentum-thickness Reynolds number and a dimensionless velocity gradient
\begin{equation}
\label{eq:gasterPavg}
\begin{aligned}
P_{avg}&=\frac{\theta^2_{sep}}{\nu}\left(\frac{\dd{u_{inv}}}{\dd{x}}\right)_{avg},\\
\text{whith}\qquad \theta&=\int_{0}^{\infty} \frac{u(\hat{y})}{u_\infty} \left(1-\frac{u(\hat{y})}{u_\infty}\right)\,d\hat{y},
\end{aligned}
\end{equation}
and where $\hat{y}$ is the axis normal to the wall surface and the velocity gradient term is the averaged inviscid (with no separation) velocity gradient over the separated section.

\citeauthor*{Pauley1990} confirmed the findings of \citet{Gaster1967} and found that increasing the suction, i.e. prescribing a stronger adverse pressure gradient, the length of the LSB increased and at the same time, small oscillations in the skin friction started appearing downstream of the reattachment point. After further suction increase, the LSB became unsteady and vortex shedding began. \citeauthor*{Pauley1990} defined this unsteady mode as \emph{``unsteady separation''}. It was also found that the strength of the shed vortices was directly proportional to the level of suction applied. Despite the unsteady character of the simulations, the flow upstream of the separation point remained steady at all times. Flow entrainment from the boundary layer into the separated region was also observed to happen in a periodic fashion and as a consequence, the height of the separated region kept fluctuating about a mean value.

The propagation velocity of the vortices was approximately $0.65u_\infty$ and the shedding frequency was found to be independent of the $Re_\infty$ if made dimensionless by the momentum thickness and boundary-layer-edge velocity
\begin{equation}
\label{eq:ftheta}
	f_\theta=\frac{f\theta_{sep}}{u_{es}}=0.00686\pm0.6\%.
\end{equation}
Additionally, an other non-dimensional frequency can be computed based on inviscid linear stability theory
\begin{equation}
\label{eq:omegastar}
	\omega^*=\frac{1}{4}\delta_\omega \frac{(2\pi f)}{\bar{u}},
\end{equation}
where $\delta_\omega=(u_e-u_0)/(\partial{u}/\partial{y})_{max}$ is the vorticity thickness, $\bar{u}=(u_e+u_0)/2$ is the average velocity across the shear layer and $u_0$ is the velocity inside the LSB. It is worth noting that if $u_0=0$, then $\omega^*$ becomes
\begin{equation}
\label{eq:omegastar2}
	\omega^*=\frac{\pi f}{(\partial{u}/\partial{y})_{max}},
\end{equation}
which if measured at the centre of the reverse flow vortex coincides with value of the most amplified frequency for a $\tanh$ profile as predicted by inviscid linear stability theory, i.e. $\omega^*\approx0.21$.


The authors also proposed a parameter $m$, similar to \citeauthor*{Gaster1967}'s non-dimensional pressure gradient parameter $P_{avg}$, that determines the onset of unsteady separation when
\begin{equation}
\label{eq:pauleyPmax}
	P_{max}=\frac{\theta^2_{sep}}{\nu}\left(\frac{\dd{u_{inv}}}{\dd{x}}\right)_{max}\approx-0.24,
\end{equation}
where the velocity gradient term is given by the maximum (negative) inviscid velocity gradient in the separated region. Using this parameter and \citeauthor*{Gaster1967}'s data they concluded that what \citeauthor*{Gaster1967} defined as \emph{``bursting''} was in fact the demise of unsteady separation and \emph{``long''} LSBs were just steady LSBs.

In a late work, \citet{Pauley1994} investigated the effect of three-dimensional disturbances on the above described flow. \citeauthor*{Pauley1994} used both random and harmonically distributed spanwise perturbations which, despite not changing the shedding frequency in any case, affected in a very distinct way the LSB. The random perturbations appeared to have a negligible effect on the structure of the LSB whereas the spanwise harmonic disturbances increased the length of the LSB and reduced the strength of the shed vortices. In addition, the harmonic perturbations promoted the formation of streamwise vortices which where identified as G\"ortler vortices.
