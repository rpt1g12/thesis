\FloatBarrier %Force all figures from previous sections to be placed
%**SECTION **% 
\section{Wavy Leading Edges Review}\label{sec:WLEReview}

As introduced in \S\ref{cha:Introduction}, the study of \citeauthor{Fish1995} sparked the curiosity of investigators who saw in the Humpback whale's tubercles a promising passive control method that could lead to increased lift at high AoA. Therefore, although comprehensive reviews of this geometry can be found in literature \citep{Aftab2016,Bolzon2015}, in this section a detailed review of the available literature is presented highlighting the key aspects to be considered when studying the flow around aerofoils with a WLE. 

In the first place, it is useful to describe the geometric characteristics and terms that are commonly used in literature. Figure~\ref{fig:LiteratureReview_WLEgeometry}\emph{a}  shows the typical geometry used for infinite-span simulations and experiments whereas figure~\ref{fig:LiteratureReview_WLEgeometry}\emph{c} shows the model used by \citet{Miklosovic2004} which approximates the shape of a real Humpback whale flipper. The former models are usually referred as two-dimensional, infinite-span or full-span wings, whereas the latter are known as three-dimensional or finite-span wings. It is a common practice in numerical simulations to apply periodic boundary conditions in the span direction to simulate infinite-span wings whilst for experiments, it is a common practice to use models that either have end-plates or span the whole wind tunnel testing section. The implication of using finite-span wings is the appearance of three-dimensional flow features in the wingtip, i.e. wingtip vortices.
\begin{figure}[!ht]\centering
   \tikzsetnextfilename{LiteratureReview_WLEgeometry}
   \input{\folder/LiteratureReview/WLEgeometry}
   \caption{Examples of the (\emph{a}) typical WLE geometry for numerical simulations, (\emph{b}) the main parameters that define the WLE geometry and (\emph{c}) the flipper model used by \citet{Miklosovic2004,Miklosovic2007} for their wind-tunnel experiments.}
   \label{fig:LiteratureReview_WLEgeometry}
\end{figure}

The main parameters defining the geometry are depicted in figure~\ref{fig:LiteratureReview_WLEgeometry}\emph{b} where $L_c$ is the mean chord length and $L_z$ is the span of the model. In the case of numerical simulations, this usually represents the length of the spanwise-periodic numerical domain. The leading edge shape is in most cases modelled as a sinusoidal profile of $h_{LE}$ amplitude and $\lambda_{LE}$ wavelength. The relation $\kappa_{LE}=L_z/\lambda_{LE}$ defines the number of wavelengths used in the simulation/experiment. The sections of maximum and minimum chord are referred as Peaks and Troughs in general, although some researchers have also used the term valley for the latter. Sections with chord equal to the mean chord are referred as Middle sections herein.

%=====================SUBSECTION - WLEforces=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Effect on aerodynamic forces}\label{sub:WLEforces}

The biggest attraction of using a sinusoidal leading edge is the improved post-stall aerodynamic characteristics offered. Many researchers have proved that using a sinusoidal leading edge leads to an increased lift generation at high angles of attack in comparison with the straight leading edge (SLE) geometry independently of the baseline aerofoil profile used. \citet{Miklosovic2007,Hansen2011,Rostamzadeh2013,Rostamzadeh2014,Corsini2013} and \citet{Skillen2014} found that for full-span symmetrical NACA aerofoils the WLE models outperformed the SLE models in post-stall regime by having a smoother stall at the expense of reducing the maximum $C_L$ value and generating less lift in the pre-stall regime. In terms of drag, the WLE models generate about the same drag as SLEs at low $\alpha$ but, due to their typical earlier stall point, the WLE models generate much more drag in the near-stall regime. However, past the stall point of their SLE counterpart model, for certain combinations of $h_{LE}$ and $\lambda_{LE}$ the use of WLE can result in large drag reductions. For full-span wings but for non-symmetrical aerofoil sections, \citet{Johari2007,Zhang2014,Dropkin2012,Cai2013} and \citet{Custodio2015} arrived at the same conclusions.
\begin{figure}[!t]\centering
   \tikzsetnextfilename{LiteratureReview_amplitudeClCd}
   \input{\folder/LiteratureReview/amplitudeClCd}
   \caption{Comparison of (\emph{a}) and (\emph{b}) drag coefficients for models with different $h_{LE}$ extracted from \citet{Hansen2011}.}
   \label{fig:LiteratureReview_amplitudeClCd}
\end{figure}

An example of this kind of behaviour can be seen in figure~\ref{fig:LiteratureReview_amplitudeClCd}, where an extract of the force coefficients from \citet{Hansen2011} has been plotted. In particular, the effect of varying the WLE amplitude $h_{LE}$ is clearly seen. In general, for the same WLE wavelength $\lambda_{LE}$, models with larger $h_{LE}$ tend to have a softer but earlier stall and increased drag in the near-stall regime. Although some researchers \citep{Johari2007} suggest that the WLE wavelength has little effect on both lift and drag, in figure~\ref{fig:LiteratureReview_wavelengthClCd} data again extracted from \citet{Hansen2011} has been plotted for different values of $\lambda_{LE}$ that shows that, for the same WLE amplitude, reducing the WLE wavelength can provide improvements in both lift and drag. Maximum lift and stall angle are both increased and consequently drag is reduced. However, for a given amplitude, there exists a minimum wavelength at which further reductions do not result in further improvements. In addition, \citet{Hansen2011} suggested that for WLE models with the same amplitude to wavelength ratio $h_{LE}/\lambda_{LE}$ similar lift and drag curves shall be expected.
\begin{figure}[!t]\centering
   \tikzsetnextfilename{LiteratureReview_wavelengthClCd}
   \input{\folder/LiteratureReview/wavelengthClCd}
   \caption{Comparison of (\emph{a}) and (\emph{b}) drag coefficients for models with different $\lambda_{LE}$ extracted from \citet{Hansen2011}.}
   \label{fig:LiteratureReview_wavelengthClCd}
\end{figure}

Figures~\ref{fig:LiteratureReview_amplitudeClCd} and~\ref{fig:LiteratureReview_wavelengthClCd} show the steady or time-averaged force coefficients and how the use of sinusoidal leading edges can lead to improved lift generation at high angles of attack, but WLE aerofoils can also provide unsteady aerodynamic improvements in the form of reduced force fluctuations. \citet{Skillen2014} showed that the use of a WLE reduces the force fluctuations at high angles of attack associated with the wake-vortex shedding of a stalled aerofoil. Instead of a single frequency peak at $f^*=fL_c\sin(\alpha)/u_\infty\approx0.2$, typical of bluff-body vortex shedding, the WLE has a broadband peak around $f^*\approx0.2$ with much less energy content. In addition, the variance of the force coefficient's time history is also reduced which indicates a lower level of unsteadiness in the flow. Force fluctuations are directly related to the amount of noise produced by an aerodynamic surface and therefore, WLE models have been considered also a good noise reduction solution as shown by \citet{Hansen2012,Narayanan2015,Haeri2013,Lau2013,Kim2015,Kim2016} and \citet{Chaitanya2017}. Furthermore, for very low Reynolds number, the numerical investigations of \citet{Favier2012} and \citet{Serson2017} suggest that for a given combination of amplitude and wavelength the fluctuations can be completely removed.

%=====================SUBSECTION - finiteSpan=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Finite-span wings}\label{sub:finiteSpan}
As it has been previously mentioned, both numerical and experimental investigations of finite-span wings with a WLE have also been performed by \citet{Fish1995,Ozen2010,Guerreiro2012,Yoon2011,Weber2011} using wings with a rectangular platform, \citet{Miklosovic2004,Pedro2008} for tapered wings imitating the Humpback Whale's flipper and \citet{Custodio2015} who used rectangular and tapered wings. In contrast to the infinite-span wing models, where all published literature agrees that WLEs in general stall much earlier and are outperformed by their SLE counterparts in pre-stall regime (with the exception of \citet{Rostamzadeh2016} at $Re_\infty=1.5\times10^6$), \citet{Miklosovic2004,Pedro2008} and \citet{Custodio2015} showed that for tapered finite-span WLE wings stall was significantly delayed without noticeable detriments prior to stall. \citet{Pedro2008} suggested that the stall delay was due to a compartmentalisation effect that restricted separation to the outboard section. This compartmentalisation effect was also reported by \citet{Miklosovic2004,Ozen2010,Yoon2011} and \citet{Weber2011}. Additionally, \citet{Ozen2010} showed that using a wavy leading edge reduced the strength of the wing-tip vortex.

%=====================SUBSECTION - reEffect=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Reynolds number effects}\label{sub:reEffect}
Investigations on WLE aerofoils and wings have been carried out over a large range of Reynolds numbers, from the lowest at $Re_\infty=800$ \citep{Favier2012} to the highest at $Re_\infty=3\times10^6$ \citep{Dropkin2012}. However, actual Reynolds number effects have been only tested by \citet{Dropkin2012,Guerreiro2012,Custodio2015} and \citet{Rostamzadeh2016}. It was found that while the performance of the baseline SLE models was highly dependent on the Reynolds number, the WLE models were less sensitive to the Reynolds number variation, especially at very high angles of attack. 

For high Reynolds number flows, stall is significantly delayed for both WLE and SLE models due to the fact that turbulent boundary layers are more resistant to separation. However, the maximum angle of attack before stall is increased much more in the SLE case. Consequently, due to the earlier stall of the WLE, the SLE produces more lift and less drag over a larger range of angles of attack. Nonetheless, none of the Reynolds studies mentioned included very low-Reynolds number effects, i.e. the Reynolds number ranges were $[70\times10^3,140\times10^3]$, $[1.8\times10^5,60\times10^5]$, $[90\times10^3,450\times10^3]$ and  $[120\times10^3,1500\times10^3]$ for \citet{Guerreiro2012}, \citet{Dropkin2012} and \citet{Custodio2015} respectively. In fact, for the lowest Reynolds number investigations, \citet{Favier2012} at $Re_\infty=800$ and \citet{Serson2017} at $Re_\infty=1000$, it was observed that WLE models generated less lift than the baseline models in the post-stall regime. These findings suggest that, contrary to what has been extensively reported for moderate and high Reynolds numbers, the use of a WLE geometry can be detrimental for very low and very high Reynolds number flows.

%=====================SUBSECTION - peaks&Troughs=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Peaks, troughs and spanwise periodic flow patterns}\label{sub:peaks&Troughs}
Spanwise periodic flow patterns are highly encouraged by the use of sinusoidal leading edge modifications. In particular, many of the published work \citep{Pedro2008,VanNierop2008,Yoon2011,Rostamzadeh2013,Rostamzadeh2014,Rostamzadeh2016,Skillen2014,Hansen2016} reports a spanwise pressure distribution. The free-stream flow, as it approaches the aerofoil's leading edge, is deflected towards the trough regions where it is channelled between its adjacent peaks and increases velocity. As a consequence, the suction peak is much higher, i.e. pressure is much lower, than at the peak and middle sections, resulting in a cyclic spanwise pressure variation. The spanwise pressure gradients promote the three-dimensionality of the flow and flow exchange between neighbouring spanwise-sections.

Due to the trough's more pronounced suction peak, the chordwise pressure gradient is much more severe increasing the likelihood of boundary layer separation at the troughs. For transitional Reynolds numbers, such as those commonly used in the vast majority of WLE published literature, and if the free-stream disturbances are low enough, the initial boundary layer starting from the leading edge is laminar. Laminar boundary layers are much more prone to separate than turbulent boundary layers which in conjunction with the steep adverse pressure gradient makes trough regions prone to flow separation. Therefore, flow at the troughs separates creating a laminar shear layer that eventually transitions to turbulence. Depending on the Reynolds number, the level of incoming free-stream disturbances and angle of attack, the separated shear layer may reattach and a turbulent boundary layer starts redeveloping. The region confining the reversed flow region is commonly known as laminar separation bubble (LSB) or transitional bubble and it will be discussed in depth in the following section since it plays a key role in the flow fields surrounding a WLE aerofoil.

In SLE aerofoils, a single two-dimensional LSB is formed that spans the entire upper surface. In WLE aerofoils, multiple three-dimensional LSBs are found at the troughs \citep{Hansen2011,Dropkin2012,Rostamzadeh2013,Rostamzadeh2014,Hansen2016,Skillen2014}. The distribution of LSBs changes abruptly at some angle of attack near stall from a spanwise-periodic pattern where LSBs are located at each trough to more complicated patterns where some of the LSBs burst while others remain \citep{Hansen2011,Hansen2016,Dropkin2012,Rostamzadeh2013,Rostamzadeh2014,Rostamzadeh2016}. Nonetheless, \citet{Skillen2014} reported a span-wise periodic LSB distribution even at large angles of attack. In the latter case, the pressure inside each LSB is almost identical, whereas in the former cases where the LSBs group together in a rather collocated fashion, the pressure inside each LSB greatly varies from one to another.

%=====================SUBSECTION - svPair=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Streamwise vortex pairs}\label{sub:svPairs}
The most commonly reported flow feature for flows around WLE geometries is by far the presence of vortex pairs on the sides of the leading edge peaks \citep{Fish1995,Ozen2010,Hansen2011,Zhang2014,Pedro2008,Yoon2011,Weber2011,Zhang2012,Favier2012,Skillen2014,Rostamzadeh2014,VanNierop2008,Dropkin2012,Rostamzadeh2013,Rostamzadeh2016,Hansen2016,Turner2017,Wei2015}. These vortices are traditionally the foundation on which explanations for WLE aerodynamic benefits are built upon. The most accepted theory describing the creation of the vortex pair is based on Prandtl's secondary flow of the first type as described by \citet{Rostamzadeh2014} and \citet{Hansen2016}. This is an inviscid mechanism by which spanwise and wall-normal vorticity is reoriented towards the streamwise direction by the presence of flow skewness induced by the spanwise pressure gradients. In fact, \citet{Turner2017}, who solved the flow for a spanwise vortex impinging on a WLE flat plate using the inviscid Euler equations, showed that spanwise vorticity is reoriented in the leading edge area into two sets of horseshoe vortices of opposite rotation for the upstroke and downstroke sections of the impinging vortex. The inviscid analysis of \citet{VanNierop2008} also showed that the use of a WLE leads to a spanwise variation in circulation $\Gamma$ which generates streamwise vortices (SVs). An alternative explanation was given by \citet{Favier2012} who suggested that the streamwise vortices were created due to the presence of a spanwise velocity gradient that triggers a Kelvin-Helmholtz instability which in turn leads to the formation of wall-normal vortices that are reoriented towards the streamwise direction similarly to the mechanism described by \citet{Rostamzadeh2014} and \citet{Hansen2016}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{LiteratureReview_gammaLE}
   \input{\folder/LiteratureReview/gammaLE}
   \caption{Sketch of the leading edge geometry of a WLE describing the peak angle $\gamma_{LE}$ defined in (\ref{eq:gammaLE}).}
   \label{fig:LiteratureReview_gammaLE}
\end{figure}

The strength of the SV pair is proportional to the angle of attack and to the ratio of the WLE amplitude to wavelength $h_{LE}/\lambda_{LE}$ as described in \citet{Hansen2011,Hansen2016}. The ratio of amplitude to wavelength can be seen as a measure of the peak angle $\gamma_{LE}$ sketched in figure~\ref{fig:LiteratureReview_gammaLE} and given by
%
\begin{equation}
\label{eq:gammaLE}
	\gamma_{LE}=\atan \left(\frac{2h_{LE}}{1/2\lambda_{LE}}\right)=\atan \left(4\frac{h_{LE}}{\lambda_{LE}}\right).
\end{equation}

The strength of the vortices is maximum near the leading edge region and due to the adverse pressure gradient they are axially compressed, lose strength and increase their cross-sectional area as they are convected downstream as shown in figure~\ref{fig:LiteratureReview_HansenVorticity} for data extracted from \citet{Hansen2016} for a WLE NACA0021 aerofoil at $Re_\infty=2230$.
%
\begin{figure}[H]\centering
   \tikzsetnextfilename{LiteratureReview_HansenVorticity}
   \input{\folder/LiteratureReview/HansenVorticity}
   \caption{Streamwise evolution of the SV's core vorticity at two different angles of attack extracted from \citet{Hansen2016}.}
   \label{fig:LiteratureReview_HansenVorticity}
\end{figure}

A direct consequence of the SVs, as pointed out by \citet{VanNierop2008}, is that an upwash and downwash velocity is induced at the troughs and peaks respectively as sketched in figure~\ref{fig:LiteratureReview_inducedVelocity}. The downwash-induced velocity at the peaks decreases the effective angle of attack whereas the upwash-induced velocity increases it at the troughs, which reinforces the hypothesis of flow separation at the troughs.
\begin{figure}[H]\centering
   \tikzsetnextfilename{LiteratureReview_inducedVelocity}
   \input{\folder/LiteratureReview/inducedVelocity}
   \caption{Sketch of the mechanism described in \citet{VanNierop2008} leading to upwash flow in the troughs and downwash in the peaks.}
   \label{fig:LiteratureReview_inducedVelocity}
\end{figure}

The role played by the SV is still in debate, however the majority of the researchers agree that they have a similar effect of that of vortex generators \citep{Fish1995,Miklosovic2004,Hansen2011,Pedro2008,Yoon2011,Skillen2014,Rostamzadeh2014,Wei2015,Rostamzadeh2016,Hansen2016} that enhance momentum exchange between spanwise sections, pulling fluid out of the troughs and entraining fluid to the peaks promoting separation in the former but keeping the boundary layer attached to the latter. \citet{Hansen2011} further explains that as vortex strength in vortex generators (VGs) depends on the device angle, so it does for WLE aerofoils where the device angle is defined by the angle $\gamma_{LE}$. Additionally, \citeauthor*{Hansen2016} also suggest that the effect of the WLE wavelength $\lambda_{LE}$ is to reduce the distance between vortices and hence enhances their interaction in the same way the distance between VGs plays a key role for optimising performance. On the other hand, \citet{VanNierop2008} suggested that the mechanism by which WLE aerofoils delay stall cannot be compared with VGs since the order of magnitude of both $h_{LE}$ and $\lambda_{LE}$ is much larger than the boundary layer thickness whereas, for VGs, the ratio of the device height to the boundary layer thickness $h_{VG}/\delta$ usually takes values between 0.2 and 0.5.

An alternative explanation for the extra lift generated by the WLE aerofoils in the post-stall regime is given by \citet{Miklosovic2004, Miklosovic2007, Johari2007, Weber2011} and \citet{Custodio2015}, who suggest that the extra lift generated is due to lower pressure carried by the SV pairs generated in the leading edge peaks. The mechanism described is similar to the one occurring in delta wings where strong vortices lay on top of the aerodynamic surface creating a pressure difference between the top and bottom surfaces that creates lift. A depiction of the delta-wing effect on WLE aerofoils is shown in figure~\ref{fig:LiteratureReview_Wei2015Vortices} taken from \citet{Wei2015}.
\begin{figure}\centering
   \tikzsetnextfilename{LiteratureReview_Wei2015Vortices}
   \input{\folder/LiteratureReview/Wei2015Vortices}
   \caption{Sketch of the delta wing mechanism extracted from \citet{Wei2015}.}
   \label{fig:LiteratureReview_Wei2015Vortices}
\end{figure}
