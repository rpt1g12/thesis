%=====================SUBSECTION - lowAoA_steadyOverview=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Overview of the time-averaged flow field}\label{sub:lowAoA_steadyOverview}
Figure~\ref{fig:lowAoA_cpContoursTop} depicts the pressure contours on the suction surface of both SLE-1 and WLE-1 models at three different angles of attack. It shows that while for the SLE-1 case the pressure remains almost perfectly two-dimensional for all the incidence angles studied, the undulated leading edge introduces three-dimensional pressure variations on its surface, even at zero incidence angle. As the angle of attack $\alpha$ increases, low-pressure spots start to become more prominent in the WLE-1 case denoted by the closed contour lines in the trough region near the leading edge. As demonstrated earlier in \S\ref{cha:StalledWLE}, the low-pressure spots are the pressure signature of three-dimensional LSBs. It seems that the most negative pressure peak is always located in the trough region of the WLE-1 cases and it is lower than the lowest pressure for the corresponding SLE-1 case.
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{lowAoA_cpContoursTop}
   \input{\folder/lowAoA/cpContoursTop}
   \caption{Contour plots of the time-averaged wall pressure coefficient on the suction side obtained from the (\emph{a}) SLE-1 and (\emph{b}) WLE-1 cases for $\alpha=0\dg$, $6\dg$ and $10\dg$. The number of contour levels is 20 with $\Delta_{C_p}=0.175$.}
   \label{fig:lowAoA_cpContoursTop}
\end{figure}

LSBs spanning the entire geometry can also be seen in the SLE-1 case. The location and extent of the LSB are identified by regions of almost null streamwise pressure gradient, i.e. regions where contour lines in figure~\ref{fig:lowAoA_cpContoursTop} are absent or separated by large portions of the chord length. In contrast, the location of the rear part of the LSBs is identified by regions of high adverse pressure gradient, i.e. regions where contour lines are very close to each other. For the wavy case, downstream of the LSB, it is seen how the waviness in the pressure contour lines is reduced. Since the location of the LSB moves upstream as the angle of attack $\alpha$ is increased, the recovery of the quasi-two-dimensional flow also occurs further upstream for higher angles of attack.

Figure~\ref{fig:lowAoA_cpContoursBot} displays the pressure side counterparts of figure~\ref{fig:lowAoA_cpContoursTop}. Three-dimensional perturbations are also observed on the pressure side for the wavy geometry at all angles of attack. Similarly to what happens on the suction side, wavy features in the pressure contours seem to be stronger at lower $\alpha$ and get progressively weaker as $\alpha$ increases. 
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_cpContoursBot}
   \input{\folder/lowAoA/cpContoursBot}
   \caption{Contour plots of the time-averaged wall pressure coefficient on the pressure side obtained from the (\emph{a}) SLE-1 and (\emph{b}) WLE-1 cases for $\alpha=0\dg$, $6\dg$ and $10\dg$. The number of contour levels is 20 with $\Delta_{C_p}=0.1$.}
   \label{fig:lowAoA_cpContoursBot}
\end{figure}
%

More insight is gained through investigation of pressure distributions at particular spanwise sections presented in figures~\ref{fig:lowAoA_1WLE0cpDist} to \ref{fig:lowAoA_1WLE10cpDist}. Figure~\ref{fig:lowAoA_1WLE0cpDist} clearly reveals the existence of an LSB in both wavy and straight case for $\alpha=0\dg$ through regions of zero streamwise pressure gradient. The pressure coefficient is almost identical in both cases for $x<-0.35$. From that point, pressure distributions for the WLE-1 deviate from the SLE-1 case at all sections plotted. For $-0.15<x<0.05$, the pressure coefficient for the wavy case remains constant for all sections for both the pressure and suction sides. On the other hand, the pressure plateau for the SLE-1 case extends from $x\approx-0.05$ to $x\approx0.2$. At $x\approx0.25$, the characteristic parabolic shape described by \citet{Perry1975} denotes the position of the rear side of the LSB. From figure~\ref{fig:lowAoA_1WLE0cpDist}, it seems clear that for the SLE-1 case: (i) the size of the LSB is bigger and (ii) its location moves further downstream in comparison to those of the WLE-1 case. Nonetheless, the pressure level is lower inside the LSB formed in the WLE-1. Figure~\ref{fig:lowAoA_1WLE0cpDist} also confirms that the lowest pressure is found in the trough section of the WLE-1 case at $x\approx-0.375$, upstream of the LSB.
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_1WLE0_cpDist}
   \input{\folder/lowAoA/1WLE0_cpDist}
   \caption{Profiles of the time-averaged wall pressure coefficient along the streamwise direction for $\alpha=0\dg$ obtained at various spanwise cross-sections for the WLE-1 case compared with the spanwise-averaged profile for the SLE-1 case. Locations of the spanwise cross-sections are indicated in figure~\ref{fig:lowAoA_cpContoursBot}.}
   \label{fig:lowAoA_1WLE0cpDist}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_1WLE6_cpDist}
   \input{\folder/lowAoA/1WLE6_cpDist}
   \caption{Profiles of the time-averaged wall pressure coefficient along the streamwise direction for $\alpha=6\dg$ obtained at various spanwise cross-sections for the WLE-1 case compared with the spanwise-averaged profile for the SLE-1 case. Locations of the spanwise cross-sections are indicated in figure~\ref{fig:lowAoA_cpContoursBot}.}
   \label{fig:lowAoA_1WLE6cpDist}
\end{figure}

When the angle of attack is increased pressure variations along the span become more prominent for the WLE-1 case as shown in figure~\ref{fig:lowAoA_1WLE6cpDist}. Although the magnitude of the most negative pressure peak increases for all sections of the WLE and SLE cases, this is more pronounced in the WLE's T1 section. After the trough, middle sections seem to produce the second lowest pressure peak for the WLE-1 case, although still lower in magnitude than that obtained in the SLE-1 case. The lowest suction peak is found again in the peak section and signs of the presence of an LSB are seen in all three sections via pressure plateaus in the region $-0.4<x<-0.3$. Nevertheless, in contrast with the previous incidence angle ($\alpha=0\dg$) the pressure levels within each of the plateaus are different for each spanwise section. Pressure at both middle and trough sections is identical, but pressure in the peak's plateau is slightly higher (less negative). At this angle of attack, the typical parabolic shape at the end of the LSB can be identified at $x\approx-0.275$. Downstream of this point, a strong pressure recovery is accomplished over the region $-0.275<x<-0.225$, all lines collapse and the two-dimensional character of the flow is recovered. 

On the other hand, the plateau region is located in the region $-0.3<x<-0.1$ for the SLE-1 case. It is confirmed that the length of the LSB is reduced in both cases as the adverse pressure gradient is increased \citep{Horton1968,Gaster1967,Gaster1968,Gaster1975,Pauley1994}, although the reduction is significantly larger for the wavy geometry. For the WLE-1 case at $\alpha=6\dg$, the pressure inside the LSB is lower than that of the SLE-1 but higher on the pressure side of the SLE-1 case along most of the chord. As a consequence, and despite the pressure being higher inside the SLE-1 LSB, the combination of its larger size and the higher pressure on the pressure side results in higher lift production of the SLE-1 model as shown in table~\ref{tab:lowAoA_ClCd}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_1WLE10_cpDist}
   \input{\folder/lowAoA/1WLE10_cpDist}
   \caption{Profiles of the time-averaged wall pressure coefficient along the streamwise direction for $\alpha=10\dg$ obtained at various spanwise cross-sections for the WLE-1 case compared with the spanwise-averaged profile for the SLE-1 and SLE-8 cases. Locations of the spanwise cross-sections are indicated in figure~\ref{fig:lowAoA_cpContoursBot}.}
   \label{fig:lowAoA_1WLE10cpDist}
\end{figure}

Further increase in the angle of attack $\alpha$ leads to the pressure distributions shown in figure~\ref{fig:lowAoA_1WLE0cpDist}. As expected, the most negative pressure is found in the trough section of the WLE-1, and although the pressure in the plateau ($-0.45<x<-0.4$) is the same for sections M1,M2 and T1, the suction peak at both M1 and M2 has disappeared. At the same time, the absence of a pressure plateau region at the P1 section indicates that the LSB gets narrower as $\alpha$ increases. Downstream of the LSB, all sections of the WLE-1 case undergo a strong pressure recovery and the collapse of all curves is achieved at around $x\approx-0.225$.

For $\alpha=10\dg$, both SLE-1 and SLE-8 cases have been included in figure~\ref{fig:lowAoA_1WLE10cpDist}. It appears that the lift increase of the SLE-8 model with respect to the SLE-1 is due to an increased suction peak. While the suction peak for SLE-8 is similar to that of the M1 and M2 sections, the suction peak of SLE-1 is closer to that of the P1 section. It is also observed that the length of the LSB is reduced for larger values of $\kappa_{LE}$. Still, both LSBs for SLE-1 and SLE-8 cases, are longer than the WLE-1 LSB, covering the regions $-0.4<x<-0.25$ and $-0.4<x<-0.275$ respectively. It appears that although the suction level in the SLE-8 LSB is lower than that of the WLE-1 LSB, its bigger dimensions lead to a slightly higher lift production as reflected in table~\ref{tab:lowAoA_ClCd}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_wxContoursTop}
   \input{\folder/lowAoA/wxContoursTop}
   \caption{Contour plots of the streamwise time-averaged wall-shear-stress on the suction side obtained from the (\emph{a}) the SLE-1 and (\emph{b}) WLE-1 cases for $\alpha=0\dg$, $6\dg$ and $10\dg$. The locations of $\avg{\tau_{wx}}=0$ are highlighted by thick black lines.}
   \label{fig:lowAoA_wxContoursTop}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_wxContoursBot}
   \input{\folder/lowAoA/wxContoursBot}
   \caption{Contour plots of the streamwise time-averaged wall-shear-stress on the pressure side obtained from the (\emph{a}) the SLE-1 and (\emph{b}) WLE-1 cases for $\alpha=0\dg$, $6\dg$ and $10\dg$. The locations of $\avg{\tau_{wx}}=0$ are highlighted by thick black lines.}
   \label{fig:lowAoA_wxContoursBot}
\end{figure}
%

Figures~\ref{fig:lowAoA_wxContoursTop} and \ref{fig:lowAoA_wxContoursBot} present contours of the wall shear stress in the streamwise direction for the suction and pressure sides of both wavy and straight leading edge aerofoils. On the suction side (figure~\ref{fig:lowAoA_wxContoursTop}) at $\alpha=0\dg$, a clear region of reversed flow can be identified for both WLE-1 and SLE-1 cases. While separation in the SLE-1 case is two-dimensional, i.e. the streamwise location of separation is the same for all spanwise sections, in the WLE-1 case separation is three-dimensional and does not occur at the same streamwise location for all spanwise sections. In contrast, reattachment is in both cases two-dimensional. As suggested by the pressure distributions shown in figures~\ref{fig:lowAoA_cpContoursTop} to \ref{fig:lowAoA_1WLE10cpDist}, the separation region, which is commonly used to measure the extent of the LSB \citep{Pauley1990,Alam2000,Jones2006,Jones2008,Jones2010}, is larger in the SLE-1 case. For the WLE case at $\alpha=6\dg$, the reversed flow region is entirely contained in a small region in the trough and $\avg{\tau_{wx}}$ remains positive elsewhere. At the same time, surface contours of $\avg{\tau_{wx}}$ for the SLE-1 case still show two-dimensional separation and reattachment. In both cases, straight and wavy, the separation region moves upstream and reduces size as the angle of attack is increased. Therefore, as $\alpha$ is increased to $\alpha=10\dg$, both bubbles move upstream and decrease their size.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_1WLE0_cfDist}
   \input{\folder/lowAoA/1WLE0_cfDist}
   \caption{Profiles of the time-averaged friction coefficient along the streamwise direction for $\alpha=0\dg$ obtained at various spanwise cross-sections for the WLE-1 case compared with the spanwise-averaged profile for the SLE-1 case for the (\emph{a}) suction side and (\emph{b}) pressure side. Locations of the spanwise cross-sections are indicated in figure~\ref{fig:lowAoA_wxContoursBot}.}
   \label{fig:lowAoA_1WLE0cfDist}
\end{figure}

In contrast, on the pressure side, separation (which also occurs on this side) moves downstream as the angle of attack $\alpha$ increases as shown in figure~\ref{fig:lowAoA_wxContoursBot}. At zero incidence, the separation locations are exactly the same as on the suction side. For $\alpha=6\dg$ and $10\dg$, the shape of the separation region is different from that depicted in figure~\ref{fig:lowAoA_wxContoursTop}. On the pressure side separation occurs at a different streamwise location for each spanwise section. Reattachment, on the other hand, does occur at the same streamwise location everywhere in the span at $x=x_{TE}$, i.e. all sections reattach at the trailing edge. The width of the separation region gets wider as $\alpha$ increases which contrast with the behaviour of the LSBs on the suction side.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_1WLE6_cfDist}
   \input{\folder/lowAoA/1WLE6_cfDist}
   \caption{Profiles of the time-averaged friction coefficient along the streamwise direction for $\alpha=6\dg$ obtained at various spanwise cross-sections for the WLE-1 case compared with the spanwise-averaged profile for the SLE-1 case for the (\emph{a}) suction side and (\emph{b}) pressure side. Locations of the spanwise cross-sections are indicated in figure~\ref{fig:lowAoA_wxContoursBot}.}
   \label{fig:lowAoA_1WLE6cfDist}
\end{figure}

Again, in order to get a better description of the streamwise-shear-stress, distributions of the friction coefficient are provided in figures~\ref{fig:lowAoA_1WLE0cfDist} to~\ref{fig:lowAoA_1WLE10cfDist} for $\alpha=0\dg$, $6\dg$ and $10\dg$ respectively. For the WLE case at $\alpha=0\dg$, figure~\ref{fig:lowAoA_1WLE0cfDist} shows that the maximum shear stress peak occurs in the trough region (T1) at $x\approx-0.475$. It is also clear that the trough section is the first to undergo separation at $x=-0.2580$. Middle sections undergo separation at $x=-0.1172$ and the peak section does so at $x=0.0718$. A negative peak in the friction coefficient is observed for all sections immediately before reattachment at $x=0.1268$. Both the negative and positive skin-friction peaks are due to the presence of the reversed-flow-vortex (RFV) at the rear of the LSB (see \S\ref{sec:LSBReview}). The negative peak is a consequence of the flow being pushed backwards by the vortex whereas the positive one is due to the impingement of the flow driven by the vortex onto the wall surface. After reattachment, curves are almost identical. Reattachment comes as a consequence of the transition to turbulence in the separated shear layer \citep{Gaster1967,Gaster1968,Gaster1975} and therefore, recovery of the two-dimensional character is accomplished only after transition, where the turbulent motion promotes the energy exchange between different spanwise sections. SLE separation is located at $x=-0.1406$ and reattachment at $x=0.3516$. The $\avg{C_f}$ distribution on the pressure side is identical to that of the suction side for both WLE and SLE cases.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_1WLE10_cfDist}
   \input{\folder/lowAoA/1WLE10_cfDist}
   \caption{Profiles of the time-averaged friction coefficient along the streamwise direction for $\alpha=10\dg$ obtained at various spanwise cross-sections for the WLE-1 case compared with the spanwise-averaged profile for the SLE-1 and SLE-8 cases for the (\emph{a}) suction side and (\emph{b}) pressure side. Locations of the spanwise cross-sections are indicated in figure~\ref{fig:lowAoA_wxContoursBot}.}
   \label{fig:lowAoA_1WLE10cfDist}
\end{figure}

Figure~\ref{fig:lowAoA_1WLE6cfDist} presents the friction coefficient distributions at $\alpha=6\dg$. At this angle of attack, only the trough section undergoes separation on the suction side, from $x=-0.4350$ to $x=-0.2388$. The skin friction peaks immediately before and after reattachment are stronger than those at $\alpha=0\dg$ which could indicate that the transition process gets more violent as the angle of attack increases. The region of negative skin friction for the SLE-1 case covers the region $-0.3594<x<-0.0039$. On the pressure side, all sections undergo separation, including the SLE-1. Separation occurs first in the trough section followed by the middle sections and finally the peak section. The SLE-1 separates at the same streamwise position as the middle sections of the WLE-1.

Finally, figure~\ref{fig:lowAoA_1WLE10cfDist} shows the friction coefficient at $\alpha=10\dg$ for the WLE-1, SLE-1 and SLE-8 cases. Separation in the WLE-1 case still only occurs in the trough section from $-0.4465<x<-0.3311$. In this case, however, the negative skin friction peak surpasses in value that of the SLE-1 in contrast to the lower angle of attack cases. Separation on the SLE-1 and SLE-8 cases is contained in the region $-0.4180<x<-0.1328$ and $-0.4162<x<-0.1719$ respectively. In all cases, the separation region moves upstream and reduces size. Regarding the SLE geometry, the SLE-8 case separates at almost the same streamwise location as the SLE-1 but it re-attaches earlier on leading to a shorter LSB. The negative skin friction peak also occurs further upstream for the SLE-8 case and it is of larger magnitude. This indicates that transition to turbulence is encouraged by the use of wider computational domains. On the pressure side, separation moves downstream for all cases and sections and the maximum (positive) value is found in both SLE cases.
