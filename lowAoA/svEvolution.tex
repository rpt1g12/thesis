%=====================SUBSECTION - svEvolution=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Evolution of streamwise vortices inside the LSBs}\label{sub:lowAoA_svEvolution}

In \S\ref{cha:StalledWLE} the importance of the streamwise vortex pair for high angle of attack conditions has been demonstrated. Only vortices in contact with separated shear layers (SSLs) were able to successfully develop. These vortices served as a buffer protecting the LSBs from bursting. In this chapter, for pre-stall angles of attack, the same methodology of \S\ref{sub:StalledWLE_SVevolution} is used where cross-flow sections of constant $x$ show contours of the streamwise vorticity component. Figure~\ref{fig:lowAoA_OmegaX0}, \ref{fig:lowAoA_OmegaX6} and \ref{fig:lowAoA_OmegaX10} show contours of $\avg{\omega_x}$ inside the LSBs for the WLE-1 case at $\alpha=0\dg$, $6\dg$ and $10\dg$ respectively. 

For $\alpha=0\dg$ in figure~\ref{fig:lowAoA_OmegaX0}, it is seen that two streamwise vortex sheets are formed on each side of the P1 section, which as explained in \S\ref{sub:StalledWLE_SVorigin} originate in the leading edge at the peak sections due to the turning of spanwise vorticity. Similarly to what happens at high angles of attack, the SV sheets are uplifted by the presence of the LSB. It is not until $x=0.0306$ that an SV pair starts to show up through the contour of streamwise vorticity. Nonetheless, this vortex pair seems to be of much weaker intensity than that observed in the case of $\alpha=20\dg$ shown in the previous chapter (see \S\ref{sub:StalledWLE_SVevolution}). In fact, at $x=0.0947$ the initial SVs are no longer present, nevertheless, a new pair of stronger SVs is observed. Interestingly, these SVs are of opposite sign to the initial ones and also to those shown previously in the stalled case (see \S\ref{cha:StalledWLE}). The vortices referred to as Lower SVs herein, remain present for a longer distance downstream. However, the distance between vortices seems to increase as they are convected downstream until they meet their neighbouring vortex at the peak section downstream of the reattachment position $x=0.1909$.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_OmegaX0}
   \input{\folder/lowAoA/OmegaX0}
   \caption{Contour plots of time-averaged streamwise vorticity ($\avg{\omega_x}$) for the WLE-1 case at $\alpha=0\dg$ at various streamwise locations. The frames cover from the LSB's start at $x=x_s$ to some distance downstream of the LSB's end at $x=x_r$.}
   \label{fig:lowAoA_OmegaX0}
\end{figure}

Figure~\ref{fig:lowAoA_OmegaX6} shows the streamwise evolution of the SVs in the case of $\alpha=6\dg$. Likewise the previous case, initially the streamwise vorticity field is dominated by the SV sheets that are then uplifted by the LSB. Inside the LSB, and immediately below the uplifted SV sheets, due to the turning of the spanwise vorticity present on the shear layer that encloses the LSB, concentrations of streamwise vorticity are appreciated. At $x=-0.3206$, again an initial weaker vortex pair can be observed inside the LSB. However, as the rear of the LSB is approached, both the reoriented/turned $\omega_z$ and SV sheets break apart and start grouping into two vortex pairs of opposite sign about $x=-0.2715$. The turned $\omega_z$ vortices (lower SVs in figure~\ref{fig:lowAoA_OmegaX6}) lay inside the LSB and beneath the vortices derived from the SV sheets (upper SVs in figure~\ref{fig:lowAoA_OmegaX6}). As mentioned, each lower streamwise vortex is of opposite sign to its upper counterpart. The vortex on the right side of T1 is negative and positive on the left. In contrast, the upper right vortex is positive and the upper left negative. Both SV pairs originate from originally spanwise vorticity that is reoriented by the streamwise velocity gradient in the $z$-axis, i.e. $\partial{u}/\partial{z}$. Nonetheless, given that the sign of $\partial{u}/\partial{z}$ changes from the leading edge region to the LSB region as explained in figure~\ref{fig:StalledWLE_turnskt} and $\omega_z$ remains negative on the suction side, the signs of the resulting streamwise vortices are of opposite sign. Clearly, the lower SVs are of higher magnitude than the upper ones and therefore they take longer to be diffused and they can be easily discerned downstream of the LSB at $x=-0.1898$
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_OmegaX6}
   \input{\folder/lowAoA/OmegaX6}
   \caption{Contour plots of time-averaged streamwise vorticity ($\avg{\omega_x}$) for the WLE-1 case at $\alpha=6\dg$ at various streamwise locations. The frames cover from the LSB's start at $x=x_s$ to some distance downstream of the LSB's end at $x=x_r$.}
   \label{fig:lowAoA_OmegaX6}
\end{figure}
%
Increasing the angle of attack to $\alpha=10\dg$ results in an increased intensity of the streamwise vortices and vortex sheets. The evolution of them is quite similar to the $\alpha=6\dg$ case. However, due to the shorter extent of the LSB, all the three vortex pairs are forced to coexists at least for a small time. As an example, figure~\ref{fig:lowAoA_OmegaX10} at $x=-0.3696$ shows how all of the SV pairs stack on top of each other: the weak SVs immediately above the wall, then the lower SVs and on the top the upper SVs. Again, as the cross-flow slice is moved downstream, the weak SVs disappear and only the upper and lower SV pairs remain, although the intensity of the later is much higher as it happened for lower angles of attack.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_OmegaX10}
   \input{\folder/lowAoA/OmegaX10}
   \caption{Contour plots of time-averaged streamwise vorticity ($\avg{\omega_x}$) for the WLE-1 case at $\alpha=10\dg$ at various streamwise locations. The frames cover from the LSB's start at $x=x_s$ to some distance downstream of the LSB's end at $x=x_r$.}
   \label{fig:lowAoA_OmegaX10}
\end{figure}
%
In order to get three-dimensional insight of the streamwise vortical structures identified in figures~\ref{fig:lowAoA_OmegaX0} to \ref{fig:lowAoA_OmegaX10}, iso-contours of the second invariant of the velocity gradient coloured by the streamwise component of the vorticity vector are provided in figure~\ref{fig:lowAoA_isoQ}. On it, the lower and upper SVs can be identified, although not without difficulty. More clearly, the sides/legs of the LSB can be observed. This LSB's legs delimit the LSB in the spanwise direction and appear as streamwise vortical structures which could not be identified in figures~\ref{fig:lowAoA_OmegaX0} to \ref{fig:lowAoA_OmegaX10}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_isoQ}
   \input{\folder/lowAoA/isoQ}
   \caption{Iso-surfaces of constant time-averaged Q-criterion ($\avg{Q}=100$) coloured by the time-averaged streamwise vorticity for the WLE-1 case at (\emph{a}) $\alpha=0\dg$, (\emph{b}) $\alpha=6\dg$ and (\emph{c}) $\alpha=10\dg$.}
   \label{fig:lowAoA_isoQ}
\end{figure}

Another quantity that is usually used for vortex identification purposes is helicity $H_e$. Helicity is a measure of the vorticity-velocity alignment. Mathematically, helicity is the projection of the vorticity vector in the direction of the velocity expressed as
\begin{equation}
\label{eq:helicity}
	H_e=\avg{\bm{u}}\cdot\avg{\bm{\omega}}.
\end{equation}
Since the most important vortical structures present in the flow around an aerofoil with a WLE are aligned with the flow direction, helicity gives a better representation of the SVs in comparison with the $Q$-criterion which is used to identify all kinds of vortical structures, independently of their orientation. Iso-surfaces of constant helicity are displayed in figure~\ref{fig:lowAoA_helicity}. Using this quantity, the upper an lower vortical structures are more easily discerned. It is clear that the upper SVs, as it has been discussed above, originate from the vortex sheets originated in the leading edge region, and with the LSB's legs, form a connected region that spreads from the leading edge to the rear of the LSB. The lower SVs, on the other hand, are originated inside the LSB and they extend further downstream of the LSB and inside the turbulent boundary layer region.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_helicity}
   \input{\folder/lowAoA/helicity}
   \caption{Volumes of positive (red) and negative (blue) helicity for the WLE-1 case at (\emph{a}) $\alpha=0\dg$, (\emph{b}) $\alpha=6\dg$ and (\emph{c}) $\alpha=10\dg$.}
   \label{fig:lowAoA_helicity}
\end{figure}

