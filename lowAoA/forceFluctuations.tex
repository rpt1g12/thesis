%=====================SUBSECTION - lowAoA_forceFluctuations=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Aerodynamic force fluctuations}\label{sub:lowAoA_forceFluctuations}
At low angles of attack, table~\ref{tab:lowAoA_ClCd} shows that fluctuations on the aerodynamic forces acting on the aerofoils are low both for WLE and SLE cases; many orders of magnitude lower in comparison with the stalled case of \S\ref{cha:StalledWLE}. The spectra of these fluctuations are shown in figures~\ref{fig:lowAoA_forcePSD0high} to~\ref{fig:lowAoA_forcePSD10high} for both lift and drag fluctuations (including pressure and viscous components) for the WLE and SLE cases at $\alpha=0\dg$, $6\dg$ and $10\dg$. In this figure, however, only the high-frequency spectra are shown, i.e. only ${fL_c}/{u_\infty}=f_{L_c}>1$ is shown. It is to be noted that, in contrast to what was done in \S\ref{sub:StalledWLE_forceFluctuations}, the characteristic length used to produce a dimensionless frequency is not the projection of the chord on the flow-normal direction, i.e. $L_c\sin\alpha$; in this case, the just the chord length has been used since $\sin(\alpha=0\dg)=0$. Besides this, it is found that using this frequency scaling characteristic tonal frequencies are found to converge to similar values for all the investigated cases.
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{lowAoA_forcePSD0high}
   \input{\folder/lowAoA/forcePSD0high}
   \caption{Power spectral density of the (\emph{a} and \emph{c}) lift and (\emph{b} and \emph{d}) drag and their pressure and viscous components for the (\emph{a} and \emph{b}) the WLE-1 and (\emph{c} and \emph{d}) the SLE-1 cases at $\alpha=0\dg$. Pressure component is denoted by a ``$p$'' subscript whereas ``$v$'' subscript denotes the viscous component. The frequency axis is expressed in terms of the dimensionless frequency $f_{L_c}=fL_c/u_\infty$.}
   \label{fig:lowAoA_forcePSD0high}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_forcePSD6high}
   \input{\folder/lowAoA/forcePSD6high}
   \caption{Power spectral density of the (\emph{a} and \emph{c}) lift and (\emph{b} and \emph{d}) drag and their pressure and viscous components for the (\emph{a} and \emph{b}) the WLE-1 and (\emph{c} and \emph{d}) the SLE-1 cases at $\alpha=6\dg$. Pressure component is denoted by a ``$p$'' subscript whereas ``$v$'' subscript denotes the viscous component. The frequency axis is expressed in terms of the dimensionless frequency $f_{L_c}=fL_c/u_\infty$.}
   \label{fig:lowAoA_forcePSD6high}
\end{figure}

In the case of $\alpha=0\dg$, shown in figure~\ref{fig:lowAoA_forcePSD0high}, it is seen that the total lift and drag spectra are basically the same as their respective pressure components. Fluctuations of the viscous components of drag and lift are smaller than the pressure ones at all frequencies investigated, and only at higher frequencies, i.e. $f{L_c}>100$, comparable levels of the energy content of each component are found. The spectra do not show any clear peaks, only the lift spectra for both the WLE-1 and SLE-1 cases show a mild local peak at around $f_{L_c}\approx 3$, although the peak seems to be more clear for the SLE-1 case.

For $\alpha=6\dg$, figure~\ref{fig:lowAoA_forcePSD6high} presents spectra distributions that show more clear signs of discrete tone frequencies for both lift and drag at $f_{L_c}\approx 3$ and its first harmonic for the WLE-1 case and $f_{L_c}\approx 4.5$ for the SLE-1 case. It is observed that the lift spectrum is mostly driven by the pressure component at all frequencies, however, the drag spectrum seems to be governed by the pressure component in the mid-to-low frequency range and by the viscous component in the mid-to-high range. This can be explained by the fact that the viscous component of drag spectra is mostly affected by the presence of a turbulent boundary layer, which contains small-scale flow features that dominate the higher end of the spectrum. On the other hand, pressure fluctuations are mostly due to big scale vortical structures such as the spanwise vortices that originate in the LSB's shear layer region. It is also worth noting that if scaled with the cross-flow projection of the chord, i.e. as in \S\ref{sub:StalledWLE_forceFluctuations} ($f^*={fL_c\sin\alpha}/{u_\infty}$), the frequency at which the peaks are located is higher than that identified in the stalled case as shown in table~\ref{tab:lowAoA_peaksFreq}.
%
%\begin{figure}[H]\centering
%   \tikzsetnextfilename{lowAoA_forcePSD0}
%   \input{\folder/lowAoA/forcePSD0}
%   \caption{forcePSD0}
%   \label{fig:lowAoA_forcePSD0}
%\end{figure}

%\begin{figure}[H]\centering
%   \tikzsetnextfilename{lowAoA_forcePSD6}
%   \input{\folder/lowAoA/forcePSD6}
%   \caption{forcePSD6}
%   \label{fig:lowAoA_forcePSD6}
%\end{figure}
%\begin{figure}[H]\centering
%   \tikzsetnextfilename{lowAoA_forcePSD10}
%   \input{\folder/lowAoA/forcePSD10}
%   \caption{forcePSD10}
%   \label{fig:lowAoA_forcePSD10}
%\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_forcePSD10high}
   \input{\folder/lowAoA/forcePSD10high}
   \caption{Power spectral density of the (\emph{a} and \emph{c}) lift and (\emph{b} and \emph{d}) drag and their pressure and viscous components for the (\emph{a} and \emph{b}) the WLE-1 and (\emph{c} and \emph{d}) the SLE-8 cases at $\alpha=10\dg$. Pressure component is denoted by a ``$p$'' subscript whereas ``$v$'' subscript denotes the viscous component. The frequency axis is expressed in terms of the dimensionless frequency $f_{L_c}=fL_c/u_\infty$.}
   \label{fig:lowAoA_forcePSD10high}
\end{figure}
%
\begin{table}[t!]
\centering
\caption{Equivalences between the dimensionless frequencies $f_{L_c}=fL_c/u_\infty$ and $f^*=fL_c\sin\alpha/u_\infty$ for each different cases.}
\label{tab:lowAoA_peaksFreq}
\begin{tabular}{c c c c}
&WLE-1 $\alpha=6\dg$ &SLE-1 $\alpha=6\dg$ &SLE-8 $\alpha=20\dg$\\
\hline
$f_{L_c}$ &3 &4.5 &0.58\\
$f^*$ &0.31 &0.47 &0.2
\end{tabular}
\end{table}
%
For the case of $\alpha=10\dg$ shown in figure~\ref{fig:lowAoA_forcePSD10high}, local frequency peaks cannot be found in the lift spectra for ${fL_c}/{u_\infty}<10$ as it happened for the $\alpha=0\dg$ and $6\dg$ cases. Instead, in this case, the energy seems to be spread across a wider range of frequencies from $10<{fL_c}/{u_\infty}<100$. On the other hand, the drag spectrum for the WLE-1 case presents a distinct peak at $f_{L_c}\approx 28$. This peak, as it will be shown in the following section (\S\ref{sub:LSBshedding}), is due to the vortex shedding event present in the LSB's shear layer region. Again, it is observed that only the high end of the drag spectrum is governed by the viscous forces. And more interestingly, the LSB's vortex shedding does not produce any energy peaks at the local frequency of $f_{L_c}\approx 28$ which reinforces the idea that pressure fluctuations are mainly governed by large-scale flow features such as the spanwise vortical structures produced by inviscid instabilities in the LSB's shear layer \citep{Pauley1990, Watmuff1999}.
