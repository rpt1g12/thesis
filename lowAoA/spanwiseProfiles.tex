%=====================SUBSECTION - spanwiseProfiles=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Spanwise profiles and LSB parametrisation}\label{sub:lowAoA_spanwiseProfiles}
In \S\ref{sub:lowAoA_steadyOverview} and \S\ref{sub:lowAoA_chaLengths} streamwise profiles of $\avg{C_p}$ and $\avg{C_f}$ have been analysed; in this section, spanwise profiles are analysed instead, and together with the use of the separation and reattachment points, the three dimensional LSB present in the WLE cases is parametrised. Spanwise profiles are only presented for the WLE cases since the spanwise distribution for the SLE cases is almost constant. 
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{lowAoA_1WLE0cpspanProfile}
   \input{\folder/lowAoA/1WLE0cpspanProfile}
   \caption{Spanwise profiles of the time-averaged pressure coefficient at the key streamwise positions (KSP) of table~\ref{tab:lowAoA_chaLengths} at (\emph{a}) $\alpha=0\dg$, (\emph{b}) $\alpha=6\dg$ and (\emph{c}) $\alpha=10\dg$.}
   \label{fig:lowAoA_1WLE0cpspanProfile}
\end{figure}

Figure~\ref{fig:lowAoA_1WLE0cpspanProfile} depicts the spanwise profiles of the pressure coefficient at the KSP of table~\ref{tab:lowAoA_chaLengths}. At $x=x_{mp}$ the pressure distribution highly resembles a sinusoid with the same wavelength as the leading edge. At a position immediately downstream of the separation point ($x_s^+$) pressure is constant across the entire span for $\alpha=0\dg$. For $\alpha=6\dg$ and $10\dg$ on the other hand, pressure is only constant inside the LSB and outside of it rises, which creates a favourable pressure gradient from peak to trough. At the start of the pressure recovery position $x_{rp0}$, pressure is still constant across the span for $\alpha=0\dg$ but for $\alpha=6\dg$, pressure follows a similar spanwise profile to that at $x_s^+$, only that pressure is higher there. For $\alpha=10\dg$ and $x=x_{rp0}$, pressure is lower in the LSB region compared with the spanwise profile at $x=x_s^+$. At the position of maximum streamwise pressure gradient ($x_{rp1}$), for $\alpha=0\dg$ pressure is higher in the T1 section ($z=-0.25L_z$). In contrast, for $\alpha=6\dg$ and $10\dg$ pressure remains lower inside the LSB and maximum in the peak section ($z=0.25L_z$). For all angles of attack, it appears that at $x_{rp1}$ pressure across the width of the LSB is no longer constant and it has a local maximum at $z=-0.25L_z$. At a streamwise position immediately downstream of the reattachment position ($x_r^+$), the spanwise profiles of the pressure coefficient show that maximum pressure is achieved at the trough section for all angles of attack. Therefore, downstream of the LSB, the spanwise pressure gradient is reversed and a favourable pressure gradient is found from trough to peak.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_1WLE0cfspanProfile}
   \input{\folder/lowAoA/1WLE0cfspanProfile}
   \caption{Spanwise profiles of the time-averaged skin friction coefficient at five streamwise positions $x_i=x_s+i\cdot l_{LSB}/8$ for $i=0,1,...,4$  at (\emph{a}) $\alpha=0\dg$, (\emph{b}) $\alpha=6\dg$ and (\emph{c}) $\alpha=10\dg$.}
   \label{fig:lowAoA_1WLE0cfspanProfile}
\end{figure}

Similarly, for $\alpha=0\dg$, $6\dg$ and $10\dg$, figure~\ref{fig:lowAoA_1WLE0cfspanProfile} shows spanwise profiles of the friction coefficient at five different constant streamwise locations from the start of the LSB to half the length of the LSB, i.e from $x=x_s$ to $x=x_s+l_{LSB}/2$. Each profile in figure~\ref{fig:lowAoA_1WLE0cfspanProfile} is separated by $\Delta_x=l_{LSB}/(2\cdot4)$. For all angles of attack, the skin friction profiles initially resemble the sinusoidal profile that defines the leading edge geometry. However, the sinusoid-like profiles are sharper in the trough sections ($z=-0.25L_z$) and wider in the peaks ($z=0.25L_z$). Moving downstream, the profiles tend to form a plateau of constant skin friction in the trough region while keeping an (inverse) parabolic shape in the peak ($z=0.25L_z$).
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_1WLEZab}
   \input{\folder/lowAoA/1WLEZab}
   \caption{Locations of $z_{LSB0}$ and $z_{LSB1}$ with the streamwise position $x\in[x_s,(x_s+x_r)/2]$ for the WLE-1 case at $\alpha=0\dg$, $6\dg$ and $10\dg$. Each series of points defines the edges of the LSB in platform view. Dashed lines are second order polynomials least-squared-fitted whose parameters are given in (\ref{eq:parabola}) and its values in table~\ref{tab:lowAoA_fhk}.}
   \label{fig:lowAoA_1WLEZab}
\end{figure}

From the profiles of figure~\ref{fig:lowAoA_1WLE0cfspanProfile} the width of the LSB can be measured by the distance between the crossing points of $\avg{C_f}$ on either side of the trough section. That is, two spanwise locations $z_{LSB0}$ and $z_{LSB1}$ can be defined such that 
\begin{equation}
\label{eq:zLSB}
\begin{aligned}
z_{LSB0}&=\text{min}(z)&\text{for}\quad z\in\left\{z>z_{T1}\,\mid\,\avg{C_f(z)}=0\right\}, \\
z_{LSB1}&=\text{max}(z)&\text{for}\quad z\in\left\{z<z_{T1}\,\mid\,\avg{C_f(z)}=0\right\}. \\
\end{aligned}
\end{equation}

The locations of $z_{LSB0}$ and $z_{LSB1}$ are plotted in figure~\ref{fig:lowAoA_1WLEZab} as a function of the streamwise coordinate $x$ for the three angles of attack. Along with them, a parabolic curve fitted by the least-squares method is also included. It is clear that the fit is almost perfect for $\alpha=0\dg$ and $\alpha=6\dg$ with minimal deviations near the vertex for $\alpha=10\dg$. The equation of a parabola is given by 
%
\begin{equation}
\label{eq:parabola}
	p(x)=\frac{1}{4f}\left(x-h\right)^2+k,
\end{equation}
%
where $4f$, commonly known as \emph{latus rectum}, is the distance between the two branches of the parabola passing through its focus, $h$ is the horizontal distance between the focus (F) and $y$-axis and $k$ is the vertical distance between the vertex (V) and the $x$-axis. A graphical description of the parabola's parameters is given in figure~\ref{fig:lowAoA_parabola} and the exact values for the parabolic fits of figure~\ref{fig:lowAoA_1WLEZab} are given in table~\ref{tab:lowAoA_fhk}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_parabola}
   \input{\folder/lowAoA/parabola}
   \caption{Definition of the parameters of a generic parabola ($f$, $h$ and $k$ in (\ref{eq:parabola})) where $V$ and $F$ are the vertex and focal points.}
   \label{fig:lowAoA_parabola}
\end{figure}
%
\begin{table}[t!]
\centering
\caption{Parameters defining the least-squares fit of a parabola over the locus of points defined by (\ref{eq:zLSB}) which define the platform shape of the LSB in the WLE-1 case for three different angles of attack. Additionally, differences with the spanwise coordinate of the T1 section $z_{T1}$ and the streamwise coordinate of the most upstream separation point $x_s$ are provided for reference.}
\label{tab:lowAoA_fhk}
%\resizebox{1\textwidth}{!}{
\begin{tabular}{c c c c c c}
$\alpha[\dg]$  &$f$   &$h$  &$k$ &$(h-z_{T1})/L_c$  &$(k-x_s)/L_c$   \\
\hline
0   &0.0013   &-0.0275  &-0.2611 &0.0  &0.0031\\
6   &0.0012   &-0.0275  &-0.4373 &0.0  &0.0023\\
10  &0.0015   &-0.0275  &-0.4680 &0.0  &0.0215
\end{tabular}%}
\end{table}

In the case of the parabolas defining the edges of the LSB, the symmetry axis is located at the P1 spanwise location ($z=z_{T1}=h=-0.0275$) as shown in table~\ref{tab:lowAoA_fhk}. On the other hand, parameter $k$ represents the streamwise position of the most upstream separation point $x=x_s\approx k$. For the latter, the last column in table~\ref{tab:lowAoA_fhk} indicates that the parabolic model for the LSB slightly deteriorates with increasing $\alpha$, although the deviation, even for $\alpha=10\dg$ is less than 3\% of the chord length. Finally, parameter $f$ gives a good measure of the LSB's width since it represents one-quarter of the LSB's width at $x=k+f$.

