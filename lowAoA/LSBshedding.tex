%=====================SUBSECTION - LSBshedding=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{LSB vortex shedding}\label{sub:LSBshedding}
The shedding of spanwise vortical structures in the LSB region has been anticipated in the previous section. In this section, a series of 6 probes were placed along the location of the boundary layer edge (or the LSB's edge) in a similar fashion to those used in \S\ref{sub:StalledWLE_vortexDynamics}. The spectral content of the probe readings for the SLE-1 and WLE-1 cases at $\alpha=6\dg$ and $10\dg$ are presented in figures~\ref{fig:lowAoA_WLE06LSBshedding} and \ref{fig:lowAoA_SLE06LSBshedding}. 
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{lowAoA_WLE06LSBshedding}
   \input{\folder/lowAoA/WLE06LSBshedding}
\caption{Power spectral density (PSD) of fluctuating pressure in the WLE-1 case obtained at some probe points (from the most upstream probe $p_0$ to $p_5$, the most downstream one): the locations of the probe points aligned on the shear layer for the (\emph{a}) $\alpha=6\dg$ and (\emph{b}) $\alpha=10\dg$; PSDs obtained at the probe points for (\emph{c}) $\alpha=6\dg$ and, (\emph{d}) for $\alpha=10\dg$. All probes are located in the spanwise section corresponding to the trough section. The location of the separation and reattachment positions are denoted by dotted lines in the top two sub-figures.}
   \label{fig:lowAoA_WLE06LSBshedding}
\end{figure}
%
Figure~\ref{fig:lowAoA_WLE06LSBshedding} shows the spectral content of a series of probes aligned with the edge of the boundary layer for the WLE-1 case for $\alpha=6\dg$ and $10\dg$. For both angles of attack, a clear peak is observed in the spectra at $f_{L_c}\approx 28$, the same frequency of the peak observed in figure~\ref{fig:lowAoA_forcePSD0high}\emph{d}. This is identified to be the fundamental LSB shedding frequency, $f_{LSB}=28f_{L_c}$. For the three most upstream probes, the spectra show also peaks at the first and second harmonics. As the probes move upstream, the energy content of their signals increases and some of the energy is leaked towards lower frequencies, and the harmonics are noticeably reduced, especially for the two most upstream probes $p_4$ and $p_5$.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_SLE06LSBshedding}
   \input{\folder/lowAoA/SLE06LSBshedding}
\caption{Power spectral density (PSD) of fluctuating pressure in the SLE-1 case obtained at some probe points (from the most upstream probe $p_0$ to $p_5$, the most downstream one): the locations of the probe points aligned on the shear layer for the (\emph{a}) $\alpha=6\dg$ and (\emph{b}) $\alpha=10\dg$; PSDs obtained at the probe points for (\emph{c}) $\alpha=6\dg$; and, (\emph{d}) for $\alpha=10\dg$. The location of the separation and reattachment positions are denoted by dotted lines in the top two sub-figures.}
   \label{fig:lowAoA_SLE06LSBshedding}
\end{figure}
%
For the SLE-1 case, shown in figure~\ref{fig:lowAoA_SLE06LSBshedding}, it can be seen that the shedding frequency of the spanwise vortical structures is exactly the same as in the WLE case. Harmonics at higher frequencies are also observed for the most upstream probes as well as the growth of energy content for frequencies in a wide range centred around $f_{L_c}\approx 10$. In contrast to the wavy case, the two most upstream probes do not show any local peaks at the fundamental LSB shedding frequency $f_{LSB}$.

As it has been mentioned, the clear discrete tone $f_{LSB}$ present on all the pressure probes inside the LSB for both the SLE and WLE cases is related to the shedding of spanwise vortical structures. Given that the mechanics of the shedding event are very similar for all cases, only the WLE-1 case at $\alpha=10\dg$ is discussed here. Figure~\ref{fig:lowAoA_SLE06LSBshedding} presents 24 snapshots of the flow field at the trough spanwise section, therefore, this plane section cuts through the middle of the spanwise vortical structures. On the first snapshot at $t_0=155$ three main vortical structures (A, B and C) are identified. Structure A is an anti-clockwise spanwise vortex that is being convected downstream by the mean flow. 

Structure C is another anti-clockwise spanwise vortex located some distance upstream of A and which seems to be a latter occurrence of the vortex shedding event. Structure B is a small clockwise spanwise vortex that seats in between vortices A and C. It seems that C is created by the presence of strong anti-clockwise vortices such as A that pushes the near-wall flow upstream creating a region of positive spanwise vorticity due to $\partial{u}/\partial{y}<0$. Moving forward in time, on the one hand, it can be seen that both anti-clockwise vortices A and C are convected downstream at very similar speeds. This can be roughly estimated by comparing the two dash-dotted lines that connect the vortex cores trough snapshots $t_0$ to $t_0+7\Delta_t$ on the first column of figure~\ref{fig:lowAoA_1WLE10LSBPics}. On the other hand, vortex B seems to be moving upstream at a very slow pace. As a result, it is seen how C climbs over B as it convects downstream. Also at $t_0+3\Delta$ a new vortex D is formed. It appears that the region of vortex formation is located around $x\approx-0.42$ for the WLE-1 case at $\alpha=10\dg$. Vortex D seems to convect downstream at a similar speed of that of vortices A and C.

On the second column of figure~\ref{fig:lowAoA_1WLE10LSBPics}, comprising from $t_0+8\Delta_t$ to $t_0+15\Delta_t$, shows that vortices A and C reduce their convection speed and as a result, the distance between the vortices A, C and D is reduced. In fact, vortex A seems to be moving upstream at a very slow pace in the region of the reversed-flow-vortex (RFV). Therefore, it can be concluded that the appearance of the RFV structure in the time-averaged flow field is due to vortices being ``stuck'' in the rear part of the LSB. A secondary effect of vortex A being ``stuck'' is the merging of vortices C and D. At $t_0+13\Delta_t$, there appears to be a set of streamlines connecting C and D, and eventually, at $t_0+16\Delta$ the merging process is finished.

On the rightmost column of figure~\ref{fig:lowAoA_1WLE10LSBPics}, it can be seen how finally vortex A gives way to the newly born CD vortex, who takes its place at the RFV position, and keeps convecting downstream towards the turbulent region downstream of the LSB where due to vortex stretching breaks down into the smaller vortical structures that characterise a turbulent boundary layer. Meanwhile, the near-wall-clockwise vortex B keeps moving slowly upstream until at $t_0+21\Delta_t$ loses strength and disappears. 

In addition, from the pressure coefficient contours of figure~\ref{fig:lowAoA_1WLE10LSBPics}, it can be concluded that, although the vortices are created ``off-the-wall'', when they reach the rear side of the LSB, they have grown in size and strength, trapping low-pressure in their cores which is eventually transmitted to the wall and leads to the low-pressure zone inside the LSB seen in figure~\ref{fig:lowAoA_cpContoursTop}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_1WLE10LSBPics}
   \input{\folder/lowAoA/1WLE10LSBPics}
   \caption{A series of instantaneous snapshots for the WLE-1 at $\alpha=10\dg$ showing the vortex shedding cycle inside the LSB by means pressure coefficient contours and integrated streamlines coloured by spanwise vorticity. The time series comprises 24 snapshots from $t_0=155$ to $t_23=t_0+23\Delta_t$ with $\Delta_t=1/64$. Four main structures A, B, C and D are identified and followed through the cycle. Additional dash-dotted lines are provided make the tracking of the structures easier.}
   \label{fig:lowAoA_1WLE10LSBPics}
\end{figure}

