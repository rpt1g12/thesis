%=====================SUBSECTION - blAnalysis=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Boundary layer analysis of the trough section}\label{sub:blAnalysis}
In the previous subsection (\S\ref{sub:lowAoA_spanwiseProfiles}) a characterisation of the platform shape of the LSBs formed in the trough regions of the WLE modes has been given. In this subsection, the boundary layer characteristics of the trough section are described with the intention of providing a characterisation of the frontal shape of the LSBs. The computation of the displacement and momentum thickness $\delta^*$ and $\theta$ requires the knowledge of the local free stream velocity which is not clearly defined in the cases of curved wall-bounded flows \citep{Spalart2000}. In this work, a pseudo-velocity $u'(x,y)$ is defined as
\begin{equation}
\label{eq:pseudoUe}
	u'(x,y)=-\int_{0}^{y} \avg{\omega_z}(x,y')\,dy',
\end{equation}
which leads to 
\begin{equation}
\label{eq:blThicknesses}
	\begin{aligned}
	\delta^*&=\int_{0}^{\infty} \left(1-\frac{\rho(y) u'(y)}{\rho_e u_e}\right)\,dy, \\
	\theta&=\int_{0}^{\infty} \frac{\rho(y)u'(y)}{\rho_e u_e} \left(1-\frac{u'(y)}{u_e}\right)\,dy \\
	H&={\delta^*}/{\theta}
	\end{aligned}
\end{equation}
where $H$ is the shape factor and $u_e=0.99\cdot u'(x,\infty)$ and $\rho_e=\rho(x,\delta_{99})$ are the boundary layer edge velocity and density respectively. The wall-normal distance at which the pseudo-velocity reaches 99\% of the free stream value is used to define the boundary layer thickness $\delta_{99}$, i.e. $u'(x,\delta_{99})=u_e$.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_blAnalysis0}
   \input{\folder/lowAoA/blAnalysis0}
   \caption{Evolution of (\emph{b}) boundary layer thickness $\delta_{99}$ and displacement thickness $\delta^*$, (\emph{c}) momentum thickness $\theta$ and (\emph{d}) shape factor $H$ along the LSB on the trough section ($z=0.25L_z$). Additional plots of the (\emph{e}) velocity magnitude $\avg{|\bm{u}|}$ and (\emph{f}) pressure coefficient $\avg{C_p}$ along the boundary layer edge are provided. The location of the boundary layer edge is depicted in (\emph{a}) along the pressure coefficient contours for the trough spanwise section for $\alpha=0\dg$.}
   \label{fig:lowAoA_blAnalysis0}
\end{figure}

These quantities have been computed along the portion of the chord covered by the LSB for $\alpha=0\dg$, $6\dg$ and $10\dg$ and are presented in figures~\ref{fig:lowAoA_blAnalysis0}, \ref{fig:lowAoA_blAnalysis6} and \ref{fig:lowAoA_blAnalysis10} along with values of the velocity magnitude and pressure coefficient at the boundary layer edge. Also in these figures, pressure coefficient contours are included at the trough section ($z=0.25L_z$) along with the location of the boundary layer edge given by $\delta_{99}$ (dashed blue line in the figures). For the case of zero angle of attack shown in figure~\ref{fig:lowAoA_blAnalysis0} it can be observed that the thickness of the boundary layer is tripled from separation to reattachment. On the other hand, the momentum thickness is kept constant until $x_{LSB}\approx0.7$ which leads to a maximum on the shape factor at that same streamwise position. The absence of contour lines on figure~\ref{fig:lowAoA_blAnalysis0}\emph{a} inside the LSB suggest that pressure is kept almost constant inside the LSB. The lack of a streamwise pressure gradient leads to also a constant velocity along the boundary layer edge as shown in figure~\ref{fig:lowAoA_blAnalysis0}\emph{e} and \emph{f}. Nevertheless, the pressure recovery is seen to start at about 80\% of the LSB's length which is consequently accompanied by a decrease of the velocity magnitude.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_blAnalysis6}
   \input{\folder/lowAoA/blAnalysis6}
   \caption{Evolution of (\emph{b}) boundary layer thickness $\delta_{99}$ and displacement thickness $\delta^*$, (\emph{c}) momentum thickness $\theta$ and (\emph{d}) shape factor $H$ along the LSB on the trough section ($z=0.25L_z$). Additional plots of the (\emph{e}) velocity magnitude $\avg{|\bm{u}|}$ and (\emph{f}) pressure coefficient $\avg{C_p}$ along the boundary layer edge are provided. The location of the boundary layer edge is depicted in (\emph{a}) along the pressure coefficient contours for the trough spanwise section for $\alpha=6\dg$.}
   \label{fig:lowAoA_blAnalysis6}
\end{figure}

For the $\alpha=6\dg$ case, illustrated in figure~\ref{fig:lowAoA_blAnalysis6}, a similar evolution of the boundary layer properties is seen which leads to similar values of $\delta^*$, $\delta_{99}$ and $\theta$ at $x_{LSB}=1.1$. In this case, however, the thickening of the boundary layer is much more abrupt since $\delta_{99}$ and $\delta^*$ are smaller at separation ($x_{LSB}=0$). Again, the variations of the momentum thickness inside the bubble are minimum for the most part of it which leads to a maximum in the shape factor at $x_{LSB}\approx0.6$. The start of the pressure recovery region is observed to occur about $x_{LSB}\approx0.8$. The pressure contours for $\alpha=6\dg$ seem to follow closely the shape of the LSB as shown in figure~\ref{fig:lowAoA_blAnalysis6}\emph{a} and both pressure and velocity are constant for the most part of the LSB. 
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_blAnalysis10}
   \input{\folder/lowAoA/blAnalysis10}
   \caption{Evolution of (\emph{b}) boundary layer thickness $\delta_{99}$ and displacement thickness $\delta^*$, (\emph{c}) momentum thickness $\theta$ and (\emph{d}) shape factor $H$ along the LSB on the trough section ($z=0.25L_z$). Additional plots of the (\emph{e}) velocity magnitude $\avg{|\bm{u}|}$ and (\emph{f}) pressure coefficient $\avg{C_p}$ along the boundary layer edge are provided. The location of the boundary layer edge is depicted in (\emph{a}) along the pressure coefficient contours for the trough spanwise section for $\alpha=10\dg$.}
   \label{fig:lowAoA_blAnalysis10}
\end{figure}
%
\begin{table}[t!]
\centering
\caption{Parameters defining the least-squares fit of the parabolas shown in figure~\ref{fig:lowAoA_deltaHalfLSB} which define the frontal shape of the LSB in the WLE-1 case for three different angles of attack.}
\label{tab:lowAoA_fhkDelta}
%\resizebox{1\textwidth}{!}{
\begin{tabular}{c c c c}
$\alpha[\dg]$  &$f_\delta$   &$h_\delta$  &$k_\delta$ \\
\hline
0   &-0.0320   &-0.0275  &0.0222 \\
6   &-0.0222   &-0.0275  &0.0166 \\
10  &-0.0192   &-0.0274  &0.0168 
\end{tabular}%}
\end{table}

Figure~\ref{fig:lowAoA_blAnalysis10} presents the boundary layer integral quantities for $\alpha=10\dg$. The boundary layer thickness and the displacement thickness seem to evolve in a similar manner to that observed for lower angles of attack, leading to similar values downstream of the reattachment point. In contrast, it is evident that the location at which the momentum thickness stops being constant moves upstream as $\alpha$ increases. As a result, the maximum peak in the shape factor is observed at $x_{LSB}\approx0.5$. In the same fashion, the start of the pressure recovery also moves upstream to about $x_{LSB}\approx0.65$. It is worth noting that the pressure contours shown in figure~\ref{fig:lowAoA_blAnalysis10}\emph{a} provide a clear depiction of the reversed-flow-vortex introduced in \S\ref{sec:LSBReview}.

In \S\ref{sub:lowAoA_spanwiseProfiles} the platform shape of the LSB has been parametrised using a second-order polynomial. Using the boundary layer thickness $\delta_{99}$ at half the LSB's length the height of the LSB can be parametrised. The height of the LSB across the span is displayed in figure~\ref{fig:lowAoA_deltaHalfLSB} for the three angles of attack studied in this chapter. A series of second-order polynomials have been used to fit the data and the parameters defining the parabola are presented in table~\ref{tab:lowAoA_fhkDelta}. In this case $f_\delta$ is negative due to the fact that the parabola is inverted. Still, $f_\delta$ gives a measure of the LSB's width, $h_\delta$ represents the spanwise location of the symmetry plane and $k_\delta$ represents the maximum height of the LSB at $x_{LSB}=0.5$. It is clear that the height of the LSB is reduced as the angle of attack increases and it seems to saturate at $\alpha=10\dg$ since the parabolas of figure~\ref{fig:lowAoA_deltaHalfLSB} for $\alpha=6\dg$ and $10\dg$ overlap and $k_\delta$ only differs on the fourth decimal digit.
%

\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_deltaHalfLSB}
   \input{\folder/lowAoA/deltaHalfLSB}
   \caption{Boundary layer thickness across the span at half the LSB's length ($\delta_{99}(x_{LSB}=0.5,z)$) for the WLE-1 case at $\alpha=0\dg$, $6\dg$ and $10\dg$. Each series define the edge of the LSB in front view. Dashed lines are second-order polynomials least-squared-fitted whose parameters are given in (\ref{eq:parabola}) and its values in table~\ref{tab:lowAoA_fhkDelta}.}
   \label{fig:lowAoA_deltaHalfLSB}
\end{figure}
