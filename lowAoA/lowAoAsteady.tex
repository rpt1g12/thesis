%=====================SECTION - Time-averaged characteristics=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Time-averaged Characteristics}\label{sec:lowAoAsteady}
Table~\ref{tab:lowAoA_ClCd} provides time-averaged values for the lift and drag coefficients for the SLE and WLE cases for three different angles of attack ($\alpha=$ 0$\dg$, 6$\dg$ and 10$\dg$) prior to the SLE stall angle ($\alpha^{SLE}_{stall}\approx12\dg$) at $Re_\infty=120\times10^3$ and $M_\infty=0.3$. Simulation data shows some deviation from the experimental data of \citet{Hansen2011} which could be attributed to transitional Reynolds number effects. As it has been shown in \S\ref{cha:StalledWLE} for $\alpha=20\dg$, at $Re_\infty=1.2\times 10^5$, LSBs are expected to form in the leading edge region. Due to the high sensibility that LSBs have to external disturbances (see \S~\ref{sec:LSBReview}) accurate agreement with wind tunnel data is quite challenging, especially for drag measurements. 
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_clAlpha}
   \input{\folder/lowAoA/clAlpha}
   \caption{Time-averaged lift and drag coefficients variations with respect to the angle of attack $\alpha$ for (\emph{a}) the WLE cases and (\emph{b}) SLE cases.}
   \label{fig:lowAoA_clAlpha}
\end{figure}
%
\begin{table}[t!]
\centering
\caption{Time-averaged lift and drag coefficients ($\avg{C_L}$ and $\avg{C_D}$), their respective standard deviations $\sigma_{C_L}$ and $\sigma_{C_D}$; and relative differences of lift and drag coefficients with respect to the experimental data by \citet{Hansen2011} ($\epsilon_{C_L}$ and $\epsilon_{C_D}$); for three different angles of attack. Relative difference of lift has been ignored for $\alpha=0\dg$.}
\label{tab:lowAoA_ClCd}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l r c c c c c c c c c}
&$\alpha$  &$AR$  &$\kappa_{LE}$  &$\avg{C_L}$  &$\avg{C_D}$  &$\avg{C_L}/\avg{C_D}$  &$\sigma_{C_L}$  &$\sigma_{C_D}$ &$\epsilon_{C_L}$ &$\epsilon_{C_D}$\\
&$[\dg]$   &      &               &             &             &                       &	               &               &(\%)             &(\%)            \\
\hline\Tstrut 
SLE-1 (Current)		&0	&0.11   &1	&0.000	   &0.032	&0.000	&1.73$\times 10^{-5}$	&1.28$\times 10^{-6}$  &--- &7   \\
			&6	&0.11   &1	&0.823	   &0.041	&20.073	&6.73$\times 10^{-5}$	&1.08$\times 10^{-6}$  &25  &3   \\
			&10	&0.11   &1	&0.880	   &0.052	&16.923	&5.47$\times 10^{-4}$	&1.06$\times 10^{-5}$  &-13 &-13  \\
SLE-8 (Current)		&10	&0.88   &8	&0.995	   &0.050	&19.900	&2.31$\times 10^{-5}$	&6.59$\times 10^{-7}$  &-1  &-17 \\
SLE-66			&0	&7.26   &66	&\tld0.00  &\tld0.03    &\tld0.00	&---	&---   &--- &--- \\
(\citet{Hansen2011})	&6	&7.26   &66	&\tld0.66  &\tld0.04    &\tld16.50	&---	&---   &--- &--- \\
			&10	&7.26   &66	&\tld1.01  &\tld0.06    &\tld16.83	&---	&---   &--- &---
\Bstrut\\\hline\Tstrut 
WLE-1 (Current)		&0	&0.11   &1	&0.000	   &0.022     &0.000	 &1.73$\times 10^{-5}$	 &1.38$\times 10^{-7}$  &--- &-27 \\
			&6	&0.11   &1	&0.659	   &0.032     &20.594	 &1.91$\times 10^{-4}$	 &1.73$\times 10^{-6}$  &20  &20  \\
			&10	&0.11   &1	&0.967	   &0.041     &23.585	 &1.17$\times 10^{-4}$	 &2.60$\times 10^{-6}$  &9   &-32 \\
WLE-8 (Current)		&10     &0.88   &8      &0.969     &0.039     &24.846    &2.20$\times 10^{-5}$   &2.50$\times 10^{-6}$  &9   &-35 \\
WLE-66 			&0	&7.26   &66	&\tld0.00  &\tld0.03  &\tld0.00	 &---	&---   &--- &--- \\
(\citet{Hansen2011})	&6	&7.26   &66	&\tld0.55  &\tld0.04  &\tld13.75 &---	&---   &--- &--- \\
			&10	&7.26   &66	&\tld0.89  &\tld0.06  &\tld14.83 &---	&---   &--- &--- 
\end{tabular}}
\end{table}

Interestingly, although lift is lower for the WLE models in comparison with their SLE counterparts (for $\alpha=10\dg$ the comparison is between WLE-1 and SLE-8), drag is also reduced as shown in figure~\ref{fig:lowAoA_clAlpha}. As a result, the aerodynamic efficiency ($\avg{C_L}/\avg{C_D}$) of the modified wings is in general higher than the baseline ones. Besides this, while the SLE cases obtain maximum efficiency at $\alpha=6\dg$, the efficiency in the WLE cases seems to keep growing over the range of angles of attack presented in this work.

Prior to stall, and in absence of \vK vortex shedding in the wake region, force fluctuations ($\sigma_{C_L}$ and $\sigma_{C_D}$) in table~\ref{tab:lowAoA_ClCd} are many orders of magnitude smaller than those shown in table~\ref{tab:StalledWLE_ClCd} for both SLE and WLE cases. No particular trend can be established for the force fluctuations as for some cases the fluctuations for the wavy geometry are equal or even higher compared with their SLE counterpart but for others, the opposite happens. On a side note, the results presented in table~\ref{tab:lowAoA_ClCd} show that the $AR$ effect is more important for the SLE cases, in particular for $\alpha=10\dg$.

\input{lowAoA/steadyOverview}

\input{lowAoA/chaLengths}

\input{lowAoA/spanwiseProfiles}

\input{lowAoA/blAnalysis}

\input{lowAoA/influences}

\input{lowAoA/channelling}

\input{lowAoA/svEvolution}

\input{lowAoA/svOrigin}
