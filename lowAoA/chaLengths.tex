%====================SUBSECTION - charLengths=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Key streamwise positions and characteristic streamwise lengths}\label{sub:lowAoA_chaLengths}
In the previous section (\S\ref{sub:lowAoA_steadyOverview}) a series of key streamwise positions (KSPs) have been identified such as the separation and reattachment point at each spanwise section ($x_s$ and $x_r$), defined as the streamwise positions where the friction coefficient changes sign.  Additionally, three more KSPs are defined: the minimum pressure position $x_{mp}$ is the streamwise location of the negative $\avg{C_p}$ peak; (ii) the streamwise position of the maximum streamwise pressure gradient $x_{rp1}$ and (iii) the start of the pressure recovery region $x_{rp0}$, defined as the streamwise position where $\partial\avg{C_p}/\partial{x}=0$ immediately upstream of $x_{rp1}$. The position of each of these KSPs is shown for the WLE-1 trough section in figure~\ref{fig:lowAoA_chaLengths}. Based on these, the LSB's streamwise length $l_{LSB}$, the distance between the minimum pressure point and the separation point $l_{mp}$ and the pressure recovery length $l_{rp}$ are defined as
%
\begin{equation}
\label{eq:lLSB}
\begin{aligned}
	l_{LSB}&=x_r-x_s, \\
	l_{mp}&=x_s-x_{mp}, \\
	l_{rp}&=x_{rp1}-x_{rp0}.
\end{aligned}
\end{equation}
The values for both positions and lengths for the SLE-1, SLE-8 and WLE-1 cases at all angles of attack studied are summarised in table~\ref{tab:lowAoA_chaLengths} and depicted in figure~\ref{fig:lowAoA_xPos} and figure~\ref{fig:lowAoA_lengths}. It is seen that all KSPs move upstream as the angle of attack is increased. However, the rate at which they move is different for each KSP. This is most evident when comparing evolutions of $x_{mp}$ and $x_{s}$ with the rest of KSPs. All KSPs seem to asymptote to a certain value as $\alpha$ is increased but, due to the limitation of not being able to go further than the leading edge ($x_{LE}$) both $x_{mp}$ and $x_{s}$ seem to converge faster to their asymptotic values. As a result of this, the length of the LSB keeps decreasing as $\alpha$ increases as shown in figure~\ref{fig:lowAoA_lengths}\emph{a}. Despite this, the rate of reduction is higher in the WLE-1 case. 
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_chaLengths}
   \input{\folder/lowAoA/chaLengths}
   \caption{Examples of key streamwise locations (KSPs) used to define characteristic lengths in (\ref{eq:lLSB}) where $x_{s}$ and $x_{r}$ are the first and last locations where $\avg{C_f}=0$, $x_{mp}$ is the location where $\avg{C_p}(x=x_{mp})=\left.\avg{C_p}\right|_{\text{min}}$, $x_{rp1}$ is the location where $\partial\avg{C_p}(x=x_{rp1})/\partial{x}=\left.\partial\avg{C_p}/\partial{x}\right|_{\text{max}}$ and $x_{rp0}$ is the immediately upstream location where $\partial\avg{C_p}/\partial{x}=0$.}
   \label{fig:lowAoA_chaLengths}
\end{figure}

%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_xPos}
   \input{\folder/lowAoA/xPos}
   \caption{Variations of the KSP with $\alpha$ for (\emph{a}) the spanwise-averaged SLE-1 case and (\emph{b}) the WLE-1 case at the T1 section. For each KSP the location of the leading edge at the same spanwise cross-section ($x_{LE}$) has been subtracted. Exact values of each KSP can be found in table~\ref{tab:lowAoA_chaLengths}.}
   \label{fig:lowAoA_xPos}
\end{figure}
%
\begin{table}[t!]
\centering
\caption{Key streamwise positions (KSP) where $x_s$, $x_r$, $x_{mp}$, $x_{rp0}$ and  $x_{rp1}$ are the separation, reattachment, minimum pressure, pressure recovery start and pressure recovery end. The characteristic lengths $l_{LSB}$, $l_{mp}$ and $l_{rp}$ are the length of the LSB, the distance between the minimum pressure and the start of the LSB and the pressure recovery length. Definitions of the KSPs and the characteristic lengths are given in figure~\ref{fig:lowAoA_chaLengths} and (\ref{eq:lLSB}) respectively.}
\label{tab:lowAoA_chaLengths}
\resizebox{1\textwidth}{!}{
\begin{tabular}{l|c c c c c c c c c}
&$\alpha[\dg]$  &$x_s$  &$x_r$  &$x_{mp}$  &$x_{rp0}$  &$x_{rp1}$  &$l_{LSB}$  &$l_{mp}$  &$l_{rp}$ \\
\hline\Tstrut 
$\avg{\text{SLE-1}}_z$  &0  &-0.1406  &0.3516  &-0.3555  &0.2500   &0.3047   &0.4922  &0.2149  &0.0547   \\
                        &6  &-0.3594  &-0.0039 &-0.4570  &-0.0781  &-0.0391  &0.3555  &0.1076  &0.0391   \\
                        &10 &-0.4180  &-0.1328 &-0.4727  &-0.2109  &-0.1719  &0.2852  &0.0547  &0.0391   
\Bstrut\\\hline\Tstrut 
$\avg{\text{SLE-8}}_z$  &10 &-0.4162  &-0.1719  &-0.4766  &-0.2344  &-0.1992  &0.2461  &0.0547  &0.2461
\Bstrut\\\hline\Tstrut 
WLE-1 (T1) &0  &-0.2580  &0.1268   &-0.3734  &0.0460   &0.1075   &0.3848  &0.1154  &0.0616   \\
           &6  &-0.4350  &-0.2388  &-0.4619  &-0.2849  &-0.2580  &0.1962  &0.0269  &0.0269   \\
           &10 &-0.4465  &-0.3311  &-0.4735  &-0.3734  &-0.3503  &0.0462  &0.0270  &0.0231   
\Bstrut\\\hline\Tstrut 
WLE-1 (P1) &0  &0.0718   &0.1352   &-0.3405  &---      &---       &0.0634  &0.4123  &---
\Bstrut\\\hline\Tstrut                                                            
WLE-1 (M1) &0  &-0.1172  &0.1289   &-0.3555  &---      &---       &0.2461  &0.4123  &---
\end{tabular}
}
\end{table}

Given the evolution of $x_{mp}$ and $x_{s}$ for both SLE-1 and WLE-1, it looks as if the minimum pressure is always reached upstream of the LSB for angles of attack below the stall angle. In contrast, for high angles of attack, figure~\ref{fig:StalledWLE_cpSecAoA20} shows that at least for cases WLE-4 and WLE-8, the minimum pressure is reached at the rear of the LSB, that is, for $\alpha=20\dg$ $l_{mp}<0$. Furthermore, figure~\ref{fig:lowAoA_lengths}\emph{b} shows that $l_{mp}$ also decreases with $\alpha$ and it asymptotes to $l_{mp}\approx0.0547$ and $l_{mp}\approx0.027$ for the SLE-1 and WLE-1 cases respectively.

Although at first sight of figure~\ref{fig:lowAoA_xPos} it may seem that $l_{rp}=x_{rp1}-x_{rp0}$ is constant, figure~\ref{fig:lowAoA_lengths}\emph{c} shows that the pressure recovery length at the rear of the LSB also reduces with $\alpha$. This reduction in the pressure recovery length ($l_{rp}$) in combination with the decrease in pressure shown in figure~\ref{fig:lowAoA_minMaxP}\emph{a} leads to an increased streamwise pressure gradient as shown in figure~\ref{fig:lowAoA_minMaxP}\emph{b} and table~\ref{tab:lowAoA_minMaxP}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_lengths}
   \input{\folder/lowAoA/lengths}
   \caption{Variations of the (\emph{a}) the LSB streamwise length, (\emph{b}) the distance between the minimum pressure location and the separation point and (\emph{c}) the recovery pressure length. Exact values of characteristic length can be found in table~\ref{tab:lowAoA_chaLengths} and their definitions in (\ref{eq:lLSB}).}
   \label{fig:lowAoA_lengths}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{lowAoA_minMaxP}
   \input{\folder/lowAoA/minMaxP}
   \caption{Variations of (\emph{a}) the minimum pressure coefficient and (\emph{b}) maximum streamwise pressure coefficient gradient with $\alpha$. Exact values can be found in table~\ref{tab:lowAoA_minMaxP}.}
   \label{fig:lowAoA_minMaxP}
\end{figure}
%
\begin{table}[t!]
\centering
\caption{Pressure coefficient and its streamwise gradient at $x=x_{mp}$ and $x_{rp1}$ respectively for the spanwise-averaged SLE-1 and SLE-8 cases and the WLE-1 case at the T1 spanwise cross-section for $\alpha=0\dg$, $6\dg$ and $10\dg$.}
\label{tab:lowAoA_minMaxP}
%\resizebox{1\textwidth}{!}{
\begin{tabular}{l|c c c}
&$\alpha[\dg]$  &$\avg{C_p(x=x_{mp})}$  &${\partial{\avg{C_p(x=x_{rp1})}}}/{\partial{x}}$ \\
\hline\Tstrut 
$\avg{\text{SLE-1}}_z$  &0   &-0.7240  &08.4728  \\
                        &6   &-1.9235  &17.0426  \\
                        &10  &-2.4574  &18.8474  
\Bstrut\\\hline\Tstrut 
$\avg{\text{SLE-8}}_z$  &10  &-2.7054  &25.6101  
\Bstrut\\\hline\Tstrut 
WLE-1 (T1)              &0   &-0.7574  &08.3894  \\
                        &6   &-2.0453  &18.3611  \\
                        &10  &-3.1596  &41.9610  \\
\end{tabular}
%}
\end{table}
