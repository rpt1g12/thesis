%**SECTION **%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Motivation}\label{sec:Motivation}

The aerodynamic forces produced by an aerofoil submerged in a stream of air are highly dependent on the relative angle between the air's free stream velocity and the aerofoil's chord. This angle $\alpha$ is known as angle of attack, or AoA in short. It is a common practice in the aeronautics community to make use of non-dimensional quantities in order to make the comparison between experiments easier. The non-dimensional quantities related to the aerodynamic forces are the lift and drag coefficients defined as:
%
\begin{equation}
\label{eq:liftCoefficient}
	c_l=\frac{L}{\frac{1}{2}\rho_\infty u_\infty^2 L_c} \qquad \text{and} \qquad c_d=\frac{D}{\frac{1}{2}\rho_\infty u_\infty^2 L_c}
\end{equation}
%
where $L$ and $D$ are the lift and drag forces, $\rho_\infty$ and $u_\infty$ are the density and velocity magnitude in the free stream respectively, and $L_c$ is the distance between the leading and trailing edges, i.e. the aerofoil's chord. For low $\alpha$, the lift produced by the aerofoil varies linearly with respect to $\alpha$, i.e. $\dd c_l/\dd\alpha=\text{const}$. Assuming an incompressible, inviscid and irrotational flow over a two-dimensional thin aerofoil, we can obtain by Thin Aerofoil Theory (TAT) the value of such constant:
%
\begin{equation}
\label{eq:twoPiSlope}
	\frac{\dd c_l}{\dd\alpha}=2\pi \qquad \rightarrow \qquad c_l(\alpha)=\int_{}^{} 2\pi\,\dd\alpha=c_{l0}+2\pi\alpha. 
\end{equation}

For any symmetric aerofoil, the value of the integration constant $c_{l0}$ is zero whereas for cambered aerofoils it will determine the lift produced at $\alpha=0$. Figure~\ref{fig:Naca0012_Re6x10^6} shows a typical $c_l$ versus $\alpha$ for the NACA0012 aerofoil section for experimental data~\citep{Abbott1959}, Thin Aerofoil Theory and the inviscid/viscous interaction code \emph{``XFOIL''}. It is seen that the TAT result is only valid for low angles of attack in Region I. In this region the increase in the AoA creates an increase in the circulation $\Gamma$ around the aerofoil. The flow remains attached over the entire upper surface and the streamlines are curved down as seen in figure~\ref{fig:StallFormation}\emph{a}. Due to the increased curvature in the streamlines, the airspeed increases and the pressure decreases above the aerofoil whereas velocity decreases and pressure increases below it. This creates a pressure difference between both sides (upper/suction side and bottom/pressure side) that produces lift. A representative pressure distribution of Region I can be seen in figure~\ref{fig:Naca0012_AoA5_Cp} for $\alpha=8\dg$. The minimum pressure is achieved close to the leading edge where the velocity of the flow is highest. As the flow moves downstream pressure recovers until at the trailing edge pressure is equal on both sides of the aerofoil. Faster pressure recovery means a smaller difference in pressure between the upstream and downstream regions of the aerofoil, and hence smaller drag.
\begin{figure}[!t]\centering
   \tikzsetnextfilename{Naca0012_Re6x10^6}
   \input{\folder/Introduction/Naca0012_Re6x10^6}
   \caption{Lift coefficient versus angle of attack for the NACA0012 aerofoil section at $Re_\infty=6\times10^6$ from (\emph{a}) \citet{Abbott1959} and (\emph{b})\emph{``XFOIL''} compared to Thin Aerofoil Theory slope. Pre-stall (I), near-stall (II) and post-stall (III) regions are delimited by vertical dash-dotted lines.}
   \label{fig:Naca0012_Re6x10^6}
\end{figure}

An increase in the AoA further decreases the pressure on the suction side of the aerofoil as seen in figure~\ref{fig:Naca0012_AoA5_Cp} for $\alpha=20\dg$ in Region II. The decrease in pressure results in a higher lift coefficient as seen in figure~\ref{fig:Naca0012_Re6x10^6}\emph{b}. However, the pressure recovery is now much steeper. As it will be shown later on, an adverse pressure gradient ($\dd p/\dd x>0$) works against the direction of the flow and eventually causes flow reversal. A situation like this is sketched in figure~\ref{fig:StallFormation}\emph{b}. The flow has already started separating close to the trailing edge and the streamlines travelling above and below the aerofoil do not meet anymore in the trailing edge.
\begin{figure}[!t]\centering
   \tikzsetnextfilename{StallFormation}
   \input{\folder/Introduction/StallFormation}
   \caption{The three stages of stall formation. Pre-stall condition (\emph{a}) at $\alpha\ll\alpha_{stall}$, near-stall condition (\emph{b}) at $\alpha\lesssim\alpha_{stall}$ and post-stall condition (\emph{c}) at $\alpha>\alpha_{stall}$.}
   \label{fig:StallFormation}
\end{figure}
%
\begin{figure}[!b]\centering
   \tikzsetnextfilename{Naca0012_AoA5_Cp}
   \input{\folder/Introduction/Naca0012_AoA5_Cp}
   \caption{Pressure coefficient distribution at different $\alpha$ representative of regions shown in figure~\ref{fig:Naca0012_Re6x10^6} for the NACA0012 at $Re_\infty=6\times 10^6$ obtained using \emph{``XFOIL''}.}
   \label{fig:Naca0012_AoA5_Cp}
\end{figure}


Figure~\ref{fig:Naca0012_Re6x10^6} shows that a further increase in $\alpha$ will no longer result in additional lift. The aerofoil is now stalled. Any increase in the AoA will result in actually less lift and more drag. The adverse pressure gradient is causing the flow to separate straight from the leading edge, and although the aerofoil is still producing some lift, most of the aerodynamic force projects in the flow parallel direction, i.e. mostly drag. Such a condition is sketched in figure~\ref{fig:StallFormation}\emph{c}. The aerofoil behaves similarly to a bluff body, leaving behind a strong and wide wake. 

Depending on the Reynolds number, the wake of a stalled aerofoil can be highly turbulent and contain large vortical structures that are shed periodically from the leading and trailing edges as shown in figure~\ref{fig:aerofoil_wake}. These large vortical structures originate due to the presence of strong and unstable shear layers that enclose the recirculating region immediately downstream of the stalled aerofoil. The frequency of shedding of these vortices has been extensively measured and found to be similar to the frequency of shedding of any bluff body immersed in a stream of fluid if scaled appropriately. For a non-dimensional frequency based on the free stream velocity $u_\infty$ and the projection of the aerofoil's chord on a cross-flow plane $L_c\sin{(\alpha)}$ the frequency of shedding has been found to be $f^*=fL_c\sin{(\alpha)}/u_\infty\approx0.2$.

\begin{figure}[!b]\centering
   \tikzsetnextfilename{aerofoil_wake}
   \input{\folder/Introduction/aerofoil_wake}
   \caption{Flow visualisation of the wake of a stalled aerofoil section. The wake of this aerofoil contains several vortical structures shed from the leading and trailing edges.}
   \label{fig:aerofoil_wake}
\end{figure}

In conclusion, stall is an undesirable flow condition at which not only the aerofoil is producing almost no lift and high drag forces, but also the flow is highly unsteady due to the presence of vortex shedding. As a result, engineers and researchers have been investigating different ways to mitigate, delay, and control stall for all kinds of lifting surfaces. Both active and passive methods ranging from geometry modifications of trailing and leading edges, use of multi-element aerofoils, suction and blowing mechanisms, Gurney Flaps, etc. have been used. 

More recently a peculiar whale, with uncommon flippers has drawn the attention of many researchers (see figure~\ref{fig:humpback_whale}\emph{a}). This whale is known as the Humpback whale, very popular between Australian and American \emph{"whale-watchers"}. The flippers of the Humpback whale or \emph{Megaptera novaeangliae}, are the longest among all cetaceans. These flippers are particularly special due to the presence of large protuberances or tubercles located at its leading edge as shown in figure~\ref{fig:humpback_whale}\emph{b}. It is well known by marine biologists that the Humpback whale also has a unique feeding behaviour which demands high manoeuvrability from the animal. Despite its big dimensions, the Humpback whale is capable of performing high speed and sharp U-turns. For these manoeuvres, the whale orientates its flippers towards a high angle of attack while still maintaining the lift. It is believed that these enhanced aerodynamic capabilities of the Humpback whale are due to the characteristic shape of its flippers. 

\citet{Fish1995} performed a detailed study on the Humpback whale's flippers and postulated that the tubercles on the leading edge of the flippers act as passive control devices which improve its hydrodynamic capabilities. They compared the tubercles with aircraft strakes, which are a type of vortex generators used to control the flow around aircraft and especially to modify the stall characteristics of their wings. They proposed that the tubercles generate streamwise vortices that exchange momentum within the boundary layer re-energising it and finally delaying stall. However, as strakes, the tubercles do not increase the maximum lift of the flippers, but they help to maintain a certain level of lift while the whale is turning at high AoA. Additionally, \citeauthor{Fish1995} postulated that the tubercles create channels of high-speed flow (compared to other span locations on the flipper). They based this hypothesis on the fact that no barnacles were found between tubercles. The absence of these barnacles, which are usually found on the upper leading edge of the whale's tubercles, suggest that the reason they fail to attach to the surface in between tubercles is the higher velocity achieved there.

\begin{figure}\centering
   \tikzsetnextfilename{humpback_whale}
   \input{\folder/Introduction/humpback_whale}
   \caption{A (\emph{a}) humpback breaching in the Stellwagen Bank National Marine Sanctuary in Massachusetts Bay and (\emph{b}) detail of the flipper geometry.}
   \label{fig:humpback_whale}
\end{figure}

As a result of such promising findings, undulated leading edges has been one of the hottest topics in the recent years. Since Frank Fish published his first paper revealing the aerodynamic benefits of wavy leading edges in 1995, a lot of effort has been put into the design of wings that are similar in shape to the Humpback whale's flippers. Inspired by the Humpback whale's flippers, researchers have found that wings with a sinusoidal/wavy leading edge have some properties that may overcome the problem of stall. Wings with wavy leading edges (WLEs) produce in general more lift, less drag, and are less sensitive to AoA variations than the regular straight leading edge wings in the post-stall region.


Despite the fact that the topic of aerofoils with a WLE has been covered by several groups of researchers in the past, the understanding as to how WLEs improve post-stall performance is still underdeveloped and in debate. Additionally, many of the speculative explanations have not fully been supported by high-fidelity flow data and are also limited to the time-averaged domain. Besides this, due to the promising features offered by WLEs in post-stall, most of the studies published are focused on aerofoils in full stall and observations about pre-stall and near-stall angles of attack are usually concerned about force measurements only rather than flow physics. Therefore, this work aims to provide a more detailed investigation into the full operational range (pre-stall, near-stall and post-stall) of aerofoils with a WLE in terms of both time-averaged and unsteady aerodynamics by using high-resolution large-eddy simulations. With this in mind, the following objectives are set:
%
\begin{itemize}
	\item To provide a thorough analysis of the flow of wings with a WLE in deep stall and to be able to identify the key aerodynamic features that allow this modified geometry to provide enhanced capabilities in this regime.
	\item To provide a detailed characterisation of the flow features present in wings with a WLE at angles of attack prior to stall. Especially, the structure of the typical LSBs that form in the trough regions and how it varies with the angle of attack was to be investigated. 
	\item To provide a characterisation of the unsteady flow features of wings with a WLE and in particular to find an explanation for the reduced force fluctuations presented by such type of wings in deep stall conditions.
	\item To investigate the stall development process in wings with a WLE and in particular the process that leads to the breakdown of the peak-to-peak periodicity of the flow.
\end{itemize}

%=====================SECTION - outline=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Outline of the Thesis}\label{sec:outline}

This first chapter provides an introduction to the topic and the motivations for undertaking an investigation on aerofoils with a WLE. Chapter \ref{cha:LiteratureReview} provides a review of the main topics treated in this thesis. In chapter \ref{cha:Methodology} the numerical methods and methodologies used to perform the CFD simulations are detailed. Chapter \ref{cha:StalledWLE} focuses on the aerodynamic characteristics of aerofoils with a WLE in deep stall and the effect of spanwise domain length on the simulations whereas chapter \ref{cha:lowAoA} aims to provide a thorough description of the aerodynamics of WLEs prior to stall. In chapter \ref{cha:nearStall} the near-stall behaviour is investigated and in particular the stall development and stall recovery phases. Finally, chapter \ref{cha:Conclusions} provides a global summary of the whole thesis as well as recommendations for future lines of investigation.
