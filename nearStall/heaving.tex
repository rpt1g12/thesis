%=====================SECTION - In construction=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Slow Heaving Motion}\label{sec:heaving}

In order to investigate the process that leads to the collocated mode in the post-stall regime models WLE-4, WLE-8 and SLE-8 have been submerged in a free stream flow at an oscillating incidence angle. The incidence angle variation is imposed by the variation of the wall-boundary condition. A reference frame technique is used so the flow remains still and the wing moves through the fluid. By imposing a positive or negative component in the y-axis the ramp up and down motions are achieved, respectively. Simulations are divided into three sections: (i) a ramp-up section, (ii) a constant $\alpha$ section and (iii) a ramp-down section. During the ramp-up and ramp-down sections, the angle of attack varies as
%
\begin{equation}
\label{eq:alphaProfile}
  \alpha(t)=\alpha_0+\Delta_\alpha\sin^2\left(\frac{\pi t'}{2T}\right), \qquad \text{with} \quad t'=t-t_0, \\
\end{equation}
where
\begin{equation}
  \Delta_\alpha=\alpha_1-\alpha_0 \qquad \text{and} \qquad T=t_1-t_0.
\end{equation}

For all cases shown here the ramp-up/down period is $T=120$ where the values of $t_0$ and $t_1$ are case dependent. The amplitude $\Delta_\alpha$ is positive for the ramp-up motion, i.e. $\alpha_0=10$ and $\alpha_1=20\dg$, and negative for the ramp-down motion, i.e. $\alpha_0=20\dg$ and $\alpha_1=10\dg$. In between each ramping section, the angle of attack is kept constant for 30 time-units, i.e. from $120<t'<150$ where $t'=t-t_0$. The resulting angle of attack profile is shown in figure~\ref{fig:nearStall_aoaTime}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_aoaTime}
   \input{\folder/nearStall/aoaTime}
   \caption{Angle of attack time profile used for the slow heaving motion simulations. Figure contains (\emph{a}) the time profile and (\emph{b}) its time derivative.}
   \label{fig:nearStall_aoaTime}
\end{figure}

The reason for such a long cycle period of 270 time-units is to approximate as much as the computational resources allow for a quasi-stationary flow. The objective of approximating this as a quasi-stationary flow is fixed so that the spectral and flow decomposition analysis to be shown in the following sections gains validity. In addition, windowing techniques and splitting the transient data into smaller time subsets have been performed in order to get a better approximation to a quasi-steady state at the expense of losing resolution in the lower-end of the frequency spectrum.

The resulting force coefficient time histories for the WLE-4, WLE-8 and SLE-8 cases are presented in figure~\ref{fig:nearStall_clcdHistory}. At the beginning of the ramp-up motion, the SLE-8 produces more lift than the WLE-4 and WLE-8 cases, however, after $t'=30$ the WLE-8 case starts producing steadily more lift than the other two cases. This is maintained for the rest of the ramp-up section. Maximum lift is achieved for all cases at around $t'=60$, a time position where the maximum $\dd\alpha/\dd{t}$ is found as seen in figure~\ref{fig:nearStall_aoaTime}\emph{b}. After maximum $C_L$ is achieved, the stall process begins. The SLE-8 case suffers the steepest lift loss whereas the wavy cases WLE-4 and WLE-8 undergo a much more soft stall process. In the stall regime, the WLE-8 case continues producing more lift than the SLE-8 and WLE-4 cases, in this order. However, for $t'>150$, when the ramp-down motion starts, the SLE-8 case produces more lift than the WLE-4 case for a short period of time. Nonetheless, for $t'>180$, $C_L$ is higher for the WLE-4 case. Recovery from stall is also faster in the wavy cases. While the SLE-8 case only starts recovering for $t'>240$, the WLE-4 does so at $t'>210$ and the WLE-8 at $t'>230$.

Prior to stall recovery, just at the beginning of the ramp-down motion, the SLE-8 case momentarily has more lift than the other two cases. For $150<t'<180$, the SLE-8 increases lift in time despite the ramp-down motion. This is also accompanied by a rise in the drag coefficient and after that, both lift and drag coefficients start plunging again; in the case of lift to find its absolute minimum just before the stall recovery.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_clcdHistory}
   \input{\folder/nearStall/clcdHistory}
   \caption{Time histories of the (\emph{a}) lift and (\emph{b}) drag coefficients, and (\emph{c}) aerodynamic efficiency ($C_L/C_D$) for the WLE-4, WLE-8 and SLE-8 cases.}
   \label{fig:nearStall_clcdHistory}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_clcdAlpha}
   \input{\folder/nearStall/clcdAlpha}
   \caption{Variations of the (\emph{a}) lift and (\emph{b}) drag coefficients, and (\emph{c}) aerodynamic efficiency ($C_L/C_D$) for the WLE-4, WLE-8 and SLE-8 cases with the angle of attack. Ramp-up and ramp-down motions are represented by solid and dashed lines respectively.}
   \label{fig:nearStall_clcdAlpha}
\end{figure}

Regarding drag, the SLE-8 case continuously produces more drag than the wavy cases. Nonetheless, the drag produced by the WLE-4 case is much lower than in the other two cases, which leads to an increased aerodynamic efficiency as shown in figure~\ref{fig:nearStall_clcdHistory}\emph{c}, at least for $t'<75$. It is observed that the steep lift loss shown in figure~\ref{fig:nearStall_clcdHistory}\emph{a} produces a peak in the drag coefficient time history at about $t'\approx 85$. For the wavy cases, an exponential rise of the drag coefficient is also observed during the stall formation region although the drag peak is not the absolute maximum. The softer stall process observed in the WLE-8 case leads to higher $C_L$ and lower $C_D$ in the range of $75<t'<90$ and consequently, the aerodynamic efficiency is higher than that of the SLE-8 and WLE-4 cases.

Sections of the heaving cycle where the models are in deep-stall can be identified by the plateau region in the $C_L/C_D$ plot. Clearly, recover from stall is achieved much earlier in the wavy cases and therefore the average aerodynamic efficiency is much higher for the wavy cases. Additionally, the intensity of the fluctuations in the deep-stall region is much larger due to the characteristic \vonK vortex shedding observed in \S\ref{cha:StalledWLE}.

Figure~\ref{fig:nearStall_clcdAlpha} presents the evolution of the force coefficients with respect to the angle of attack $\alpha$. In figure~\ref{fig:nearStall_clcdAlpha}\emph{a} shows that initially lift increases linearly with $\alpha$ until about $\alpha\approx 15\dg$, i.e. the point of maximum $\dd(\alpha)/\dd t$ (see figure~\ref{fig:nearStall_aoaTime}\emph{b}), for all cases. As shown in figure~\ref{fig:nearStall_clcdHistory}, figure~\ref{fig:nearStall_clcdAlpha} also shows that stall clearly happens much more rapidly for the SLE-8 case. The most negative $\dd{C_L}/\dd\alpha$ for the SLE-8 case happens at $\alpha=18\dg$ and coincides with the maximum $C_D$.

\input{nearStall/stallDevelopment}

\input{nearStall/peakOscillations}

\input{nearStall/stallRecovery}

\input{nearStall/flowModalDecomposition}
