%=====================SECTION - forcedSolution=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Forced Trough-to-Trough Solution}\label{sec:forcedSolution}
In the past section, it has been shown that flow mode-A, i.e. the trough-to-trough periodic mode, is not a stable solution for high angles of attack. The LSBs move upstream as the angle of attack is increased until they meet the leading edge and then start getting wider. At the same time, the peak sections start to receive spanwise velocity perturbations due to their interaction with the vortical structures being shed by the neighbouring LSBs. It appears that the combination of these two events lead to the bursting of one of the LSBs and flow mode-A transitions to flow mode-B, i.e. the collocated mode.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLEtwzTopForced}
   \input{\folder/nearStall/8WLEtwzTopForced}
   \caption{Snapshot-sequence showing the evolution of the trough-to-trough periodic solution via contours of the spanwise component of the wall-shear-stress contours on the suction surface of the WLE-8 model. Time range shown is from $t'=0$ to $t'=5.5$.}
   \label{fig:nearStall_8WLEtwzTopForced}
\end{figure}

\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLEtwzTopForced_2}
   \input{tikzSource/nearStall/8WLEtwzTopForced_2}
   \caption{Snapshot-sequence showing the evolution of the trough-to-trough periodic solution via contours of the spanwise component of the wall-shear-stress contours on the suction surface of the WLE-8 model. Time range shown is from $t'=6$ to $t'=10$.}
   \label{fig:nearStall_8WLEtwzTopForced_2}
\end{figure}

Despite the fact that the last chapter has shed some light on the flow-mode-transition process, it is believed that the bursting of the LSB and therefore the point of transition between the two flow modes was influenced by the heaving motion profile since the bursting of the T4's LSB occurs little after the point of maximum $\dd\alpha/\dd{t}$.

As a result, inspired by the work of \citet{Jones2010}, a new experiment was designed that consisted in forcing a trough-to-trough periodic solution at a high angle of attack and then leaving the flow to freely evolve into a collocated LSB solution. In order to obtain a trough-to-trough periodic solution at a high angle of attack, a single WLE wavelength ($\kappa_{LE}=1$) solution was pitched until an angle of attack of $\alpha=17\dg$. The $\kappa_{LE}=1$ solution was advanced in time until dissipation of transient effects was accomplished. Once the stable trough-to-trough had been reached, the one wavelength solution was copied over an 8 wavelength domain ($\kappa_{LE}=8$). 

Unfortunately, due to the limited computational resources available, the simulation was only run for 10 time-units. Due to the limited simulation time available, the full transition from flow mode-A to mode-B is not complete. Nonetheless, it is seen how after removing the forcing, flow mode-A rapidly becomes unstable and starts to evolve into flow mode-B.

Figures~\ref{fig:nearStall_8WLEtwzTopForced} and  \ref{fig:nearStall_8WLEtwzTopForced_2} show the evolution of the forced solution through contours of the spanwise wall-shear-stress $\tau_{wz}$ in the leading edge region. Using the spanwise wall-shear-stress the LSBs can be easily identified by the enclosed regions between high positive and negative shear regions, indicative of the LSB's ``legs'' or streamwise vortices (SVs). The initial forced solution can be seen at $t'=0$. The flow is perfectly periodic in the spanwise direction and each trough has a LSB as evidenced by the $\tau_{wz}$ contours of opposite sign that surround the LSBs. As soon as the solution is advanced the LSB start to interact and the spanwise flow movements at the rear of the LSBs intensify. At $t'=1$, although the flow's trough-to-trough periodicity is still dominant, small-scale perturbations start to grow for $x>-0.45$ and the LSBs get much smaller. The increased spanwise domain length is known to have a great impact in transition to turbulence since it allows room for secondary flow flow instabilities that are otherwise damped down in domains with a smaller spanwise length. At $t'=2$, the LSB in trough T6 shows signs of bursting and the initial flow periodicity in the span direction is therefore lost. Nonetheless, the T6's LSB redevelops back at $t'=3$. At $t'=4$, T4's LSB bursts and begins the process that leads to complete leading-edge flow separation characteristic of flow mode-B. The bursting of the T4's LSB has a great impact on the other LSBs. After the bursting, the other bubbles show an increase (in magnitude) of the $\tau_{wz}$ contours which indicates that the streamwise vortices (SVs) that surround the LSB are intensified. The strengthening of the vortices is especially strong for the two neighbouring LSBs on troughs T3 and T5. For $t'>6$ in figure~\ref{fig:nearStall_8WLEtwzTopForced_2}, it is seen that the strength of the SVs increases steadily at all trough sections but T4. At the furthest simulated point, at $t'=10$, the full mode-B has not yet been reached in the sense that the SSL has not yet formed. Nevertheless, it seems quite evident that trough T4 will eventually become an SSL while the other troughs will retain their LSBs, since for the first 10 time units after the forcing was removed it was the only trough region that has consistently shown signs of bursting.

Another point of view of the same evolution is shown in figures~\ref{fig:nearStall_8WLECpTopForced} and \ref{fig:nearStall_8WLECpTopForced_2} in terms of the surface pressure coefficient. At $t'=0$ the forced solution is seen where the pressure inside each of the LSBs is exactly the same. As soon as the forcing is released disturbances to the trough-to-trough solution begin to be noticeable until $t'=2.5$ where the differences between the LSBs is quite evident. As time advances the pressure inside most of the LSBs decreases. The exception is the T4 trough which has been identified as the most likely to eventually become the SSL. The pressure in the T4 region increases steadily from $t'=5$ (figure~\ref{fig:nearStall_8WLECpTopForced}) to $t'=10$ (figure~\ref{fig:nearStall_8WLECpTopForced_2}). The LSBs in T1 and T5 troughs seem to trap the lowest pressure with values as low as $C_p\approx-3.5$ at $t'=10$. The pressure coefficient contours also show that the increase in LSB strength manifested by the decrease in the pressure inside them is accompanied by a reduction of the LSB size. While at $t'=0$ the LSBs stretch as far as $x\approx-0.38$ at $t'=10$ reattachment at the rear of the LSBs happens at $x\approx-0.42$.

It is interesting to observe that as soon as one of the LSBs burst, the others seem to immediately gain strength as demonstrated by the decrease in the trapped pressure and the increase in the strength of the spanwise wall-shear-stress. This indicates that LSB group forms all at once. It appears that merging of smaller LSB groups into a final bigger one does not occur.

\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLECpTopForced}
   \input{tikzSource/nearStall/8WLECpTopForced}
   \caption{Snapshot-sequence showing the evolution of the trough-to-trough periodic solution via contours of the pressure coefficient contours on the suction surface of the WLE-8 model. Time range shown is from $t'=0$ to $t'=5.5$.}
   \label{fig:nearStall_8WLECpTopForced}
\end{figure}

\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLECpTopForced_2}
   \input{tikzSource/nearStall/8WLECpTopForced_2}
   \caption{Snapshot-sequence showing the evolution of the trough-to-trough periodic solution via contours of the pressure coefficient contours on the suction surface of the WLE-8 model. Time range shown is from $t'=6$ to $t'=10$.}
   \label{fig:nearStall_8WLECpTopForced_2}
\end{figure}

%=====================SUBSECTION - spanwiseCorr=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Spanwise correlation over time}\label{sub:spanwiseCorr}

One way to measure in a more quantitative manner the evolution from flow mode-A to mode-B is to track the spanwise flow correlation. Figure~\ref{fig:nearStall_pspanForced} shows contours of the space autocorrelation function $R_{pp}(\delta_z)$ for pressure measurements on the aerofoil surface at $x=-0.45$, defined as
\begin{equation}
\label{eq:spanCorrelation}
\begin{aligned}
	&R_{pp}(\delta_z)=\frac{r_{pp}(\delta_z)}{r_{pp}(\delta_z=0)}\\
	\text{where}\qquad &r_{pp}(\delta_z)=(p(z+\delta_z)-\avg{p}_z)(p(z)-\avg{p}_z).
\end{aligned}
\end{equation}

Due to the trough-to-trough periodicity of flow mode-A at $t'=0$, the flow shows high correlation at multiples of $\lambda_{LE}$. However, for $t'>4$ the trough-to-trough periodicity shows clear signs of deterioration. Instead, the flow starts to show high correlation at multiples of $2\lambda_{LE}$. In relation to figures~\ref{fig:nearStall_8WLEtwzTopForced} to \ref{fig:nearStall_8WLECpTopForced_2}, this means that while at the beginning LSBs are very similar, later on LSBs at every other trough seem to be of a similar strength while neighbouring LSBs are not. This is clearly seen in figure~\ref{fig:nearStall_8WLECpTopForced_2} at $t'=8$ where LSBs at T1, T3, T5 and T7 are stronger than the ones at T2, T4, T6 and T8. Eventually, by the end of the simulation time, the correlation also drops at multiples of $2\lambda_{LE}$ and remains only high every $4\lambda_{LE}$, i.e. the LSB group is starting to form at T1, T2, T3, T5, T6 and T7 with an SSL at T4.

\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_pspanForced}
   \input{tikzSource/nearStall/pspanForced}
   \caption{Spanwise pressure correlation at the wall $R_{pp}$ over time for the forced WLE-8 case.}
   \label{fig:nearStall_pspanForced}
\end{figure}
