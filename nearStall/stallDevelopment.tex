%=====================SUBSECTION - stallDevelopment=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Overview of the flow field leading to stall}\label{sub:stallDevelopment}

In this section the time period leading to flow detachment and therefore stall for the SLE-8 and WLE-8 models is investigated. The instantaneous flow fields are examined at the upper surface of the SLE-8 and WLE-8 models as well as at key spanwise planes of constant $z$.

%=====================SUBSUBSECTION - SLEstall=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsubsection{SLE stall}\label{ssub:SLEstall}

Figure~\ref{fig:nearStall_8SLETimeSequence} presents the time histories of lift and drag coefficients leading to stall in the SLE-8 case. Along with the histories, vertical dashed lines indicate the time position at which the flow field has been sampled to provide the pressure and velocity contours of figures~\ref{fig:nearStall_8SLECpTop} to \ref{fig:nearStall_8SLEUSide}. 
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{nearStall_8SLETimeSequence}
   \input{\folder/nearStall/8SLETimeSequence}
   \caption{Time histories of lift and drag coefficients leading to stall for the SLE-8 case. The dashed lines indicate the time location of the series of snapshots used in figures~\ref{fig:nearStall_8SLECpTop} to \ref{fig:nearStall_8SLEUSide}.}
   \label{fig:nearStall_8SLETimeSequence}
\end{figure}

In figure~\ref{fig:nearStall_8SLECpTop} the instantaneous surface pressure contours are shown. It is seen how at $t'=75$ the leading edge region is subject to high suction. Inside the high suction region it is seen that pressure is almost constant in the first 10\% of the chord, i.e. for $x<-0.4$, due to the presence of a LSB as shown in the past chapters (see \S\ref{cha:lowAoA}). This constant streamwise pressure section spans the whole SLE-8 model, however, a region of small-scale pressure features follows which indicates the onset of the turbulent region. Moving forward in time it is observed that the pressure inside of the LSB rises steadily and three-dimensional flow features start to appear ($t'=77$). In contrast, at the rear part pressure decreases indicating that pressure recovery to the free stream values is not accomplished which results in a drag increase as shown in figure~\ref{fig:nearStall_8SLETimeSequence}. At $t'=81$ the absence of a region of low pressure spanning the SLE-8 model in the leading edge region indicates that the LSB has burst. This represents the onset of stall and from then onward lift decreases sharply as shown in figure~\ref{fig:nearStall_8SLETimeSequence}. From $t'=82$ pressure in the first half of the chord increases more rapidly until at $t'=86$ the same pressure level is found over the entire upper surface of the SLE-8 model. Besides the increase in pressure, it is observed that after the LSB bursts the small-scale turbulent structures start to disappear from the pressure contours the flow becomes more two-dimensional.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8SLECpTop}
   \input{\folder/nearStall/8SLECpTop}
   \caption{Snapshot-sequence showing the pressure coefficient contours on the suction surface of the SLE-8 model. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8SLETimeSequence}}
   \label{fig:nearStall_8SLECpTop}
\end{figure}
%
%\begin{figure}[t!]\centering
%   \tikzsetnextfilename{nearStall_8SLEtwxTop}
%   \input{\folder/nearStall/8SLEtwxTop}
%   \caption{8SLEtwxTop}
%   \label{fig:nearStall_8SLEtwxTop}
%\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8SLECpSide}
   \input{\folder/nearStall/8SLECpSide}
   \caption{Snapshot-sequence showing the pressure coefficient contours on the $z=0.44$ spanwise section of the SLE-8 model. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8SLETimeSequence}}
   \label{fig:nearStall_8SLECpSide}
\end{figure}

Figure~\ref{fig:nearStall_8SLECpSide} presents another perspective of the same time sequence captured in figure~\ref{fig:nearStall_8SLECpTop} via the pressure coefficient contours on a constant-$z$ plane at $z=0.4$. The presence of the LSB becomes more evident in this side view. At $t'=75$ the contours show that as advanced earlier, the high suction is related to the LSB in the leading edge region. Along with the LSB, the small vortical structures shed by the bubble are also easily distinguished. These low-pressure-containing eddies are convected downstream as denoted by the small-scale and low-pressure contours downstream of the LSB. As time advances, and therefore as the angle of attack is increased, the angle at which the eddies are convected increases. At the same time pressure inside the bubble increases until at $t'=81$ the bubble bursts. For $t'>82$, the time sequence shows the formation of a large-scale region of low pressure. This low-pressure region is identified as the pressure signature of the leading edge \vonK vortex highlighted in figure~\ref{fig:nearStall_8SLECpSide}($t'=83$). Since for the present simulations the angle of attack is varied in time, the \vonK vortex could also be referred as dynamic stall vortex \citep{Mulleners2012}.  This large vortical structure is then convected by the mean flow as shown in the following snapshots $t'=84$ to $86$. The bursting of the LSB and the sudden rise in pressure in the leading edge explain the loss of lift and the detachment of the boundary layer and the subsequent formation of the \vonK vortices justify the drag increase.

Separation of the boundary layer is further demonstrated via the instantaneous velocity contours of figure~\ref{fig:nearStall_8SLEUSide}. Due to the no-slip boundary condition, the total energy of the mean flow is dissipated within the boundary layer. Therefore, contours of the non-dimensional total energy $e^*_t=e_t/e_{t\infty}$ presented in figure~\ref{fig:nearStall_8SLEUSide} can be used to provide a qualitative estimate of the boundary layer. Already at $t'=75$ at the trailing edge it is observed that the thickness of the boundary layer is much larger than in the leading edge which is indicative of trailing edge stall. As time advances separation advances upstream and the region of low energy thickens. From $t'=81$, the time location that previously has been identified as the SLE-8 stall onset, full separation from the leading edge is evident. The flow separates from the leading edge at a very high angle, tangent to the leading edge surface.

\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8SLEUSide}
   \input{\folder/nearStall/8SLEUSide}
   \caption{Snapshot-sequence showing the dimensionless total energy $e^*_t=e_t/e_{t\infty}$ contours on the $z=0.44$ spanwise section of the SLE-8 model. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8SLETimeSequence}}
   \label{fig:nearStall_8SLEUSide}
\end{figure}

%=====================SUBSUBSECTION - WLEstall=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsubsection{WLE stall}\label{ssub:WLEstall}

Due to the softer stall of the WLE-8 case the period of time leading to the onset of stall lasts longer. The lift and drag coefficients during the stall development stage and the time locations at which the flow is analysed are displayed in figure~\ref{fig:nearStall_8WLETimeSequence} represented by dashed lines. Instantaneous contours of the pressure coefficient (figure~\ref{fig:nearStall_8WLECpTop}) and the spanwise component of the wall-shear-stress $\tau_{wz}$ (figure~\ref{fig:nearStall_8WLEtwzTop}) show that previous to stall, a strong periodicity dominates the leading edge region due to the leading edge waviness. 
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{nearStall_8WLETimeSequence}
   \input{\folder/nearStall/8WLETimeSequence}
   \caption{Time histories of lift and drag coefficients leading to stall for the WLE-8 case. The dashed lines indicate the time location of the series of snapshots used in figures~\ref{fig:nearStall_8WLECpTop} to \ref{fig:nearStall_8WLEUSide}.}
   \label{fig:nearStall_8WLETimeSequence}
\end{figure}

Figure~\ref{fig:nearStall_8WLECpTop} shows low-pressure spots in each leading edge trough for $t'=60$ and $63$. This low-pressure spots represent the pressure signature of the three-dimensional LSBs described in the previous chapter \S\ref{cha:lowAoA}. It is in this region where most of the lift and drag are produced as shown in \S\ref{sub:lowAoA_influences} and \S\ref{sub:StalledWLE_influences}. At rear of the LSBs a strong streamwise pressure gradient rises the pressure for all spanwise sections. This reinforces the idea that transition to turbulence at the rear of the LSBs promotes flow uniformity in the span direction.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLECpTop}
   \input{\folder/nearStall/8WLECpTop}
   \caption{Snapshot-sequence showing the pressure coefficient contours on the suction surface of the WLE-8 model. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8WLETimeSequence}}
   \label{fig:nearStall_8WLECpTop}
\end{figure}

Although initially the pressure inside the bubbles is similar, at $t'=66$ the LSB in the T2 trough section seems to suffer from a strong (positive) pressure fluctuation. This is a behaviour that seems to occur erratically for all the bubbles, i.e. at certain times a single bubble would suddenly increase its pressure for a short period of time while the rest keep similar interior pressure values. This single LSB pressure increments could explain the small lift fluctuations during this stage as shown in figure~\ref{fig:nearStall_8WLETimeSequence}. It appears to be that as the angle of attack is increased, the sensibility of each LSB to exterior fluctuations is increased which in turn leads to sudden changes in the LSBs shape and flow properties. At some point these fluctuations become too strong for a particular LSB and the bubble bursts. This appears to be the case at $t'=72$ for the T4 LSB. The T4 LSB bursts and stall starts developing at that particular spanwise section. For $t'>74$ pressure rises everywhere along the suction surface of the WLE-8, however, the pressure rise is stronger for the T4 section which no longer manages to redevelop a stable LSB. It is worth noting that the bursting of the LSB in this simulation happens at a different trough to the case shown in \S\ref{cha:StalledWLE}, which proves that the LSB-burst is equally likely to happen at any trough. 
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLEtwzTop}
   \input{\folder/nearStall/8WLEtwzTop}
   \caption{Snapshot-sequence showing the spanwise component of the wall-shear-stress contours on the suction surface of the WLE-8 model. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8WLETimeSequence}}
   \label{fig:nearStall_8WLEtwzTop}
\end{figure}

In figure~\ref{fig:nearStall_8WLEtwzTop}, a closer look at the leading edge region during the stall development is provided through contours of the spanwise wall-shear-stress ($\tau_{wz}$). The location of the LSBs is easily identified. Each LSB is surrounded by opposite sign wall-shear-stress regions that indicate that flow is attracted towards the interior of the LSB due to its low pressure. The opposite sign contours of the $\tau_{wz}$ represent the legs of the LSB described in \S\ref{sub:lowAoA_svEvolution}. At the same time, the spanwise component of the wall-shear-stress at the peaks is very small and therefore the flow in the peaks goes straight in between it neighbouring LSBs. At the rear of the LSBs, due to the turbulent character of the flow in that region, the contours of the wall-shear-stress appear to be much more chaotic.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLECpSide}
   \input{\folder/nearStall/8WLECpSide}
   \caption{Snapshot-sequence showing the pressure coefficient contours on the T4 spanwise section of the WLE-8 model. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8WLETimeSequence}}
   \label{fig:nearStall_8WLECpSide}
\end{figure}

The short-time-burst of the T2 LSB aforementioned is clearly seen in figure~\ref{fig:nearStall_8WLEtwzTop} at $t'=66$. At this time instant the opposite sign contours that surround the LSB are much smaller in comparison with the other LSBs. However, these periods of instability seem to have a short duration since only a short time later, at $t'=69$, the T2 LSB has already recovered. This short-time-bursting events that seem to happen randomly are envisaged to be the result of a lost of structural stability of flow mode A, i.e. the trough-to-trough periodic LSB distribution, that will be investigated in the sections that follow.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLEUSide}
   \input{\folder/nearStall/8WLEUSide}
   \caption{Snapshot-sequence showing the dimensionless total energy $e^*_t=e_t/e_{t\infty}$ contours on the T4 spanwise section of the WLE-8 model. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8WLETimeSequence}}
   \label{fig:nearStall_8WLEUSide}
\end{figure}

At $t'=72$, the burst of the T4 LSB is reflected by the absence of the opposite sign $\tau_{wz}$ contours. In contrast to the T2 LSB, this bubble does not recover and as previously mentioned stall starts to develop at this particular spanwise section. It is observed that as T4 LSB bursts (and becomes a SSL as shown in \S\ref{cha:StalledWLE}) the two neighbouring trough sections, T3 and T5, seem to become stronger as denoted by the increased value of the $\tau_{wz}$ contours (see figure~\ref{fig:nearStall_8WLEtwzTop} from $t'=72$ to $81$). From $t'>84$ onwards, the separated shear layer starts becoming much stronger, grows in size and starts pushing the LSBs on the neighbouring troughs (T3 and T5).
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLET8CpSide}
   \input{\folder/nearStall/8WLET8CpSide}
   \caption{Snapshot-sequence showing the pressure coefficient contours on the T8 spanwise section of the WLE-8 model. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8WLETimeSequence}}
   \label{fig:nearStall_8WLET8CpSide}
\end{figure}

Contours of the instantaneous pressure coefficient ($C_p$) and the dimensionless total energy ($e^*_t$) at the T4 of the WLE-8 section, where stall starts developing, are provided in figures~\ref{fig:nearStall_8WLECpSide} and \ref{fig:nearStall_8WLEUSide} respectively. Pressure is minimum inside the LSB, which also sheds low-pressure vortical structures that drift downstream carried by the mean flow as shown in figure~\ref{fig:nearStall_8WLECpSide}. Similarly to the straight case, pressure increases in the leading edge region, however, this increase is not as severe as in the SLE-8 case, which explains the higher lift production of the WLE-8 case. As time advances the separation angle, denoted by the imaginary line traced by the LSB vortices, increases. Despite this and, in contrast to the straight case, complete separation of the boundary layer is no achieved until $t'=84$. The shed vortices seem to trace a line that, although is still located far from the wall surface, follows the shape of the aerofoil more closely than in the SLE-8 case for $t'<84$. From $t'=84$ onwards stall advances upstream rapidly towards the leading edge. Especially for $t'=90$ and $93$, pressure contours show that the pressure in the leading edge region is almost the same that in the free stream.

The flow evolution towards complete separation in the T4 spanwise section is more clearly seen in figure~\ref{fig:nearStall_8WLEUSide}, where the extend of the boundary layer can be estimated by regions of $e^*_t<1$. During the time covered by the snapshot sequence shown, trailing edge separation does exists but its upstream advance is much more gradual than in the straight case. Full stall is only observed for $t'>81$, which coincides with the time instant where the lift coefficient starts plunging (see figure~\ref{fig:nearStall_8WLETimeSequence}). Notwithstanding the large amount of separated flow at the T4 section, the large \vonK vortex shown in figure~\ref{fig:nearStall_8SLECpSide} for the SLE-8 case is not present in figure~\ref{fig:nearStall_8WLECpSide}. This is, as explained in \S\ref{sub:StalledWLE_coherence}, due to the highly three-dimensional character of the flow in the leading edge region, where the co-existence of both attached and detached flow regions reduces the spanwise coherence of the flow and avoids the formation of large spanwise-oriented coherent structures such as the \vonK vortices or the dynamic stall vortex \citep{Mulleners2012}.

Contrary to the SLE-8 case, where most of the spanwise sections undergo the same stalling process, for the WLE-8 case, figures~\ref{fig:nearStall_8WLECpTop} and \ref{fig:nearStall_8WLEtwzTop} showed that bursting does not occur in any of the LSBs but one (the LSB of T4 as described in the previous paragraph). In particular, figure~\ref{fig:nearStall_8WLECpTop} shows that even when the WLE-8 is already immersed in the deep stall regime some trough sections manage to maintain their LSB without bursting. Figures~\ref{fig:nearStall_8WLET8CpSide} and \ref{fig:nearStall_8WLET8USide} depict the flow at one of this trough sections where the LSBs do not burst, the T8 section. For this section, despite separation also advances upstream from the trailing edge, it never goes further upstream than $x\approx-0.3$. Furthermore, the presence of the LSB even at large angles of attack provides the WLE-8 model with a constant source for lift generation in the front part of the model as shown by the low pressure contours of figure~\ref{fig:nearStall_8WLET8CpSide}. The delayed separation at the T4 section results in a deflated wake as shown in figure~\ref{fig:nearStall_8WLET8USide} which ultimately results in a reduced drag production.

%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLET8USide}
   \input{\folder/nearStall/8WLET8USide}
   \caption{Snapshot-sequence showing the dimensionless total energy $e^*_t=e_t/e_{t\infty}$ contours on the T8 spanwise section of the WLE-8 model. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8WLETimeSequence}}
   \label{fig:nearStall_8WLET8USide}
\end{figure}
