%=====================SUBSECTION - flowModalDecomposition=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Flow modal decomposition}\label{sub:flowModalDecomposition}

For complex flows with a wide range of time and space scales, such as the one studied here, it can be very difficult to extract or identify the most important structures directly from snapshots of the flow-field. It is on this occasions when flow field decompositions turn to be useful. Using these flow decomposition techniques, one can methodologically extract meaningful coherent structures within an otherwise noisy flow field. In the following pages the \emph{Proper Orthogonal Decomposition} and the \emph{Dynamic Mode Decomposition} techniques will be briefly introduced and applied to the flow fields shown in the past subsections to extract meaningful spatiotemporal structures within the flow.

%=====================SUBSUBSECTION - POD=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsubsection{Proper Orthogonal Decomposition}\label{ssub:POD}

One of the most popular decomposition techniques is the \emph{Proper Orthogonal Decomposition} (POD). Firstly introduced by \citet{Lumley1967} for the field of fluid dynamics, POD is a modal decomposition that is closely related to matrix decomposition techniques such as the \emph{Eigen Value Decomposition} (EVD) and the \emph{Singular Value Decomposition} (SVD).

The basis of POD relies on the assumption that the perturbed flow field $\bm{q'}(\bm{\xi},t)=\bm{q}(\bm{\xi},t)\avg{\bm{q}(\bm{\xi},t)}$ can be constructed as a linear combination of some temporal coefficients $\bm{a}(t)$ and spatial modes $\bm{\phi}(\bm{\xi})$ such that 
%
\begin{equation}
\label{eq:POD0}
	\bm{q'}(\bm{\xi},t)=\sum_{i=0}^{m-1} a_i(t)\bm{\phi}_i(\bm{\xi}).
\end{equation}

Therefore, the objective of POD is to find set of $n$ basis vectors or spatial modes $\bm{\phi}(\bm{\xi})$ that optimally represent the spatial features of $\bm{q'}(\bm{\xi},t)$. Equation (\ref{eq:POD0}) can be expressed in matrix form by vectorising $\bm{q'}$ into $\bm{x}(t)=\bm{q'}(\bm{\xi},t)$ and stacking the vectors as
%
\begin{equation}
\label{eq:POD1}
\resizebox{0.95\textwidth}{!}{$
	\bm{X}=\begin{bmatrix}
		| & \dots & | \\
		\bm{x}(t=0) & \dots & \bm{x}(t)\\
		| & \dots & | 
	\end{bmatrix}, %\quad
	\bm{\Phi}=\begin{bmatrix}
		| & \dots & | \\
		\bm{\phi}_{i=0} & \dots & \bm{\phi}_{i}\\
		| & \dots & | 
	\end{bmatrix}, %\quad
	\bm{A}=\begin{bmatrix}
		| & \dots & | \\
		\bm{a}_{i=0}(t=0) & \dots & \bm{a}_{i}(t)\\
		| & \dots & | 
	\end{bmatrix},
$}
\end{equation}
%
where $\bm{X}\in \mathbb{R}^{n\times m}$, $\bm{\Phi}\in\mathbb{R}^{n\times m}$, $\bm{A}\in\mathbb{R}^{m\times m}$ and
\begin{equation}
\label{eq:POD2}
	\bm{X}=\bm{\Phi}\bm{A}.
\end{equation}
%
The size of the matrices is given therefore by the number of spatial points $n$ and the number of snapshots $m$ taken. 

In the original POD formulation, a solution of such problem can be found by performing an EVD of the covariance matrix $\bm{R}=\bm{X}\bm{X}^T$ by solving 
%
\begin{equation}
\label{eq:POD3}
	\bm{R}\bm{\phi}_i=\lambda_i\bm{\phi}_i,
\end{equation}
%
where $\lambda_i$ are the eigenvalues and $\bm{\phi}_i$ are the eigenvectors (and the POD modes). If the eigenvalues are sorted in descending magnitude order, a low-rank representation of $X$ can be achieved using only $r$ modes for $r<m$. That is, each flow field can be reconstructed using 
%
\begin{equation}
\label{eq:POD4}
	\bm{x}(t)\approx \sum_{i=0}^{r} a_i(t)\bm{\phi}_i,
\end{equation}
%
where the time coefficients $a_i(t)$ can be found by projecting the POD modes into the original flow fields.

Since in fluid dynamics the number of degrees of freedom, i.e. the number of spatial points, tends to be rather large, the size of the covariance matrix $\bm{R}$ can make the problem intractable. \citet{Sirovich1987} realised that the same spatial modes $\bm{\phi}$ could also be obtained by finding the eigenvalues and eigenvectors of the temporal correlation matrix $\bm{T}=\bm{X}^T\bm{X}$. In the case of fluid dynamics, the number of degrees of freedom tends to be in general much larger than the number of snapshots taken in an experiment, i.e. $n>>m$, and therefore using $\bm{T}$ instead of $\bm{R}$ reduces the size of the problem considerably. This is commonly known as the \emph{method of snapshots} for which at first, the following eigenvalue problem is solved
%
\begin{equation}
\label{eq:POD5}
	\bm{T}\bm{\phi}^*_i=\lambda_i\bm{\phi}^*_i,
\end{equation}
%
and finally, the original POD modes are recovered using the following relationship
\begin{equation}
\label{eq:POD6}
	\bm{\Phi}=\bm{X}\bm{\Phi}^*\bm{\Lambda}^{-1/2},
\end{equation}
%
where $\bm{\Lambda}=\bm{\lambda}\bm{I}$.

Alternatively, the POD modes can be obtained by performing an SVD of the snapshot matrix $\bm{X}$
%
\begin{equation}
\label{eq:POD7}
	\bm{X}=\bm{\Phi}\bm{\Sigma}\bm{\Psi}^T
\end{equation}
%
where the spatial POD modes, the temporal information and the singular values  are contained in $\bm{\Phi}\in\mathbb{R}^{n\times m}$, $\bm{\Psi}\in\mathbb{R}^{m\times m}$ and $\bm{\Sigma}\in \mathbb{R}^{m\times m}$ respectively. The singular values $\bm{\sigma}$ in the diagonal of $\bm{\Sigma}$ are sorted from largest to smallest and related to the eigenvalues by $\sigma_i^2=\lambda_i$. Again, a low-rank representation of $\bm{X}$ can be obtained by only using the first $r$ modes
%
\begin{equation}
\label{eq:POD8}
	\bm{x}(t)\approx \sum_{i=0}^{r} \bm{\phi}_i\sigma_i\psi_i(t)
\end{equation}

%=====================SUBSUBSECTION - DMD=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsubsection{Dynamic Mode Decomposition}\label{ssub:DMD}

Another popular decomposition technique (closely related with POD) is the \emph{Dynamic Mode Decomposition} (DMD) \citep{Schmid2010,Tu2013,Taira2017}. While POD provides a least-squares approximation of the snapshot matrix, DMD provides a mode decomposition in terms of the dynamically significant modes along with their oscillation frequency and growth/decay rate. The DMD algorithm assumes a linear relation between successive state vectors in time in the form of the following linear mapping
%
\begin{equation}
\label{eq:DMD0}
	\bm{x}(t+\Delta_t)=\bm{A}\bm{x}(t),
\end{equation}
%
where the (linear) dynamics of the system are given by the eigenvalues and eigenvectors of $\bm{A}$ since
%
\begin{equation}
\label{eq:DMD1}
	\bm{x}(i\Delta_t)=\bm{A}^i\bm{x}(t=0)=\bm{W}\bm{\Lambda}^i\bm{W}^{-1},
\end{equation}
%
where $\bm{\Lambda}=\bm{I}\bm{\lambda}$ is a diagonal matrix containing the eigenvalues and $\bm{W}$ is the matrix whose columns are the eigenvectors of $\bm{A}$. 

One way to obtain matrix $\bm{A}$ is to define two snapshot matrices
\begin{equation}
\label{eq:DMD2}
	\bm{X}=\begin{bmatrix}
		| & \dots & | \\
		\bm{x}(t=0) & \dots & \bm{x}(t=m\Delta_t)\\
		| & \dots & | 
	\end{bmatrix}, %\quad
	\bm{Y}=\begin{bmatrix}
		| & \dots & | \\
		\bm{x}(t=\Delta_t) & \dots & \bm{x}(t=(m+1)\Delta_t)\\
		| & \dots & | 
	\end{bmatrix}, %\quad
\end{equation}
%
where $\bm{A}=\bm{Y}\bm{X}^+$ and $\bm{X}^+$ is the \emph{Moore-Penrose} pseudoinverse of $\bm{X}$. However, given the large dimensions of $\bm{X}$ the \emph{exact DMD} (eDMD) algorithm \citep{Tu2013} is used instead. The eDMD algorithm consists of the following steps:
\begin{enumerate}
	\item Construct the two snapshot matrices $\bm{X}$ and $\bm{Y}$.
	\item Compute the reduced SVD of $\bm{X}=\bm{U}\bm{\Sigma}\bm{V}^T$.
	\item Compute matrix $\bm{\tilde{A}}\triangleq\bm{U}_r^T\bm{A}\bm{U}_r=\bm{U}_r^T\bm{Y}\bm{V}_r\bm{\Sigma}_r^{-1}$ where $\bm{U}_r$, $\bm{\Sigma}_r$ and $\bm{V}_r$ are reduced rank versions of the original SVD matrices. Taking into account that $\bm{U}$ contains the POD modes of $\bm{X}$, matrix $\bm{\tilde{A}}$ is no more than the projection of $\bm{A}$ onto the vector space spanned by the first $r$ POD modes.
	\item Compute the eigen-decomposition of $\bm{\tilde{A}}$ to find the eigenvalues $\bm{\tilde{\lambda}}$ and eigenvectors $\bm{\tilde{W}}$ that solve $\bm{\tilde{A}}\bm{\tilde{w}}_i=\tilde{\lambda}_i\bm{\tilde{w}}_i$.
	\item For each non-zero eigenvalue, the corresponding DMD mode is given by $\bm{\chi}_i=\tilde{\lambda}_i^{-1}\bm{Y}\bm{V}_r\bm{\Sigma}^{-1}\bm{\tilde{w}}_i$.
	\item The frequency and growth rate of each DMD mode $\bm{\chi}$ is obtained by mapping the eigenvalues $\bm{\tilde{\lambda}}$ to the complex plane. The frequency and growth rate are given by the imaginary $f_i=\Im(\mu_i)/(2\pi)$ and real $G_i=\Re(\mu_i)$ parts of the mapped eigenvalue where $\mu_i=\log(\tilde{\lambda})/\Delta_t$.
\end{enumerate}

One of the major inconvenience of the DMD is that, in contrast to the POD, it does not provide a clear way of identifying the most relevant modes. In the present work, a projection factor $k_{POD}^{0-p}$ is used based on a subset of the first $p$ POD modes. For example, for a DMD mode $\bm{\chi}_i$, the projection factor is given by
\begin{equation}
\label{eq:DMD3}
	k^{0-p}_{POD}=\sqrt{(\bm{\chi}_i\cdot\bm{\phi_0})^2+(\bm{\chi}_i\cdot\bm{\phi_1})^2+\dots+(\bm{\chi}_i\cdot\bm{\phi_{p}})^2},
\end{equation}
%
where $(\bm{u}\cdot\bm{v})$ represents the scalar product of vectors $\bm{u}$ and $\bm{v}$.

In the following pages, both POD and DMD techniques are used to identify key flow features of the flow in a constant-$z$ plane that cuts through the LSB in the T4 trough. The modal analysis is divided into two different phases:
\begin{itemize}
	\item \textbf{Pre-burst}: The phase previous to the bursting of the T4's LSB.
	\item \textbf{Post-burst}: This phase covers the period of time following to the bursting of the T4's LSB.
\end{itemize}

As previously mentioned, splitting of the cycle into smaller time subsets allows the separation of different mechanisms that arise during the slow-heaving motion.

In each case, the state vector $\bm{x}(t)$ used to construct the snapshot matrices was obtained by vectorising the pressure values at each grid point within the regions inside the blue boundaries shown in figure~\ref{fig:nearStall_boundaries}.

\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_boundaries}
   \input{\folder/nearStall/boundaries}
   \caption{Pressure contours showing the regions used to perform the flow decomposition techniques for (\emph{a}) the pre-burst and (\emph{b}) post-burst phases enclosed by thick blue lines.}
   \label{fig:nearStall_boundaries}
\end{figure}


%=====================SUBSUBSECTION - preBurst=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsubsection{Pre-burst}\label{ssub:preBurst}

For the pre-burst phase, a total of 1728 snapshots covering the region highlighted in figure~\ref{fig:nearStall_boundaries}\emph{a} were used to form the snapshot matrices $\bm{X}\in\mathbb{R}^{9211\times1727}$ and $\bm{Y}\in\mathbb{R}^{9211\times1727}$ which cover the space of time between $t'=43$ and $t'=70$.

On figure~\ref{fig:nearStall_sigmaPOD}\emph{a} the normalised POD singular values shown in a log scale indicate that most of the energy is concentrated in only a small percentage of the modes. This is more clearly seen in figure~\ref{fig:nearStall_sigmaPOD}\emph{b} where the ratio
%
\begin{equation}
\label{eq:sigmaNormalise}
	r_\sigma=\frac{\sum_{i=0}^{m} \sigma_i}{\sum_{i=0}^{M-1} \sigma_i}=\frac{s_\sigma^m}{s_\sigma^{M-1}}
\end{equation}
%
that indicates the amount of energy captured by the first $m$ modes in relation to the total number of modes $M$ is plotted against the percentage of modes used. In this figure, one can see that 90\% of the flow can be captured by using less than 20\% of the POD modes. 
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{nearStall_sigmaPOD}
   \input{\folder/nearStall/sigmaPOD}
   \caption{Plots of the (\emph{a}) normalised POD singular values $\sigma_m/s_\sigma^{M-1}$ and (\emph{b}) cumulative ratio $r_\sigma$.}
   \label{fig:nearStall_sigmaPOD}
\end{figure}

Figure~\ref{fig:nearStall_PODmodes} presents POD modes from $\phi_1$ to $\phi_8$. Mode $\phi_0$ has not been included since it represents only a drifting mode that has nearly constant value in the investigated spatial region and therefore does not provide any meaningful information about the coherent structures that compose the flow. Nonetheless, the temporal information of the mode ($\psi_0$) is shown in figure~\ref{fig:nearStall_PODtime}\emph{a} pre-multiplied by its singular value $\sigma_0$.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_PODmodes}
   \input{\folder/nearStall/PODmodes}
   \caption{Contour plots of the modes that arise from the POD of the pressure field for the pre-burst phase. Modes have been normalised by its maximum value.}
   \label{fig:nearStall_PODmodes}
\end{figure}

Going back to figure~\ref{fig:nearStall_PODmodes}, one can see that mode $\phi_1$ captures the structure of the LSB. In particular, the reversed flow vortex (RFV) introduced back in \S\ref{sec:LSBReview} has been captured. In comparison with mode $\phi_0$, mode $\phi_1$ is of oscillatory kind as shown in figure~\ref{fig:nearStall_PODtime}\emph{a}. The rest of the modes shown, from $\phi_2$ to $\phi_8$ capture the differently sized vortices shed by the LSB. The modes that capture larger vortex trains represent a higher percentage of the total energy. Additionally, modes $\phi_7$ and $\phi_8$, which capture low scale vortices, show that the location of these are located in the former region of the LSB whereas larger scale vortices, like the ones captured in modes $\phi_2$ or $\phi_3$ appear to be located at the rear part of the LSB. This serves to reinforce the hypothesis that small-scale vortices that are created in the front part of the merge to form larger vortices further downstream. The process is ceased after the vortices enter the turbulent region for $x>-0.3$, where none of the modes shows signs of LSB-related vortical structures. 
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_PODtime}
   \input{\folder/nearStall/PODtime}
   \caption{Time coefficients of the POD modes shown in figure~\ref{fig:nearStall_PODmodes}. (\emph{a}) Modes 0 and 1 and (\emph{b}) modes 2 to 7. }
   \label{fig:nearStall_PODtime}
\end{figure}

In figure~\ref{fig:nearStall_PODtime}\emph{b} it is clear that the LSB-vortex associated modes oscillate at high frequencies around a zero mean. The spectral components of those oscillations are presented in figure~\ref{fig:nearStall_PODpsd}.

The power spectral density of the time coefficients show two frequency regions of high amplitude oscillation: i) a broadband range from $f_{L_c}\approx10$ to $f_{L_c}=40$ and ii) a peak at $f_{L_c}=69$. The $f_{L_c}=69$ peak was to be expected since the spectrogram in figure~\ref{fig:nearStall_pSSLSpectogram} showed high power concentrated around this frequency for the period of time studied in this pre-burst phase. For modes representing larger vortical structures, most of the energy is contained in the low-frequency broadband region whereas for the modes with smaller vortices the peak region is more dominant. The fact that for low frequencies there exists not a clear peak at a particular frequency but rather broadband indicates that the merging of high frequency-small-scale-vortices results in larger vortices of varied frequencies. 
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_PODpsd}
   \input{\folder/nearStall/PODpsd}
   \caption{PSD of the POD time coefficients shown in figure~\ref{fig:nearStall_PODmodes} in the pre-burst phase.}
   \label{fig:nearStall_PODpsd}
\end{figure}

Although performing a Fourier analysis of the POD time coefficients $\bm{\psi}$ can provide a good insight into the oscillating characteristics of the modes, it cannot provide a modal decomposition that maps each mode to a particular frequency. For that, DMD is a more appropriate tool since it provides a flow decomposition that focuses on the dynamics of the system rather than the energy/importance of each mode composing the time sequence of flow fields. 

Performing DMD using a full projection into the POD basis results in the same number of modes as the number of snapshots used. As a result, and due to the lack of a clear way to identify the key DMD modes, using a reduced rank approximation of the POD matrices is usually preferred. In this study, a reduced rank approximation containing 90\% of the original information was used which first, makes the problem much more affordable in computational terms; and second, it provides a filtered version where most of the noisy modes are discarded. 

The resulting DMD spectrum for the pre-burst phase is shown in figure~\ref{fig:nearStall_DMDspectrum} where frequencies are shown on the horizontal axis whereas the vertical axis represents the growth/decay rate of each mode. A consequence of using real-valued source data the resulting spectrum is symmetric with respect to the frequency axis, i.e the imaginary axis. Each DMD mode is therefore represented as a pair of circles in the scatter plot of figure~\ref{fig:nearStall_DMDspectrum} the size of which is given by the projection factor $k_{POD}^{1-8}$, that is the projection factor using POD modes $\phi_1$ to $\phi_8$. The colour of the circles is given by the magnitude of the eigenvalues $\bm{\lambda}\in\mathbb{C}$.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_DMDspectrum}
   \input{\folder/nearStall/DMDspectrum}
   \caption{DMD spectrum for the pre-burst phase using the growth rate $G$ and the frequency $f_{L_c}$ as real and imaginary axis.}
   \label{fig:nearStall_DMDspectrum}
\end{figure}

Among all modes shown in figure~\ref{fig:nearStall_DMDspectrum}, it is observed that the ones that oscillate at a frequency between $f_{L_c}=11$ and $f_{L_c}=40$ show a high correlation with the POD modes (given by the size of the circles). This comes as no surprise since it is exactly this range where the PSD of the POD time coefficients contains most of their energy. Additionally, the least damped oscillatory mode is found at a frequency of $f_{L_c}=69$. 

In figure~\ref{fig:nearStall_DMDmodes} the modes with the highest POD projection factor are presented along with annotations providing their growth rate $G$ and frequency of oscillation $f_{L_c}$ non-dimensionalised using the aerofoil mean-chord-length and the free stream velocity. Again, it is found that modes with a higher frequency have their higher (absolute) values located much further upstream than those of the lower and mid frequencies.

Interestingly, and in contrast to the POD modes shown in figure~\ref{fig:nearStall_PODmodes}, it is seen how some of the modes penetrate in the turbulent boundary layer downstream of the LSB. This indicates that low-frequency large-scale structures can remain undamped within the turbulent boundary layer. Besides this, the low-frequency modes have more elongated structures that reach the surface of the aerofoil. These elongated structures are slightly inclined towards the streamwise direction. The inclination of the flow features is hypothesised to be related to the velocity difference between the top and bottom part of the structures due to the boundary layer velocity gradient in the wall-normal direction. On the other hand, high-frequency mode's features seem to be located on the edge of the boundary layer and are convected almost parallel to the aerofoil surface.
%
\begin{figure}[H]\centering
   \tikzsetnextfilename{nearStall_DMDmodes}
   \input{\folder/nearStall/DMDmodes}
   \caption{Contour plots of the modes arising from the DMD of the pressure field for the pre-burst phase. For each mode, an annotation of the mode's frequency and growth rate is provided. Modes have been normalised by its maximum value.}
   \label{fig:nearStall_DMDmodes}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_qCriterion}
   \input{\folder/nearStall/qCriterion}
   \caption{Perspective view of two (\emph{a} and \emph{b}) instantaneous flow field depicting iso-contours of the $Q=300$ coloured by the velocity magnitude and contours of the spanwise velocity on the peak plane P5. An additional isolated view of a hairpin-shaped structure (\emph{c}) is also provided.}
   \label{fig:nearStall_qCriterion}
\end{figure}

A link between these vortical flow features shed by the LSB and the spanwise flow perturbations described in \S\ref{sub:nearStall_peakOscillations} can be established via observation of figure~\ref{fig:nearStall_qCriterion}. On it, the spanwise-aligned K-H vortices are seen to create spanwise velocity perturbations that highly resemble the DMD and POD modes presented in figures~\ref{fig:nearStall_PODmodes} and \ref{fig:nearStall_DMDmodes}. In particular, the $w$-perturbations on the plane are also inclined towards the streamwise direction similarly to the DMD modes. The strongest spanwise velocity perturbations on the peak seem to be related to large hairpin-shaped vortical structures. An isolated view of such characteristic coherent structure is shown in  figure~\ref{fig:nearStall_qCriterion}\emph{c}. The hairpin vortex present in the current simulation highly resembles that of \citet{Watmuff1999} found by using small-magnitude pressure disturbances created by a surface jet. In both cases, the appearance of the large hairpin vortex is due to the presence of an obstacle in the flow (see for example \citet[p.~111]{deTulio2013}). The eventually hairpin-shaped vortex is formed in the shear layer that separates the LSB and the mean flow oriented almost perpendicular to the wall, resembling a semi-toroidal vortex. Further on, as the vortex advances downstream the streamwise velocity gradient in the wall-normal direction causes the semi-toroidal vortex to tilt into the streamwise direction acquiring the hairpin shape shown in figure~\ref{fig:nearStall_qCriterion}\emph{c}. The tilted structures of DMD modes like $\chi_9$ in figure~\ref{fig:nearStall_DMDmodes}, where the tilting process can also be observed along the streamwise direction, seem therefore to be the pressure signature of the hairpin vortices.

%=====================SUBSUBSECTION - postBurst=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsubsection{Post-burst}\label{ssub:postBurst}

For the post-burst phase, a total of 1000 snapshots covering the region highlighted in figure~\ref{fig:nearStall_boundaries}\emph{b} were used to form the snapshot matrices $\bm{X}\in\mathbb{R}^{15000\times999}$ and $\bm{Y}\in\mathbb{R}^{15000\times999}$ which cover the space of time between $t'=83.75$ and $t'=102.375$. The singular values extracted from the POD decomposition of the snapshot matrix $\bm{X}$ are plotted in figure~\ref{fig:nearStall_sigmaPOD2} and, like it happened in the pre-burst phase, about 90\% of the energy is concentrated in only 20\% of the modes.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_sigmaPOD2}
   \input{\folder/nearStall/sigmaPOD2}
   \caption{Plots of the (\emph{a}) normalised POD singular values $\sigma_m/s_\sigma^{M-1}$ and (\emph{b}) cumulative ratio $r_\sigma$ for the post-burst phase.}
   \label{fig:nearStall_sigmaPOD2}
\end{figure}

After the LSB has burst, an SSL is immediately formed and the shed vortices start to shed at a higher angle, aligned with the separated flow. As a consequence, the POD modes for the post-burst phase shown figure~\ref{fig:nearStall_PODmodes2} depict vortex trains of different sizes that are aligned with the SSL. The first mode ($\phi_0$), captures the quasi-steady (see figure~\ref{fig:nearStall_PODtime2}) part of the SSL whereas the rest of the modes contain the information about the shed SSL vortices. Similarly to the pre-burst phase, smaller scale structures are always located further upstream and closer to the aerofoil surface while the larger ones are always found downstream of $x=-0.40$. As expected, the modes with the highest singular values oscillate at lower frequencies as shown in figure~\ref{fig:nearStall_PODtime2} and \ref{fig:nearStall_PODpsd2}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_PODmodes2}
   \input{\folder/nearStall/PODmodes2}
   \caption{Contour plots of the modes that arise from the POD of the pressure field for the post-burst phase. Modes have been normalised by its maximum value.}
   \label{fig:nearStall_PODmodes2}
\end{figure}

The time coefficient of mode $\phi_0$ in figure~\ref{fig:nearStall_PODtime2} shows that the importance of the mode grows in time until it settles to a plateau value after $t'>95$. The rest of the modes oscillate about a zero mean as it happened in the pre-burst case for the vortex-associated modes. However, the frequency of oscillation of the post-burst modes is much smaller than that of the pre-burst phase. This is more clearly seen in figure~\ref{fig:nearStall_PODpsd2} which contains the spectra of the POD time coefficients of figure~\ref{fig:nearStall_PODtime2}. In this case, there appears that each mode has a different spectral portrait although $\phi_1$, $\phi_2$ and $\phi_3$ have a broad power spectral peak centred around $f_{L_c}\approx1$. Also, modes $\phi_5$, $\phi_6$ and $\phi_7$ have a concentrated region of energy for $6\le f_{L_c}\le7$ and $f_{L_c}\approx10$. The fact that most of the energy content is concentrated in the lower end of the energy spectrum agrees with figure~\ref{fig:nearStall_pSSLSpectogram} where it was shown that for $80\le f_{L_c}\le110$ the frequency of the most dominant oscillations drops compared with the pre-burst phase. The lower frequencies can be explained by the fact that the shed vortices dispose of a larger space to interact and merge to form larger vortical structures that convect at much lower speeds as shown in \S\ref{sub:StalledWLE_vortexDynamics} or in \citet{Yarusevych2009}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_PODtime2}
   \input{\folder/nearStall/PODtime2}
   \caption{Time coefficients of the POD modes shown in figure~\ref{fig:nearStall_PODmodes2}.}
   \label{fig:nearStall_PODtime2}
\end{figure}
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_PODpsd2}
   \input{\folder/nearStall/PODpsd2}
   \caption{PSD of the POD time coefficients shown in figure~\ref{fig:nearStall_PODmodes2} in the post-burst phase.}
   \label{fig:nearStall_PODpsd2}
\end{figure}

The DMD of the snapshot matrices for the post-burst phase further reinforces the vortex merging mechanism taken place on the T4 trough section after the LSB bursts and transforms into the SSL. Most of the DMD modes are concentrated in frequencies lower than $f_{L_c}=10$ as shown in figure~\ref{fig:nearStall_DMDspectrum2}.

Figure~\ref{fig:nearStall_DMDmodes2} presents the DMD modes that present the highest projection coefficient $k_{POD}^{0-7}$ sorted in descendent oscillation frequency order. The DMD modes show that vortical structures are shed at a frequency $f_{L_c}\approx11$ and due to a merging process form larger vortical structures that shed at a much lower frequency of $f_{L_c}\approx1$. These findings suggest that the v-K vortex shedding happening on the trailing edge (see \S\ref{sub:StalledWLE_forceFluctuations} for further details) could have a very strong link with the leading edge vortex shedding and merging process.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_DMDspectrum2}
   \input{\folder/nearStall/DMDspectrum2}
   \caption{DMD spectrum for the post-burst phase using the growth rate $G$ and the frequency $f_{L_c}$ as real and imaginary axis.}
   \label{fig:nearStall_DMDspectrum2}
\end{figure}
%
\begin{figure}[b!]\centering
   \tikzsetnextfilename{nearStall_DMDmodes2}
   \input{\folder/nearStall/DMDmodes2}
   \caption{Contour plots of the modes arising from the DMD of the pressure field for the post-burst phase. For each mode, an annotation of the mode's frequency and growth rate is provided. Modes have been normalised by its maximum value.}
   \label{fig:nearStall_DMDmodes2}
\end{figure}
