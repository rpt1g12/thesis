%=====================SUBSECTION - stallRecovery=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Stall recovery}\label{sub:stallRecovery}

In \S\ref{sub:stallDevelopment} an inspection of the instantaneous flow fields in the stall development phase was performed. In this section, the stall recovery phase is investigated instead. Figure~\ref{fig:nearStall_8WLETimeSequenceDown} presents a close-up view of the aerodynamic force coefficients in the stall recovery phase. Also in figure~\ref{fig:nearStall_8WLETimeSequenceDown}, vertical dashed lines are provided to indicate the time locations used to extract instantaneous flow fields to be shown in figures~\ref{fig:nearStall_8WLECpTopDown} and \ref{fig:nearStall_8WLECpSideDown}. Regarding the time history of the force coefficients plotted in figure~\ref{fig:nearStall_8WLETimeSequenceDown}, the drag coefficient drops significantly in the range $229<t'235$, which is a clear indication of stall recovery and flow reattachment. During the same time range, the lift coefficient also decreases. During the stall recovery phase lift is supposed to increase due to the flow reattachment that allows flow acceleration in the leading edge region and the consequent pressure drop that creates a net lift force on the wing. Nonetheless, this lift drop between $t'=229$ and $t'=233$ is due to the conclusion of the v-K vortex shedding as it will be proved in this section.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLETimeSequenceDown}
   \input{\folder/nearStall/8WLETimeSequenceDown}
   \caption{Time histories of lift and drag coefficients leading to stall recovery for the WLE-8 case. The dashed lines indicate the time location of the series of snapshots used in figures~\ref{fig:nearStall_8WLECpTopDown} and \ref{fig:nearStall_8WLECpSideDown}.}
   \label{fig:nearStall_8WLETimeSequenceDown}
\end{figure}

For $t'>233$ lift starts recovering until finally for $t'>255$ the pre-stall lift level is recovered. Lift, therefore, takes much longer to recover its pre-stall levels in comparison with drag. This contrasts with the stall development phase shown in \S\ref{sub:stallDevelopment} where both lift and drag take a similar amount of time to achieve their post-stall levels.

It was shown that the stall process was tightly linked with the bursting of T4's LSB therefore, it is not surprising that the stall recovery is also related with the T4's LSB. In this case, the stall recovery is achieved when the SSL disappears and a new LSB is redeveloped instead, i.e. when flow-mode-A is recovered. This process is clearly seen in figures~\ref{fig:nearStall_8WLECpTopDown} and \ref{fig:nearStall_8WLECpSideDown}. In figure~\ref{fig:nearStall_8WLECpTopDown} the pressure coefficient on the surface of the WLE-8 case is plotted at the times shown in figure~\ref{fig:nearStall_8WLETimeSequenceDown}. In the first snapshot, at $t'=226$, the separated flow region centred at the T4 spanwise section is denoted by a higher pressure level in the leading edge region. At $t'=228.5$ two low-pressure spots are identified in the half chord region (see blue dashed line in figure~\ref{fig:nearStall_8WLECpTopDown}) which are the pressure signature of the focus pair driving the stall cell (see \S\ref{sub:StalledWLE_surfaceFlowSkeletons}). It is seen how as time advances, the foci drift downstream indicating the detachment of the stall cell and the onset of flow reattachment. At the same time, in the leading edge area, pressure decreases steadily in the T4 trough until at $t'=230.5$ the contour levels show that the periodic (trough-to-trough) pressure distribution is recovered and the T4's LSB is formed again. In other words, flow-mode-A is recovered.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLECpTopDown}
   \input{\folder/nearStall/8WLECpTopDown}
   \caption{Snapshot-sequence showing the pressure coefficient contours on the suction surface of the WLE-8 model where the blue dashed line connects the stall-cell's foci (see \S\ref{sub:StalledWLE_surfaceFlowSkeletons}). Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8WLETimeSequenceDown}.}
   \label{fig:nearStall_8WLECpTopDown}
\end{figure}

The redevelopment of T4's LSB is more clearly seen in figure~\ref{fig:nearStall_8WLECpSideDown} showing spanwise planes at the T4 section containing contours of the pressure coefficient. In figure~\ref{fig:nearStall_8WLECpSideDown}, a train of small size low-pressure-structures indicate the presence of the K-H vortices formed in the separated shear layer as proved in \S\ref{cha:StalledWLE}. The angle at which these small size vortices are convected can be used as an indication of the flow direction and therefore the flow separation angle.  At the same time, at $t'=227$ a large low-pressure region is observed which represents the pressure signature of the upper side v-K vortex. While the convection direction of the large v-K vortex remains unchanged for the time sequence presented in figure~\ref{fig:nearStall_8WLECpSideDown}, the convection direction of the small size K-H vortices keeps changing, indicating that the separation angle is being reduced and the LSB in the T4 trough is being redeveloped. At $t'=230$, two different regions can be identified: an attached flow region in the first half of the chord and a detached flow region in the second half. In the attached flow region, the presence of the newly formed LSB is clear. Small size vortical structures are still being created in the LSB region due to a K-H instability present in the shear layer that separates the free stream flow and the interior of the bubble \citep{Burgmann2008, Pauley1990, Watmuff1999, Yarusevych2009}. However, these small-scale vortices stay close to the aerofoil surface as they are convected by flow inside the turbulent boundary layer that redevelops downstream of the LSB. 

The boundary between attached and separated flow regions in figure~\ref{fig:nearStall_8WLECpSideDown} is denoted by a high-pressure region (blue dash-dotted line) immediately upstream of the large v-K vortex. As the v-K vortex moves downstream the attached region gains terrain indicating that the process of regaining the pre-stall flow topology has already begun. The presence of this high-pressure zone that separates the attached and detached flow regions explains why lift decreases between $229<t'<333$ in figure~\ref{fig:nearStall_8WLETimeSequenceDown}. For $t'>333$ the attached flow region covers the entire aerofoil surface and the flow progressively returns to pre-stall state studied in \S\ref{cha:lowAoA}.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_8WLECpSideDown}
   \input{\folder/nearStall/8WLECpSideDown}
   \caption{Snapshot-sequence showing the pressure coefficient contours on the T4 spanwise section of the WLE-8 model with a blue dash-dotted line indicating the boundary between the attached and separated regions. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8WLETimeSequenceDown}.}
   \label{fig:nearStall_8WLECpSideDown}
\end{figure}
%
%\begin{figure}[t!]\centering
%   \tikzsetnextfilename{nearStall_8WLEUSideDown}
%   \input{\folder/nearStall/8WLEUSideDown}
%   \caption{Snapshot-sequence showing the dimensionless total energy $e^*_t=e_t/e_{t\infty}$ contours on the T4 spanwise section of the WLE-8 model. Lift and drag coefficients during the snapshot sequence are shown in figure~\ref{fig:nearStall_8WLETimeSequenceDown}.}
%   \label{fig:nearStall_8WLEUSideDown}
%\end{figure}
