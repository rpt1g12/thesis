%=====================SUBSECTION - peakOscillations=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\subsection{Peak oscillations}\label{sub:nearStall_peakOscillations}
In the previous section, it has been shown that while at low angles of attack flow mode-A dominates, as the angle of attack is increased instabilities start to develop and the flow transitions to flow mode-B. Such flow instabilities leading to flow mode-B seem to be more evident at the peaks as shown in Figure~\ref{fig:nearStall_8WLEtwzTop} due to the following: 
\begin{itemize}
	\item Due to the geometry of the model, the peak section is unstable by nature and any spanwise oriented perturbations in the peak causes the flow to be deflected towards either of its neighbouring troughs.
	\item As the angle of attack increases, the LSBs sitting on each trough move upstream as shown in \S\ref{cha:lowAoA}. However, due to the physical restriction of not being able to go further upstream than the leading edge, the most upstream separation position asymptotes to $x_s\approx-0.44$. The LSBs still do try to move further upstream and as a consequence, the LSB gets squashed in its longitudinal direction and starts getting wider. 
	\item The LSBs' structure is known to be by nature unsteady. Spanwise oriented vortical structures are formed in its shear layer due to a K-H type flow instability \citep{Pauley1990, Watmuff1999, Yarusevych2009, Burgmann2008}. The shedding of these K-H vortices is a continuous source of perturbations that affect the flow in the nearby regions, like the peak sections.
\end{itemize}

As a consequence, this subsection will focus on the leading-edge region flow fluctuations. Several probes are used to capture time-histories of the flow quantities and studied in the frequency domain.  

Figure~\ref{fig:nearStall_wHistory} shows the time history of the spanwise velocity fluctuations in four peak sections neighbouring two troughs where the LSB remains (trough T1 and peaks P2 and P3) and burst (trough T4 and peaks P4 and P5) after stall occurs. Peaks on the sides of trough T1, e.g. peaks P2 and P3, seem to have a strong correlation. A strong correlation is also appreciated in figure~\ref{fig:nearStall_wHistory}\emph{b} for peaks P4 and P5 on the sides of T4. Although both pairs of signals initially oscillate around $w\approx0$, it is clear that signals from P4 and P5 quickly deviate from the zero mean value due to the effect of the bursting process of the LSB in trough T4. It has been mentioned in the introduction of this section, the LSB move upstream as the angle of attack is increased and they start to get wider as they approach the leading edge. As a consequence, the spanwise velocity recorded on peak P4 and P5 show a clear trend towards a local positive and negative mean respectively for $t'>90$. The bursting effect is also sensed in the other two probes at P2 and P3. Since P2 and P3 are located on the positive side of T4, e.g. $z_{P2}>z_{T4}$ and $z_{P3}>z_{T4}$, the probe signals suffer a positive mean shift for $t'>90$ too.

In order to estimate the phase difference between each pair of signals in figure~\ref{fig:nearStall_wHistory} a Fourier study is performed on the cross-correlation of the spanwise velocity signals. That is
\begin{equation}
\label{eq:crossCorrelation}
	R_{w_1w_2}(t)=\avg{w_1(\tau)w_2(\tau+t)} \quad \text{and}\quad S_{w_1w_2}(f)=\int_{-\infty}^{\infty} R_{w_1w_2}(t)e^{-2\pi ft}\,\dd t.
\end{equation}
%
Despite the fact that the time scale of the heaving motion is much larger than the inherent flow time-scales, the flow is catalogued as non-stationary. Therefore, special pre-processing steps have to be done. The spectra are produced using a windowing technique \citep{Welch1967}. The signals are split into smaller segments of $1.42$ time-units with a 30\% overlap between the segments. The Fourier analysis is performed individually on each segment and then the final result is the average of all of them. In addition, in order to avoid that the averaging process masks certain frequencies, the flow has been split into two chunks of 30 time-units, i.e. one-fourth of the ramp-up or ramp-down motions. Focusing on smaller time sub-segments reduces the non-stationary character of the flow and avoids mixing frequencies. 
\begin{figure}[b!]\centering
   \tikzsetnextfilename{nearStall_wHistory}
   \input{\folder/nearStall/wHistory}
   \caption{Time histories of the spanwise velocity $w$ for (\emph{a}) sections P2 and P3 neighbouring trough T1 and (\emph{b}) P4 and P5 sections neighbouring trough T4.}
   \label{fig:nearStall_wHistory}
\end{figure}

Using the angle $\phi(f)=\atan(\Im\{S_{w_1w_2}(f)\}/\Re\{S_{w_1w_2}(f)\})$ the positive and negative correlation in the frequency domain can be estimated by $\cos\phi$. On the one hand, when  for a particular frequency  $\cos\phi=1$, the signals are said to be in phase; if on the other hand $\cos\phi=-1$, the signals are completely out of phase. Figure~\ref{fig:nearStall_P2P3Phase} shows in separated plots the time histories for $t'<90$ and $t'>90$ along with a $\cos\phi$ plot. It is clear that in the pre-stall regime ($60\le t'\le90$), where the trough-to-trough periodic flow-mode-A dominates, the low end of the frequency spectrum is completely out of phase as shown in figure~\ref{fig:nearStall_P2P3Phase}\emph{c}. In contrast, for $90<t'\le120$, where stall has already developed and flow-mode-B is prominent, the low-frequency oscillations get in phase. It is postulated that in flow-mode-A, the spanwise velocity at the peaks is locally governed by low-frequency pressure oscillations in LSBs around the peaks; whereas in flow-mode-B the spanwise flow is mostly influenced by the low-frequency pressure oscillations of the burst bubble in trough T4, e.g. the separated shear layer (SSL). When the T4's LSB bursts the separated region quickly expands as shown in figures~\ref{fig:nearStall_8WLECpTop} and \ref{fig:nearStall_8WLEtwzTop} and pushes the flow away from it. That is, for regions where $z>z_{T4}$ the flow is pushed in the positive $z$-axis direction and for regions where $z<z_{T4}$ the flow is pushed in the opposite direction. In the example given in figure~\ref{fig:nearStall_P2P3Phase}\emph{b} the flow is pushed in the positive $z$-axis direction.

Interestingly, oscillations around the LSB's K-H shedding frequency ($f_{LSB}$) and its harmonics appears to be always in phase. This seems to be a reasonable behaviour since, as shown in figures~\ref{fig:nearStall_8WLECpSide} and \ref{fig:nearStall_8WLET8CpSide}, the shedding of vortical structures due to the K-H instability present on the edge of the LSB do not cease even after stall, or in other words, the bursting of the T4 LSB has already occurred.
%
\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_P2P3Phase}
   \input{\folder/nearStall/P2P3Phase}
   \caption{Time histories of the spanwise velocity at peaks P2 and P3 separated into two segments: (\emph{a}) for $60\le t'\le 90$ and (\emph{b}) for $90<t'\le 120$. The cosine of the phase angle between signals (\emph{c}) is also presented for both segments.}
   \label{fig:nearStall_P2P3Phase}
\end{figure}

In contrast, figure~\ref{fig:nearStall_P4P5Phase} provides inside on the phase spectra of the cross-correlation of spanwise velocity signals on each side of trough T4. In this case, low-frequency oscillations are always completely out of phase independently of the time range. Again, it is found that frequencies near the LSB's K-H shedding frequency (and its harmonics) are always in phase. The low-frequency phase difference ($\phi\approx-\pi$ for $f_{L_c}<20$) reinforces the idea that when the signals are acquired on different sides of the SSL section, i.e. on each side of trough T4, local maximums in one will coincide in time with local minimums on the other and vice versa.

\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_P4P5Phase}
   \input{\folder/nearStall/P4P5Phase}
   \caption{Time histories of the spanwise velocity at peaks P4 and P5 separated into two segments: (\emph{a}) for $60\le t'\le 90$ and (\emph{b}) for $90<t'\le 120$. The cosine of the phase angle between signals (\emph{c}) is also presented for both segments.}
   \label{fig:nearStall_P4P5Phase}
\end{figure}

%The spectral content of the signals presented in figure~\ref{fig:nearStall_wHistory} is
%presented in figure~\ref{fig:nearStall_wHistoryPSD}. All four signals have power spectral
%density (PSD) profiles that look very much alike. The low-frequency content introduced in
%the paragraphs above is captured although no single low-frequency component is identified
%due to the loss of frequency resolution associated with: i) the lack of a long enough
%signal and ii) the effect of the windowing technique used \citep{Welch1967}. Besides the
%low-frequency content, peaks in the spanwise velocity spectra are appreciated at $f_{LSB}$ and $f_{LSB}/2$. 
%%
%\begin{figure}[t!]\centering
%   \tikzsetnextfilename{nearStall_wHistoryPSD}
%   \input{\folder/nearStall/wHistoryPSD}
%   \caption{Power spectral density of the time histories shown in figure~\ref{fig:nearStall_wHistory}.}
%   \label{fig:nearStall_wHistoryPSD}
%\end{figure}

As mentioned, a windowing technique \citep{Welch1967} has been used to provide cleaner spectra. This technique consists in splitting the acquired signal into a number of smaller segments with some degree of overlap whose spectra are then averaged in order to reduce undesired noise at the expense of reducing the frequency resolution. The resulting spectrum is commonly known as a periodogram. Another drawback of this averaging technique is that it is not capable of tracking frequency variations along time. If on the other hand the spectrum of each segment is plotted as a contour map like in figure~\ref{fig:nearStall_wSpectogram} using time as the abscissae, frequency as the ordinates and PSD as the colour contour, variations in PSD across frequencies can be seen as a function of time, i.e a Spectrogram. This permits the analysis of frequency variations in a non-stationary flow.

Figure~\ref{fig:nearStall_wSpectogram} presents spectrograms of spanwise velocity signals in the peak sections. The plots reveal that in the time range of $70<t'<90$ the energy contained near the half LSB's shedding frequency ($f_{LSB}/2$) rises. Previously, on figures~\ref{fig:nearStall_8WLECpTop} and \ref{fig:nearStall_8WLEtwzTop} it has been shown that it is in fact during this time span that the bursting of the LSB on T4, and consequently the onset of stall, occurs. Besides the energy increase near $f_{LSB}/2$, the spectrograms of P2, P3 and P5 signals show an excitation of frequencies of $f_{Lc}\approx 10$. Moreover, it appears that once the airflow fully stalls for $t'>90$ energy across all frequencies suddenly decreases, especially in the high-frequency range. Only some energy content seems to persist at a frequency of $f_{LSB}/2$.

\begin{figure}[t!]\centering
   \tikzsetnextfilename{nearStall_wSpectogram}
   \input{\folder/nearStall/wSpectogram}
   \caption{Spectrograms of spanwise velocity signals taken from (\emph{a}) P2, (\emph{b}) P3, (\emph{c}) P4 and (\emph{d}) P5 peak sections.}
   \label{fig:nearStall_wSpectogram}
\end{figure}

Due to the connection with the burst of the T4's LSB it is interesting to investigate the spectrogram of pressure fluctuations within the T4 shear layer. It is important to bear in mind that the position of this shear layer varies in time due to the bursting process that leads the T4's LSB to become the SSL (see figure~\ref{fig:nearStall_8WLEUSide}). Therefore, in order to obtain an accurate enough measurement of the shear layer position, short-time-averaged flow fields using an averaging period of $0.5t'$ were used to compute the boundary layer integral quantities needed to locate the LSB's shear layer wall-distance coordinate. These short-time-averaged locations are then used as probe locations for obtaining the pressure fluctuations in time. The resulting spectrogram is presented in figure~\ref{fig:nearStall_pSSLSpectogram}.

Once again, a shift in the frequency content is clearly appreciated after $t'>90$. For $t'<90$ most of the oscillations seem to be in the high end of the measured frequency spectrum. In the time range of $50<t'<80$ the frequency of the most energetic oscillations increases steadily. For $t'>90$ a similar energy transfer towards the low-end frequency spectrum is seen. An increase in the PSD in frequencies around $f_{L_c}\approx10$ is found to happen from $t'\approx82$ to $t'\approx92$, similarly to that observed in figure~\ref{fig:nearStall_wSpectogram}.

\begin{figure}[!t]\centering
   \tikzsetnextfilename{nearStall_pSSLSpectogram}
   \input{\folder/nearStall/pSSLSpectogram}
   \caption{Spectrogram of the pressure fluctuations in the separated shear layer (SSL) at the trough section T4.}
   \label{fig:nearStall_pSSLSpectogram}
\end{figure}
