%=====================CHAPTER - Inviscid Tandem Results=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\chapter{Inviscid Tandem Tests}\label{cha:InviscidTandemTests}

Initially, this project was aimed to study the effect of using a WLE on the downstream element of a tandem aerofoil configuration. Consequently, a preliminary study was made using the inviscid Euler equations since computations were much less expensive than full Navier-Stokes computations. The results of this preliminary study were compared to an inviscid-irrotational panel method based on constant strength sources and vortices~\citep{Hess1967}. Because the original version of the code was oriented to single element aerofoils, the code had to be updated using the method of \citet{Chaves2012}.

Because the panel method used does not include compressibility effects, for comparison with the Euler's code it is necessary to apply some correction to the results. The following rule can be used to transform from incompressible to compressible results
%
\begin{equation}
\label{eq:prandtl}
	\begin{aligned}
	&C_p=\frac{C_{p_I}}{\beta}	\qquad	&\text{with }\beta=\sqrt{1-M_\infty^2}
	\end{aligned}
\end{equation}

which is known as the Prandtl-Glauer rule, where $C_{p_I}$ is the incompressible value, $\beta$ is the Prandtl-Glauert coefficient and $M_\infty$ is the free-stream Mach number. Notice here that the Mach number used here to obtain the transformed results is the free-stream Mach number. This can imply that transformed results may still differ from the Euler's code results at critical locations such as the leading and trailing edge, as well as the location of the suction side pressure peak~\citep{Woodward1980}. \citet{Woodward1980} proved that transformations using the local Mach number at the surface would lead to much better results.

%=====================SECTION - Grid Dependency Tests=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Grid Dependency Tests}\label{sec:gridTests}

The consistency of the results with regards the grid resolution where tested using three different grids with coarse, medium, and fine resolutions respectively. The number of points used per block can be seen in Table~\ref{tab:gridTestC},~\ref{tab:gridTestM},~and~\ref{tab:gridTestF} where only bottom blocks' number of points are shown since the top row has the same configuration.

\begin{table}[H]
\centering
\caption{Number of Points per Block for the Coarse Grid}
\label{tab:gridTestC}
\begin{tabular}{r|*{4}{c}l}
& \multicolumn{5}{ c }{Block No.} \\
Direction 	&  0		& 1		&  2		& 3		& 4	 	\\
\hline
$\xi$		& 100		& 50		& 50		& 50		& 100		\\
$\eta$		& 100		& 100		& 100		& 100		& 100		\\
$\zeta$		& 15		& 15		& 15		& 15		& 15		\\
\end{tabular}
\end{table}
%
\begin{table}[H]
\centering
\caption{Number of Points per Block for the Medium (baseline) Grid}
\label{tab:gridTestM}
\begin{tabular}{r|*{4}{c}l}
& \multicolumn{5}{ c }{Block No.} \\
Direction 	&  0		& 1		&  2		& 3		& 4	 	\\
\hline
$\xi$		& 200		& 100		& 100   	& 100   	& 200		\\
$\eta$		& 200		& 200		& 200		& 200		& 200		\\
$\zeta$		& 15		& 15		& 15		& 15		& 15		\\
\end{tabular}
\end{table}
%
\begin{table}[H]
\centering
\caption{Number of Points per Block for the Fine Grid}
\label{tab:gridTestF}
\begin{tabular}{r|*{4}{c}l}
& \multicolumn{5}{ c }{Block No.} \\
Direction 	&  0		& 1		&  2		& 3		& 4	 	\\
\hline
$\xi$		& 400		& 200		& 150   	& 200		& 400		\\
$\eta$		& 400		& 400		& 400		& 400		& 400		\\
$\zeta$		& 15		& 15		& 15		& 15		& 15		\\
\end{tabular}
\end{table}

The results obtained from this simulations are showed in Figure~\ref{fig:gridComp} in the form of pressure coefficient distributions over the aerofoils. It seems that for the solving the Euler equations the coarse grid would be enough, however since the final aim is to switch to the Navier-Stokes equations as soon as the development stage is finished, it was worth trying finer grid resolutions because when viscosity effects are account, the length scales of the flow that the LES method aims to solve become really small.
%
\begin{figure}[H]
	\centering
	\includegraphics[trim= 1cm 2cm 2cm 1cm,clip,width=0.8\textwidth]{gridComp/gridComp.pdf}
	\caption{$C_p$ comparison for three different grid resolutions at zero AoA, where "C" means coarse resolution grid, "M" means medium resolution grid and "F" means fine resolution grid.}
	\label{fig:gridComp}
\end{figure}

%=====================SECTION - Domain Dependency Tests=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Domain Dependency Tests}\label{sec:domTests}

In this section the effect of the domain size over the final solution is tested. The domain sizes used ranged between 5 and 11 chord lengths and pressure coefficient distributions are measured over the two elements of the tandem configuration. Results of the study are compared with values obtained from the potential code with compressibility corrections. Figure~\ref{fig:domComp1A} shows how results seem to be quite similar for both elements at different domain sizes $L_\Omega$. However a close look at the first element trailing edge reveals that the domain size has a direct impact in the determination of the leading edge pressure peak as Figure~\ref{fig:domComp1B} shows. It seems that longer domain sizes better agree with the panel method results. This behaviour had been observed earlier by \citet{Kim2010a}, who related the effect of this pressure peak differences with the sponge zone. Nevertheless the level of agreement with the panel code is high.
%
\begin{figure}[H]
	\centering
	\includegraphics[trim= 1cm 2cm 2cm 1cm,clip,width=0.8\textwidth]{DomTest/domComp5_11.pdf}
	\caption{$C_p$ for different domain sizes at $\delta_1=4\dg$ and $\delta_2=4\dg$. Enlarged View}
	\label{fig:domComp1A}
\end{figure}
%
\begin{figure}[H]
	\centering
	\includegraphics[trim= 1cm 2cm 2cm 1cm,clip,width=0.8\textwidth]{DomTest/domComp5_11Z.pdf}
	\caption{$C_p$ for different domain sizes at $\delta_1=4\dg$ and $\delta_2=4\dg$. Zoomed view}
	\label{fig:domComp1B}
\end{figure}

Although it could be though that the forcing due to the inclusion of the sponge zone at the boundaries to ensure non-reflecting boundary conditions could affect this pressure peak differences, it turns out that varying the value of the sponge coefficient $\sigma_0$ (see Equation~\ref{eq:sponge2}) does not disturb the results. The results of Figure~\ref{fig:domComp1A}~and~\ref{fig:domComp1B} where calculated using a value of $\sigma_0=8$. In Figure~\ref{fig:domComp2A} the different sponge zone coefficients' results are compared. It is clearly seen that the general shape of the pressure distribution has not changed form Figure~\ref{fig:domComp1A}. 

On top of that a close look at the leading edge (Figure~\ref{fig:domComp2B}) reveals that the differences are inappreciable. This is confirmed by other test done with values of $\sigma_0$ ranging from 5 to 11, but due to the fact that all of them lead to very similar results the case of $\sigma_0=11$ is shown in Figure~\ref{fig:domComp3A}~and~\ref{fig:domComp3B}  for the sake of completion. This is a good sign since indicates that the sponge parameter does not affect the solutions and plus, it is seen from Figure~\ref{fig:domComp1B} that solutions between the domain of 9 chord lengths and 11 does not vary that much.
%
\begin{figure}[H]
	\centering
	\includegraphics[trim= 1cm 2cm 2cm 1cm,clip,width=0.8\textwidth]{DomTest/domComp5n9S6.pdf}
	\caption{$C_p$ for domain sizes of 5 and 9 chord lengths at $\delta_1=4\dg$ and $\delta_2=4\dg$ and $\sigma_0=6$ compared with the original sponge configuration with $\sigma_0=8$. Enlarged View}
	\label{fig:domComp2A}
\end{figure}
%
\begin{figure}[H]
	\centering
	\includegraphics[trim= 1cm 2cm 2cm 1cm,clip,width=0.8\textwidth]{DomTest/domComp5n9S6Z.pdf}
	\caption{$C_p$ for domain sizes of 5 and 9 chord lengths at $\delta_1=4\dg$ and $\delta_2=4\dg$ and $\sigma_0=6$ compared with the original sponge configuration with $\sigma_0=8$. Zoomed view}
	\label{fig:domComp2B}
\end{figure}
%
\begin{figure}[H]
	\centering
	\includegraphics[trim= 1cm 2cm 2cm 1cm,clip,width=0.8\textwidth]{DomTest/domComp5n9S11.pdf}
	\caption{$C_p$ for domain sizes of 5 and 9 chord lengths at $\delta_1=4\dg$ and $\delta_2=4\dg$ and $\sigma_0=11$ compared with the original sponge configuration with $\sigma_0=8$. Enlarged View}
	\label{fig:domComp3A}
\end{figure}
%
\begin{figure}[H]
	\centering
	\includegraphics[trim= 1cm 2cm 2cm 1cm,clip,width=0.8\textwidth]{DomTest/domComp5n9S11Z.pdf}
	\caption{$C_p$ for domain sizes of 5 and 9 chord lengths at $\delta_1=4\dg$ and $\delta_2=4\dg$ and $\sigma_0=11$ compared with the original sponge configuration with $\sigma_0=8$. Zoomed view}
	\label{fig:domComp3B}
\end{figure}


%=====================SECTION - Angle of Attack Study=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Angle of Attack Study}\label{sec:aoaTests}

In order to validate the method used in this project to investigate the tandem aerofoil configuration, which is the base of the final study of WLE in the aft element, different simulations were conducted. In first place following what \citet{Fanjoy1996} did, the effect of varying the inflow incidence angle was tested for small angles between $0\dg$ and $4\dg$. The results of this study can be observed in Figure~\ref{fig:aoaCpFix}. Results agree with literature since it is seen that an increase in the angle of attack is much beneficial for the fore element than for the second element. Additionally the agreement with the panel method results is still high. Note here that the angle of attack has been varied the same way for the panel method  by changing the free-stream flow's incidence angle too.

However as aforementioned, the implications of such study (changing the flow incidence angle) is only valid for small angles of attack since the interference between both elements is the ultimate goal of the study. If the incidence angle is too high the wake of the first element would be convected at such a high angle that the wake would not impact on to the aft element. And because also one of the main objectives is to perform a parametric study of both elements relative position and WLEs parameters, it is important to be able to rotate the aerofoils at different angles independently. To achieve such thing it is necessary to physically rotate the aerofoils and then create a brand new mesh for each combination of $\delta_1$ and $\delta_2$, whereas changing the flow incidence can be achieved by modification of the boundary conditions, which is much easier and less time consuming. In a sense that is the reason why that approach was initially the one taken. Nonetheless the rotation of the elements was also implemented and results comparing both approaches for $\delta_1=\delta_2=4\dg$ are shown in Figure~\ref{fig:aoaCpRot1}.
%
\begin{figure}[H]
	\centering
	\includegraphics[trim= 1cm 2cm 2cm 1cm,clip,width=0.8\textwidth]{aoaFix/cpA0to4D7S8.pdf}
	\caption{$C_p$ distributions for different flow incidence angles. The number after "A" indicates the incidence angle. Black dashed line represents the correspondent panel method computation}
	\label{fig:aoaCpFix}
\end {figure}

It is seen in Figure~\ref{fig:aoaCpRot1} that the level of agreement between both mentioned approaches is very high. Compared with the results from the panel method with element's rotation results are also in good agreement. Being sure that the element's rotation had been correctly implemented, another study was carried where the AoA of the first element was kept fixed at $\delta_1=0$ and then the aft element's AoA ($\delta_2$) was varied between $2\dg$ and $6\dg$. Published literature on tandem aerofoils shows that even while keeping the fore element at $\delta_1=0\dg$ it is still possible to increase its lifting capabilities by increasing $\delta_2$. And that effect is clearly seen in Figure~\ref{fig:aoaCpRot2}. However, and although the agreement with the panel method is still really high for the aft element, the potential code predicts slightly higher pressure differences between upper and lower surfaces of the fore element. It must be noted that the panel method does not attend to compressibility effects and the results of it have only been transformed using the Prandtl-Glauert rule (Equation~\ref{eq:prandtl}). Therefore the effect of the dowstream element is much important in the panel method than in the Euler code.
%
\begin{figure}[H]
	\centering
	\includegraphics[trim= 1cm 2cm 2cm 1cm,clip,width=0.8\textwidth]{aoaFix/cpA4_4D7S8.pdf}
	\caption{$C_p$ distributions for fixed $\delta_1=4\dg$ and $\delta_2=4\dg$ compared with incidence angle case $AoA=4\dg$. Black dashed line represents the correspondent panel method computation}
	\label{fig:aoaCpRot1}
\end {figure}
%
\begin{figure}[H]
	\centering
	\includegraphics[trim= 1cm 2cm 2cm 1cm,clip,width=0.8\textwidth]{aoaFix/cpA0_2to6D7S8.pdf}
	\caption{$C_p$ distributions for fixed $\delta_1=0\dg$ and different $\delta_2$. Black lines represent the correspondent panel method computation}
	\label{fig:aoaCpRot2}
\end {figure}
