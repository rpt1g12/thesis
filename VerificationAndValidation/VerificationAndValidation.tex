\chapter{Verification and Validation}\label{cha:VerificationAndValidation}
%=====================SECTION - Introduction=====================%
The \citeauthor{AIAA} defines verification and validation as:

\emph{Verification:} the process of determining that a model implementation accurately represents the developer's conceptual description of the model and the solution to the model.

\emph{Validation:} the process of determining the degree to which a model is an accurate representation of the real world from the perspective of the intended uses of the model.

\begin{figure}[H]\centering
   \tikzsetnextfilename{VandV}
   \input{\folder/VerificationAndValidation/VandV}
   \caption{Verification and Validation diagram~\citep{Oberkampf2002}}
   \label{fig:VandV}
\end{figure}

Figure~\ref{fig:VandV} represents the verification and validation process as described by \citet{Oberkampf2002}. By observation of Figure~\ref{fig:VandV}, the process of verification can be related to the process of checking that the computerised model faithfully represents the conceptual model which in the case of Computational Fluid Mechanics are the Navier-Stokes equations Equation~\ref{eq:govEq}. On the other hand, validation is related to the linkage between the computerised model or code with the real world. 

The circle is also closed by the \emph{Qualification} process which the Society of Computer Simulation defines as: "Determination of adequacy of the conceptual model to provide an acceptable level of agreement for the domain of intended application". This is understood as if the model (the set of PDEs) are the best approximation of the reality. Both qualification and validation are more rooted in the question of how accurate is the solution in the physical sense whereas verification is more rooted in the question of how accurate is the solution in the mathematical sense. 

In terms of verification, the real problem is given in a continuous media as it is the air surrounding the aerofoil. However the PDEs are solved by a discrete method (Finite Difference Technique) which approximates the real continuous solution. The use of infinite elements in the discretisation would lead to a perfect solution, nevertheless this will be extremely expensive, in the computational sense. Consequently the grid needs to be fine enough so the solutions are not affected by the discretisation errors while still being computationally efficient. A grid dependency study can show that the errors decrease with increasing number of points. Similarly, the real problem happens in an almost unbounded domain whereas the computational model must be defined by a finite bounded region. The domain needs to be as well big enough so the far upstream and dowstream conditions are met. Nonetheless, similarly to what happens in wind tunnel experiments if the walls of the testing section are too close to the model under investigation, i.e. too high \emph{Blockage ratio}, if the boundaries of the computational domain are located too close to the model of investigation the flow can be affected by the flow conditions imposed at the boundaries. 

In the following section the Direct Numerical Simulation (DNS) solution of a NACA0012 at $Re_\infty=50\times10^5$, $M_\infty=0.4$ and $\alpha=5\dg$ obtained by \citet{Jones2008} is used to verify and validate the numerical solver. In the work of Jones the solution is obtained solving the N-S equations by using a fourth-order accurate finite difference schemes in both streamwise and normal directions and spectral in the spanwise direction. Time integration was performed using a classical fourth-order Runge-Kutta method.

It is shown in~\citet{Jones2008} that for a NACA0012 aerofoil at $\alpha=5\dg$, $M_\infty=0.4$ and $Re_\infty=5\times10^4$ the boundary layer separates near the leading edge due to the presence of a strong adverse pressure gradient forming a separation shear layer. Further downstream the shear layer starts rolling up due to a Kelvin-Helmholtz instability where the flow transitions to turbulent and reattaches. Although the flow has a strong unsteady character, the time-averaged solution shows a region of recirculating flow which is commonly known as a Laminar Separation Bubble (LSB). The length of the LSB is given by the separation $x_s$ and reattachment $x_r$ points that can be estimated by the point at which the time-averaged friction coefficient changes sign. 

The characteristics of the above described flow are highly similar to the ones to be expected in the numerical simulations that form the body of this work and therefore are useful to validate the capabilities of the code. Additionally, by obtaining a grid at a lower $Re$, the number of points needed and the domain sizes can be projected into higher $Re$ simulations.

%=====================SECTION - Grid Dependence Study=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Grid Dependence Study at $Re=50\times10^3$}\label{sec:ViscGridDependence}

Several grids have been used to test the sensitivity of the simulations to different discretisation levels. In Table~\ref{tab:grids} the number of points used per block in each direction can be checked, where $\xi_n$, $\eta_n$ and $\zeta_n$ are the number of points used in the $\xi$,$\eta$ and $\zeta$ directions of the computational grid. 

%
\begin{table}[H]
\centering
\caption{Number of points in each direction for all grid cases.}
\label{tab:grids}
\begin{tabular}{c|c c c}
 &G1&G2&G3\\
 \hline                                                 
 $\xi_n$&570&620&620\\
 $\eta_n$&240&340&340\\
 $\zeta_n$&50&50&75\\
\end{tabular}
\end{table}

The results from each grid are compared in figure~\ref{fig:cfComparison}. It is observed that the biggest differences are observed in the friction coefficient plot (figure~\ref{fig:cfComparison}\emph{a}). On the first half of the aerofoil there is a great agreement with DNS. On the second half, where the boundary layer is turbulent, there exists some discrepancy. It can be observed that the negative peak is better captured by the finer grid.  Additionally, from Table~\ref{tab:sepReat} it can be seen that the deviations for both separation and reattachment points  are always below 2\% of the chord length ($L_c$) for all cases. 

\begin{figure}[H]\centering
   \tikzsetnextfilename{cfComparison}
   \input{\folder/VerificationAndValidation/cfComparison}
   \caption{Comparison of time-span averaged friction (\emph{a}) and pressure (\emph{b}) coefficients for different grids with data from \citet{Jones2008}.}
   \label{fig:cfComparison}
\end{figure}

%
\begin{table}[H]
\centering
\caption{Separation ($x_s$) and Reattachment ($x_r$) points for each grid case compared to the data from \citet{Jones2008}.}
\label{tab:sepReat}
\begin{tabular}{c c c}
&$x_s$&$x_r$\\
\hline
G1&-0.399&0.107\\
G2&-0.397&0.088\\
G3&-0.397&0.088\\
Jones (2008)&-0.401&0.107\\
\end{tabular}
\end{table}

In Table~\ref{tab:gridCl} the correspondent lift and force coefficients are presented for each grid case showing that all grids agree quite well with the DNS case. The biggest difference seems to be present in the pressure part of the force coefficients, specially for the finer grids. Nonetheless, it is seen that the agreement with the viscous part of the drag coefficient is better for finer grids.

\begin{table}[H]\centering
\caption{Force coefficient for each Grid case}
\label{tab:gridCl}
\begin{tabular}{r l l l l}
&$C_L$&$C_D$&$C_{Dp}$&$C_{Dv}$\\
\hline
G1&0.620&0.0360&0.0282&0.0078\\
G2&0.609&0.0340&0.0258&0.0082\\
G3&0.611&0.0348&0.0266&0.0082\\
\citet{Jones2008}&0.621&0.0358&0.0278&0.0081\\
\end{tabular}
\end{table}

Figure~\ref{fig:OmegaZ_comparison} contains contours of the instantaneous spanwise vorticity for each grid case and the DNS. Although the DNS contains much more detail on the smaller scales, all the LES cases seem to be able to capture the flow physics with reasonable agreement, qualitatively speaking. The axis of figure~\ref{fig:OmegaZ_comparison} correspond to the ones used in \citet{Jones2008} and which relate to the ones used in the current simulations according to~\ref{eq:xhat2x}.

\begin{equation}
\label{eq:xhat2x}
   \left .
   \begin{aligned}
      \hat{x}&=(x-0.5)\cos\alpha+y\sin\alpha+1 \\
      \hat{y}&=-(x-0.5)\sin\alpha+y\cos\alpha
   \end{aligned}
   \right\rbrace \quad \text{where}\;\alpha=5\dg
\end{equation}

\begin{figure}[H]\centering
   \tikzsetnextfilename{OmegaZ_comparison}
   \input{\folder/VerificationAndValidation/OmegaZ_comparison}
   \caption{Comparison of $\omega_z$ with \citet{Jones2008} for (\emph{a}) G1 and (\emph{b}) G3.}
   \label{fig:OmegaZ_comparison}
\end{figure}

%%=====================SECTION - Spectral Analysis=====================%
%\FloatBarrier %Force all figures from previous sections to be placed
%\section{Spectral Analysis}\label{sec:spectral}
%
%In order to investigate the resolution capabilities of the LES code the Power spectra of turbulent kinetic energy $K=\frac{1}{2}\left({\avg{u'u'}}+{\avg{v'v'}}+{\avg{w'w'}}\right)$, is computed and then compared to the DNS~\citep{Jones2008}. Data acquisition took place over 50 non-dimensional units (non-dimensinalised using the free stream velocity and the chord length). Eight windows with the shape of a squared sinusoidal were used with a 50\% overlap to improve spectra quality at the expense of reducing the range at the low frequency end. The data was measured at $x=0.3961$ and $y^+=13.2$.
%
%\begin{figure}[H]\centering
%   \tikzsetnextfilename{spectraComparison}
%   \input{\folder/VerificationAndValidation/spectraComparison}
%	\caption{Temporal Power Spectra of $K$ taken at the mid-span compared with DNS~\citep{Jones2008} and the $-5/3$ slope of the Inertial Subrange.}
%   \label{fig:spectraComparison}
%\end{figure}
%
%Figure~\ref{fig:spectraComparison} shows how the data from LES approaches quite well with both DNS and the theoretical slop up until $f\approx 25$. At higher frequencies both DNS and LES diverge from the $-5/3$ slope of the turbulence's inertial subrange. Then at the high frequency end due to the coarser resolution used in the LES case the energy contained at those scales shows to be lesser than the DNS results.

%=====================SECTION - gridDependenceHighRe=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Grid dependence at $Re=120\times10^3$}\label{sec:gridDependenceHighRe}

Based on the results of the previous section, three different levels of grid resolution were tested for the higher $Re$ case. The tests were performed using a WLE-4 model at $\alpha=20\dg$, $M=0.3$ and $Re=120\times10^3$. The convergence of the results can be seen in figure~\ref{fig:VerificationAndValidation_StalledGridTests}, the resolution in terms of wall units is presented in figure~\ref{fig:VerificationAndValidation_StalledYPlus}, and the number of points used in table~\ref{tab:gres}.
%
\begin{table}\centering
\caption{Four different levels of grid resolution (G1 to G4) used for a grid dependency test based on the WLE-4 case. The list shows the number of grid cells used in the streamwise ($n_\xi$), vertical ($n_\eta$) and spanwise ($n_\zeta$) directions; and, the resulting lift and drag coefficients (time averaged).}
\begin{tabular}{l c c c c c}
&$n_\xi$&$n_\eta$&$n_\zeta$&$\avg{C_L}$&$\avg{C_D}$\\\hline
G1&800&480&200&0.600&0.270\\
G2&642&407&200&0.607&0.263\\
G3&642&407&160&0.611&0.264\\
G4&500&420&200&0.605&0.263
\end{tabular}
\label{tab:gres}
\end{table}

\begin{figure}[t!]\centering
   \tikzsetnextfilename{VerificationAndValidation_StalledGridTests}
   \input{\folder/VerificationAndValidation/StalledGridTests}
\caption{The time- and spanwise-averaged profiles of pressure and skin-friction coefficient for the WLE-4 case at $\alpha=20\dg$, $M=0.3$ and $Re=120\times10^3$ obtained from four different levels of grid resolution listed in table \ref{tab:gres}.}
   \label{fig:VerificationAndValidation_StalledGridTests}
\end{figure}

\begin{figure}[t!]\centering
   \tikzsetnextfilename{VerificationAndValidation_StalledYPlus}
   \input{\folder/VerificationAndValidation/StalledYPlus}
\caption{Distributions of the surface mesh sizes in wall units (averaged in span) for the WLE-4 case at $\alpha=20\dg$, $M=0.3$ and $Re=120\times10^3$ using G1 grid level.}
   \label{fig:VerificationAndValidation_StalledYPlus}
\end{figure}
