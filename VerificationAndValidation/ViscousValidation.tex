%=====================SECTION - Viscous Validation=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Viscous Validation}\label{sec:ViscVandVIntro}

In this chapter results are compared with those of Jones et al.~\citep{Jones2008} where Direct Numerical Simulation (DNS) is performed at $Re_\infty=50\times10^4$ over a NACA0012 aerofoil using a finite difference technique on both the streamwise and normal directions whilst a spectral method is used on the spanwise direction.

Jones~\citep{Jones2008} uses a volume forcing technique to trigger turbulence within the boundary layer. Such technique is also used here in order to obtain similar results. 
%
\begin{table}[H]
\centering
\caption{Separation and Reattachment points}
\label{tab:sepReat}
\begin{tabular}{c c c}
&$x_s$&$x_r$\\
\hline
G1&-0.399&0.107\\
G2&-0.397&0.088\\
3DU~\citep{Jones2008}&-0.401&0.107\\
\end{tabular}
\end{table}

It is shown in~\citep{Jones2008} that for a NACA0012 aerofoil at $\alpha=5\dg$, $M_\infty=0.4$ and $Re_\infty=5\times10^4$ a Laminar Separation Bubble (LSB) is present, the length of which is given by the separation $x_s$ and reattachment $x_r$ points. 

From Table~\ref{tab:sepReat} it can be seen that the deviations are $-1.049\%$ and $-2.77\%$ of the chord length ($L_c$) for the separation point respectively in each case, and $+1.274\%$ for the reattachment point for both cases. In this particular case, where it is known from literature where the forcing needs to be applied, the Volume Forcing might be more convenient, however if this was not known, the SIT method proves to give close results in terms of separation and reattachment points. In future computations where the nature of transition is not known a priori, the SIT method seems to be the way to go.

Nevertheless for the sake of this validation case, the Volume Forcing method is used in all results shown in this chapter unless it is stated otherwise.

%=====================SECTION - Grid Dependence Study=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Grid Dependence Study}\label{sec:ViscGridDependence}

In the past section the grid used for both VF and SIT cases was based on the G1 grid parameters (see Table~\ref{tab:grids}), but other three more grids have been used to test the sensitivity of the simulations to different discretisation levels. In Table~\ref{tab:grids} the number of points used per block in each direction can be checked, where $\xi_n$ is the number of points used in blocks of column n in the $\xi$ direction and $\eta_n$ is the number of points used in blocks of row $n$ in the $\eta$ direction. The row and column distribution can be seen in Figure~\ref{fig:newGridCol}. Note that row 2 is equal to row 1 and row 3 is equal to row 0 because the problem is set to be symmetric with respect to the centre line in terms of grid points.

\begin{figure}[H]\centering
   \tikzsetnextfilename{newGridCol}
   \input{\folder/VerificationAndValidation/newGridCol}
   \caption{Columns and Rows structure for the grid}
   \label{fig:newGridCol}
\end{figure}
%
\begin{table}[H]
\centering
\caption{Grid case study}
\label{tab:grids}
\begin{tabular}{c|c c c}
 &G1&G2&G3\\
 \hline                                                 
 $\xi$&570&620&620\\
 $\eta$&240&340&340\\
 $\zeta$&50&50&75\\
\end{tabular}
\end{table}

The results from this four grids are compared in Figure~\ref{fig:cfComparison}. It is observed that the most determinant parameter is the number of points used in the span direction, since the curves converge to G4 when the number of points is increased. It is also observed that agreement with the DNS case (3DU) is best up until the 40\% of the chord length and then after the 70\% of the chord length. However both G3 and G4 disagree in the middle section with respect to the DNS, where coarser grids seem to give closer results to 3DU.

\begin{figure}[H]\centering
   \tikzsetnextfilename{cfComparison}
   \input{\folder/VerificationAndValidation/cfComparison}
   \caption{Comparison of $C_f$ for different grids}
   \label{fig:cfComparison}
\end{figure}

In Table~\ref{tab:gridCl} the correspondent lift and force coefficients are presented for each grid case showing that G4 and G3 are the closest to the DNS case in terms of $C_l$. However there is a big disagreement in terms of $C_d$ despite the fact that curves in Figure~\ref{fig:cfComparison} seem to be quite close to the DNS curve.

\begin{table}[H]\centering
\caption{Force coefficient for each Grid case}
\label{tab:gridCl}
\begin{tabular}{r l l l l}
&$C_L$&$C_D$&$C_{Dp}$&$C_{Dv}$\\
\hline
G1&0.620&0.0360&0.0282&0.0078\\
G2&0.609&0.0340&0.0258&0.0082\\
Jones (2008)~\citep{Jones2008}&0.621&0.0358&0.0278&0.0081\\
\end{tabular}
\end{table}

%=====================SECTION - Spectral Analysis=====================%
\FloatBarrier %Force all figures from previous sections to be placed
\section{Spectral Analysis}\label{sec:spectral}

In order to investigate the resolution capabilities of the LES code the Power spectra of turbulent kinetic energy $K=\frac{1}{2}\left({\avg{u'u'}}+{\avg{v'v'}}+{\avg{w'w'}}\right)$, is computed and then compared to the DNS~\citep{Jones2008}. Data acquisition took place over 50 non-dimensional units (non-dimensinalised using the free stream velocity and the chord length). Eight windows with the shape of a squared sinusoidal were used with a 50\% overlap to improve spectra quality at the expense of reducing the range at the low frequency end. The data was measured at $x=0.3961$ and $y^+=13.2$.

\begin{figure}[H]\centering
   \tikzsetnextfilename{spectraComparison}
   \input{\folder/VerificationAndValidation/spectraComparison}
	\caption{Temporal Power Spectra of $K$ taken at the mid-span compared with DNS~\citep{Jones2008} and the $-5/3$ slope of the Inertial Subrange.}
   \label{fig:spectraComparison}
\end{figure}

Figure~\ref{fig:spectraComparison} shows how the data from LES approaches quite well with both DNS and the theoretical slop up until $f\approx 25$. At higher frequencies both DNS and LES diverge from the $-5/3$ slope of the turbulence's inertial subrange. Then at the high frequency end due to the coarser resolution used in the LES case the energy contained at those scales shows to be lesser than the DNS results.
